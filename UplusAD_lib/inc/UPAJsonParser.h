//
//  UPAJsonParser.h
//  UplusAdSDK
//
//  Created by U+AD
//  Copyright 2011 LG Uplus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UPAJSON.h"
#import "UPAReachability.h"

#define TIMEOUT_INTERVAL	5.0

/// 광고물의 형식
typedef enum {
	AD_TYPE_BANNER = 0,
	AD_TYPE_VIDEO
}AD_TYPE_JSON;

/// 동영상 광고의 노출 형식
typedef enum {
	VIDEO_TYPE_FULLSCREEN = 0,
	VIDEO_TYPE_EMBEDED
}VIDEO_TYPE;

/// 광고 컨텐츠 타입
typedef enum _CONTENT_TYPE {
	CONTENT_TYPE_NONE = 0,	
	CONTENT_TYPE_IMAGE,		///< 이미지 광고
	CONTENT_TYPE_VIDEO,		///< 동영상 광고
	CONTENT_TYPE_TEXT,		///< 텍스트 광고
	CONTENT_TYPE_KEYWORD	///< 키워드 광고
} CONTENT_TYPE;

/** 배너 이미지 노출 타입<br>
 Content Type이 이미지인 경우에만 적용
 */
typedef enum _DISPLAY_TYPE {
	DISPLAY_TYPE_NORMAL = 0,	///< 일반이미지 배너광고
	DISPLAY_TYPE_EXPANSION,		///< 확장형 광고
	DISPLAY_TYPE_SLIDING,		///< 슬라이딩 광고
	DISPLAY_TYPE_FULLSCREEN,	///< FullScreen Banner
	DISPLAY_TYPE_WEBVIEW		///< 웹뷰 컨트롤
} DISPLAY_TYPE;

/// 배너 터치시 랜딩되는 유형
typedef enum _RANDING_TYPE {
	RANDING_TYPE_LINK = 1,	///< 웹 페이지를 호출
	RANDING_TYPE_CALL,		///< 지정된 번호로 전화걸기
	RANDING_TYPE_PLAY,		///< 동영상 광고를 재생
	RANDING_TYPE_SCHEDULE,	///< 스케쥴에 일정을 등록
	RANDING_TYPE_APPSTORE,	///< 앱스토어 어플을 호출
	RANDING_TYPE_NONE		///< 랜딩 동작이 등록되지 않았음
} RANDING_TYPE;

/// 캘린더 등록 정보 구조체
typedef struct _STR_SCHEDULE {
	NSString *	dateTime;		///< 일정으로 사용할 일시
	NSString *	title;			///< 일정으로 등록할 제목
	NSString *	desc;			///< 일정의 상세정보 내용
	NSInteger	alarmBefore;	///< 일정을 알리기 위한 알람 시간 = dateTime-alarmBefore(min)
}STR_SCHEDULE, *PSTR_SCHEDULE;

/// 동영상 광고 오버레이 버튼 구조체
typedef struct _STR_ICON {
	NSInteger startTime;	///< 버튼 노출시점
	NSInteger displayTime;	///< 노출시간
	NSString * buttonUrl;	///< 버튼 이미지 url
	NSString * landingUrl;	///< 버튼 랜딩 url
}STR_ICON, *PSTR_ICON;

/// 이미지 광고 구조체
typedef struct _STR_IMAGE_INFO {
	STR_ICON		icon;
	NSString *		imageUrl;			///< 배너 광고 이미지 주소
	NSString *		landingUrl;			///< 배너 터치시 랜딩할 주소
	NSString *		logUrl1;			///< 로그를 남기기 위한 서버주소 1
	NSString *		logUrl2;			///< 로그를 남기기 위한 서버주소 2
	NSString *		videoWF;			///< WiFi용 동영상 광고 주소
	NSString *		video3G;			///< 3G용 동영상 광고 주소
	NSInteger		landingType;		///< 배너 터치시 랜딩할 타입
	NSInteger		displayTime;		///< 노출 시간
	NSInteger		displayType;		///< 노출 타입
	NSInteger		srcWidth;			///< 배너 이미지의 원본 너비
	NSInteger		srcHeight;			///< 배너 이미지의 원본 높이
	NSInteger		dispWidth;			///< 배너 이미지를 출력할 너비
	BOOL			bFullScreen;		///< 광고 데이터의 전면광고 플래그
	STR_SCHEDULE	schedule;			///< 일정데이터 구조체 포인터
}STR_IMAGE_INFO, *PSTR_IMAGE_INFO;

/// 동영상 광고 구조체
typedef struct _STR_VIDEO_INFO {
	STR_ICON icon;
	NSString * thumbnailUrl;	///< 동영상 썸네일 주소
	NSString * videoUrl;		///< 광고동영상 주소
	NSString * videoWF;			///< WiFi용 동영상 광고 주소
	NSString * video3G;			///< 3G용 동영상 광고 주소
	NSString * landingUrl;		///< 랜딩 주소
	NSString * logUrl1;			///< 배너 클릭시 로그를 남기기 위한 서버주소
	NSString * logUrl2;			///< 광고 동영상 재생 종료시 로그를 남기기 위한 서버주소
}STR_VIDEO_INFO, *PSTR_VIDEO_INFO;


/**
 @brief 광고데이터를 다운로드, 파싱하여 데이터 처리를 담당하는 클래스
 */
@interface UPAJsonParser : NSObject {
	NSString *		DeviceId;
	NSString *		SlotID;
	NSString *		AdGdsID;
	BOOL			bFree;
	BOOL			bHouseAD;
	NSInteger		IntervalTime;	// 광고 데이터 재요청 시간
	NSDictionary*	dicContents;	// JSON Data를 파싱하기 위해서 사용되는 dictionary
	AD_TYPE_JSON	adType;			// 광고 타입
	
	// variables
	NSInteger		contentType;	// content type
	STR_VIDEO_INFO	videoInfo;		// video info
	STR_IMAGE_INFO	imageInfo;		// image info
	UPAReachability* internetReach;	// 망 접속 상태
};

// URL Init & Parse basic data
- (BOOL) initUPAJsonParserWithURL:(NSURL*)url;

// set slotID
- (void) setSlotID:(NSString*)slotID;

// set houseAD
- (void) setHouseAD:(BOOL)bTrue;

// set ad type (video or banner)
- (void) setAdType:(AD_TYPE_JSON)type;

// get Contents Type (Video or banner)
- (NSInteger) getContentType;

// get deviceId
- (NSString*) getDeviceId;

// set interval
- (void) setInterval:(NSInteger)interval;
- (NSInteger) getInterval;

// set isFree
- (void) setFree:(BOOL)isFree;
- (BOOL) getFree;

// Banner URL
- (NSString*) getBannerURL;

// check fullscreen image
- (BOOL) isFullScreen;

// Banner Info
- (NSInteger) getBannerDisplayType;
- (NSInteger) getBannerLandingType;
- (NSString*) getBannerLandingUrl;
- (NSInteger) getBannerSourceWidth;
- (NSInteger) getBannerSourceHeight;
- (NSInteger) getBannerDisplayWidth;
- (NSInteger) getBannerDisplayTime;
- (NSString*) getBannerVideoUrl;
- (NSString*) getBannerLogUrl1;
- (NSString*) getBannerLogUrl2;

// Video Info
- (VIDEO_TYPE) getVideoType;
- (NSString*) getVideoUrl;
- (NSString*) getVideoThumbnail;
- (NSString*) getVideoLogUrl1;
- (NSString*) getVideoLogUrl2;

// Icon info
- (NSString*) getIconLandingUrl;
- (NSString*) getIconButtonUrl;
- (NSInteger) getIconStartTime;
- (NSInteger) getIconDisplayTime;

// schedule data
- (NSInteger) getYear;
- (NSInteger) getMonth;
- (NSInteger) getDay;
- (NSInteger) getHour;
- (NSInteger) getMinute;
- (NSString*) getScheduelTitle;
- (NSString*) getScheduleDescription;
- (NSInteger) getAlarmTime;

// Log URL
- (NSString*) getLogUrl1;
- (NSString*) getLogUrl2;

@end
