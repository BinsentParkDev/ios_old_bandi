//
//  UPAPlayer.h
//  UplusAdSDK
//
//  Created by U+AD
//  Copyright 2011 LG Uplus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

/// 닫기 버튼을 동적으로 사용할 경우
#define _USING_ACTION_BUTTON

/**
 @brief 동영상 광고물을 재생하기 위해서 사용되는 플레이어 컨트롤 클래스
 */
@interface UPAPlayer : UIViewController {
	MPMoviePlayerController		*player;			///< 동영상 플레이어
	UIStatusBarStyle			oldBarStyle;		///< 상태바 저장을 위한 변수
	UIInterfaceOrientation		oldBarOrientation;	///< Orientation 상태 저장을 위한 변수
	
	NSString					*buttonImageURL;	///< 오버레이 버튼
	NSInteger					startTime;			///< 오버레이 시작시간
	NSInteger					endTime;			///< 오버레이 진행시간
	UIButton					*actionBtn;			///< 오버레이 버튼 컨트롤
	UIButton					*closeBtn;			///< 닫기 버튼 컨트롤
	NSTimer						*actionTimer;		///< 오버레이 표시를 위한 타이머
	NSTimer						*closeTimer;		///< 닫기 버튼 숨김 타이머
	
	id							pParent;			///< 부모 뷰 아이디
	BOOL						bShowActionBtn;		///< 오버레이 버튼 상태 플래그
	BOOL						bShowCloseBtn;		///< 닫기 버튼 상태 플래그
	
	UIActivityIndicatorView		*indicator;			///< 네트웍 인디케이터
	NSString					*playURL;			///< 재생할 컨텐츠 주소
	NSString					*logUrl;			///< 로그전송에 사용할 주소
	
	NSInteger					deviceType;			///< 디바이스 타입
	BOOL						StatusBarHidden;	///< 상태바 체크
}

- (void) play:(NSString*)urlStr;
- (void) setButton:(NSString*)btnUrl startTime:(NSInteger)start endTime:(NSInteger)end logUrl:(NSString*)log devType:(NSInteger)type;
- (void) setParent:(id)parent;
- (BOOL) isUpdateWeb;
- (void) updateWeb;
@end
