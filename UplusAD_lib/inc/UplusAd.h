//
//  UplusAd.h
//  UplusAdSDK
//
//  Created by U+AD
//  Copyright 2011 LG Uplus. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "UPAJsonParser.h"
//#import "UPAPlayer.h"
//#import "UPALanding.h"

@class UPAJsonParser;
@class UPAPlayer;
@class UPALanding;

#define _USE_GPS
#ifdef USE_GPS
#import "GPGManager.h"
#endif
//#import "SensorManager.h"
#define _USING_INTERNAL_SERVER

/** 
 @breif 광고 SDK로 부터 광고 창이 닫히는 동작을 전달받기 위한 델리게이트
 */
@protocol UPlusAdDelegate;


typedef enum {
	UPLUS_AD_TYPE_BANNER = 0,
	UPLUS_AD_TYPE_VIDEO
}UPLUS_AD_TYPE;

typedef enum {
	UPLUS_DEVICE_IPHONE = 0,
	UPLUS_DEVICE_IPAD
}UPLUS_DEVICE_TYPE;

/**
 @brief UPlus 광고 SDK를 사용하기 위한 라이브러리 <br>
 요구사항 : iOS 4.0 이상의 버전에서 동작 <br>
 프레임워크 추가항목<br>
  - MediaPlayer.framework<br>
  - EventKit.framework	
 */
@interface UplusAd : UIView <UIWebViewDelegate, UIAccelerometerDelegate> {
	/// JSON Parser	
	UPAJsonParser*	json;
	UPAJsonParser*	json_temp;
	/// 부모 뷰의 아이디
	id				parent;
	/// 광고 노출 타입
	UPLUS_AD_TYPE	adType;
	/// 디바이스 타입
	UPLUS_DEVICE_TYPE deviceType;
	/// 광고 슬롯 아이디
	NSString*		adSlotID;
	NSString*		defaultPath;
	NSString*		DeviceId;
	/// 광고 서버 주소
	NSString	*adServerURL;
	/// 광고 노출에 사용되는 뷰
	UIWebView		*bannerWeb;
	UIWebView		*bannerBuf;
	//	광고 로딩 표시를 위한 Indicator
	UIActivityIndicatorView *indicator;
	/// 전면광고에서 사용되는 닫기 버튼
	UIButton		*closeButton;
	
	/// 동영상 광고를 사용하기 위한 동영상 플레이어
	UPAPlayer * player;
	
	UPALanding *mLanding;
	/// 광고 데이터 다운로드를 위한 타이머
	NSTimer		*updateTimer;
	/// 전면광고 디스플레이 시간이 되면 광고창을 닫기 위한 타이머
	NSTimer		*closeTimer;
	/// 델리게이트 : 전면광고의 경우 광고뷰가 종료될 때 이벤트를 받고자 하는 경우 세팅 필요
	id <UPlusAdDelegate> delegate;
	/// 광고 팝업 타입 변수
	NSInteger	alertType;	
	/// 현재 광고의 디스플레이 타입
	NSInteger	curDisplayType;	
	/// retry회수
	NSInteger	retryCnt;
	/// 확장형 광고의 이미지 뷰
	UIWebView	*bannerExp;		
	/// 광고 배너의 위치 값을 저장하기 위한 변수
	CGRect		oldRect;	
	// for thread
	CGRect		customRect;		///< 배너 위치 임시저장 변수
	/// Async 형태로 광고뷰 생성
	BOOL			bFullScreen;
	/// 프로세스 종료 플래그
	BOOL		bExitProcess;
	/// 광고 업데이트를 하지 않기 위한 플래그
	BOOL		useIgnore;
	/// 배너의 위치가 변경중인지를 확인하기 위한 플래그
	BOOL		bMoving;	
	/// 확장배너 이미지 다운로드 완료 플래그
	BOOL		bDownloadExp;	
    /// StatusBar 접근 플래그
    BOOL        useStatus;
	///< 어플에 상태바가 있는지를 저장 : 20110513
	BOOL		IsStatusBarHidden;	
	//	배너 과금 여부 
	BOOL		bFree;
#ifdef USE_GPS
	GPGManager*	location;		// GPS Manager
#endif
}

/// 어플에 적용할 슬롯아이디를 세팅하기 위한 함수
- (void)setSlotID:(NSString*)slotID;

/// 디폴트 배너를 세팅하기 위한 함수
- (void)setdefaultAdPath:(NSString*)defaultAdPath;

/// 부모 뷰의 아이디를 세팅하기 위한 함수
- (void)setParent:(id)pParent;

/// 어플이 시작되는 시점에서 사용하는 광고의 경우 세팅하는 함수
- (void)setFullScreen:(BOOL)fullScreen;

/// 배너광고를 하나로만 사용하고자 하는 경우에 세팅하는 함수
- (void) useIgnore:(BOOL)use;

/// 디바이스 타입을 설정 (default:UPLUS_DEVICE_IPHONE)
- (void) setDeviceType:(UPLUS_DEVICE_TYPE)type;

/// StatusBar의 영역을 앱에서 사용하기 위한 설정 (default:NO)
- (void) useStatusBar:(BOOL)use;

/// 업데이트가 필요한 배너 광고인지 체크.
- (BOOL) isUpdateWeb;

/// 센서 정보 전달을 시작함.
- (void) startSensor;

/// 센서 정보 전달을 종료함.
- (void) endSensor;

/// 요청 데이터를 만드는 함수.
- (NSString*) makeParams;

/// 광고를 보여주기 위한 뷰 생성 함수.
- (void) dispWebView :(BOOL)bHide;

/// 웹뷰 생성 함수.
- (void) landToWebView:(NSString*)url;

@property (nonatomic,assign) id <UPlusAdDelegate> delegate;
@end

@protocol UPlusAdDelegate <NSObject>;
@required

/* 광고뷰가 종료될 때 호출되는 이벤트 함수
 delegate를 설정하였을 경우에만 호출됨
 delegate를 설정한 경우 반드시 함수를 선언해 주어야 함
 */

/// 델리게이트 이벤트 함수
-(void) isFreeUPlusAdView:(BOOL)bFree;
-(void) didCloseUPlusAdView:(NSString*)slotID;
@end
