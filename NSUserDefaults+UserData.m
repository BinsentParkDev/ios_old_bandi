//
//  NSUserDefaults+UserData.m
//  Bandi2
//
//  Created by admin on 2016. 8. 5..
//  Copyright © 2016년 EBS. All rights reserved.
//

#import "NSUserDefaults+UserData.h"
#import "Util.h"


// ref. : http://stackoverflow.com/questions/6828831/sending-const-nsstring-to-parameter-of-type-nsstring-discards-qualifier
NSString * const USER_ID = @"SAVED_ID";
NSString * const PASSWORD = @"SAVED_PW";
NSString * const ENCRYPTED = @"ENCRYPTED";


@implementation NSUserDefaults (UserData)


- (void) setUserId:(NSString *)userId
{
    [self setObject:[Util encryptString:userId] forKey:USER_ID];
    [self doEncrypt:YES];
}

- (void) setPassword:(NSString *)password
{
    [self setObject:[Util encryptString:password] forKey:PASSWORD];
    [self doEncrypt:YES];
}

/**
 * 기기에 저장된 사용자 ID를 반환 (자동로그인, ID저장 설정 시 사용)
 */
- (NSString *)getUserId
{
    NSString *userid = (NSString *)[self objectForKey:USER_ID];
    if ([self isEncrypted]) userid = [Util decryptString:userid];
    return userid;
}

- (NSString *)getPassword
{
    NSString *pw = (NSString *)[self objectForKey:PASSWORD];
    if ([self isEncrypted]) pw = [Util decryptString:pw];
    return pw;
}

- (BOOL) isEncrypted
{
    return (BOOL)[self boolForKey:ENCRYPTED];
}

- (void) doEncrypt:(BOOL)yesOrNo
{
    [self setBool:yesOrNo forKey:ENCRYPTED];
}

@end
