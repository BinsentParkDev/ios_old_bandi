//
//  GA.h
//  Bandi2
//
//  Created by admin on 2017. 4. 5..
//  Copyright © 2017년 EBS. All rights reserved.
//

#ifndef GA_h
#define GA_h


#endif /* GA_h */

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

typedef NS_ENUM(NSInteger, GAChannel) {
    GA_UNKNOWN=0,
    GA_FM=1,
    GA_IRADIO=2
};


@interface GA : NSObject
{
}

/**
 * Google Analytics Screen View 전송 (screen name)
 */
+ (void)sendScreenView:(NSString *)screen;

/**
 * Google Analytics Screen View 전송 (screen name, channel)
 */
+ (void)sendScreenView:(NSString *)screen withChannel:(GAChannel)channel ;


@end
