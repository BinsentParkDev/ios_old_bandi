//
//  WebViewController.h
//  Bandi2
//
//  Created by admin on 2014. 1. 22..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface LogContentController : UIViewController<FBLoginViewDelegate, UIWebViewDelegate>
{
    NSUserDefaults *userDefaults ;
}

- (IBAction)closeView:(id)sender;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

- (void)onUnload;

@property (assign, nonatomic) BOOL bGoboard;
@property (assign, nonatomic) BOOL bGoBack;
@property (assign, nonatomic) BOOL bOnlyEBS;
//@property (strong, nonatomic) IBOutlet LoginView *wrapperView;
//@property (strong, nonatomic) IBOutlet UIView *loginFormView;
//@property (strong, nonatomic) IBOutlet UIView *loginOptionView;
//@property (strong, nonatomic) IBOutlet UIView *closeFormView;

@property (strong, nonatomic) IBOutlet UITextField *userId;
@property (strong, nonatomic) IBOutlet UITextField *passwd;
- (IBAction)doLogin:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *saveIdBtn;
- (IBAction)saveId:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *autoLoginBtn;
@property (strong, nonatomic) IBOutlet FBLoginView *fbLoginView;
@property (strong, nonatomic) NSString *objectID;
@property (strong, nonatomic) IBOutlet UIButton *twitterLogin;

@property (strong, nonatomic) IBOutlet UIButton *keyHideBt;

@property (strong, nonatomic) IBOutlet UIView *twLoginV;

@property (strong, nonatomic) IBOutlet UIView *webControll;

- (IBAction)autoLogin:(id)sender;
- (IBAction)twitterLogin:(id)sender;
- (void)EBSLoginOnly;
- (void)loginBack:(BOOL)bb;
//- (IBAction)closeLoginView:(id)sender;

-(IBAction)findId:(id)sender;
-(IBAction)findPass:(id)sender;
-(IBAction)Join:(id)sender;

-(IBAction)hidekey:(id)sender;
-(IBAction)loginTmp:(id)sender;
@end
