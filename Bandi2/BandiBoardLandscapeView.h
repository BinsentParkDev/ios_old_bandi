//
//  BandiBoardLandscapeView.h
//  Bandi2
//
//  Created by admin on 2014. 2. 4..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BandiBoardLandscapeView : UIView

@property (strong, nonatomic) IBOutlet BandiBoardLandscapeView *wrapperView;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIView *iconBgView;
- (IBAction)reloadWeb:(id)sender;
- (IBAction)exitBoard:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *reloadWebBtn;
@property (strong, nonatomic) IBOutlet UIButton *exitBoardBtn;
@end
