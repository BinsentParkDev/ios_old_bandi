//
//  AirContentsView.m
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "AirContentsView.h"
#import "TimeTable.h"
#import "ProgramData.h"
#import "ServerSide.h"
#import "Util.h"

#define TAG @"AirContents"


@implementation AirContentsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        
        [[NSBundle mainBundle] loadNibNamed:@"AirContentsView" owner:self options:nil];
        
        [_wrapperView addSubview:_programLabel];
        [_programLabel setFrame:CGRectMake(0, 0, 320.0f, 36.0f)];
        
        [_wrapperView addSubview:_contentsView];
        [_contentsView setFrame:CGRectMake(0, 36.0f, 320.0f, bodyHeight-36)];
        
        // 프로그램명 설정
        TimeTable *tt = [[TimeTable alloc] init];
        NSDictionary *ttDic = [tt get];
        
        _programLabel.text = ttDic[@"progname"];
        
        // 방송내용 텍스트 설정
        ProgramData *pd = [[ProgramData alloc] init];
        NSDictionary *pdDic = [pd get];
        _contentsView.text = pdDic[@"CONTENTS"];
        
        
        // 방송내용 텍스트 백그라운드 이미지 설정
        UIGraphicsBeginImageContext(_contentsView.frame.size);
        [[UIImage imageNamed:@"bg_base.png"] drawInRect:_contentsView.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        _contentsView.backgroundColor = [UIColor colorWithPatternImage:image];
        
        
        [self addSubview:_wrapperView];
        
    }
    
    [ServerSide saveEventLog:TAG];
    
    return self;
}

- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
