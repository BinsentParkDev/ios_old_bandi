//
//  AirContentsView.h
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AirContentsView : UIView
{
    
}
- (void)onUnload;

@property (strong, nonatomic) IBOutlet AirContentsView *wrapperView;
@property (strong, nonatomic) IBOutlet UILabel *programLabel;
@property (strong, nonatomic) IBOutlet UITextView *contentsView;
@end
