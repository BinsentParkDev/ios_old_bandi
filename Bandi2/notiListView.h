//
//  BandiBoardView.h
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "notiListCell.h"

@interface notiListView : UIView<onNotiCellDelegate>
{
    NSMutableArray *boardList;
    NSString *strEventUrl;
    NSString *strEventDetailUrl;
}
- (void)onUnload;
@property (strong, nonatomic) IBOutlet UITableView *tbList;
@property (strong, nonatomic) IBOutlet notiListView *wrapperView;
@property (strong, nonatomic) IBOutlet UILabel *programLabel;
//@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) IBOutlet UIView * notiView;

@property (strong, nonatomic) IBOutlet UILabel * notiTitle;
@property (strong, nonatomic) IBOutlet UILabel * titleText;

@property (strong, nonatomic) IBOutlet UIView * detailView;
@property (strong, nonatomic) IBOutlet UIImageView * detailIcon;
@property (strong, nonatomic) IBOutlet UIImageView * detailEventImage;
@property (strong, nonatomic) IBOutlet UILabel * detailTitle;
@property (strong, nonatomic) IBOutlet UIScrollView * detailContentScroll;
@property (strong, nonatomic) IBOutlet UILabel * detailNotiContent;
@property (strong, nonatomic) IBOutlet UIButton * btEventDetail;
@property (strong, nonatomic) IBOutlet UIButton * eventDetailBt;

- (IBAction)writeBulletin:(id)sender;
- (IBAction)openNoti:(id)sender;
- (IBAction)closeNotiDetail:(id)sender;

- (IBAction)openEventUrl:(id)sender;
-(IBAction)eventDetailUrlOpen:(id)sender;
@end



