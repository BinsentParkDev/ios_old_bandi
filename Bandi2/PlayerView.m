//
//  PlayerView.m
//  Bandi2
//
//  Created by admin on 2013. 12. 20..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "PlayerView.h"
#import "IIViewDeckController.h"
#import "BandiBoardView.h"
#import "Util.h"
#import "Dialogs.h"
#import "WebViewController.h"

//#define NO_WIFI_WARNNING @"현재 WiFi에 연결되어 있지 않습니다.\n데이터 네트워크(3G/4G/LTE)를 사용하시려면 설정에서 데이터 네트워크 사용을 체크해 주세요. (단, 데이터 이용 요금이 별도로 부과될 수 있습니다.)"
#define NO_WIFI_WARNNING @"현재 사용 가능한 네트워크를 확인해 주십시오.\n데이터 네트워크(3G/4G/LTE)를 사용하려면 설정에서 네트워크 사용을 체크해주세요. (데이터 이용 요금이 별도로 부과될 수 있습니다.)"

@implementation PlayerView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        bAutoStop = NO;
        
        PlayerView *playerView = [[[NSBundle mainBundle] loadNibNamed:@"PlayerView" owner:self options:nil] lastObject];
        [self addSubview:playerView];

        volumeSlider = [[UISlider alloc]initWithFrame:CGRectMake(75, 4, 100, 31)];
        [_onAirPlayer addSubview:volumeSlider];
        [volumeSlider setMinimumTrackImage:[UIImage imageNamed:@"list_progress_active_bg.png"] forState:UIControlStateNormal];
        [volumeSlider setMaximumTrackImage:[UIImage imageNamed:@"list_progress_inactive_bg.png"] forState:UIControlStateNormal];
        [volumeSlider setThumbImage:[UIImage imageNamed:@"ico_seek_btn_on.png"] forState:UIControlStateNormal];
        
        [volumeSlider addTarget:self action:@selector(controlVolume:) forControlEvents:UIControlEventValueChanged];
        [volumeSlider setThumbImage:[UIImage imageNamed:@"ico_seek_btn_off.png"] forState:UIControlStateNormal];
        [_volumeSlider2 setThumbImage:[UIImage imageNamed:@"ico_seek_btn_off.png"] forState:UIControlStateNormal];
        volumeSlider.value = [MPMusicPlayerController applicationMusicPlayer].volume; // 시스템 볼륨 값 슬라이드 적용
        _volumeSlider2.value = [MPMusicPlayerController applicationMusicPlayer].volume; // 시스템 볼륨 값 슬라이드 적용
        
        // 백그라운드 이미지 설정
//        UIGraphicsBeginImageContext(playerView.frame.size);
//        [[UIImage imageNamed:@"bg_player_area.png"] drawInRect:playerView.bounds];
//        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        playerView.backgroundColor = [UIColor colorWithPatternImage:image];
        
        twitterBtn = [[UIButton alloc]initWithFrame:CGRectMake(180, 5, 30, 30)];
        faceBookBtn = [[UIButton alloc]initWithFrame:CGRectMake(212, 5, 30, 30)];
        kakaoBtn = [[UIButton alloc]initWithFrame:CGRectMake(244, 5, 30, 30)];
        homeBtn = [[UIButton alloc]initWithFrame:CGRectMake(279, 5, 30, 30)];
        
        [twitterBtn setImage:[UIImage imageNamed:@"ico_sns_twitter_off.png"] forState:UIControlStateNormal];
        [faceBookBtn setImage:[UIImage imageNamed:@"ico_sns_facebook_off.png"] forState:UIControlStateNormal];
        [kakaoBtn setImage:[UIImage imageNamed:@"ico_sns_kakaostory_off.png"] forState:UIControlStateNormal];
        [homeBtn setImage:[UIImage imageNamed:@"ico_home_off.png"] forState:UIControlStateNormal];
        
        [twitterBtn addTarget:self action:@selector(actionTw:) forControlEvents:UIControlEventTouchUpInside];
        [faceBookBtn addTarget:self action:@selector(actionFb:) forControlEvents:UIControlEventTouchUpInside];
        [kakaoBtn addTarget:self action:@selector(actionKk:) forControlEvents:UIControlEventTouchUpInside];
        [homeBtn addTarget:self action:@selector(actionHp:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [_onAirPlayer addSubview:twitterBtn];
        [_onAirPlayer addSubview:faceBookBtn];
        [_onAirPlayer addSubview:kakaoBtn];
        [_onAirPlayer addSubview:homeBtn];
        
        
        
        userDefaults = [NSUserDefaults standardUserDefaults];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerStateDidChange:)
                                                     name:MPMoviePlayerLoadStateDidChangeNotification
                                                   object:_audioPlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playBackStateDidChange:)
                                                     name:MPMoviePlayerPlaybackStateDidChangeNotification
                                                   object:_audioPlayer];
        
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(audio_volumeChanged:)
                                                     name:@"AVSystemController_SystemVolumeDidChangeNotification"
                                                   object:nil];
        
//        [self loadSound];
        
        strUrltw = [[NSString alloc]init];
        strUrlHm = [[NSString alloc]init];
        strUrlfb = [[NSString alloc]init];
        strUrlkk = [[NSString alloc]init];
        
        arrSnsBt= [[NSMutableArray alloc]initWithCapacity:0];
        faceBookBtn.hidden = YES;
        twitterBtn.hidden = YES;
        kakaoBtn.hidden = YES;
        homeBtn.enabled = NO;
        
    }
    return self;
}

- (void)onUnload
{
//    if (wifiCheckTimer.isValid) {
//        [wifiCheckTimer invalidate];
//    }
    
    NSLog(@"Unload : %@", [self class]);
}



- (void)loadSound:(NSString *)strUrl
{

    NSError *error;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    
//    NSString *hostIP = [Util getHostIP:@"ebsonairiosaod.ebs.co.kr"]; // 2014.07.28 변경.
//    NSString *streamURL = [NSString stringWithFormat:@"http://%@/fmradiobandiaod/bandiappaac/playlist.m3u8", hostIP];

    NSLog(@"streaming url : %@", strUrl);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    
    //* --------- host -> IP 변경 ---------------
    @try {
        NSString *hostIP = [Util getHostIP:[url host]];
        strUrl = [strUrl stringByReplacingOccurrencesOfString:[url host] withString:hostIP];
        NSLog(@"host changed ------> %@", strUrl);
        url = [NSURL URLWithString:strUrl];
    }
    @catch (NSException *exception) {
        // pass
    }
    @finally {
        // <#Code that gets executed whether or not an exception is thrown#>
    }
    //--------------  host -> IP 변경 --------- */
    
    _audioPlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    _audioPlayer.shouldAutoplay = NO;
    _audioPlayer.controlStyle = MPMovieControlStyleNone;
    _audioPlayer.movieSourceType = MPMovieSourceTypeStreaming;
    
    [_audioPlayer prepareToPlay];
    

    /**** init 으로 이동..
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStateDidChange:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:_audioPlayer];

    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(audio_volumeChanged:)
												 name:@"AVSystemController_SystemVolumeDidChangeNotification"
											   object:nil];
    *******/
    
    NSLog(@"network -> %ld", (long)[Util getNetworkReachable]);
    
    
    if ([Util getNetworkReachable] != NETWORK_WIFI && ![userDefaults boolForKey:@"PAID_NETWORK"]) {

        UIAlertView *alertV = [[UIAlertView alloc]initWithTitle:@"알림" message:NO_WIFI_WARNNING delegate:self cancelButtonTitle:@"취소" otherButtonTitles:@"확인",nil];
        alertV.tag = 1001;
        [alertV show];
        bAutoStop = NO;
        
    } else {
        [self playAudio:nil];
    }
    
    [self btSnsSort];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

# pragma mark - volume 조절
- (IBAction)controlVolume:(id)sender {
    UISlider *slider = (UISlider *)sender;
    [[MPMusicPlayerController applicationMusicPlayer] setVolume:slider.value];
    //NSLog(@"%f == %f", slider.value, [MPMusicPlayerController applicationMusicPlayer].volume);
}

- (void)audio_volumeChanged:(NSNotification *)notification {
    CGFloat volume = [[[notification userInfo] objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"]
					  floatValue];
    
    [volumeSlider setValue:volume];
    [_volumeSlider2 setValue:volume];
    if(volume > 0)
    {
        _muteBtn.selected = NO;
        _muteBtn2.selected = NO;
    }

    
}

- (IBAction)muteSound:(id)sender {
    UIButton *button = (UIButton *)sender;
    if (button.selected) {
        [[MPMusicPlayerController applicationMusicPlayer] setVolume:volumeSlider.value];
        
        button.selected = NO;
    } else {
        [[MPMusicPlayerController applicationMusicPlayer] setVolume:0.0f];
        button.selected = YES;
    }
}

- (IBAction)muteSound2:(id)sender {
    UIButton *button = (UIButton *)sender;
    if (button.selected) {
        [[MPMusicPlayerController applicationMusicPlayer] setVolume:_volumeSlider2.value];
        
        button.selected = NO;
    } else {
        [[MPMusicPlayerController applicationMusicPlayer] setVolume:0.0f];
        button.selected = YES;
    }
}

# pragma mark - 재생 조절

- (void)playBackStateDidChange:(NSNotification *)notification
{

    if(MPMoviePlaybackStatePlaying == _audioPlayer.playbackState)
    {
        _playBtn.selected = YES;
        _playBtn2.selected = YES;
        if(wifiCheckTimer)
        {
            [wifiCheckTimer invalidate];
            wifiCheckTimer = nil;
            
        }
        
        wifiCheckTimer = [NSTimer scheduledTimerWithTimeInterval:1.
                                                          target:self
                                                        selector:@selector(checkWifi:)
                                                        userInfo:nil
                                                         repeats:YES ];


    }
    else
    {
        _playBtn.selected = NO;
        _playBtn2.selected = NO;

    }
//    NSLog(@"@@@@@@@@@ player state did change -> %ld", (long)_audioPlayer.playbackState);
}
- (void)playerStateDidChange:(NSNotification *)notification
{
//    NSLog(@"############ player state did change -> %ld", (long)_audioPlayer.playbackState);

    
    if (_audioPlayer.playbackState == MPMoviePlaybackStatePlaying) {
        _playBtn.selected = YES;
        _playBtn2.selected = YES;
        
        
        
    } else {
        _playBtn.selected = NO;
        _playBtn2.selected = NO;
        if(!bNoWifiError)
        {
            if(wifiCheckTimer)
            {
                [wifiCheckTimer invalidate];
                wifiCheckTimer = nil;
                
            }

        }
    }

}


- (void)stopAudio
{
    
    if ([_audioPlayer playbackState] == MPMoviePlaybackStatePlaying) {
        [_audioPlayer stop];
        _playBtn.selected = NO;
        _playBtn2.selected = NO;
    }
}

- (IBAction)playAudio:(id)sender {
    
    if(vodPlaying)
    {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayerPlay" object:nil];
        [self btSnsSort];
        return;
    }
    
    if(playerCheckTimer)
    {
        [playerCheckTimer invalidate];
        playerCheckTimer = nil;
        
    }
    
    if(wifiErrorTimer)
    {
        [wifiErrorTimer invalidate];
        wifiErrorTimer = nil;
        
    }

    
    
    //UIButton *playBtn = (UIButton *)sender;
    if ([_audioPlayer playbackState] == MPMoviePlaybackStatePlaying) {
        [_audioPlayer stop];
        //_playBtn.selected = NO; // playerStateDidChange 로 기능 위임
    } else {
        if ([Util getNetworkReachable] != NETWORK_WIFI && ![userDefaults boolForKey:@"PAID_NETWORK"]) {
//            [Dialogs alertView:NO_WIFI_WARNNING];
            UIAlertView *alertV = [[UIAlertView alloc]initWithTitle:@"알림" message:NO_WIFI_WARNNING delegate:self cancelButtonTitle:@"취소" otherButtonTitles:@"확인",nil];
            alertV.tag = 1001;
            [alertV show];
                bAutoStop = NO;

        } else {

            [self performSelector:@selector(loadTable) withObject:nil afterDelay:0.5f];
            
            //_playBtn.selected = YES; // playerStateDidChange 로 기능 위임
        }
        
    }
    
}

# pragma mark - 반디 게시판 열기
- (IBAction)bandiBoard:(id)sender {
     [[NSNotificationCenter defaultCenter] postNotificationName:@"MoveTab3" object:nil];
    
}

# pragma mark - 와이파이 상태 체크

- (void)checkNetworkAfter:(NSTimer *)timer
{
    if(wifiCheckAfterTimer)
    {
        [wifiCheckAfterTimer invalidate];
        wifiCheckAfterTimer = nil;
        
    }

    
    if(playerCheckTimer)
    {
        [playerCheckTimer invalidate];
        playerCheckTimer = nil;
        
    }
    
    playerCheckTimer = [NSTimer scheduledTimerWithTimeInterval:10.
                                                        target:self
                                                      selector:@selector(checkNetworkFail:)
                                                      userInfo:nil
                                                       repeats:NO ];
    NSLog(@"checkNetworkFail ---");
    NSLog(@"c&&&&&&&&&&& bAutoStop GOGOGO ");
    bAutoStop = YES;
    [_audioPlayer stop];
    

}
- (void)checkNetworkFail:(NSTimer *)timer
{
    NSLog(@"checkNetworkFail");
    if(playerCheckTimer)
    {
        [playerCheckTimer invalidate];
        playerCheckTimer = nil;
    }
    bNoWifiError  = NO;
    [_audioPlayer stop];
//    [Dialogs alertView:NO_WIFI_WARNNING];
    UIAlertView *alertV = [[UIAlertView alloc]initWithTitle:@"알림" message:NO_WIFI_WARNNING delegate:self cancelButtonTitle:@"취소" otherButtonTitles:@"확인",nil];
    
    alertV.tag = 1001;
    [alertV show];
    bAutoStop = NO;


}

- (void)checkWifi:(NSTimer *)timer
{
   
//    NSLog(@"PlayerView.m > checkWifi (network reachable) = %d",[Util getNetworkReachable]);

    if ([Util getNetworkReachable] != NETWORK_WIFI && ![userDefaults boolForKey:@"PAID_NETWORK"]) {
        
        if (_audioPlayer.playbackState == MPMoviePlaybackStatePlaying) {

            if(bNoWifiError == NO)
            {
                if(wifiCheckAfterTimer)
                {
                    [wifiCheckAfterTimer invalidate];
                    wifiCheckAfterTimer = nil;
                    
                }


                float turn = _audioPlayer.playableDuration - _audioPlayer.currentPlaybackTime;
                NSLog(@"c&&&&&&&&&&& wifiCheckAfterTimer GOGOOGOGO %f",turn);
                wifiCheckAfterTimer = [NSTimer scheduledTimerWithTimeInterval:turn
                                                                  target:self
                                                                selector:@selector(checkNetworkAfter:)
                                                                userInfo:nil
                                                                 repeats:NO ];
                bNoWifiError = YES;
            }
     
        }
       return;        
    }
    else
    {
        if(wifiCheckAfterTimer)
        {
                NSLog(@"c&&&&&&&&&&& wifiCheckAfterTimer cancle ");
            [wifiCheckAfterTimer invalidate];
            wifiCheckAfterTimer = nil;
            
        }

    }
    bNoWifiError = NO;

    if(_audioPlayer.playableDuration - _audioPlayer.currentPlaybackTime < 1)
    {
        
        //playerCheckTimer
        if(bNodata == NO)
        {
            if(playerCheckTimer)
            {
                [playerCheckTimer invalidate];
                playerCheckTimer = nil;
                
            }
            NSLog(@"c&&&&&&&&&&& playerCheckTimer GOGOOGOGO ");
            
            playerCheckTimer = [NSTimer scheduledTimerWithTimeInterval:10.
                                                                target:self
                                                              selector:@selector(checkNetworkFail:)
                                                              userInfo:nil
                                                               repeats:NO ];

        }
        
        bNodata = YES;
    }
    else
    {
        if(bNodata)
        {
            if(playerCheckTimer)
            {
                NSLog(@"c&&&&&&&&&&& playerCheckTimer cancle ");

                [playerCheckTimer invalidate];
                playerCheckTimer = nil;
                
            }
        }
        bNodata = NO;
    }
    
    if(bAutoStop && bNoWifiError  == NO )
    {
        NSLog(@"c&&&&&&&&&&& bAutoStop cancle bNoWifiError = %d",bNoWifiError);
        bAutoStop = NO;
        [self playAudio:nil];

    }

    
}
-(void)facebookEnable:(BOOL)bEnable
{
    faceBookBtn.hidden = !bEnable;
}
-(void)twitterEnable:(BOOL)bEnable
{
    twitterBtn.hidden = !bEnable;
}
-(void)kakaoEnable:(BOOL)bEnable
{
    kakaoBtn.hidden = !bEnable;
}
-(void)homeEnable:(BOOL)bEnable
{
    homeBtn.enabled = bEnable;
}
-(void)btSnsSort
{
    
    int nn = 0;
    if (!kakaoBtn.isHidden)
        nn ++;
    if(!twitterBtn.isHidden)
        nn ++;
    if(!faceBookBtn.isHidden)
        nn ++;
    
    if(nn ==0)
    {
    }
    else if(nn ==1)
    {
        if(!kakaoBtn.isHidden) kakaoBtn.frame = CGRectMake(244, 5, 30, 30);
        else if(!twitterBtn.isHidden) twitterBtn.frame = CGRectMake(244, 5, 30, 30);
        else if(!faceBookBtn.isHidden) faceBookBtn.frame = CGRectMake(244, 5, 30, 30);
    }
    else if(nn ==2)
    {
        if(!kakaoBtn.isHidden && !twitterBtn.isHidden)
        {
            kakaoBtn.frame = CGRectMake(244, 5, 30, 30);
            twitterBtn.frame = CGRectMake(212, 5, 30, 30);
        }
        else if(!kakaoBtn.isHidden && !faceBookBtn.isHidden)
        {
            kakaoBtn.frame = CGRectMake(244, 5, 30, 30);
            faceBookBtn.frame = CGRectMake(212, 5, 30, 30);
        }
        else if(!twitterBtn.isHidden && !faceBookBtn.isHidden)
        {
            faceBookBtn.frame = CGRectMake(244, 5, 30, 30);
            twitterBtn.frame = CGRectMake(212, 5, 30, 30);
        }
    }
    else if(nn ==3)
    {
        kakaoBtn.frame = CGRectMake(244, 5, 30, 30);
        faceBookBtn.frame = CGRectMake(212, 5, 30, 30);
        twitterBtn.frame = CGRectMake(180, 5, 30, 30);
    }
    
    int volWidth = ((3 - nn) * 32)+100;
    
    volumeSlider.frame = CGRectMake(volumeSlider.frame.origin.x,
                                     volumeSlider.frame.origin.y,
                                     volWidth, volumeSlider.frame.size.height);
    
}
- (void)loadTable {
    [_audioPlayer play];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MainPlayerPlay" object:nil];

    
}
-(void)urlFacebook:(NSString *)strUrl
{
    strUrlfb = [NSString stringWithFormat:@"%@",strUrl];
}
-(void)urlTwitter:(NSString *)strUrl
{
    strUrltw = [NSString stringWithFormat:@"%@",strUrl];
}
-(void)urlKakao:(NSString *)strUrl
{
    strUrlkk = [NSString stringWithFormat:@"%@",strUrl];
}
-(void)urlHome:(NSString *)strUrl
{
    strUrlHm = [NSString stringWithFormat:@"%@",strUrl];
}

-(IBAction)actionFb:(id)sender
{
    NSURL *url = [NSURL URLWithString:strUrlfb];
//    [[UIApplication sharedApplication] openURL:url];
    WebViewController *vc = [[WebViewController alloc] init];
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
        [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }];

}
-(IBAction)actionTw:(id)sender
{
    NSURL *url = [NSURL URLWithString:strUrltw];
//    [[UIApplication sharedApplication] openURL:url];
    WebViewController *vc = [[WebViewController alloc] init];
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
        [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }];


}
-(IBAction)actionKk:(id)sender
{
    NSURL *url = [NSURL URLWithString:strUrlkk];
//    [[UIApplication sharedApplication] openURL:url];
    WebViewController *vc = [[WebViewController alloc] init];
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
        [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }];


}
-(IBAction)actionHp:(id)sender
{
    NSURL *url = [NSURL URLWithString:strUrlHm];
//    [[UIApplication sharedApplication] openURL:url];
    WebViewController *vc = [[WebViewController alloc] init];
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
        [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }];


}

- (void)vodPlay:(BOOL)bPlay
{
    vodPlaying = bPlay;
    

}
- (void)audioInfo:(NSDictionary *)dic
{
//    NSLog(@"dic =%@",dic);
    if(![dic count])
    {
        twitterBtn.hidden = YES;
        faceBookBtn.hidden = YES;
        kakaoBtn.hidden = YES;
        homeBtn.enabled = NO;
        [self btSnsSort];
        return;
    }

    
    if([dic valueForKey:@"twitter"] != (id)[NSNull null])
    {
        strUrltw = [NSString stringWithFormat:@"%@",[dic valueForKey:@"twitter"]];
        if([strUrltw length] > 1)
            twitterBtn.hidden = NO;
    }
    else
        twitterBtn.hidden = YES;

    if(![[dic valueForKey:@"facebookUrl"] isKindOfClass:[NSNull class]])
    {
        strUrlfb = [NSString stringWithFormat:@"%@",[dic valueForKey:@"facebookUrl"]];
        if([strUrlfb length] > 1)
            faceBookBtn.hidden = NO;
        
    }
    else
        faceBookBtn.hidden = YES;

    if(![[dic valueForKey:@"kakaostoryUrl"] isKindOfClass:[NSNull class]])
    {
         strUrlkk = [NSString stringWithFormat:@"%@",[dic valueForKey:@"kakaostoryUrl"]];
        if([strUrlkk length] > 1)
            kakaoBtn.hidden = NO;
    }
    else
        kakaoBtn.hidden = YES;
    

    if(![[dic valueForKey:@"mobHmpUrl"] isKindOfClass:[NSNull class]])
    {
        strUrlHm = [NSString stringWithFormat:@"%@",[dic valueForKey:@"mobHmpUrl"]];

        if([strUrlHm length] > 1)
            homeBtn.enabled = YES;
    }
    else homeBtn.enabled = NO;
    
    [self btSnsSort];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        if(buttonIndex)
        {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MoveTab7" object:nil];
            NSLog(@"aa");
        }
    }
    

}

@end
