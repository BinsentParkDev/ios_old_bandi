//
//  BandiBoardView.m
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "BandiBoardSmallView.h"
#import "TimeTable.h"
#import "IIViewDeckController.h"
#import "LoginView.h"
#import "BandiBoardWriteView.h"
#import "ServerSide.h"
#import "Util.h"
#import "boardCell.h"
#import "LogContentController.h"
#import "WebViewController.h"

#define TAG @"BandiBoard"

#define RgbColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@implementation BandiBoardSmallView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
//      int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        
        boardList = [[NSMutableArray alloc]init];
        if(IS_IPHONE5)
            [[[NSBundle mainBundle] loadNibNamed:@"BandiBoardSmallView" owner:self options:nil] lastObject];
        else
            [[[NSBundle mainBundle] loadNibNamed:@"BandiBoardSmallView_480" owner:self options:nil] lastObject];
        
        

        notiDic = [[NSMutableDictionary alloc]init];
        strEventDetailUrl = [[NSString alloc]init];
        
        [NSThread detachNewThreadSelector:@selector(loadBandiBoardWeb) toTarget:self withObject:nil];

        [_wrapperView bringSubviewToFront:_inputView];
        [self addSubview:_wrapperView];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillAnimate:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillAnimate:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reloadBoarding:)
                                                     name:@"reloadBoardingData"
                                                   object:nil];
        
    }
    
    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"UseBoard"] isEqualToString:@"Y"])
    {
        _noUserList.hidden = NO;
        _tbList.hidden = YES;
    }
    else
    {
        _noUserList.hidden = YES;
        _tbList.hidden = NO;
    }
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"] length] < 1)
    {
        _noUserList.hidden = NO;
        _tbList.hidden = YES;
        
    }
    pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:30.0f tableView:_tbList withClient:self];
    
    

    
//    [ServerSide saveEventLog:TAG];
    [self SNSIcon];
    [[UIApplication sharedApplication].keyWindow addSubview:_detailView];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_detailView];
    _detailView.hidden = YES;

    
    return self;
}

-(void)SNSIcon
{
    NSString * strLoginStateCode =[[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"];
    
    if([strLoginStateCode isEqualToString:@"001"])
    {
        _snsIcon.image = [UIImage imageNamed:@"bandi_sns_ico_3.png"];
        
    }
    else if([strLoginStateCode isEqualToString:@"002"])
    {
        _snsIcon.image = [UIImage imageNamed:@"bandi_sns_ico_2.png"];
    }
    else
    {
        if([SharedAppDelegate.sessionUserId length])
        {
            _snsIcon.image = [UIImage imageNamed:@"bandi_sns_ico_1.png"];
            
        }
        else
        {
            _snsIcon.hidden = YES;
            [_inputIcon setFrame:CGRectMake(4, 4, 268, 26)];
            [_txtField setFrame:CGRectMake(12, 4, 245, 26)];
            return;
        }
    }
    
    _snsIcon.hidden = NO;
    [_inputIcon setFrame:CGRectMake(34, 4, 238, 26)];
    [_txtField setFrame:CGRectMake(42, 3, 225, 30)];

}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}

- (void)reloadBoarding:(NSNotification *)notification
{
    [self loadBandiBoardWeb];
    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"UseBoard"] isEqualToString:@"Y"])
    {
        _noUserList.hidden = NO;
        _tbList.hidden = YES;
    }
    else
    {
        _noUserList.hidden = YES;
        _tbList.hidden = NO;
    }

    
}

-(void)selectMenuIndex:(int)nIndex //1 책읽어주는 라디오 // 2외국어 라디오
{
    if(nIndex == 1)
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(63,56,51)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(158,145,136)];
        self.btMenuReadRadio.selected = YES;
        self.btMenuEngRadio.selected = NO;
//        [player stopAudio];
//        [player loadSound:[streamingData valueForKey:@"streamUrlIos"]];
//        selectFmradio
        [[NSNotificationCenter defaultCenter] postNotificationName:@"selectFmradio" object:nil];
        
    }
    else if(nIndex == 2)
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(158,145,136)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(63,56,51)];
        self.btMenuReadRadio.selected = NO;
        self.btMenuEngRadio.selected = YES;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"selectIradio" object:nil];
    }
}

- (void)loadBandiBoardWeb
{
    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }
    
    if([channelString isEqualToString:@"RADIO"])
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(63,56,51)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(158,145,136)];
        self.btMenuReadRadio.selected = YES;
        self.btMenuEngRadio.selected = NO;
    }
    else
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(158,145,136)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(63,56,51)];
        self.btMenuReadRadio.selected = NO;
        self.btMenuEngRadio.selected = YES;
    }
    
    _titleText.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramTitle"];
    
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=%@&listType=L&pageSize=10", [[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"]];

    NSLog(@"bandi board call url = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    

    if(!jsonString || jsonString == nil)
        return;
    
//    NSLog(@"bandi board end");
    
    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
//    NSLog(@"jsonDic = %@",person);
    
    
    
    if([boardList count])
        [boardList removeAllObjects];
    [boardList addObjectsFromArray:[person valueForKey:@"bandiPosts"]];
    [_tbList reloadData];
    [pullToRefreshManager_ tableViewReloadFinished];
    
    
    
    [self noti];
}

-(void)noti
{
    
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiEventNoticeList?appDsCd=03&fileType=json"];

//    NSLog(@"bandi bnotioard call url = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
//    NSLog(@"load noti end");
    if(!jsonString || jsonString == nil)
        return;

//    SBJsonParser* parser = [ [ SBJsonParser alloc ] init ];
//    NSMutableDictionary* person = [ parser objectWithString: jsonString ];
    
    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
//    NSLog(@"person = %@",person);
    if([[person valueForKey:@"bandiEventLists"] count] < 1)
        return;
    _notiTitle.text =[[[person valueForKey:@"bandiEventLists"] objectAtIndex:0] valueForKey:@"evtTitle"];
    

    if([[notiDic valueForKey:@"evtClsCd"] isEqualToString:@"001"])
    {
        strEventDetailUrl =[NSString stringWithFormat:@"%@",[[[person valueForKey:@"bandiEventLists"] objectAtIndex:0] valueForKey:@"linkUrl"]];
        if([strEventDetailUrl length] > 2)
        {
            _eventDetailBt.hidden = NO;
            if (IS_IPHONE5)
                _detailContentScroll.frame = CGRectMake(5, 66, 306, 400);
            else
                _detailContentScroll.frame = CGRectMake(5, 73, 306, 317);
        }
        else
        {
            _eventDetailBt.hidden = YES;
            if (IS_IPHONE5)
                _detailContentScroll.frame = CGRectMake(5, 66, 306, 450);
            else
                _detailContentScroll.frame = CGRectMake(5, 73, 306, 350);
        }
        

    }
    else
    {
        _eventDetailBt.hidden = YES;
        if (IS_IPHONE5)
            _detailContentScroll.frame = CGRectMake(5, 66, 306, 450);
        else
            _detailContentScroll.frame = CGRectMake(5, 73, 306, 350);
    }


}

-(IBAction)eventDetailUrlOpen:(id)sender
{
    if([strEventDetailUrl length] > 2)
    {
        NSRange strRange;
        strRange = [strEventDetailUrl rangeOfString:@"http://"];
        
        if (strRange.location == NSNotFound) {
            strEventDetailUrl = [NSString stringWithFormat:@"http://%@",strEventDetailUrl];
        }

        NSURL *url = [NSURL URLWithString:strEventDetailUrl];
        [[UIApplication sharedApplication] openURL:url];
//        WebViewController *vc = [[WebViewController alloc] init];
//        [self.window.rootViewController presentViewController:vc animated:YES completion:^{
//            [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
//        }];
        _detailView.hidden = YES;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)writeBulletin:(id)sender {
    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
    
    //[[[cc.view subviews] lastObject] removeFromSuperview];
    
    CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);
    
    SharedAppDelegate.latestMenu = @"반디게시판";
    
    if ([ServerSide autoLogin]) {
        BandiBoardWriteView *myView = [[BandiBoardWriteView alloc] initWithFrame:frameRect];
        [cc.view addSubview:myView];
        [cc.view addSubview:myView];
        [cc.view bringSubviewToFront:myView];
        cc.navigationItem.title = @"반디게시판";
    } else {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginN" object:nil];
        return;
        /*
        NSString * strStrView;
        if (IS_IPHONE5)
            strStrView = @"LogContentController";
        else
            strStrView = @"LogContentController_480";
        
        LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
        
//        [vc setBGoBack:YES];
//        [vc EBSLoginOnly];
        
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
         */

//        LoginView *myView = [[LoginView alloc] initWithFrame:frameRect];
//        [cc.view addSubview:myView];
//        [cc.view bringSubviewToFront:myView];
//        cc.navigationItem.title = @"로그인";
    }
    
}
/*
 [[NSUserDefaults standardUserDefaults] setObject:@"002" forKey:@"SNSLogin"];
 [[NSUserDefaults standardUserDefaults] setObject:FHSTwitterEngine.sharedEngine.authenticatedID forKey:@"SNSId"];
 [[NSUserDefaults standardUserDefaults] setObject:FHSTwitterEngine.sharedEngine.authenticatedUsername forKey:@"SNSName"];
 */

- (IBAction)writePost:(id)sender
{
//    _txtField.text = @"";
    [_txtField resignFirstResponder];
    
    if([_txtField.text length] < 1)
    {
        
    }
    
    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"UseBoard"] isEqualToString:@"Y"])
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"해당 프로그램은 게시판이 운영되지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        return;
    }
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"] length] < 1)
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"해당 프로그램은 게시판이 운영되지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        return;
    }

    
//    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
//    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
    
//    CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);
    
    SharedAppDelegate.latestMenu = @"반디게시판";
    
    if ([ServerSide autoLogin]) {

        [self postBoard];
    
    } else {
        NSString * strLoginStateCode =[[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"];

        if([strLoginStateCode isEqualToString:@"001"] || [strLoginStateCode isEqualToString:@"002"])
        {
              [self postBoard];
        }
        else
        {

            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(loginStateIcon:)
                                                         name:@"LoginStateIcon"
                                                       object:nil];

            
//            LoginView *myView = [[LoginView alloc] initWithFrame:frameRect];
//            [cc.view addSubview:myView];
//            [cc.view bringSubviewToFront:myView];
//            cc.navigationItem.title = @"로그인";
            /*
            NSString * strStrView;
            if (IS_IPHONE5)
                strStrView = @"LogContentController";
            else
                strStrView = @"LogContentController_480";
            
            LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
             */
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginN" object:nil];
            return;

        }
        
        
    }
}

-(void)postBoard
{
    NSString *userId = SharedAppDelegate.sessionUserId;
    NSString *userName = [SharedAppDelegate.sessionUserName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *userText = [_txtField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    if([_txtField.text length] < 1)
//    if ([_txtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {

    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"UseBoard"] isEqualToString:@"Y"])
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"해당 프로그램은 게시판이 운영되지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        return;
    }

    
    NSLog(@"_txtField.text = %@",_txtField.text);
    if([_txtField.text length] < 1){
        //
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"내용을 작성해주세요." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];

        //
        return;
    }
    NSString *userSnsName = [[[NSUserDefaults standardUserDefaults] valueForKey:@"SNSName"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    if(!userId)
        userId = [NSString stringWithFormat:@""];
    if(!userName)
        userName = [NSString stringWithFormat:@""];
    
    //
    
    NSString *strCh;
    if (self.btMenuReadRadio.selected ==1 )
        strCh = [NSString stringWithFormat:@"radio"];
    else
        strCh = [NSString stringWithFormat:@"iradio"];
    
    NSString *strSnsUse;
    NSString *strSnsCode;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"] isEqualToString:@"000"])
    {
        strSnsUse = [NSString stringWithFormat:@"N"];
        strSnsCode = @"";
    }
    else
    {
        strSnsUse = [NSString stringWithFormat:@"Y"];
        strSnsCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"];
    }

    NSString *urlStr =  [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppPost2?progcd=%@&userid=%@&writer=%@&contents=%@&appDsCd=03&broadType=%@&snsDsCd=%@&snsUserId=%@&snsUserNm=%@&snsUseYn=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"],userId,userName,userText,strCh,strSnsCode,[[NSUserDefaults standardUserDefaults] valueForKey:@"SNSId"],userSnsName,strSnsUse];
    //
    NSLog(@"urlStr = %@",urlStr);
    NSError *error;
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    if(!jsonString || jsonString == nil)
        return;

    NSLog(@"jsonString = %@",jsonString);
    if([jsonString isEqualToString:@"0"])
    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                        message:@"등록되었습니다."
//                                                       delegate:nil
//                                              cancelButtonTitle:@"닫기"
//                                              otherButtonTitles:nil ];
//        [alert show];
        [self loadBandiBoardWeb];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"등록에 실패하였습니다."
                                                       delegate:nil
                                              cancelButtonTitle:@"닫기"
                                              otherButtonTitles:nil ];
        [alert show];
    }
    
    _txtField.text = @"";
}

#pragma -table Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [boardList count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([[[boardList objectAtIndex:indexPath.row] valueForKey:@"adminYn"] isEqualToString:@"N"])
    {
        NSString *strContent = [NSString stringWithFormat:@"%@",[[boardList objectAtIndex:indexPath.row] valueForKey:@"contents"] ];

        
        if([strContent isKindOfClass:[NSNull class]])
            return 50.f;
        
            
        float fh;
               fh = [self adjustUILabelHeightSize:strContent widthSize:262.f font:[UIFont systemFontOfSize:13]];
        fh = fh + 20.f;
        
        if (fh < 50.f)
            return 50.f;
        else
            return fh;
    }
    
    return 50.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    boardCell *cell = (boardCell *)[tableView dequeueReusableCellWithIdentifier:@"boardCell"];
    if (cell == nil)
    {
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"boardCell" owner:nil options:nil];
        cell = (boardCell *)[arr objectAtIndex:0];
    }
    
    NSDictionary *Dic = [boardList objectAtIndex:indexPath.row];
    
    [cell cellData:Dic];
    float fh;
    
    NSString *strContent = [NSString stringWithFormat:@"%@",[[boardList objectAtIndex:indexPath.row] valueForKey:@"contents"] ];
    
    
    if([strContent isKindOfClass:[NSNull class]])
        strContent = @"aaa";
    
    fh = [self adjustUILabelHeightSize:strContent widthSize:262.f font:[UIFont systemFontOfSize:13]];
    
    if (fh < 30.f)
        fh = 30;
    cell.lbContent.frame = CGRectMake(cell.lbContent.frame.origin.x, cell.lbContent.frame.origin.y,
                                      cell.lbContent.frame.size.width, fh);
    
    if(indexPath.row %2== 1)
        [cell setBackgroundColor:RgbColor(252,248,238)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
  

}


-(float)adjustUILabelHeightSize:(NSString*)textlabel widthSize:(float)width font:(UIFont*)ft{
    
    CGSize maximumHeightSize = CGSizeMake(width, 9999);
    
    CGSize extendLabelSize = [textlabel sizeWithFont:ft
                                   constrainedToSize:maximumHeightSize
                                       lineBreakMode:NSLineBreakByCharWrapping];
    
//    NSLog(@"extendLabelSize.height = %f",extendLabelSize.height);
    return extendLabelSize.height;
}

- (void)setNotiDic:(NSDictionary *)dic
{
    [notiDic removeAllObjects];
    [notiDic  addEntriesFromDictionary:dic];
}
- (IBAction)openNoti:(id)sender
{
    NSLog(@"noti");
    _notiTitle.text =[notiDic valueForKey:@"evtTitle"];
    NSLog(@"_notiTitle.text = %@",_notiTitle.text);
    
    //_detailIcon.image = [UIImage imageNamed:@"ico_notice.png"];
    _detailTitle.text = [notiDic valueForKey:@"evtTitle"];
    
    float fh;
    fh =[self adjustUILabelHeightSize:[NSString stringWithFormat:@"%@",[notiDic valueForKey:@"evtCntn"]] widthSize:_detailNotiContent.frame.size.width font:[UIFont systemFontOfSize:14]];
    
    [_detailContentScroll setContentSize:CGSizeMake(306, fh+10)];
    _detailNotiContent.text = [NSString stringWithFormat:@"%@",[notiDic valueForKey:@"evtCntn"]];

    _detailNotiContent.frame = CGRectMake(_detailNotiContent.frame.origin.x,
                                          _detailNotiContent.frame.origin.y,
                                          305,
                                          fh+10);
    _detailNotiContent.hidden = NO;
    

    ////

    if([[notiDic valueForKey:@"evtClsCd"] isEqualToString:@"001"])
    {
        NSString * strUrl =[NSString stringWithFormat:@"%@",[notiDic valueForKey:@"linkUrl"]];
        if([strUrl length] > 2)
        {
            _eventDetailBt.hidden = NO;
            if (IS_IPHONE5)
                _detailContentScroll.frame = CGRectMake(5, 66, 306, 400);
            else
                _detailContentScroll.frame = CGRectMake(5, 73, 306, 317);
        }
        else
        {
            _eventDetailBt.hidden = YES;
            if (IS_IPHONE5)
                _detailContentScroll.frame = CGRectMake(5, 66, 306, 450);
            else
                _detailContentScroll.frame = CGRectMake(5, 73, 306, 350);
        }

    }
    else
    {
        _eventDetailBt.hidden = YES;
        if (IS_IPHONE5)
            _detailContentScroll.frame = CGRectMake(5, 66, 306, 450);
        else
            _detailContentScroll.frame = CGRectMake(5, 73, 306, 350);
    }

    ////
    
    _detailView.hidden = NO;
    
}

- (IBAction)tabPlayFmRadio:(id)sender
{
    [self selectMenuIndex:1];
}
- (IBAction)tabPlayEndRadio:(id)sender
{
    [self selectMenuIndex:2];
}

- (void)playIPradio:(NSNotification *)notification
{
    
    //    [self selectMenuIndex:2];
//    nSelectedMenu = 2;
    [self loadBandiBoardWeb];
}

- (void)playFmPradio:(NSNotification *)notification
{
    [self loadBandiBoardWeb];
    //    [self selectMenuIndex:1];
//    nSelectedMenu = 1;
//    
//    [self.btMenuReadRadio setBackgroundColor:RgbColor(63,56,51)];
//    [self.btMenuEngRadio setBackgroundColor:RgbColor(158,145,136)];
//    self.btMenuReadRadio.selected = YES;
//    self.btMenuEngRadio.selected = NO;

}


- (void)keyboardWillAnimate:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    keyboardBounds = [self convertRect:keyboardBounds toView:nil];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    if([notification name] == UIKeyboardWillShowNotification)
    {
        if(IS_IPHONE5)
        {
            if(keyboardBounds.size.height > 250)
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, 0, _inputView.frame.size.width, 44)];
            else
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, 35, _inputView.frame.size.width, 44)];
        }
        else
        {
            if(keyboardBounds.size.height > 250)
            {
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, 0, _inputView.frame.size.width, 44)];
                 [self setFrame:CGRectMake(self.frame.origin.x, 125, self.frame.size.width, self.frame.size.height)];
            }
            else
            {
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, 0, _inputView.frame.size.width, 44)];
                [self setFrame:CGRectMake(self.frame.origin.x, 167, self.frame.size.width, self.frame.size.height)];

            }
          
        }
    }
    else if([notification name] == UIKeyboardWillHideNotification)
    {
        if(IS_IPHONE5)
        {
            int posH = self.frame.size.height-_notiView.frame.size.height - _inputView.frame.size.height + 8;
            _inputView.frame = CGRectMake(0, _notiView.frame.size.height + posH, 320, 34);

        }
        else
        {
            [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, 86, _inputView.frame.size.width, 34)];
            [self setFrame:CGRectMake(self.frame.origin.x, 191, self.frame.size.width, self.frame.size.height)];
        }
       
        _notiView.hidden = NO;
    }
    [UIView commitAnimations];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{//키보드 엔터 delegate
    [self writePost:nil];
    return YES;
}

- (void)loginStateIcon:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LoginStateIcon" object:nil];
    [self SNSIcon];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    //
//    [_txtField becomeFirstResponder];
//

    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"UseBoard"] isEqualToString:@"Y"])
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"해당 프로그램은 게시판이 운영되지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        return NO;
    }
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"] length] < 1)
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"해당 프로그램은 게시판이 운영되지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        return NO;
    }

    if (SharedAppDelegate.sessionUserName && SharedAppDelegate.sessionUserId) {
        return YES;
    }
    
    NSString * strLoginStateCode =[[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"];
    if([strLoginStateCode isEqualToString:@"001"] || [strLoginStateCode isEqualToString:@"002"])
    {
        return YES;
    }
    
    
//    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
//    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
    
//    CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginStateIcon:)
                                                 name:@"LoginStateIcon"
                                               object:nil];
    
//    LoginView *myView = [[LoginView alloc] initWithFrame:frameRect];
//    [cc.view addSubview:myView];
//    [cc.view bringSubviewToFront:myView];
//    cc.navigationItem.title = @"로그인";
//
//    
//    NSString * strStrView;
//    if (IS_IPHONE5)
//        strStrView = @"LogContentController";
//    else
//        strStrView = @"LogContentController_480";
//    
//    LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
//    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginN" object:nil];

    
    return NO;
    
}

-(void)hideKeypad
{
    [_txtField resignFirstResponder];
}
- (IBAction)closeNotiDetail:(id)sender
{
    _detailView.hidden = YES;
}
- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager {
    
    [self performSelector:@selector(loadTable) withObject:nil afterDelay:1.0f];
}

- (void)loadTable {
    

    if([boardList count] < 1)
        return;
    
    NSLog(@"ld");
    //http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=10009938&listType=N&pageSize=5&seq=86
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=%@&listType=N&pageSize=10&seq=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"],[[boardList objectAtIndex:[boardList count]-1] valueForKey:@"seq"]];
    

    NSLog(@"bandi bnotioard small call url = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    if(!jsonString || jsonString == nil)
        return;

    NSLog(@"bandi load end");
    
    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
    NSLog(@"jsonString = %@",person);
    
    [boardList addObjectsFromArray:[person valueForKey:@"bandiPosts"]];
    [_tbList reloadData];
    
    [pullToRefreshManager_ tableViewReloadFinished];
}
#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient

/**
 * This is the same delegate method as UIScrollView but required in MNMBottomPullToRefreshManagerClient protocol
 * to warn about its implementation. Here you have to call [MNMBottomPullToRefreshManager tableViewScrolled]
 *
 * Tells the delegate when the user scrolls the content view within the receiver.
 *
 * @param scrollView: The scroll-view object in which the scrolling occurred.
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [pullToRefreshManager_ tableViewScrolled];
}

/**
 * This is the same delegate method as UIScrollView but required in MNMBottomPullToRefreshClient protocol
 * to warn about its implementation. Here you have to call [MNMBottomPullToRefreshManager tableViewReleased]
 *
 * Tells the delegate when dragging ended in the scroll view.
 *
 * @param scrollView: The scroll-view object that finished scrolling the content view.
 * @param decelerate: YES if the scrolling movement will continue, but decelerate, after a touch-up gesture during a dragging operation.
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [pullToRefreshManager_ tableViewReleased];
}

-(void)bandiBoardShowBig
{
    _wrapperView.frame = CGRectMake(_wrapperView.frame.origin.x, _wrapperView.frame.origin.y, _wrapperView.frame.size.width,self.frame.size.height);
   
    int posH = self.frame.size.height-_notiView.frame.size.height - _inputView.frame.size.height;

   
    _notiView.frame =CGRectMake(0, 0, 320, 25);

    _tbList.frame =CGRectMake(0, _notiView.frame.size.height, 320, posH);

    _inputView.frame = CGRectMake(0, _notiView.frame.size.height + posH, 320, 34);
//    self.frame = CGRectMake(0, self.frame.origin.y, 320, 166);
    
}


@end
