//
//  IntoAniViewController.m
//  Bandi2
//
//  Created by Hyeoung seok Yoon on 2015. 1. 8..
//  Copyright (c) 2015년 EBS. All rights reserved.
//

#import "IntoAniViewController.h"
#import "UIImageView+AnimationCompletion.h"
#import "Bandi2AppDelegate.h"

@interface IntoAniViewController ()

@end

@implementation IntoAniViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"intro viewDidLoad");
    CGRect screenRect = [[UIScreen mainScreen] bounds];

    self.view.frame = screenRect;
    
    
    
    
    /////
    UIImageView *imageView2 = [[UIImageView alloc]initWithFrame:screenRect];
    imageView2.image = [[UIImage alloc]init];
    imageView2.image = [UIImage imageNamed:@"loding_bg.png"];
    
    [self.view addSubview:imageView2];
    
     NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int  n = 0; n < 41; n ++) {

        NSString * strName = [NSString stringWithFormat:@"intro00%02d.png",n+1];
        NSLog(@"nn(%d) %@",n, strName);
        [images addObject:[UIImage imageNamed:strName]];
        
    }
    int nPos;
    if(IS_IPHONE5)nPos =120;
    else nPos =90;
    
    UIImageView *animationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(60, nPos, 200, 250)];
    [self.view addSubview:animationImageView];
    animationImageView.animationImages = images;
    animationImageView.animationDuration = 2.0f;
    animationImageView.animationRepeatCount = 1;
    animationImageView.image = [UIImage imageNamed:@"intro0041.png"];
    

     [animationImageView startAnimatingWithCompletionBlock:^(BOOL success){
         NSLog(@"end");
//         animationImageView.hidden = NO;
//         animationImageView.image = [UIImage imageNamed:@"intro0041.png"];
//         [self.view addSubview:animationImageView];

         [self performSelector:@selector(loadNext) withObject:nil afterDelay:0.1f];
//         Bandi2AppDelegate *appDelegate = (Bandi2AppDelegate *)[[UIApplication sharedApplication] delegate];
//         [appDelegate endAnimationIntro];
     }];

}
-(void)loadNext
{
    NSLog(@"loadNext");
    Bandi2AppDelegate *appDelegate = (Bandi2AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"loadNext1");
    [appDelegate endAnimationIntro];
    NSLog(@"loadNext2");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
