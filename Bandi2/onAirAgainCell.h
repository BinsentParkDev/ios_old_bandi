//
//  ProgramListCell.h
//  EBSTVApp
//
//  Created by Hyeoung seok Yoon on 2013. 11. 10..
//  Copyright (c) 2013년 Hyeoung seok Yoon. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol onAirAgainCellDelegate;

@interface onAirAgainCell : UITableViewCell
{
    IBOutlet UIButton       *btPlay;
    IBOutlet UILabel        *lbText;

    IBOutlet UIImageView    *ivIcon;
    BOOL bPlayState;
    int listIndex;
    
    BOOL bTouching;
    float fPer;
    
    //id<onAirAgainCellDelegate> cellDelegate;
    
}
@property (nonatomic, assign) int listIndex;
@property (nonatomic, assign) id<onAirAgainCellDelegate> cellDelegate;
@property (nonatomic, strong) UIButton      *btPlay;
@property (nonatomic, strong) IBOutlet UIButton      *btStop;
@property (nonatomic, strong) UILabel       *lbText;
@property (nonatomic, strong) IBOutlet UILabel       *testlb;
@property (nonatomic, strong) IBOutlet UILabel       *lbDate;

@property (nonatomic, strong) IBOutlet UISlider       *playerSlider;



-(IBAction)btActionPlay:(id)sender;
-(void)setSliderValue:(float)per;

@end

@protocol onAirAgainCellDelegate <NSObject>

//-(void)selectCheck:(int)indx btState:(BOOL)state;
-(void)selectPlay:(int)indx;
-(void)selectStop:(int)indx;
-(void)selectValue:(float)per index:(int)indx;
-(void)progressing:(BOOL)bSet;
@end
