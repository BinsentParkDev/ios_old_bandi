//
//  LoginLandscapeView.h
//  Bandi2
//
//  Created by admin on 2014. 2. 4..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginLandscapeView : UIView
{
    NSUserDefaults *userDefaults ;
}

@property (strong, nonatomic) IBOutlet LoginLandscapeView *wrapperView;
@property (strong, nonatomic) IBOutlet UIView *loginFormView;
@property (strong, nonatomic) IBOutlet UIView *loginOptionView;

@property (strong, nonatomic) IBOutlet UITextField *userId;
@property (strong, nonatomic) IBOutlet UITextField *passwd;
- (IBAction)doLogin:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *saveIdBtn;
- (IBAction)saveId:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *autoLoginBtn;
- (IBAction)autoLogin:(id)sender;

- (IBAction)closeLoginView:(id)sender;
@end
