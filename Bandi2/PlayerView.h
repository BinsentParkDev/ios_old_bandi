//
//  PlayerView.h
//  Bandi2
//
//  Created by admin on 2013. 12. 20..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>


@interface PlayerView : UIView <UIAlertViewDelegate>
{
    NSUserDefaults *userDefaults;
    NSTimer *wifiCheckTimer;
    NSTimer *wifiCheckAfterTimer;
    NSTimer *wifiErrorTimer;
    
    NSTimer *playerCheckTimer;
    BOOL wasPlaying;
    
    NSString *strUrltw;
    NSString *strUrlHm;
    NSString *strUrlfb;
    NSString *strUrlkk;
    
    BOOL vodPlaying;
    
    NSMutableArray * arrSnsBt;
    
    UISlider *volumeSlider;
    
    UIButton * twitterBtn;
    UIButton * faceBookBtn;
    UIButton * kakaoBtn;
    UIButton * homeBtn;
    
    BOOL bNodata;
    BOOL bNoWifi;
    BOOL bNoWifiError;
    BOOL bAutoStop;
    
}
- (void)onUnload;
- (void)stopAudio;

@property (strong, nonatomic) MPMoviePlayerController *audioPlayer;


//@property (strong, nonatomic) IBOutlet UISlider *volumeSlider;
@property (strong, nonatomic) IBOutlet UISlider *volumeSlider2;
- (IBAction)controlVolume:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *playBtn;
@property (strong, nonatomic) IBOutlet UIButton *playBtn2;
- (IBAction)playAudio:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *muteBtn;
@property (strong, nonatomic) IBOutlet UIButton *muteBtn2;
- (IBAction)muteSound:(id)sender;

- (IBAction)bandiBoard:(id)sender;

- (void)loadSound:(NSString *)strUrl;
- (void)vodPlay:(BOOL)bPlay;
- (void)audioInfo:(NSDictionary *)dic;

//@property (strong, nonatomic) IBOutlet UIButton *twitterBtn;
//@property (strong, nonatomic) IBOutlet UIButton *faceBookBtn;
//@property (strong, nonatomic) IBOutlet UIButton *kakaoBtn;
//@property (strong, nonatomic) IBOutlet UIButton *homeBtn;
@property (strong, nonatomic) IBOutlet UIView *onAirPlayer;
@property (strong, nonatomic) IBOutlet UIView *gPlayer;

-(void)facebookEnable:(BOOL)bEnable;
-(void)twitterEnable:(BOOL)bEnable;
-(void)kakaoEnable:(BOOL)bEnable;
-(void)homeEnable:(BOOL)bEnable;

-(void)urlFacebook:(NSString *)strUrl;
-(void)urlTwitter:(NSString *)strUrl;
-(void)urlKakao:(NSString *)strUrl;
-(void)urlHome:(NSString *)strUrl;

-(IBAction)actionFb:(id)sender;
-(IBAction)actionTw:(id)sender;
-(IBAction)actionKk:(id)sender;
-(IBAction)actionHp:(id)sender;

-(void)btSnsSort;

@end
