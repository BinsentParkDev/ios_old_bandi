//
//  BandiBoardWriteLandscapeView.m
//  Bandi2
//
//  Created by admin on 2014. 2. 5..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "BandiBoardWriteLandscapeView.h"
#import "TimeTable.h"
#import "Dialogs.h"

@implementation BandiBoardWriteLandscapeView
{
    NSDictionary *ttDic;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [[NSBundle mainBundle] loadNibNamed:@"BandiBoardWriteLandscapeView" owner:self options:nil];
        ttDic = [[[TimeTable alloc] init] get];
        
        // 백그라운드 이미지 설정
        UIGraphicsBeginImageContext(_wrapperView.frame.size);
        [[UIImage imageNamed:@"bg_base.png"] drawInRect:_wrapperView.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        _wrapperView.backgroundColor = [UIColor colorWithPatternImage:image];
        
        // textfield round border
        UIColor *borderColor = [[UIColor alloc] initWithRed:(146/255.0) green:(162/255.0) blue:(174/255.0) alpha:1.0];
        [_textField.layer setCornerRadius:5.0f];
        [_textField.layer setBorderWidth:1.0f];
        [_textField.layer setBorderColor:[borderColor CGColor]];
        [_textField.layer setMasksToBounds:YES];
        
        int screenHeight = [SharedAppDelegate.screenHeight intValue];
        
        [_wrapperView setFrame:CGRectMake(0, 0, screenHeight, 320)];
        
        [self addSubview:_wrapperView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)postBulletin:(id)sender {
    NSString *userId = SharedAppDelegate.sessionUserId;
    NSString *userName = [SharedAppDelegate.sessionUserName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *userText = [_textField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if ([_textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        [Dialogs alertView:@"내용을 작성해주세요."];
        return;
    }
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppPost?progcd=%@&userid=%@&writer=%@&contents=%@&addDsCd=03", ttDic[@"progcd"], userId, userName, userText];
    NSLog(@"%@", urlStr);
    NSURL *url = [NSURL URLWithString:urlStr];
    NSError *error;
    
    NSString *resultText = [NSString stringWithContentsOfURL:url
                                                    encoding:NSUTF8StringEncoding
                                                       error:&error];
    if(!resultText || resultText == nil)
        return;

    resultText = [resultText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([resultText isEqualToString:@"0"]) {
        [Dialogs alertView:@"게시물이 등록되었습니다."];
        [self removeFromSuperview];
    } else {
        [Dialogs alertView:@"게시물 등록에 문제가 있습니다."];
    }
}

- (IBAction)cancel:(id)sender {
    [self removeFromSuperview];
}
@end
