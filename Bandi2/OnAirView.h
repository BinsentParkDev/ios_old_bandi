//
//  OnAirView.h
//  Bandi2
//
//  Created by admin on 2014. 1. 13..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "onAirAgainCell.h"
#import "PlayerView.h"
#import "BandiBoardSmallView.h"
#import "ebsPlayer.h"
#import "MNMBottomPullToRefreshManager.h"

@interface OnAirView : UIView <UITableViewDataSource,UITableViewDelegate,onAirAgainCellDelegate,UIAlertViewDelegate,ebsPlayerDelegate,MNMBottomPullToRefreshManagerClient>
{
    NSTimer *progImgTimer;
    NSTimer *rollingTextTimer;
    NSTimer *againSongTimer;

    NSTimer *videoControllerTimer;
    
    int nSelectedTabMenu;
    int nSelectedMenu;    
    BOOL bShowTabMenu;
    float againSongPer;
    BOOL bFineCh;
    BOOL bLoadFail;
    
    BOOL bInitPlayer;
    BOOL bHaveEventUrl;
    NSMutableArray *againList;
    NSMutableArray *bookInfoList;
    
    NSMutableDictionary *boardingDate;
    NSMutableDictionary *programDate;
    NSMutableDictionary *streamingData;
    NSMutableDictionary *notiData;
    NSMutableArray *boardTextList;
    PlayerView *player;
    AVPlayer *songPlayer;
    NSTimer *timer;
    
    
    ebsPlayer *ep;
    
    
    int streamIndex;
    BOOL streamPlaying;
    BOOL playingAni;
    int selectBookCol;
    int againListPage;
    
    BOOL rollingIdxInit;

    

    BOOL againPorgressing;
    BOOL bPlayerShowOnairState;
    
    BOOL bLoadAdd;

    int bPgAgainTotal;
    NSString * strEventUrl;
    NSString * strOldPd;
    
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
    UIScrollView * mainScr;
    
    
    int picH;
    
    BOOL bShowNoti;
    BOOL bHaveEvt;
    BOOL bInitEnd;//최초에 로그가 2 번 호출되서 1 번 스킵
    
    NSString *strEventDetailUrl;
    
}
- (void)onUnload;
- (void)setProgramImageCaller;
- (void)setProgramImage;
- (void)setRollingTextCaller;
- (void)setRollingText;
- (void)setPlayerFrame;



//void UIImageFromURL( NSURL * URL, void (^imageBlock)(UIImage * image), void (^errorBlock)(void) );
//@property (strong, nonatomic) IBOutlet PlayerView *player;
@property (strong, nonatomic) IBOutlet OnAirView *wrapperView;
@property (strong, nonatomic) IBOutlet UIImageView *programImageView;

@property (strong, nonatomic) IBOutlet UIView *boardView;

@property (strong, nonatomic) IBOutlet UIView *WcView;

@property (strong, nonatomic) IBOutlet UIButton *aodBtn;
@property (strong, nonatomic) IBOutlet UIButton *vRadioBtn;
@property (strong, nonatomic) IBOutlet UIButton *twitterBtn;
@property (strong, nonatomic) IBOutlet UIButton *facebookBtn;

@property (strong, nonatomic) IBOutlet UIImageView *programImage;

@property (strong, nonatomic) IBOutlet UIView *radioToolView;
@property (strong, nonatomic) IBOutlet UIView *radioTabView;
@property (strong, nonatomic) IBOutlet UIView *radioSubTabView;

///
@property (strong, nonatomic) IBOutlet UIButton *btMenuReadRadio;
@property (strong, nonatomic) IBOutlet UIButton *btMenuEngRadio;

@property (strong, nonatomic) IBOutlet UIButton *btTabAgain;
@property (strong, nonatomic) IBOutlet UIButton *btTabBookInfo;
@property (strong, nonatomic) IBOutlet UIButton *btTabBroadInfo;
@property (strong, nonatomic) IBOutlet UIButton *btTabSongList;

@property (strong, nonatomic) IBOutlet UITableView *tbAgain;
@property (strong, nonatomic) IBOutlet UITableView *tbBook;

@property (strong, nonatomic) IBOutlet UIImageView *bookImg;
@property (strong, nonatomic) IBOutlet UIView *bookInfoConView;
@property (strong, nonatomic) IBOutlet UILabel *bookTitle;
@property (strong, nonatomic) IBOutlet UILabel *bookContent;

@property (nonatomic, strong) IBOutlet UIScrollView  * scrListView;
@property (nonatomic, strong) IBOutlet UIPageControl * pgControl;

@property (strong, nonatomic) IBOutlet UIView *boadingInfoView;
@property (strong, nonatomic) IBOutlet UILabel *boadingInfoTitle;
@property (strong, nonatomic) IBOutlet UIScrollView *boadingInfoScroll;
@property (strong, nonatomic) IBOutlet UIWebView *boadingWebView;

@property (strong, nonatomic) IBOutlet UIView *songInfoView;
@property (strong, nonatomic) IBOutlet UILabel *SongInfoTitle;
@property (strong, nonatomic) IBOutlet UIScrollView *SongInfoScroll;

@property (strong, nonatomic) IBOutlet UIButton *btcheckAdd;

@property (strong, nonatomic) IBOutlet UILabel *seeRadioText;

@property (strong, nonatomic) IBOutlet BandiBoardSmallView *bandiBoardView;

@property (strong, nonatomic) IBOutlet UIButton *btFullScreen;

@property (strong, nonatomic) IBOutlet UIView *fullScrView;
@property (strong, nonatomic) IBOutlet UIButton *smallScrBt;

@property (strong, nonatomic) IBOutlet UIView *againPlayerInfo;
@property (strong, nonatomic) IBOutlet UILabel *noAgainList;
@property (strong, nonatomic) IBOutlet UILabel *noBookList;

@property (strong, nonatomic) IBOutlet UIButton *keypadHideBt;

@property (strong, nonatomic) IBOutlet UIButton * eventDetailBt;

//@property (strong, nonatomic) IBOutlet UITextView *txtViewBoardingInfo2;
//@property (strong, nonatomic) IBOutlet UITextView *txtViewSongInfo;

///

-(IBAction)hideKeyPad:(id)sender;

- (IBAction)launchAod:(id)sender;
- (IBAction)watchRadio:(id)sender;
- (IBAction)launchTwitter:(id)sender;
- (IBAction)launchFacebook:(id)sender;

- (IBAction)addCheckList:(id)sender;

- (IBAction)bookUrl:(id)sender;

- (IBAction)fullScreen:(id)sender;
-(void)reloadRadioControll;


@property (strong, nonatomic) IBOutlet UIButton *writeBulletinBtn;

- (IBAction)writeBulletin:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *rollingTextView;
@property (strong, nonatomic) IBOutlet UIView *rollingTextBorderView;

@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) MPMoviePlayerViewController *moviePlayerView;



@property (strong, nonatomic) IBOutlet UILabel * detailTitle;
@property (strong, nonatomic) IBOutlet UIScrollView * detailContentScroll;
@property (strong, nonatomic) IBOutlet UIImageView * detailEventImage;
@property (strong, nonatomic) IBOutlet UIView * notiView;
@property (strong, nonatomic) IBOutlet UIButton * btEventDetail;
@property (strong, nonatomic) IBOutlet UIWebView *songsWebView;
@property (strong, nonatomic) NSString *strCheckId;
- (IBAction)closeNotiDetail:(id)sender;

-(void)playerViewSetting:(PlayerView *)uv;
-(void)playerViewSettingNonInit:(PlayerView *)uv;

/**
 * 선곡표 웹뷰 로딩
 */
-(void)loadSongTable;

/**
 * 방송내용 웹뷰 로딩
 */
-(void)loadBroadcastingContents;


-(IBAction)hideVideoController:(id)sender;
-(IBAction)eventDetailUrlOpen:(id)sender;
@end
