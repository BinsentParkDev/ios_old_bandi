//
//  BandiBoardLandscapeView.m
//  Bandi2
//
//  Created by admin on 2014. 2. 4..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "BandiBoardLandscapeView.h"
#import "TimeTable.h"
#import "ServerSide.h"

#define TAG @"BandiBoardLandscape"


@implementation BandiBoardLandscapeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [[[NSBundle mainBundle] loadNibNamed:@"BandiBoardLandscapeView" owner:self options:nil] lastObject];
        
        // adjust _webView size & position
        int screenWidth = [SharedAppDelegate.screenHeight intValue];
        CGRect newFrame = CGRectMake(0, 0, screenWidth, 320);
        
        [_wrapperView setFrame:newFrame];
        [_webView setFrame:_wrapperView.frame];
        newFrame = _iconBgView.frame;
        newFrame.origin.x = screenWidth - _iconBgView.frame.size.width - 10;
        //[_iconBgView setFrame:newFrame];
        
        
        //[self bringSubviewToFront:_reloadWebBtn];
        //[self bringSubviewToFront:_exitBoardBtn];
        
        NSDictionary *ttDic = [[[TimeTable alloc] init] get];
        //_programLabel.text = ttDic[@"progname"];
        
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppList?progcd=%@", ttDic[@"progcd"]];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [_webView loadRequest:request];
        
        
        [self addSubview:_wrapperView];
        
    }
    
    [ServerSide saveEventLog:TAG];
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)reloadWeb:(id)sender {
    NSDictionary *ttDic = [[[TimeTable alloc] init] get];
    
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppList?progcd=%@", ttDic[@"progcd"]];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
}

- (IBAction)exitBoard:(id)sender {
    NSLog(@"exitBoard clicked.");
    [self removeFromSuperview];
}
@end
