//
//  ProgramListCell.h
//  EBSTVApp
//
//  Created by Hyeoung seok Yoon on 2013. 11. 10..
//  Copyright (c) 2013년 Hyeoung seok Yoon. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol onCheckListCellDelegate;

@interface checkListCell : UITableViewCell
{
//    IBOutlet UIButton       *btPlay;
//    IBOutlet UILabel        *lbText;
//    IBOutlet UIImageView    *ivIcon;
    

//    IBOutlet UILabel        *lbTitle;
//    IBOutlet UILabel        *lbContent;
//    IBOutlet UILabel        *lbAdmin;
//    IBOutlet UIImageView    *ivAdmin;
//    
  
//    int listIndex;
    
}
@property (nonatomic, assign) int listIndex;
@property (nonatomic, strong) IBOutlet UILabel        *lbTitle;
@property (nonatomic, strong) IBOutlet UILabel        *lbContent;
@property (nonatomic, strong) IBOutlet UIButton       *btState;
@property (nonatomic, assign) id<onCheckListCellDelegate> cellDelegate;


-(void)cellData:(NSDictionary *)dic;
-(IBAction)btStateSel:(id)sender;
@end


@protocol onCheckListCellDelegate <NSObject>

//-(void)selectCheck:(int)indx btState:(BOOL)state;
-(void)selectCheck:(int)indx state:(BOOL)select;

@end
