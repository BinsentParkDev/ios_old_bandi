//
//  Bandi2AppDelegate.h
//  Bandi2
//
//  Created by admin on 13. 12. 17..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Bandi2ViewController;

@class IIViewDeckController;

@interface Bandi2AppDelegate : UIResponder <UIApplicationDelegate>
{
    IIViewDeckController* deckController;
    BOOL bPlayingIntro;
    NSString *urlSelectedChannel;
    
}
-(void)endAnimationIntro;
@property (strong, nonatomic) UIWindow *window;

//@property (strong, nonatomic) Bandi2ViewController *viewController;
@property (strong, nonatomic) UIViewController *centerController;
@property (strong, nonatomic) UIViewController *leftController;

- (IIViewDeckController*)generateControllerStack;

@property (strong, nonatomic) NSString *sessionCookie;
@property (strong, nonatomic) NSString *sessionUserId;
@property (strong, nonatomic) NSString *sessionUserName;
@property (strong, nonatomic) NSString *sessionUserEmail;
@property (strong, nonatomic) NSString *sessionPasswd;
@property (strong, nonatomic) NSDictionary *badMember;
@property (strong, nonatomic) NSNumber *screenHeight;
@property (nonatomic, strong) NSNumber *bodyHeight;

@property (strong, nonatomic) NSTimer *quitTimer;
@property (assign, nonatomic) UISlider *quitTimerSlider;

@property (strong, nonatomic) NSString *latestMenu;

@property (assign, nonatomic) BOOL isOpenTt;
@property (assign, nonatomic) BOOL isFromLogin;


/**
 * 로그 기록용 접속 아이디
 */
@property (strong, nonatomic) NSString *logSno;

/**
 * 서버에서 받아온 현재 앱 버전 정보
 */
@property (strong, nonatomic) NSString *serverAppVersion;

/**
 * 앱의 백그라운드 상태 여부
 */
@property (assign, nonatomic) BOOL isBackground;

/*
 * URL에서 호출 시 선택된 채널
 */
@property (retain, nonatomic) NSString *urlSelectedChannel;

@end
