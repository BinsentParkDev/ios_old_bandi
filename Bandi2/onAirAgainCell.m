//
//  ProgramListCell.m
//  EBSTVApp
//
//  Created by Hyeoung seok Yoon on 2013. 11. 10..
//  Copyright (c) 2013년 Hyeoung seok Yoon. All rights reserved.
//


#import "onAirAgainCell.h"

@interface onAirAgainCell ()

@end
 
@implementation onAirAgainCell

@synthesize lbText;
@synthesize listIndex;
@synthesize cellDelegate;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated]; 
}
-(id)init
{
    bTouching = NO;
    [_playerSlider setValue:0];
    _playerSlider.hidden = YES;
    return self;
}
-(IBAction)btActionPlay:(id)sender
{
    
    [cellDelegate selectPlay:listIndex];
}

-(IBAction)btActionStop:(id)sender
{
    [cellDelegate selectStop:listIndex];
}

-(IBAction)sliderTouchIn:(id)sender
{
    bTouching = YES;
    
    [cellDelegate progressing:bTouching];
}
-(IBAction)sliderTouchOut:(id)sender
{
    bTouching = NO;
    [cellDelegate progressing:bTouching];
}
-(IBAction)sliderTouchUp:(id)sender
{
    
    [cellDelegate selectValue:_playerSlider.value index:listIndex];
    //bTouching = NO;
    bTouching = NO;
    [cellDelegate progressing:bTouching];
    
}
-(void)setSliderValue:(float)per
{
    if(_playerSlider.isHidden)
        _playerSlider.hidden = NO;
    fPer = per;
    if(!bTouching)
        [_playerSlider setValue:fPer animated:YES];
}



@end
