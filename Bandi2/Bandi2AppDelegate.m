//
//  Bandi2AppDelegate.m
//  Bandi2
//
//  Created by admin on 13. 12. 17..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "Bandi2AppDelegate.h"

#import "Bandi2ViewController.h"
#import "IIViewDeckController.h"
#import "LeftViewController.h"
#import "PlayerView.h"
#import "ServerSide.h"
#import "Dialogs.h"
#import "Util.h"

#import <FacebookSDK/FacebookSDK.h>
#import <KakaoOpenSDK/KakaoOpenSDK.h>
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "IntoAniViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@implementation Bandi2AppDelegate

@synthesize window = _window;
@synthesize centerController = _viewController;
@synthesize leftController = _leftController;
@synthesize urlSelectedChannel;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //[NSThread sleepForTimeInterval:1];
    
    _isOpenTt = NO;
    bPlayingIntro = NO;
    
    [FBLoginView class];
    [FBProfilePictureView class];
    
//**  facebook sdk 3.22 -> deprecated **
//
//    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
//        
//        // If there's one, just open the session silently, without showing the user the login UI
//        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
//                                           allowLoginUI:NO
//                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//                                          // Handler for session state changes
//                                          // This method will be called EACH time the session state changes,
//                                          // also for intermediate states and NOT just when the session open
//                                          NSLog(@"FBSession load~~ %lu",(unsigned long)state);
////                                          [self sessionStateChanged:session state:state error:error];
//                                      }];
//    }

    //** facebook sdk 4.* **
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    //** facebook sdk 4.* **/

    //
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        
        UIUserNotificationType types = UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *mySettings =
        [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
        
    } else {
        UIRemoteNotificationType myTypes =  UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    deckController = [self generateControllerStack];
    self.leftController = deckController.leftController;
    self.centerController = deckController.centerController;
      [self.window makeKeyAndVisible];
    NSLog(@"intro 1");
    bPlayingIntro = YES;
    IntoAniViewController * ani = [[IntoAniViewController alloc]initWithNibName:@"IntoAniViewController" bundle:nil];
    NSLog(@"intro 2");
    self.window.rootViewController = ani;
    NSLog(@"intro 3");
    

    NSString *playBg = (NSString *)[[NSUserDefaults standardUserDefaults] valueForKey:@"PLAY_BG"];
    if(playBg == nil)
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"PLAY_BG"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    //** Google Analytics **
    // Set default tracker without 'GoogleService-Info.plist'
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-60905597-3"];
    [[GAI sharedInstance] setDefaultTracker:tracker];
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release

    return YES;
////    IIViewDeckController* deckController = [self generateControllerStack];
////    self.leftController = deckController.leftController;
////    self.centerController = deckController.centerController;
////    self.window.rootViewController = deckController;
////    
////    
////    [self.window makeKeyAndVisible];
//    
//    
//    [ServerSide saveConnectLog];
//    
//    SharedAppDelegate.isBackground = NO;
//    [self AutoStopCheck];
    
//    return YES;
}


- (IIViewDeckController*)generateControllerStack {
    LeftViewController* leftController = [[LeftViewController alloc] initWithNibName:@"LeftViewController" bundle:nil];
    
    UIViewController *centerController = [[Bandi2ViewController alloc] initWithNibName:@"Bandi2ViewController" bundle:nil];
    
    centerController = [[UINavigationController alloc] initWithRootViewController:centerController];
    
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:centerController
                                                                                    leftViewController:leftController
                                                                                   rightViewController:nil];
    deckController.leftSize = 150;
    
    [deckController disablePanOverViewsOfClass:NSClassFromString(@"_UITableViewHeaderFooterContentView")];
    return deckController;
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

    if(bPlayingIntro == NO)
        [self performSelector:@selector(getOnairLoad) withObject:nil afterDelay:0.2f];

}
-(void)getOnairLoad
{
    IIViewDeckController *controller = (IIViewDeckController *) self.window.rootViewController;
    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
    OnAirView *onAirView = (OnAirView *)[Util getOnAirView:cc.view];
    if (onAirView) {
        if(onAirView.vRadioBtn.selected)
            [onAirView watchRadio:nil];
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    SharedAppDelegate.isBackground = YES;
    
    NSString *playBg = (NSString *)[[NSUserDefaults standardUserDefaults] valueForKey:@"PLAY_BG"];
    if (![playBg isEqualToString:@"YES"]) {
        [ServerSide saveDisconnectLog];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"STOP_AUDIO" object:nil];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

    SharedAppDelegate.isBackground = NO;

    [self performSelector:@selector(getOnairEnterForeground) withObject:nil afterDelay:0.2f];
    

}
-(void)getOnairEnterForeground
{
    IIViewDeckController *controller = (IIViewDeckController *) self.window.rootViewController;

    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;

    
    //NSLog(@"root view -> %@", cc.view.subviews);
    OnAirView *onAirView = (OnAirView *)[Util getOnAirView:cc.view];

    if (onAirView) {

        [onAirView setProgramImageCaller];
    }
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"ACTIVE_AUDIO" object:nil];

    NSString *playBg = (NSString *)[[NSUserDefaults standardUserDefaults] valueForKey:@"PLAY_BG"];
    if (![playBg isEqualToString:@"YES"]) {
        [ServerSide saveConnectLog];
        //[Dialogs alertView:@"enter foreground"];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBAppEvents activateApp];
    
    //new
    [FBSDKAppEvents activateApp];

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [ServerSide saveDisconnectLog];

    if ([Util getPlayerView:self.window.rootViewController.view]) {
        [[Util getPlayerView:self.window.rootViewController.view] stopAudio];
        NSLog(@"stop audio");
    }
    NSLog(@"will terminate.");
}

// In order to process the response you get from interacting with the Facebook login process,
// you need to override application:openURL:sourceApplication:annotation:
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{

    /* ------------------------------------------- *
     * begin - bandi app url launch option setting
     * 예) bandi://launch?channel=fm (or iradio)
     * ------------------------------------------- */
    
    if ([@"launch" isEqualToString:[url host]]) {
        NSDictionary* pDic = [Util parseQueryString:[url query]];
        if (pDic[@"channel"]) SharedAppDelegate.urlSelectedChannel = pDic[@"channel"];
        NSLog(@"appdel. urlch - %@", SharedAppDelegate.urlSelectedChannel);
    }
    
    /* ------------------------------------------- *
     * end - bandi app url launch option setting
     * ------------------------------------------- */
    
    //** facebook sdk 4.23
    BOOL fbHandled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation];
    NSLog(@"fbHandled : %d", fbHandled); // fbHandled 는 아래의  return 값으로 전달된다.

    //** facebook sdk 4.23 **/

    
    
    if ([KOSession isKakaoAccountLoginCallback:url]) { // handle login callback
        return [KOSession handleOpenURL:url];
    } else if ([KOSession isStoryPostCallback:url]) { // handle story post callback
        NSLog(@"KakaoStory post callback! query string: %@", [url query]);
        return YES;
    }else
    {
 
        if (fbHandled != YES) {
            NSLog(@"end timer");
            [SharedAppDelegate.quitTimer invalidate];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AUTO_OFF"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AUTO_STOP_RADIO" object:nil];
            NSLog(@"########## post noti AUTO_STOP_RADIO");
        }
    }
    return fbHandled;
}
-(void)endAnimationIntro
{
    NSLog(@"bPlayingIntro End");
    bPlayingIntro = NO;
    self.window.rootViewController = deckController;
    [ServerSide saveConnectLog];
    
    SharedAppDelegate.isBackground = NO;
//    [self AutoStopCheck];

}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}
- (void)updateTimerText:(NSTimer *)timer
{
    NSLog(@"end timer");
    [SharedAppDelegate.quitTimer invalidate];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AUTO_OFF"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AUTO_STOP_RADIO" object:nil];
    NSLog(@"########## post noti AUTO_STOP_RADIO");

}


@end
