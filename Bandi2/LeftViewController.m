//
//  LeftViewController.m
//  Bandi2
//
//  Created by admin on 13. 12. 17..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "LeftViewController.h"
#import "IIViewDeckController.h"
#import "LeftCell.h"
#import "AirContentsView.h"
#import "BandiBoardView.h"
#import "SongsTableView.h"
#import "FamilyServiceView.h"
#import "AirScheduleView.h"
#import "OnAirView.h"
#import "SettingView.h"
#import "WebViewController.h"
#import "TimeTable.h"
#import "PlayerView.h"
#import "UplusAd.h"
#import "AdWebView.h"
#import "ServerSide.h"
#import "Util.h"

#import "checkListView.h"
#import "notiListView.h"


#define RgbColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@interface LeftViewController ()

@end

@implementation LeftViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    nShowMode = 0;
    
    menuImages = @[@[@"drawer_ico_1_off.png",@"drawer_ico_1_on.png"],
                   @[@"drawer_ico_2_off.png",@"drawer_ico_2_on.png"],
                   @[@"drawer_ico_3_off.png",@"drawer_ico_3_on.png"],
                   @[@"drawer_ico_6_off.png",@"drawer_ico_6_on.png"],
                   @[@"drawer_ico_5_off.png",@"drawer_ico_5_on.png"],
                   @[@"drawer_ico_4_off.png",@"drawer_ico_4_on.png"],


                   @[@"drawer_ico_7_off.png",@"drawer_ico_7_on.png"] ];
    /**
    menuImages = @[@"lnb_onair_on.png",
                   @"lnb_aod_on.png",
                   @"lnb_content_on.png",
                   @"lnb_songs_on.png",
                   @"lnb_timetable_on.png",
                   @"lnb_board_on.png",
                   @"lnb_apps_on.png",
                   @"lnb_setting_on.png" ];
    **/
    
    menuLabels = @[@"온에어",
                    @"편성표",
                    @"반디게시판",
                   @"즐겨찾기",
                    @"공지/이벤트",
                    @"패밀리서비스",
                    @"설정"];
    
    menuBgs = @[@"온에어",
                   @"편성표",
                   @"반디게시판",
                   @"즐겨찾기",
                   @"공지/이벤트",
                   @"패밀리서비스",
                   @"설정"];
    

    // 백그라운드 이미지 설정 (status bar 고려)
    [self.tableView setContentInset:UIEdgeInsetsMake(20,
                                                     self.tableView.contentInset.left,
                                                     self.tableView.contentInset.bottom,
                                                     self.tableView.contentInset.right)];
    
    CGRect newFrame = self.tableView.frame;
    CGRect newBound = self.tableView.bounds;
    //NSLog(@"f, b -> %f, %f, %f, %f", newFrame.origin.y, newFrame.size.height, newBound.origin.y, newBound.size.height);
    //newFrame.origin.y = 20;
    newBound.origin.y = 0;
    self.tableView.frame = newFrame;
    self.tableView.bounds = newBound;
    
    UIGraphicsBeginImageContext(self.tableView.frame.size);
    [[UIImage imageNamed:@"bg_lnb.png"] drawInRect:self.tableView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:image];
    
    // 상단 반디 로고 이미지
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 120, 44)];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(50, 11, 60, 18)];
    imageView.image = [UIImage imageNamed:@"drawer_top_logo.png"];

    //UIImageView *bandiLogo = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"logobandi.png"]];
    [headerView addSubview:imageView];
    //self.tableView.tableHeaderView = headerView;
    
    [self.tableView setTableHeaderView:headerView];
    
    [self.tableView setBackgroundColor:RgbColor(252, 248, 238)];
    
    
    //self.tableView.tableHeaderView = (UIView *)[[NSBundle mainBundle]loadNibNamed:@"LeftHeader" owner:self options:nil][0];
    
    
    // 사용하지 않는 영역의 줄 표시 없애기.
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 10)];
    footerView.backgroundColor = [UIColor clearColor];
    [self.tableView setTableFooterView:footerView];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveTab1:)
                                                 name:@"MoveTab1"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveTab2:)
                                                 name:@"MoveTab2"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveTab3:)
                                                 name:@"MoveTab3"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveTab4:)
                                                 name:@"MoveTab4"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveTab5:)
                                                 name:@"MoveTab5"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveTab6:)
                                                 name:@"MoveTab6"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveTab7:)
                                                 name:@"MoveTab7"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goBackView:)
                                                 name:@"LoginCloseAction"
                                               object:nil];
    

    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    stackSelectList = [[NSMutableArray alloc]initWithCapacity:2];
    [stackSelectList addObject:[NSString stringWithFormat:@"%d",0]];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.f;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return menuLabels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    LeftCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = (LeftCell *) [[NSBundle mainBundle] loadNibNamed:@"LeftCell" owner:self options:nil][0];
        //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    int row = [indexPath row];
    cell.menuImage.image = [UIImage imageNamed:menuImages[row][0]];
    cell.menuImage.highlightedImage = [UIImage imageNamed:menuImages[row][1]];
    cell.menuLabel.text = menuLabels[row];
//    cell.menuLabel.textColor = [UIColor grayColor];
//    cell.menuLabel.highlightedTextColor = [UIColor orangeColor];
    
    UIGraphicsBeginImageContext(cell.frame.size);
    [[UIImage imageNamed:@"bg_lnb_item.png"] drawInRect:cell.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    cell.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    switch (row) {
        case 0:
            [cell setBackgroundColor:RgbColor(125,116,107)];

            break;
        case 1:
            [cell setBackgroundColor:RgbColor(137,128,119)];
            break;
        case 2:
            [cell setBackgroundColor:RgbColor(158,145,136)];
            break;
        case 3:
            [cell setBackgroundColor:RgbColor(172,157,147)];
            break;
        case 4:
            [cell setBackgroundColor:RgbColor(158,145,136)];
            break;
        case 5:
            [cell setBackgroundColor:RgbColor(137,128,119)];
            break;
        case 6:
            [cell setBackgroundColor:RgbColor(125,116,107)];
            break;

        default:
            break;
    }
    if (row == 0) {
        cell.menuImage.image = cell.menuImage.highlightedImage;
        cell.menuLabel.textColor =  cell.menuLabel.highlightedTextColor;
        [cell setBackgroundColor:RgbColor(63,56,51)];
    }
//    if(indexPath.row == nShowMode)
//        cell.menuLabel.textColor = RgbColor(242,211,76);
//    else
//        cell.menuLabel.textColor = RgbColor(255,255,255);
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
   
    int nMode = 0;
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        nMode = 0;
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        nMode = 1;
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            nMode = 1;
        }
        else
        {
            nMode = 0;
        }
    }


    [stackSelectList addObject:[NSString stringWithFormat:@"%d",indexPath.row]];
    if([stackSelectList count] >2)
        [stackSelectList removeObjectAtIndex:0];
    
    int row = [indexPath row];
    LeftCell *cell = nil;
    
    // 모든 셀 선택 안됨 처리 (색 변경)
    NSArray *arr = [tableView visibleCells];
    for (int i=0; i<arr.count; i++) {
        cell = arr[i];
        //NSLog(@"arr[%d] -> %@", i, arr[i]);
        cell.menuImage.image = [UIImage imageNamed:menuImages[i][0]];
     //   cell.menuLabel.textColor = [UIColor grayColor];
        switch (i) {
            case 0:
                [cell setBackgroundColor:RgbColor(125,116,107)];
                break;
            case 1:
                [cell setBackgroundColor:RgbColor(137,128,119)];
                break;
            case 2:
                [cell setBackgroundColor:RgbColor(158,145,136)];
                break;
            case 3:
                [cell setBackgroundColor:RgbColor(172,157,147)];
                break;
            case 4:
                [cell setBackgroundColor:RgbColor(158,145,136)];
                break;
            case 5:
                [cell setBackgroundColor:RgbColor(137,128,119)];
                break;
            case 6:
                [cell setBackgroundColor:RgbColor(125,116,107)];
                break;
                
            default:
                break;
        }

    }
    
    // 해당 셀 선택 처리
    cell = (LeftCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.menuImage.image = [UIImage imageNamed:menuImages[row][1]];
    [cell setBackgroundColor:RgbColor(63,56,51)];
    
   // cell.menuLabel.textColor = [UIColor orangeColor];
    
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.viewDeckController closeLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success) {
        if ([controller.centerController isKindOfClass:[UINavigationController class]]) {
            int menuIndex = [indexPath row];
            //NSLog(@"menu -> %@", menuLabels[menuIndex]);
            
            UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
            if (menuIndex == 0) {
                cc.navigationItem.title = @"온에어";
            } else {
                cc.navigationItem.title = menuLabels[menuIndex];
            }
            
            
            //NSLog(@"last object %@", [[cc.view subviews] lastObject]);
            
            @try {
                [[[cc.view subviews] lastObject] onUnload];
            }
            @catch (NSException *exception) {
                NSLog(@"unload error : %@ - %@", [[cc.view subviews] lastObject], exception.description);
            }
            
            // 화면 고정 영역
//            NSArray *fixedClsArr = @[PlayerView.class, UplusAd.class];
            
//<##> - re-defined 광고영역
            NSArray *fixedClsArr = @[ UplusAd.class];
//            NSArray *fixedClsArr = @[AdWebView.class];
            
            NSArray *arr = [cc.view subviews];
            OnAirView * vi;

            for (int i=0; i<arr.count; i++) {
                //NSLog(@"subviews -> %@", arr[i]);
                bool remove = YES;
                for (int j=0; j<fixedClsArr.count; j++) {
                    if ([arr[i] isKindOfClass:fixedClsArr[j]]) {
                        remove = NO;
                        break;
                    }
                }
                if(nShowMode == indexPath.row)
                {
                    remove = NO;
                }
                if (remove ) {
                    //NSLog(@"removed");
                    if([arr[i] isKindOfClass:[OnAirView class]])
                    {
//                        [cc.view bringSubviewToFront:arr[i]];
                        vi = arr[i];
                    }
                    
                    else
                    {
                        [arr[i] removeFromSuperview];
                    }
                    
                }
            }
            
            // 화면 상의 공간 높이.
            float screenHeight = [[UIScreen mainScreen] bounds].size.height;
            float statusBarHeight = 20.0f; //[[UIApplication sharedApplication] statusBarFrame].size.height; // 제일 상단 상태바 높이
            float naviBarHeight = 44.0f; //[[self.navigationController navigationBar] bounds].size.height;
            float adHeight = 50.0f;
            float playerHeight = 40.0f;
            if(menuIndex == 0)
                playerHeight = 0;
            float bodyHeight =  screenHeight - naviBarHeight - statusBarHeight - playerHeight - adHeight;
            SharedAppDelegate.bodyHeight = [NSNumber numberWithFloat:bodyHeight];
            
            CGRect frameRect = CGRectMake(0, naviBarHeight+statusBarHeight-[Util correctionHeight], 320, bodyHeight);
            
//            NSDictionary* dict = [NSDictionary dictionaryWithObject: menuLabels[menuIndex] forKey:@"Title"];
            //            nShowMode = menuIndex;
            if (menuIndex == 0) { // 온에어
                
                if(nShowMode != menuIndex)
                {
                    [cc.view bringSubviewToFront:vi];
                    for (int cnt = 0; cnt < [arr count]; cnt ++) {
                        if([arr[cnt] isKindOfClass:[UplusAd class]]) {
                            [cc.view bringSubviewToFront:arr[cnt]];
                        }
// webview 광고 영역 - uplus replace
//                        if ([arr[cnt] isKindOfClass:[AdWebView class]]) {
//                            [cc.view bringSubviewToFront:arr[cnt]];
//                        }
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ONAIR" object:nil];
                    if(nMode == 0)
                        [ServerSide saveEventLog:@"OnAir"];
                    else
                        [ServerSide saveEventLog:@"OnAirIradio"];
                }
             
                
                
            } else if (menuIndex == 1) { // 편성표
//                [self launchAod:cc];
                if(nShowMode != menuIndex)
                {
                    AirScheduleView *myView = [[AirScheduleView alloc] initWithFrame:frameRect];
                    [cc.view addSubview:myView];
                    if(nMode == 0)
                        [ServerSide saveEventLog:@"AirSchedule"];
                    else
                        [ServerSide saveEventLog:@"AirScheduleIradio"];

                }
                
//                self.navigationController
               

            } else if (menuIndex == 2) { // 게시판

                if(nShowMode != menuIndex)
                {
                    BandiBoardView *myView = [[BandiBoardView alloc] initWithFrame:frameRect];
                    [cc.view addSubview:myView];
                }
                
            } else if (menuIndex == 5) { // 패밀리

                if(nShowMode != menuIndex)
                {
                    FamilyServiceView *myView = [[FamilyServiceView alloc] initWithFrame:frameRect];
                    [cc.view addSubview:myView];

                }
                [ServerSide saveEventLog:@"Favorite"];
                
            } else if (menuIndex == 4) { // 공지,이벤트
                if(nShowMode != menuIndex)
                {
                    notiListView *myView = [[notiListView alloc] initWithFrame:frameRect];
                    [cc.view addSubview:myView];
                }
                [ServerSide saveEventLog:@"NoticeEvent"];

                
            } else if (menuIndex == 3) { // 즐겨찾기

                if(nShowMode != menuIndex)
                {
                    checkListView *myView = [[checkListView alloc] initWithFrame:frameRect];
                    [cc.view addSubview:myView];
                }

            } else if (menuIndex == 6) { // 설정

                if(nShowMode != menuIndex)
                {
                    SettingView *myView = [[SettingView alloc] initWithFrame:frameRect];
                    [cc.view addSubview:myView];
                }
                [ServerSide saveEventLog:@"Setting"];

            } else {
                NSArray *arr = @[[UIColor blueColor], [UIColor cyanColor], [UIColor blackColor], [UIColor purpleColor], [UIColor darkGrayColor], [UIColor magentaColor], [UIColor orangeColor], [UIColor greenColor]];
                UIView *c = [[UIView alloc] initWithFrame:frameRect];
                c.backgroundColor = arr[menuIndex];
                
                [cc.view addSubview:c];
            }

            if(menuIndex >0 && menuIndex < 7)
            {
//                NSDictionary* dictitle = [NSDictionary dictionaryWithObject: menuLabels[menuIndex] forKey:@"Title"];
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:menuLabels[menuIndex],@"Title",
                                     [NSString stringWithFormat:@"%d",menuIndex],@"Num",nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GPLAYER" object:dic];
                
            }
            nShowMode = menuIndex;


        }
    }];
    
    
}


- (void)launchAod:(UIViewController *)cc {
    NSLog(@"launchAod");
    WebViewController *vc = [[WebViewController alloc] init];
    
    [cc.view.window.rootViewController presentViewController:vc animated:YES completion:^{
        NSDictionary *ttDic = [[[TimeTable alloc] init] get];
        NSString *progcd = ttDic[@"progcd"];
        
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiMobileUrl?progcd=%@", progcd];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSError *error;
        NSString *xmlStr = [[NSString alloc] initWithContentsOfURL:url
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
        TBXML *tbxml = [TBXML newTBXMLWithXMLString:xmlStr error:&error];
        TBXMLElement *elemRoot=nil, *elemRescd=nil, *elemData=nil, *elemMobileUrl=nil;
        NSString *rescdText=nil;
        elemRoot = tbxml.rootXMLElement;
        
        if (elemRoot) {
            elemRescd = [TBXML childElementNamed:@"rescd" parentElement:elemRoot];
            if (elemRescd) {
                rescdText = [TBXML textForElement:elemRescd];
            }
            
            if ([rescdText isEqualToString:@"0000"]) {
                elemData = [TBXML childElementNamed:@"data" parentElement:elemRoot];
                elemMobileUrl = [TBXML childElementNamed:@"mobile-url" parentElement:elemData];
                urlStr = [TBXML textForElement:elemMobileUrl];
                url = [NSURL URLWithString:urlStr];
                NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
                
                [(UIWebView *)[vc.view.subviews lastObject] loadRequest:urlRequest];
                //NSLog(@"%@", [vc.view.subviews lastObject]);
            }
        }
    }];
    
//    [ServerSide saveEventLog:@"Aod"];
}

- (void)moveTab1:(NSNotification *)notification
{

    NSIndexPath *ip = [NSIndexPath indexPathForRow:0 inSection:0];
    [self tableView:self.tableView didSelectRowAtIndexPath:ip];
}

- (void)moveTab2:(NSNotification *)notification
{
    
    
    NSIndexPath *ip = [NSIndexPath indexPathForRow:1 inSection:0];
    [self tableView:self.tableView didSelectRowAtIndexPath:ip];
}
- (void)moveTab3:(NSNotification *)notification
{
    
    NSIndexPath *ip = [NSIndexPath indexPathForRow:2 inSection:0];
    [self tableView:self.tableView didSelectRowAtIndexPath:ip];
}
- (void)moveTab4:(NSNotification *)notification
{
    
    NSIndexPath *ip = [NSIndexPath indexPathForRow:3 inSection:0];
    [self tableView:self.tableView didSelectRowAtIndexPath:ip];
}
- (void)moveTab5:(NSNotification *)notification
{
    NSIndexPath *ip = [NSIndexPath indexPathForRow:4 inSection:0];
    [self tableView:self.tableView didSelectRowAtIndexPath:ip];
}
- (void)moveTab6:(NSNotification *)notification
{
    
    NSIndexPath *ip = [NSIndexPath indexPathForRow:5 inSection:0];
    [self tableView:self.tableView didSelectRowAtIndexPath:ip];
}
- (void)moveTab7:(NSNotification *)notification
{
    
    NSIndexPath *ip = [NSIndexPath indexPathForRow:6 inSection:0];
    [self tableView:self.tableView didSelectRowAtIndexPath:ip];
}

- (void)goBackView:(NSNotification *)notification
{
    if([stackSelectList count] <= 2)
    {
        if([[stackSelectList objectAtIndex:1] intValue] == 3)
        {
            int nSel = [[stackSelectList objectAtIndex:0] intValue];
            NSIndexPath *ip = [NSIndexPath indexPathForRow:nSel inSection:0];
            [self tableView:self.tableView didSelectRowAtIndexPath:ip];
        }
    }
}



@end
