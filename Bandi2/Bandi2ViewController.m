//
//  Bandi2ViewController.m
//  Bandi2
//
//  Created by admin on 13. 12. 17..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "Bandi2ViewController.h"
#import "IIViewDeckController.h"
#import "TimeTable.h"
#import "ProgramData.h"
#import "OnAirView.h"
#import "Bandi2AppDelegate.h"
#import "TBXML+HTTP.h"
#import "Util.h"
#import "ServerSide.h"
#import "WebViewController.h"
#import "BandiBoardWriteView.h"
#import "LoginView.h"
#import "programContentView.h"
#import "programGoude.h"
#import <KakaoOpenSDK/KakaoOpenSDK.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "LogContentController.h"
#import "AdWebView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


@interface Bandi2ViewController ()
{
    BOOL isFirst;
    UplusAd *adView;
    UplusAd *adViewF; // full screen banner
//    AdWebView *adView;
    
    UIView *popView;
    NSString *bannerLink;
}

@end

@implementation Bandi2ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    isFirst = YES;
    bPlayerShowOnairState = YES;
    
    [self deleteAllImgFile];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"000" forKey:@"SNSLogin"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SNSId"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SNSName"];
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"bShowEvent"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showSNSIcon:)
                                                 name:@"BOARGIND_SNS"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(HideSNSIcon:)
                                                 name:@"NO_BOARGIND_SNS"
                                               object:nil];
    
    
    // menu button
    UIImage* img = [UIImage imageNamed:@"drawer_toggle_btn_off.png"];
    UIImage* imgSel = [UIImage imageNamed:@"drawer_toggle_btn_on.png"];
    CGRect frameImg = CGRectMake(0, 0, img.size.width, img.size.height);
    UIButton* menuBtn = [[UIButton alloc]initWithFrame:frameImg];
    [menuBtn setBackgroundImage:img forState:UIControlStateNormal];
    [menuBtn setBackgroundImage:imgSel forState:UIControlStateHighlighted];
    [menuBtn addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc]initWithCustomView:menuBtn];
    self.navigationItem.leftBarButtonItem = menuButton;

    UIImage* shareImg = [UIImage imageNamed:@"share_toggle_btn_off.png"];
    UIImage* shareImgSel = [UIImage imageNamed:@"share_toggle_btn_on.png"];

    CGRect shareImgFrame = CGRectMake(0, 0, shareImg.size.width, shareImg.size.height);
    _btShare = [[UIButton alloc]initWithFrame:shareImgFrame];
    [_btShare setBackgroundImage:shareImg forState:UIControlStateNormal];
    [_btShare setBackgroundImage:shareImgSel forState:UIControlStateSelected];
    [_btShare addTarget:self action:@selector(showShareSns) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc]initWithCustomView:_btShare];
    self.navigationItem.rightBarButtonItem = shareButton;
    
    
    // navigation bar color
    //self.navigationController.navigationBar.translucent = YES;
    //UIImage *bg = [UIImage imageNamed:@"bg_top_area3.png"];
    //[[UINavigationBar appearance] setBackgroundImage:bg forBarMetrics:UIBarMetricsDefault];
    
    NSDictionary *navAttrDic = @{NSForegroundColorAttributeName:[UIColor orangeColor]};
    [[UINavigationBar appearance] setTitleTextAttributes:navAttrDic];
    
    // 화면 상의 공간 높이.
    screenHeight = [[UIScreen mainScreen] bounds].size.height;
    statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height; // 제일 상단 상태바 높이
    naviBarHeight = [[self.navigationController navigationBar] bounds].size.height;
    
    NSLog(@"height -> %f", screenHeight);
    NSLog(@"statusBarHeight -> %f", statusBarHeight);
    NSLog(@"naviBarHeight -> %f", naviBarHeight);
    SharedAppDelegate.screenHeight = [NSNumber numberWithFloat:screenHeight];
    
    float STATUS_HEIGHT = [Util correctionHeight];
    
    
    // 1. 광고
    //float adHeight = 0.0;
    float adHeight = 50.0; // 56
    adView = [[UplusAd alloc]initWithFrame:CGRectMake(0, screenHeight - STATUS_HEIGHT - adHeight, 320, adHeight)];
    [adView setSlotID:@"6049726393"]; // test slot id : 1918432445
    [adView setParent:self];
    [adView setBackgroundColor:[UIColor grayColor]];
    
    // 광고 , adWebView 로 전환
//    adView = [[AdWebView alloc] initWithFrame:CGRectMake(0, screenHeight - STATUS_HEIGHT - adHeight, 320, adHeight)];
//    adView.delegate = adView;
//    [adView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://m.ebs.co.kr/adbaner"]]];
//    [adView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://to302.phps.kr/test/banner.html"]]];
//    
    NSLog(@"position => %f, %f", screenHeight - STATUS_HEIGHT - adHeight, screenHeight - adHeight);
    
    
    // 2. 오디오 플레이어
    float playerHeight = 40.0f;
    float posY = mainView.frame.origin.y + mainView.frame.size.height - (60 + 36 + playerHeight);
    AudioPosY =posY;
    player = [[PlayerView alloc] initWithFrame:CGRectMake(0,-100,320,playerHeight)];
    [self.view addSubview:player];
    player.onAirPlayer.hidden = NO;
    player.gPlayer.hidden = YES;
    // 3. main screen
    float bodyHeight =
    screenHeight - naviBarHeight - statusBarHeight  - adHeight;
    SharedAppDelegate.bodyHeight = [NSNumber numberWithFloat:bodyHeight];

    mainView = [[OnAirView alloc] initWithFrame:CGRectMake(0, naviBarHeight+statusBarHeight-STATUS_HEIGHT, 320, bodyHeight)];
    
    [self.view addSubview:mainView];
    

     [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:_shareView];
    [_shareView setFrame:CGRectMake(0, 0, 320, 600)];


    _shareView.hidden = YES;
    
    titleV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, 40)];
    naviIcon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tit_area_logo.png"]];
    naviIcon.contentMode = UIViewContentModeScaleAspectFit;
    [naviIcon setFrame:CGRectMake(30, 0, 28, 34)];
    [titleV addSubview:naviIcon];
    naviTitle = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 150, 34)];
    naviTitle.text = @"온에어";
    naviTitle.textAlignment = NSTextAlignmentLeft;
    btHome = [[UIButton alloc]initWithFrame:naviIcon.frame];
    [btHome addTarget:self action:@selector(goHome) forControlEvents:UIControlEventTouchUpInside];
    [titleV addSubview:naviTitle];
    [titleV addSubview:btHome];

    self.navigationItem.titleView = titleV;
    
    NSString *sysVer = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compResult = [sysVer compare:@"7.0"];
    if (NSOrderedAscending != compResult) {
        [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    }
    
    
    [self.view bringSubviewToFront:player];
    // timetable xml parseing test
//    [[[TimeTable alloc] init] get];
//    [[[ProgramData alloc] init] get];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showGplayer:)
                                                 name:@"GPLAYER"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showOnairPlayer:)
                                                 name:@"ONAIR"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(postBoard:)
                                                 name:@"PostBoard"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(AutoStopPlayer:)
                                                 name:@"AUTO_STOP_RADIO"
                                               object:nil];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(twitterLogin:)
                                                 name:@"TwitterLoginN"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(nLogin:)
                                                 name:@"LoginN"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onlyLogin:)
                                                 name:@"LoginOnly"
                                               object:nil];
    

    // facebook sdk 4.23 logout
    FBSDKLoginManager *fbLoginManager = [[FBSDKLoginManager alloc] init];
    [fbLoginManager logOut];
    if ([FBSDKAccessToken currentAccessToken]) {
        [FBSDKAccessToken setCurrentAccessToken:nil];
        [FBSDKProfile setCurrentProfile:nil];
    }
    // facebook sdk 4.23 logout **/
    
    // for facebook 3.22 sdk
//    [FBSession.activeSession closeAndClearTokenInformation];
    
    // facebook sdk 4.23 share function (with default image) == for test
    //    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    //    content.contentURL = [NSURL URLWithString:@"https://www.facebook.com/FacebookDevelopers"];
    //    FBSDKShareButton *fbShareButton = [[FBSDKShareButton alloc] init];
    //    fbShareButton.shareContent = content;
    //    fbShareButton.center = self.view.center;
    //    [self.view addSubview:fbShareButton];
    //
    // facebook sdk 4.23 share function **/
    
    
    [[FHSTwitterEngine sharedEngine]permanentlySetConsumerKey:@"Eryy5JFaC1lMies2mDSSWmagM" andSecret:@"rbu7ANpzixjZS0atnQlqH7LOz9R39G0pnwhi7RezbfU6b4wxPg"];
    [[FHSTwitterEngine sharedEngine]setDelegate:self];
    
    if([KOSession sharedSession].isOpen)
    {
        [[KOSession sharedSession] logoutAndCloseWithCompletionHandler:^(BOOL success, NSError *error) {
        }];
    }
    
    MPVolumeView *volumeView = [[MPVolumeView alloc] initWithFrame:CGRectMake(-2000., -2000., 0.f, 0.f)];
    NSArray *windows = [UIApplication sharedApplication].windows;
    
    volumeView.alpha = 0.1f;
    volumeView.userInteractionEnabled = NO;
    
    if (windows.count > 0) {
        [[windows objectAtIndex:0] addSubview:volumeView];
    }
    
    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"INIT_GUIDE"] isEqualToString:@"1"])
    {
        SharedAppDelegate.isOpenTt = YES;
        
//        IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController

        if (IS_IPHONE5)
        {
            programContentView *vc = [[programContentView alloc] init];
            [self presentViewController:vc animated:YES completion:nil];
        }
        else
        {
            programGoude *vc = [[programGoude alloc] init];
            [self presentViewController:vc animated:YES completion:nil];

        }
        
    }
    


    
    [mainView.wrapperView addSubview:player];
    [mainView playerViewSetting:player];
    [mainView bringSubviewToFront:player];
    player.onAirPlayer.hidden = NO;
    player.gPlayer.hidden = YES;
    bPlayerShowOnairState = YES;
    [mainView reloadRadioControll];
    naviIcon.hidden = NO;
    naviTitle.text = @"온에어";
    
    if(_shareView.isHidden == NO)
        _shareView.hidden = YES;
    if(_btShare.isSelected == YES)
        _btShare.selected = NO;
    
    [naviIcon setFrame:CGRectMake(30, 5, 28, 34)];
    [naviTitle setFrame:CGRectMake(60, 5, 150, 30)];
    btHome.frame =naviIcon.frame;
    
}

- (void)showGplayer:(NSNotification *)notification
{

//    if(bPlayerShowOnairState)
    {
        [player setFrame:CGRectMake(0, adView.frame.origin.y-player.gPlayer.frame.size.height, 320, player.gPlayer.frame.size.height)];
        [self.view addSubview:player];
        player.onAirPlayer.hidden = YES;
        player.gPlayer.hidden = NO;
        bPlayerShowOnairState = NO;
    }
    if(_shareView.isHidden == NO)
        _shareView.hidden = YES;
    if(_btShare.isSelected == YES)
        _btShare.selected = NO;

    NSLog(@"notification = %@",notification);
    NSLog(@"notification = %@",[[notification valueForKey:@"object"]valueForKey:@"Title"]);
    
//    naviIcon.hidden = YES;
    NSLog(@"naviTitle.text= %@",naviTitle.text);
    naviTitle.text = [[notification valueForKey:@"object"]valueForKey:@"Title"];
    int nCnt = [[[notification valueForKey:@"object"]valueForKey:@"Num"] intValue];
    
    /*
     itleV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, 40)];
     [naviIcon setFrame:CGRectMake(30, 5, 30, 30)];
     naviTitle = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 150, 30)];

     */
    switch (nCnt) {
        case 0:
        case 1:
            [naviIcon setFrame:CGRectMake(30, 5, 28, 34)];
            [naviTitle setFrame:CGRectMake(60, 5, 150, 30)];

            break;
        case 2:
            [naviIcon setFrame:CGRectMake(15, 5, 28, 34)];
            [naviTitle setFrame:CGRectMake(45, 5, 150, 30)];

            break;
        case 3:
            [naviIcon setFrame:CGRectMake(20, 5, 28, 34)];
            [naviTitle setFrame:CGRectMake(50, 5, 150, 30)];

            break;
        case 4:
            [naviIcon setFrame:CGRectMake(12, 5, 28, 34)];
            [naviTitle setFrame:CGRectMake(42, 5, 150, 30)];
            break;
        case 5:
            [naviIcon setFrame:CGRectMake(9, 5, 28, 34)];
            [naviTitle setFrame:CGRectMake(39, 5, 150, 30)];

            break;
        case 6:
            [naviIcon setFrame:CGRectMake(30, 5, 28, 34)];
            [naviTitle setFrame:CGRectMake(60, 5, 150, 30)];
            break;
            
            
        default:
            break;
    }
        btHome.frame =naviIcon.frame;

    
}
- (void)onlyLogin:(NSNotification *)notification
{
    NSString * strStrView;
    if (IS_IPHONE5)
        strStrView = @"LogContentController";
    else
        strStrView = @"LogContentController_480";
    
    LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
    
    //    [vc setBGoBack:YES];
    //
    [vc EBSLoginOnly];
    
    [self presentViewController:vc animated:YES completion:nil];
}
- (void)nLogin:(NSNotification *)notification
{
    NSString * strStrView;
    if (IS_IPHONE5)
        strStrView = @"LogContentController";
    else
        strStrView = @"LogContentController_480";
    
    LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
    
//    [vc setBGoBack:YES];
//    [vc EBSLoginOnly];
    
    [self presentViewController:vc animated:YES completion:nil];
    
}
- (void)twitterEngineControllerDidCancel
{
    if (!SharedAppDelegate.isFromLogin) {
        return;
    }

    
    NSLog(@"cancle");
    NSString * strStrView;
    if (IS_IPHONE5)
        strStrView = @"LogContentController";
    else
        strStrView = @"LogContentController_480";
    
    LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
//    [vc setBGoBack:YES];
//    [vc EBSLoginOnly];
    
    [self presentViewController:vc animated:NO completion:nil];
}
- (void)twitterLogin:(NSNotification *)notification
{
    
    UIViewController *loginController = [[FHSTwitterEngine sharedEngine]loginControllerWithCompletionHandler:^(BOOL success) {
        
        if (success) {
            NSLog(success?@"L0L success":@"O noes!!! Loggen faylur!!!");
            NSLog(@"name = %@",FHSTwitterEngine.sharedEngine.authenticatedUsername);
            //        [_theTableView reloadData];
            [[NSUserDefaults standardUserDefaults] setObject:@"002" forKey:@"SNSLogin"];
            [[NSUserDefaults standardUserDefaults] setObject:FHSTwitterEngine.sharedEngine.authenticatedID forKey:@"SNSId"];
            [[NSUserDefaults standardUserDefaults] setObject:FHSTwitterEngine.sharedEngine.authenticatedUsername forKey:@"SNSName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
            //            [self dismissViewControllerAnimated:NO completion:nil];
            
        }
        else
        {
            NSLog(@"relogin");
        }
        
    }];
    
    [self presentViewController:loginController animated:NO completion:nil];
}

- (void)showSNSIcon:(NSNotification *)notification
{
//    _btShare.hidden = NO;
}
- (void)HideSNSIcon:(NSNotification *)notification
{
//    _btShare.hidden = YES;
}
- (void)AutoStopPlayer:(NSNotification *)notification
{
    [player stopAudio];
}
- (void)showOnairPlayer:(NSNotification *)notification
{
    
    [mainView.wrapperView addSubview:player];
    [mainView playerViewSettingNonInit:player];
//    [mainView bringSubviewToFront:player];
    player.onAirPlayer.hidden = NO;
    player.gPlayer.hidden = YES;
    bPlayerShowOnairState = YES;
    [mainView reloadRadioControll];
    naviIcon.hidden = NO;
    naviTitle.text = @"온에어";
    
    if(_shareView.isHidden == NO)
        _shareView.hidden = YES;
    if(_btShare.isSelected == YES)
        _btShare.selected = NO;
    
    [naviIcon setFrame:CGRectMake(30, 5, 28, 34)];
    [naviTitle setFrame:CGRectMake(60, 5, 150, 30)];
    btHome.frame =naviIcon.frame;
    




}

- (void)postBoard:(NSNotification *)notification
{
    CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);
    
    SharedAppDelegate.latestMenu = @"온에어";
    
    if ([ServerSide autoLogin]) {
        BandiBoardWriteView *myView = [[BandiBoardWriteView alloc] initWithFrame:frameRect];
        [self.view addSubview:myView];
        [self.view addSubview:myView];
        [self.view bringSubviewToFront:myView];
        self.navigationItem.title = @"반디게시판";
    } else {
        LoginView *loginView = [[LoginView alloc] initWithFrame:frameRect];
        [loginView setBGoboard:YES];
        [self.view addSubview:loginView];
        [self.view bringSubviewToFront:loginView];
        self.navigationItem.title = @"로그인";
        
    }

}


- (void)viewWillAppear:(BOOL)animated
{
    if(!isFirst)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"viewDidAppearFormSuperView" object:nil];
    
    //** Google Analytics **
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:@"Bandi2ViewController"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear is called.");
    
    if (isFirst) {
        NSLog(@"isFirst.");
        [self.view addSubview:adView];
        [self fullScreenBanner];
        isFirst = NO;
    }

        
//     [mainView setPlayerFrame];
    NSLog(@"subviews -> %@", self.view.subviews);

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 전체 화면에 보이지는 광고,이벤트
- (void)fullScreenBanner
{
    //[NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(UPlusBanner) userInfo:nil repeats:NO];

    if (! [self ebsPopBanner]) {
        [self UPlusBanner];
    }
    
}

/**
 * UPlus fullscreen banner.
 */
- (void)UPlusBanner {
    adViewF = [[UplusAd alloc] initWithFrame:CGRectMake(0, 480-56, 320, 36)];
    [adViewF setSlotID:@"7101444049"]; // (test) 4422670211
    [adViewF setParent:self];
    [adViewF setFullScreen:YES];
    [adViewF setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:adViewF];
    [self.view bringSubviewToFront:adViewF];
    NSLog(@"Uplus Full screen banner.");
}



- (BOOL)ebsPopBanner
{
    NSError *error;
    NSLog(@"pass 0");
    // 하루 동안 열지 않음 시행.
    NSDate *silentBefore = (NSDate *) [[NSUserDefaults standardUserDefaults] objectForKey:@"SILENT_ONEDAY"];
    NSLog(@"silent before %@", silentBefore);
    if (silentBefore) {
        if ([silentBefore compare:[NSDate date]] == NSOrderedDescending) {
            return NO;
        }
    }
    NSLog(@"pass 1");
    
    // 참가 여부 확인
    NSDate *joinDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"EVENT_JOIN_DATE"];
    if (joinDate) {
        return NO;
    }
    NSLog(@"pass 2");


    NSString *urlStr = [NSString stringWithFormat:@"http://m.ebs.co.kr/cs/mobile/bandiEvt/evtYn?deviceId=%@", [Util getDeviceID]];

    NSLog(@"call url = %@",urlStr);
    NSString *xmlStr2 = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                      encoding:NSUTF8StringEncoding
                                                         error:&error];
    NSString *eventYn = @""; // {'Y':참여가능 , 'N':참여불가}
    if (xmlStr2 && !error) {
        eventYn = [Util getXMLElementText:xmlStr2 elementPath:@[@"bandiEvent",@"evtYn"]];
    }
    NSLog(@"load event end");
    
    // if 이미 참가 했으면 날짜를 로컬에 저장하고, 다음에는 호출되지 않게 처리. return
    if ([eventYn isEqualToString:@"N"]) { // 참여불가 - 이미 참여함
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"EVENT_JOIN_DATE"];
        return NO;
    } else if ([eventYn isEqualToString:@""]) {
        return NO; // 참여여부 불러오기 실패 시 그냥 안보여 줌.
    }
    NSLog(@"pass 3");
    
    // 배너 띄우기
    NSString *filePath = [Util getTempFilePath:@"popBannerInfo.xml"];
    NSString *xmlStr;
    
    if ([Util existsLocalFile:filePath freshTime:60*30]) {  // fresh in 30 minutes
        xmlStr = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    } else {
        NSURL *xmlUrl = [NSURL URLWithString:@"http://m.ebs.co.kr/appbanner?appId=bandi"];
        xmlStr = [[NSString alloc] initWithContentsOfURL:xmlUrl
                                                encoding:NSUTF8StringEncoding
                                                   error:&error];
        [xmlStr writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    }
    
    NSString *rescd = [Util getXMLElementText:xmlStr elementPath:@[@"ebs", @"rescd"]];
    NSLog(@"popbanner => %@", rescd);
    
    if (! [@"0000" isEqualToString:rescd]) {
        return NO;
    }
    else {
        
        bannerLink = [Util getXMLElementText:xmlStr elementPath:@[@"ebs",@"data",@"ebs.banner",@"list",@"mobUrl"]];
        NSString *imgUrl = [Util getXMLElementText:xmlStr elementPath:@[@"ebs",@"data",@"ebs.banner",@"list",@"imgeFile"]];
        
        NSData *data = [ServerSide getResource:[NSURL URLWithString:imgUrl] localSavingTime:60];
        UIImage *img = [UIImage imageWithData:data];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
        
        // 배너 이미지 사이즈 및 위치 보정 (4inch, 3.5inch 높이 차이에 따른)
        float imageHeight =  320 * (img.size.height/img.size.width);
        //CGFloat screenHeight = [SharedAppDelegate.screenHeight floatValue]; // 위에서 이미 정의해서 뺌.
        CGFloat initY = (screenHeight-imageHeight) / 2;
        CGRect newFrame = CGRectMake(0, initY, 320, imageHeight);
        imgView.frame = newFrame;
        
        // background view
        popView = [[UIView alloc] init];
        popView.frame = CGRectMake(0,0,320,[SharedAppDelegate.screenHeight floatValue]); //newFrame;
        popView.backgroundColor = [UIColor blackColor];
        
        [popView addSubview:imgView];
        imgView.alpha = 1;
        
        // 하단 다시 열지 않음 영역 만들기
        UIView *textView = [[UIView alloc] initWithFrame:CGRectMake(0, imageHeight-50, 320, 50)];
        textView.backgroundColor = [UIColor blackColor];
        textView.alpha = 0.5;
        
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 310, 50)];
        label.text = @"하루 동안 보이지 않기 X";
        label.textAlignment = NSTextAlignmentRight;
        label.textColor = [UIColor whiteColor];
        [textView addSubview:label];
        [textView bringSubviewToFront:label];
        
        [popView addSubview:textView];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePopBanner:)];
        [textView addGestureRecognizer:tapGesture];
        
        UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openWeb:)];
        [popView addGestureRecognizer:tapGesture2];
        
        [[UIApplication sharedApplication].keyWindow addSubview:popView];
        [[UIApplication sharedApplication].keyWindow bringSubviewToFront:popView];
    }
    
    
    return YES;
}

- (void)closePopBanner:(UITapGestureRecognizer *)tapGesture
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate dateWithTimeIntervalSinceNow:24*60*60] forKey:@"SILENT_ONEDAY"];
    [popView removeFromSuperview];
    //NSLog(@"tap close pop banner");
}


- (void)openWeb:(UITapGestureRecognizer *)tapGesture
{
    bannerLink = [bannerLink stringByReplacingOccurrencesOfString:@"__DEVICE_ID__" withString:[Util getDeviceID]];
    
    [popView removeFromSuperview];
    
    WebViewController *vc = [[WebViewController alloc] init];
    [self.view.window.rootViewController presentViewController:vc animated:YES completion:^{
        NSURL *url = [NSURL URLWithString:bannerLink];
        NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
        
        [(UIWebView *)[vc.view.subviews lastObject] loadRequest:urlRequest];
    }];

}

/**
 App Store 의 반디 앱 페이지를 연다.
 */
-(void)goHome
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MoveTab1" object:nil];
}
-(void)showShareSns
{
    if(_shareView.hidden)
    {
        _btShare.selected = YES;
        _shareView.hidden = NO;
    }
    else
    {
        _btShare.selected = NO;
        _shareView.hidden = YES;
    }

}
- (void)openAppStore
{
    NSURL *url = [NSURL URLWithString:@"itms://itunes.apple.com/kr/app/ebs-bandi/id368886634?mt=8"];
    [[UIApplication sharedApplication] openURL:url];
    
    NSLog(@"open app store ");
}

#pragma mark - 공유하기 (twitter, facebook, kakao)
-(IBAction)shareTwitter:(id)sender
{
    _btShare.selected = NO;
    _shareView.hidden = YES;
    
    if(!FHSTwitterEngine.sharedEngine.isAuthorized)
    {
        UIViewController *loginController = [[FHSTwitterEngine sharedEngine]loginControllerWithCompletionHandler:^(BOOL success) {
            NSLog(success?@"L0L success":@"O noes!!! Loggen faylur!!!");
            UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"트위터" message:@"공유내용을 입력해주세요." delegate:self cancelButtonTitle:@"취소" otherButtonTitles:@"확인", nil];
            [av setAlertViewStyle:UIAlertViewStylePlainTextInput];
            //        [[av textFieldAtIndex:0]setPlaceholder:@"Write a tweet here..."];EBS
            [[av textFieldAtIndex:0]setText:@"신개념 EBS 인터넷 라디오. EBS 반디 http://me2.do/FDTk7Pyi -"];
            [av show];
            
        }];
        [self presentViewController:loginController animated:NO completion:nil];

    }
    else
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"트위터" message:@"공유내용을 입력해주세요." delegate:self cancelButtonTitle:@"취소" otherButtonTitles:@"확인", nil];
        [av setAlertViewStyle:UIAlertViewStylePlainTextInput];
//        [[av textFieldAtIndex:0]setPlaceholder:@"Write a tweet here..."];EBS
        [[av textFieldAtIndex:0]setText:@"신개념 EBS 인터넷 라디오. EBS 반디 http://me2.do/FDTk7Pyi -"];
        [av show];
    }
    
}

/**
 facebook 4.23 sdk share
 */
-(IBAction)shareFaceBook:(id)sender
{
    
    _btShare.selected = NO;
    _shareView.hidden = YES;
    
    NSURL *contentURL = [[NSURL alloc] initWithString:@"http://www.ebs.co.kr/radio/bandi/pc"];
    FBSDKShareLinkContent *fbShareContent = [[FBSDKShareLinkContent alloc] init];
    fbShareContent.contentURL = contentURL;
    
    FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
    shareDialog.mode = FBSDKShareDialogModeWeb;
    shareDialog.shareContent = fbShareContent;
    shareDialog.fromViewController = self;
    [shareDialog show];
}

-(IBAction)shareKakao:(id)sender
{
    _btShare.selected = NO;
    _shareView.hidden = YES;

    
    BOOL isInstalledKakaoTalk = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"kakaolink://"]];
    if (!isInstalledKakaoTalk) {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"카카오톡이 설치되어 있지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        
        _btShare.selected = NO;
        _shareView.hidden = YES;

        
        return;
    }

    KakaoTalkLinkObject *appText
    = [KakaoTalkLinkObject createLabel:@"신개념 EBS 인터넷 라디오 [EBS 반디] 안드로이드 http://me2.do/5tUcd9cW iOS http://me2.do/5mA1uzWc"];
    

    [KOAppCall openKakaoTalkAppLink:@[appText]];
    
    
}

#pragma mark -

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


- (void)makeRequestToPostObject
{
    // Retrieve a picture from the device's photo library
    /*
     NOTE: SDK Image size limits are 480x480px minimum resolution to 12MB maximum file size.
     In this app we're not making sure that our image is within those limits but you should.
     Error code for images that go below or above the size limits is 102.
     */
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

// When the user is done picking the image
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // Get the UIImage
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    // Dismiss the image picker off the screen
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // stage the image
    [FBRequestConnection startForUploadStagingResourceWithImage:image completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        __block NSString *alertText;
        __block NSString *alertTitle;
        if(!error) {
            NSLog(@"Successfuly staged image with staged URI: %@", [result objectForKey:@"uri"]);
            
            // Package image inside a dictionary, inside an array like we'll need it for the object
            NSArray *image = @[@{@"url": [result objectForKey:@"uri"], @"user_generated" : @"true" }];
            
            // Create an object
            NSMutableDictionary<FBOpenGraphObject> *restaurant = [FBGraphObject openGraphObjectForPost];
            
            // specify that this Open Graph object will be posted to Facebook
            restaurant.provisionedForPost = YES;
            
            // Add the standard object properties
            restaurant[@"og"] = @{ @"title":@"mytitle", @"type":@"restaurant.restaurant", @"description":@"my description", @"image":image };
            
            // Add the properties restaurant inherits from place
            restaurant[@"place"] = @{ @"location" : @{ @"longitude": @"-58.381667", @"latitude":@"-34.603333"} };
            
            // Add the properties particular to the type restaurant.restaurant
            restaurant[@"restaurant"] = @{@"category": @[@"Mexican"],
                                          @"contact_info": @{@"street_address": @"123 Some st",
                                                             @"locality": @"Menlo Park",
                                                             @"region": @"CA",
                                                             @"phone_number": @"555-555-555",
                                                             @"website": @"http://www.example.com"}};
            
            // Make the Graph API request to post the object
            FBRequest *request = [FBRequest requestForPostWithGraphPath:@"me/objects/restaurant.restaurant"
                                                            graphObject:@{@"object":restaurant}];
            [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (!error) {
                    // Success! Include your code to handle the results here
                    NSLog(@"result: %@", result);
                    _objectID = [result objectForKey:@"id"];
                    alertTitle = @"Object successfully created";
                    alertText = [NSString stringWithFormat:@"An object with id %@ has been created", _objectID];
                    [[[UIAlertView alloc] initWithTitle:alertTitle
                                                message:alertText
                                               delegate:self
                                      cancelButtonTitle:@"OK!"
                                      otherButtonTitles:nil] show];
                } else {
                    // An error occurred, we need to handle the error
                    // Check out our error handling guide: https://developers.facebook.com/docs/ios/errors/
                    NSLog(@"error %@", error.description);
                }
            }];
        } else {
            // An error occurred, we need to handle the error
            // Check out our error handling guide: https://developers.facebook.com/docs/ios/errors/
            NSLog(@"error %@", error.description);
        }
    }];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                
                NSString *tweet = [NSString stringWithFormat:@"%@", [alertView textFieldAtIndex:0].text];
                id returned = [[FHSTwitterEngine sharedEngine]postTweet:tweet];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                NSString *title = nil;
                NSString *message = nil;
                
                if ([returned isKindOfClass:[NSError class]]) {
                    NSError *error = (NSError *)returned;
                    title = [NSString stringWithFormat:@"Error %d",error.code];
                    message = error.localizedDescription;
                } else {
                    NSLog(@"%@",returned);
                    title = @"Tweet Posted";
                    message = tweet;
                }
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    @autoreleasepool {
                        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"공유" message:@"트위터에 공유되었습니다." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [av show];
                    }
                });
            }
        });
    }
}
-(void)deleteAllImgFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *docs = [paths objectAtIndex:0];
    
    NSString *dataPath = [docs stringByAppendingPathComponent:@"/imageThumb"];
    
    //
    
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:dataPath error:nil];
    NSLog(@"su = %d",success);
    
    
}

-(IBAction)sharedViewClose:(id)sender
{
    _btShare.selected = NO;
    _shareView.hidden = YES;
}

@end
