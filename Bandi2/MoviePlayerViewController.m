//
//  MoviePlayerViewController.m
//  Bandi2
//
//  Created by admin on 2014. 2. 4..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "MoviePlayerViewController.h"
#import "PlayerView.h"
#import "ServerSide.h"

#define TAG @"VideoActivity"


@interface MoviePlayerViewController ()

@end

@implementation MoviePlayerViewController


- (id)initWithURL:(NSURL *)url
{
    self = [super init];
	if (self)
	{
		movieURL = url;
	}
	
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:NO];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

    [self addButtons];
    return;
    
    _moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    _moviePlayer.controlStyle = MPMovieControlStyleNone; //MPMovieControlModeDefault;
    //_moviePlayer.shouldAutoplay = YES;
    
    [self.view addSubview:_moviePlayer.view];
//    [_moviePlayer setFullscreen:YES];
    [_moviePlayer prepareToPlay];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerLoadStateChanged:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:_moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(moviePlayBackDidFinish:)
												 name:MPMoviePlayerPlaybackDidFinishNotification
											   object:nil];
    
//    [ServerSide saveEventLog:TAG];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    // return YES to hide status bar
    return YES;
}


- (void)moviePlayerLoadStateChanged:(NSNotification*)notification
{
    MPMoviePlayerController *mp = [notification object];
    
	// Unless state is unknown, start playback
	if ([mp loadState] != MPMovieLoadStateUnknown)
	{
		// Remove observer
		[[NSNotificationCenter defaultCenter] removeObserver:self
														name:MPMoviePlayerLoadStateDidChangeNotification
													  object:nil];
        
		// When tapping movie, status bar will appear, it shows up
		// in portrait mode by default. Set orientation to landscape
		[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:NO];
        
        
		// Rotate the view for landscape playback
		[[self view] setBounds:CGRectMake(0, 0, 480, 320)];
		[[self view] setCenter:CGPointMake(160, 240)];
		[[self view] setTransform:CGAffineTransformMakeRotation(M_PI / 2)];
        
		// Set frame of movieplayer
		[[mp view] setFrame:CGRectMake(0, 0, 480, 320)];
        
		// Add movie player as subview
		[[self view] addSubview:[mp view]];
        
		// Play the movie
		[mp play];
		//[self appDelegate].isVideoPlaying = YES;
        
		// Add buttons
		[self addButtons];
	}
}

// exit visible radio
- (void)moviePlayBackDidFinish:(NSNotification *)notification
{
    // Remove observer
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification
												  object:nil];
    [_moviePlayer stop];
    
    
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    [self dismissViewControllerAnimated:NO completion:^{
        NSLog(@"completion visible radio.");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"playAgain" object:nil];
    }];
}

- (void)addButtons
{
    // 닫고 나가기
    if(IS_IPHONE5)
        _homeButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 285, 35, 35)];
    else
        _homeButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 220, 35, 35)];
	[_homeButton setBackgroundImage:[UIImage imageNamed:@"view_home.png"] forState:UIControlStateNormal];
	[_homeButton addTarget:self action:@selector(buttonPressed:) forControlEvents: UIControlEventTouchUpInside];
	_homeButton.alpha = 0.6;
	[self.view addSubview:_homeButton];

//    // 글쓰기
//    writeButton = [[UIButton alloc] initWithFrame:CGRectMake(365, 285, 35, 35)];
//	[writeButton setBackgroundImage:[UIImage imageNamed:@"view_write.png"] forState:UIControlStateNormal];
//	[writeButton addTarget:self action:@selector(buttonPressed:) forControlEvents: UIControlEventTouchUpInside];
//	writeButton.alpha = 0.6;
//	[[self view] addSubview:writeButton];
//    
//	// 반디게시판
//	boardButton = [[UIButton alloc] initWithFrame:CGRectMake(410, 285, 35, 35)];
//	[boardButton setBackgroundImage:[UIImage imageNamed:@"view_board.png"] forState:UIControlStateNormal];
//	[boardButton addTarget:self action:@selector(buttonPressed:) forControlEvents: UIControlEventTouchUpInside];
//	boardButton.alpha = 0.6;
//	[[self view] addSubview:boardButton];

}

- (void)buttonPressed:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];

    
    if (btn == _homeButton) {
        NSLog(@"home button pressed.");
        [self dismissViewControllerAnimated:NO completion:nil];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadOnairView" object:nil];
        return;
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:MPMoviePlayerPlaybackDidFinishNotification object:nil]];
    } else if (btn == writeButton) {
        NSLog(@"write button pressed.");
        if ([ServerSide autoLogin]) {
            boardWriteView = [[BandiBoardWriteLandscapeView alloc] initWithFrame:CGRectMake(0, 0, [SharedAppDelegate.screenHeight intValue], 320)];
            [self.view addSubview:boardWriteView];
        } else {
            loginView = [[LoginLandscapeView alloc] initWithFrame:CGRectMake(0, 0, [SharedAppDelegate.screenHeight intValue], 320)];
            [self.view addSubview:loginView];
        }
        
    } else if (btn == boardButton) {
        NSLog(@"board button pressed.");
        boardView = [[BandiBoardLandscapeView alloc] initWithFrame:CGRectMake(0, 0, [SharedAppDelegate.screenHeight intValue], 320)];
        [self.view addSubview:boardView];
    }
    
    [UIView commitAnimations];
}

// for keyboard orientaion
- (BOOL)shouldAutorotate
{
    return NO;
}

@end
