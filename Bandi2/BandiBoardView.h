//
//  BandiBoardView.h
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MNMBottomPullToRefreshManager.h"

@interface BandiBoardView : UIView<MNMBottomPullToRefreshManagerClient,UITextFieldDelegate>
{
    NSMutableArray *boardList;
    MNMBottomPullToRefreshManager *pullToRefreshManager_;

    BOOL bLoginShow;
    BOOL bShowKeyPad;
    
    NSString *notiCode;
    
    UILabel *lbTitleText;
    NSString *strEventDetailUrl;
    int nSelectedMenu;
//    NSUInteger reloads_;
//    UITextField * boardText;
}
- (void)onUnload;
@property (strong, nonatomic) IBOutlet UITableView *tbList;
@property (strong, nonatomic) IBOutlet BandiBoardView *wrapperView;
@property (strong, nonatomic) IBOutlet UILabel *programLabel;
//@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) IBOutlet UIView * notiView;
@property (strong, nonatomic) IBOutlet UIView * inputView;
@property (strong, nonatomic) IBOutlet UIView * tabView;

@property (strong, nonatomic) IBOutlet UILabel * notiTitle;
@property (strong, nonatomic) IBOutlet UILabel * titleText1;
@property (strong, nonatomic) IBOutlet UILabel * titleText2;

@property (strong, nonatomic) IBOutlet UIButton * btMenuReadRadio;
@property (strong, nonatomic) IBOutlet UIButton * btMenuEngRadio;
@property (strong, nonatomic) IBOutlet UITextField * boardText;

@property (strong, nonatomic) IBOutlet UIView * detailView;
@property (strong, nonatomic) IBOutlet UIScrollView * detailContentScroll;
@property (strong, nonatomic) IBOutlet UILabel * detailNotiContent;
@property (strong, nonatomic) IBOutlet UILabel * detailTitle;
@property (strong, nonatomic) IBOutlet UIImageView * snsIcon;
@property (strong, nonatomic) IBOutlet UIImageView * inputIcon;

@property (strong, nonatomic) IBOutlet UIButton * btKeyHide;

@property (strong, nonatomic) IBOutlet UIButton * eventDetailBt;



- (IBAction)tabPlayFmRadio:(id)sender;
- (IBAction)tabPlayEndRadio:(id)sender;
- (IBAction)writeBulletin:(id)sender;
- (IBAction)openNoti:(id)sender;
- (IBAction)closeNotiDetail:(id)sender;

- (IBAction)hideKeyPad:(id)sender;
-(IBAction)eventDetailUrlOpen:(id)sender;
@end
