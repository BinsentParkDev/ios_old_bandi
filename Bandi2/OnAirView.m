//

//  OnAirView.m
//  Bandi2
//
//  Created by admin on 2014. 1. 13..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "OnAirView.h"
#import "TimeTable.h"
#import "ProgramData.h"
#import "TBXML+HTTP.h"
#import "IIViewDeckController.h"
#import "LoginView.h"
#import "Bandi2AppDelegate.h"
#import "WebViewController.h"
#import "Util.h"
#import "MoviePlayerViewController.h"
#import "PlayerView.h"
#import "ServerSide.h"
#import "BandiBoardWriteView.h"

#import "programContentView.h"
#import "LogContentController.h"

#import "GA.h"

#define TAG @"OnAir"

#define RgbColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]


#define tabMenuOpenHeight   200.0f
#define tabMenuOpenHeight_S   155.f
#define tabMenuCloseHeight  55.0f
#define textScrollerHeight  36.0f
#define imgH 370
#define imgH_S 283
#define degreesToRadian(x)(M_PI*x/180.0)

@implementation OnAirView
{
    TimeTable *tt;
    NSDictionary *ttDic;
    
    ProgramData *pd;
    NSDictionary *pdDic;
    
    NSString *progcd;
    bool runLoop;
    
    NSString *facebookLink;// 임시..
}

- (id)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    if (self) {
        if(IS_IPHONE5)
            [[NSBundle mainBundle] loadNibNamed:@"OnAirView" owner:self options:nil];
        else
            [[NSBundle mainBundle] loadNibNamed:@"OnAirView_480" owner:self options:nil];
        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        bInitEnd = NO;
        //
//        [self deleteAllImgFile];
        bShowNoti = NO;
        bHaveEventUrl = NO;

        if(IS_IPHONE5)
        {
            [self setFrame:CGRectMake(0, 0, self.frame.size.width, 517)];
            picH = imgH;
        }
        else
        {
            [self setFrame:CGRectMake(0, 0, self.frame.size.width, 440)];
            picH = imgH_S;
        }

        strEventDetailUrl  = [[NSString alloc]init];
        
        mainScr = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//        [mainScr setBackgroundColor:[UIColor redColor]];
        [mainScr setDelegate:self];
        
        selectBookCol = 0;
        streamIndex = -1;
        
        againList = [[NSMutableArray alloc]initWithCapacity:0];
        bookInfoList = [[NSMutableArray alloc]initWithCapacity:0];
        boardTextList = [[NSMutableArray alloc]initWithCapacity:0];
        boardingDate = [[NSMutableDictionary alloc]init];
        programDate = [[NSMutableDictionary alloc]init];
        streamingData = [[NSMutableDictionary alloc]init];
        notiData = [[NSMutableDictionary alloc]init];
        _strCheckId = [[NSString alloc]init];
        strOldPd = [[NSString alloc]init];

        if([[NSUserDefaults standardUserDefaults] integerForKey:@"StartFm"] == 2)
            nSelectedMenu = 2; // i-radio
        else
            nSelectedMenu = 1; // fm
        
        /* ------------------------------------------------------ *
         * begin - 외부 URL호출에 의해 실행되었을 경우, 채널을 해당 채널로 재설정한다.(일회성)
         * @ 관련 : Bandi2AppDeligate.(h,m)
         * @ date : 2016-11-08
         * ------------------------------------------------------ */
        NSString *urlSelectedChannel = SharedAppDelegate.urlSelectedChannel;
        NSLog(@"urlch - %@", urlSelectedChannel);

        if (urlSelectedChannel)
        {
            if ([@"fm" isEqualToString:urlSelectedChannel])
            {
                nSelectedMenu = 1;
            }
            else if ([@"iradio" isEqualToString:urlSelectedChannel])
            {
                nSelectedMenu = 2;
            }
            
            SharedAppDelegate.urlSelectedChannel = nil; // it's a one-time variable.
        }
        /* ------------------------------------------------------ *
         * end - 외부 URL호출에 의해 실행되었을 경우, 채널을 해당 채널로 재설정한다.
         * ------------------------------------------------------ */
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",nSelectedMenu] forKey:@"ChannerNum"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [self selectMenuIndexUi:nSelectedMenu];
        
        bPlayerShowOnairState = YES;
        
        _keypadHideBt.frame = self.frame;
        _keypadHideBt.hidden = YES;
        
        CGRect newFrame = _wrapperView.frame;
        newFrame.size.height = bodyHeight;
        _wrapperView.frame = CGRectMake(0, 0, 320, bodyHeight);
        
        [self.radioTabView setFrame:CGRectMake(0, 0, 320, 45)];
        [_wrapperView addSubview:self.radioTabView];

        [_programImageView setFrame:CGRectMake(0, 45, 320.0f, bodyHeight-textScrollerHeight)];
        [_programImage setFrame:CGRectMake(0, 0, 320.0f, bodyHeight-(tabMenuCloseHeight + textScrollerHeight))]; // xib 파일에서 auto layout 해제해야 함.
        
        _programImageView.backgroundColor = [UIColor whiteColor];

        [self.radioToolView setFrame:CGRectMake(0, 0, 320, 50)];
        [_programImageView addSubview:self.radioToolView];
        [self bringSubviewToFront:self.radioToolView];
        
        _againPlayerInfo.hidden = YES;

        [_wrapperView addSubview:_programImageView];
        [_wrapperView addSubview:_boardView];
        [_boardView setFrame:CGRectMake(0, bodyHeight-(textScrollerHeight+tabMenuCloseHeight), 320.0f, textScrollerHeight)];
        [_wrapperView bringSubviewToFront:_boardView];
//        
        
        int bandiBoardViewHeight;
        if(IS_IPHONE5)
            bandiBoardViewHeight = 145;
        else
            bandiBoardViewHeight = 100;
        _bandiBoardView = [[BandiBoardSmallView alloc]initWithFrame:CGRectMake(0,
                                                                               bodyHeight-tabMenuCloseHeight-bandiBoardViewHeight, 320, bandiBoardViewHeight)];
        
        [_wrapperView addSubview:_bandiBoardView];
        _bandiBoardView.hidden = YES;
        
        [self.radioSubTabView setFrame:CGRectMake(0, 0, 320.0f, tabMenuCloseHeight)];
        [_wrapperView addSubview:self.radioSubTabView];
        [self bringSubviewToFront:self.radioToolView];
        
        
        if(IS_IPHONE5)
            [self.scrListView setContentSize:CGSizeMake(320, 140)];
        else
            [self.scrListView setContentSize:CGSizeMake(320, 95)];
        
        self.pgControl.currentPage = 0;
        self.pgControl.numberOfPages = 4;
        
        [self.scrListView addSubview:self.tbAgain];
        [self.tbAgain setDelegate:self];
        
        [mainScr addSubview:_wrapperView];

        [mainScr bringSubviewToFront:self.radioSubTabView];
        
        [self addSubview:mainScr];
        mainScr.bounces = NO;
        
        [mainScr setContentSize:CGSizeMake(320, 517)];
        [self addSubview:_keypadHideBt];

        _WcView.hidden = YES;
        _btFullScreen.hidden = YES;
        nSelectedTabMenu = 0;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playIPradio:)
                                                     name:@"selectIradio"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playFmPradio:)
                                                     name:@"selectFmradio"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerStart:)
                                                     name:@"MainPlayerPlay"
                                                   object:nil];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(eplayPlay:)
                                                     name:@"PlayerPlay"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reLoadOnairView:)
                                                     name:@"reloadOnairView"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(showGplayer:)
                                                     name:@"GPLAYER"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(showOnairPlayer:)
                                                     name:@"ONAIR"
                                                   object:nil];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playIPradio:)
                                                     name:@"selectIradioFromSch"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playIPradio:)
                                                     name:@"selectIradioFromBoard"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playFmPradio:)
                                                     name:@"selectFmradioFromSch"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playFmPradio:)
                                                     name:@"selectFmradioFromBoard"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillAnimate:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillAnimate:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        
        //
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(loginStateIcon:)
                                                     name:@"LoginStateIcon"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(viewDidAppearFromSupser:)
                                                     name:@"viewDidAppearFormSuperView"
                                                   object:nil];
        

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(guideEnd:)
                                                     name:@"guideEnd"
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(guideStop:)
                                                     name:@"STOP_AUDIO"
                                                   object:nil];
        
        
        
        
//        nSelectedMenu ==1 ) [ServerSide saveEventLog:@"SongTable"];
//        else if (nSelectedMenu ==2 ) [ServerSide saveEventLog:@"SongTableIradio"];
        
        [self getAppver];
        
        //** Google Analytics **
        if (nSelectedMenu == 1) {
            [GA sendScreenView:@"OnAir" withChannel:GA_FM];
        } else if (nSelectedMenu == 2) {
            [GA sendScreenView:@"OnAir" withChannel:GA_IRADIO];
        } else {
            [GA sendScreenView:@"OnAir" withChannel:GA_UNKNOWN];
        }
        
    }
    return self;
}

-(void)initLoad
{
    
    [self setProgramImageCaller];
    progImgTimer = [NSTimer scheduledTimerWithTimeInterval:60.0f
                                                    target:self
                                                  selector:@selector(setProgramImageCaller)
                                                  userInfo:nil
                                                   repeats:YES];
    
    [_writeBulletinBtn addTarget:self action:@selector(writeBulletin:) forControlEvents:UIControlEventTouchUpInside];

    
    [self loadNoti];

    [self getBookInfo:[boardingDate valueForKey:@"id"]];
    //    player =[Util getPlayerView:self];

    [self setPlayerFrame];
    rollingIdxInit = YES;
    [self setRollingTextCaller];
    
    if(nSelectedMenu ==2)
        [player loadSound:[streamingData valueForKey:@"streamUrlIradioIos"]];
    else
        [player loadSound:[streamingData valueForKey:@"streamUrlIos"]];
    
    if (nSelectedMenu ==1 ) [ServerSide saveEventLog:@"OnAir"];
    else if (nSelectedMenu ==2 ) [ServerSide saveEventLog:@"OnAirIradio"];
    
}


-(void)playerViewSetting:(PlayerView *)uv
{
    player = uv;
    if(!bInitPlayer)
    {
        if(nSelectedMenu ==2)
            [player loadSound:[streamingData valueForKey:@"streamUrlIradioIos"]];
        else
            [player loadSound:[streamingData valueForKey:@"streamUrlIos"]];
    }
    
    [self tabSetting];
}

-(void)playerViewSettingNonInit:(PlayerView *)uv
{
    player = uv;
    [self tabSetting];
    
}
- (void)onUnload
{
    
    if (progImgTimer.isValid) {
        [progImgTimer invalidate];
    }
    if (rollingTextTimer.isValid) {
        [rollingTextTimer invalidate];
    }
    
    runLoop = NO;
    
    NSLog(@"Unload : %@", [self class]);
}


#pragma mark - 프로그램 이미지

- (void)setProgramImageCaller
{
    [NSThread detachNewThreadSelector:@selector(setProgramImage) toTarget:self withObject:nil];
}


- (void)setProgramImage
{
    if (SharedAppDelegate.isBackground) {
        return;
    }
    [self getBroadInfo];
    if(nSelectedTabMenu == 1)
    {
        if(bShowTabMenu)
        {
             [self getBookInfo:[boardingDate valueForKey:@"id"]];
            return;
        }
    }
    [self getAgainInfo:[boardingDate valueForKey:@"id"] page:1 autoC:YES];
    
  
//    strOldPd = [boardingDate valueForKey:@"id"]
}



# pragma mark - rolling text
- (void)setRollingTextCaller
{
    [NSThread detachNewThreadSelector:@selector(setRollingText) toTarget:self withObject:nil];
}

- (void)setRollingText
{
    if (SharedAppDelegate.isBackground) {
//        NSLog(@"rolling text background.");
        rollingTextTimer = [NSTimer scheduledTimerWithTimeInterval:60.0
                                                            target:self
                                                          selector:@selector(setRollingText)
                                                          userInfo:nil
                                                           repeats:NO];
        
        return;
    }
  
    CGRect newFrame = _rollingTextView.frame;
    [_rollingTextView removeFromSuperview];
    _rollingTextView = [[UIScrollView alloc] init];
    _rollingTextView.frame = newFrame;
    [_boardView addSubview:_rollingTextView];
    
    
    float lineHeight = _rollingTextView.frame.size.height;
    float lineWidth = _rollingTextView.frame.size.width;

    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"UseBoard"] isEqualToString:@"Y"])
    {
        
        for (int i=0; i<boardTextList.count; i++) {
            //NSLog(@"rollingText(%d) -> %@", i, msgArr[i]);
            UILabel *label = [[UILabel alloc] init];
            
            if([[boardTextList objectAtIndex:i] valueForKey:@"contents"] != (id)[NSNull null])
            {
                label.text = [Util xmlEntitiesDecode:[[boardTextList objectAtIndex:i] valueForKey:@"contents"]];
//                label.text = [[boardTextList objectAtIndex:i] valueForKey:@"contents"];
            }
            else
            {
                label.text = @"";
            }
            
            [label setFont:[UIFont systemFontOfSize:14]];
            int posY = lineHeight * i;
            label.frame = CGRectMake(0, posY, lineWidth, lineHeight);
            [_rollingTextView addSubview:label];
        }

    }
    else
    {
        UILabel *label = [[UILabel alloc] init];
        label.text = @"해당 프로그램은 반디게시판이 운영되지 않습니다.";
        
        [label setFont:[UIFont systemFontOfSize:13]];
        label.frame = CGRectMake(0, 0, _rollingTextView.frame.size.width, 30);
        [_rollingTextView addSubview:label];
    }
    _rollingTextView.contentSize = CGSizeMake(lineWidth, lineHeight * boardTextList.count);

    NSArray *arr = @[[NSNumber numberWithInt:1], [NSNumber numberWithUnsignedInteger:boardTextList.count]];

    if (boardTextList.count > 1 && ([[[NSUserDefaults standardUserDefaults] valueForKey:@"UseBoard"] isEqualToString:@"Y"])) {
        runLoop = YES;
        [self loopRollingText:arr];
    } else {
        NSLog(@"rolling text retry after 60 sec");
        rollingTextTimer = [NSTimer scheduledTimerWithTimeInterval:60.0
                                                            target:self
                                                          selector:@selector(setRollingText)
                                                          userInfo:nil
                                                           repeats:NO];
    }
    [_boardView bringSubviewToFront:_writeBulletinBtn];
    
    
}


- (void)loopRollingText:(NSArray *)params
{
    int idx = [params[0] intValue];
    int total = [params[1] intValue];
    //NSLog(@"loopRollingText - %d, %d", idx, total);
    
    CGRect frame = _rollingTextView.frame;
    CGPoint bottomOffset = CGPointMake(0, frame.size.height * idx);
    if(playingAni)
        return;
    playingAni = YES;
    [UIView animateWithDuration:3.0
                     animations:^{
                         _rollingTextView.contentOffset = bottomOffset;
                     }
                     completion:^(BOOL finished){
                             playingAni = NO;
                         int idx2 = idx;
                         if (idx == (total-1)) {
                             _rollingTextView.contentOffset = CGPointZero;
                             idx2 = 0;
                             [self setRollingText];
                             return;
                         } else {
                             int nIndx = idx2+1;
                             if(rollingIdxInit)
                             {
                                 nIndx= 0;
                                 rollingIdxInit = NO;
                                 
                             
                             }
                                
                             NSArray *params2 = @[[NSNumber numberWithUnsignedInt:(nIndx)], params[1]];
                             if (runLoop) {
                                 [self performSelector:@selector(loopRollingText:) withObject:params2 afterDelay:2];
                             }
                         }
                     }];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)launchAod:(id)sender {
    NSLog(@"launchAod");
    WebViewController *vc = [[WebViewController alloc] init];
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
        NSLog(@"self -> %@",vc);
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiMobileUrl?progcd=%@", progcd];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSError *error;
        NSString *xmlStr = [[NSString alloc] initWithContentsOfURL:url
                                            encoding:NSUTF8StringEncoding
                                                error:&error];
        TBXML *tbxml = [TBXML newTBXMLWithXMLString:xmlStr error:&error];
        TBXMLElement *elemRoot=nil, *elemRescd=nil, *elemData=nil, *elemMobileUrl=nil;
        NSString *rescdText=nil;
        
        elemRoot = tbxml.rootXMLElement;
        
        if (elemRoot) {
            elemRescd = [TBXML childElementNamed:@"rescd" parentElement:elemRoot];
            if (elemRescd) {
                rescdText = [TBXML textForElement:elemRescd];
            }
            
            if ([rescdText isEqualToString:@"0000"]) {
                elemData = [TBXML childElementNamed:@"data" parentElement:elemRoot];
                elemMobileUrl = [TBXML childElementNamed:@"mobile-url" parentElement:elemData];
                urlStr = [TBXML textForElement:elemMobileUrl];
                url = [NSURL URLWithString:urlStr];
                NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
                
                [(UIWebView *)[vc.view.subviews lastObject] loadRequest:urlRequest];
                //NSLog(@"%@", [vc.view.subviews lastObject]);
            }
        }
    }];
    
//    [ServerSide saveEventLog:@"Aod"];
    
}

- (void)playAgain:(NSNotification *)notification
{
    NSLog(@"playAgin notification");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"playAgain" object:nil];
    

    if (againSongTimer != nil) {
        [againSongTimer invalidate];
        againSongTimer = nil;
        streamIndex = -1;
    }
    
//    if(songPlayer)
//        [songPlayer pause];
//    [player.audioPlayer play];
    _againPlayerInfo.hidden = YES;
    streamPlaying = NO;
    if(ep)
    {
        [ep play];
    }

    
}
- (void)playIPradio:(NSNotification *)notification
{

    [self selectMenuIndex:2];
//    [self getBoardTextData];
}

- (void)playFmPradio:(NSNotification *)notification
{
    [self selectMenuIndex:1];
//    [self getBoardTextData];
}
- (void)loginStateIcon:(NSNotification *)notification
{

    [self getBroadInfo];
}
- (void)viewDidAppearFromSupser:(NSNotification *)notification
{
    if (nSelectedTabMenu > 0) {
        float ttt = (nSelectedTabMenu -1) * 320;
        [_scrListView setContentOffset:CGPointMake(ttt, 0) animated:NO];

    }
}


- (void)guideStop:(NSNotification *)notification
{

    [player stopAudio];
    if(songPlayer)
    {
        [songPlayer pause];
        [songPlayer removeObserver:self forKeyPath:@"status"];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:[songPlayer currentItem]];
        songPlayer = nil;
        
    }
    if (againSongTimer != nil) {
        [againSongTimer invalidate];
        againSongTimer = nil;
    }
    streamIndex = -1;
    streamPlaying = NO;
    
    _againPlayerInfo.hidden = YES;

    NSLog(@"stop");
}

- (void)guideEnd:(NSNotification *)notification
{
    if(bHaveEvt)
    {
        _notiView.hidden = NO;
        
        [_notiView setFrame:CGRectMake( 0, 0, _notiView.frame.size.width, _notiView.frame.size.height)];
        [[UIApplication sharedApplication].keyWindow addSubview:_notiView];
        [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_notiView];
        SharedAppDelegate.isOpenTt = NO;
    }
    else
    {
        SharedAppDelegate.isOpenTt = NO;
    }
    
}




- (void)playerStart:(NSNotification *)notification
{
    if(songPlayer)
    {
        [songPlayer pause];
        [songPlayer removeObserver:self forKeyPath:@"status"];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:[songPlayer currentItem]];
        songPlayer = nil;
        
    }
    if (againSongTimer != nil) {
        [againSongTimer invalidate];
        againSongTimer = nil;
    }
    streamIndex = -1;
    streamPlaying = NO;
    
    _againPlayerInfo.hidden = YES;

    [self.tbAgain reloadData];
}

- (IBAction)watchRadio:(id)sender {
    NSLog(@"watchRadio");
    
    if(_vRadioBtn.selected)
    {
        [ep stop];
        ep.playerDelegate = nil;
        ep = nil;
        
        _vRadioBtn.selected = NO;
        _WcView.hidden = YES;
        _btFullScreen.hidden = YES;
        _bandiBoardView.hidden = YES;
        _boardView.hidden = NO;
        
        if(videoControllerTimer != nil)
        {
            [videoControllerTimer invalidate];
            videoControllerTimer = nil;
        }



        [_bandiBoardView.txtField resignFirstResponder];

        [player vodPlay:NO];
        [player loadSound:[streamingData valueForKey:@"streamUrlIos"]];
        
        
        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];;
        if(!bShowTabMenu)
            [_boardView setFrame:CGRectMake(0, bodyHeight-(textScrollerHeight+tabMenuCloseHeight), 320.0f, textScrollerHeight)];

        float posY = _boardView.frame.origin.y-40;
        
        if(bPlayerShowOnairState)
        {
            [player setFrame:CGRectMake(0, posY, player.frame.size.width, 40)];
            [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];
        }
   
        [self tabSetting];
        
  

        return;
    }
    
    
//     if(![[programDate valueForKey:@"vfm"] isEqualToString:@"Yes"])
//     {
//         
//         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                         message:@"해당 프로그램은 보이는 라디오 서비스를 제공하지 않습니다"
//                                                        delegate:nil
//                                               cancelButtonTitle:@"닫기"
//                                               otherButtonTitles:nil ];
//         [alert show];
//         return;
//     }
//
    [player vodPlay:YES];

  
    [songPlayer pause];
    [player stopAudio];
    streamIndex = -1;
    [self.tbAgain reloadData];
        
    NSString *hostIP = [Util getHostIP:@"ebsonairios.ebs.co.kr"];
    NSString *streamURL = [NSString stringWithFormat:@"http://%@/fmradiotablet500k/tablet500k/playlist.m3u8", hostIP];
    NSLog(@"streaming(vod) url : %@", streamURL);
    [self startVideo:streamURL];

    _boardView.hidden = NO;
    _bandiBoardView.hidden = NO;

    if(bPlayerShowOnairState)
    {
        [self tabSetting];
    }

    if(videoControllerTimer != nil)
    {
        [videoControllerTimer invalidate];
        videoControllerTimer = nil;
    }
    videoControllerTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f
                                                    target:self
                                                  selector:@selector(hideVideControll)
                                                  userInfo:nil
                                                   repeats:YES];

    return;
 
}
-(void)reloadBoarding
{
    [self getBroadInfo];
}
-(void)loadPlayerBt
{
    [player audioInfo:programDate];
}
-(void)loadBookList
{

    [self getAgainInfo:[boardingDate valueForKey:@"id"]  page:1 autoC:NO];
}

- (IBAction)launchTwitter:(id)sender {
    NSLog(@"twitter -> %@", pdDic[@"TWITTER"]);
    
    WebViewController *vc = [[WebViewController alloc] init];
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
        NSLog(@"self -> %@",vc);
        NSString *twitterUrl = pdDic[@"TWITTER"];
        
        if (! [twitterUrl isEqualToString:@""]) {
                NSURL *url = [NSURL URLWithString:twitterUrl];
                NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
                
                [(UIWebView *)[vc.view.subviews lastObject] loadRequest:urlRequest];
        }
    }];
    
}

- (IBAction)launchFacebook:(id)sender {
    
    WebViewController *vc = [[WebViewController alloc] init];
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
        NSLog(@"self -> %@",vc);
        NSString *facebookUrl = facebookLink; //pdDic[@"TWITTER"];
        
        if (! [facebookUrl isEqualToString:@""]) {
            NSURL *url = [NSURL URLWithString:facebookUrl];
            NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
            
            [(UIWebView *)[vc.view.subviews lastObject] loadRequest:urlRequest];
        }
    }];
}


- (IBAction)writeBulletin:(id)sender {
    

    [[NSNotificationCenter defaultCenter] postNotificationName:@"MoveTab3" object:nil];
    return;
    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
    
    CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);
    
    SharedAppDelegate.latestMenu = @"온에어";
    
    if ([ServerSide autoLogin]) {
        BandiBoardWriteView *myView = [[BandiBoardWriteView alloc] initWithFrame:frameRect];
        [cc.view addSubview:myView];
        [cc.view addSubview:myView];
        [cc.view bringSubviewToFront:myView];
        cc.navigationItem.title = @"반디게시판";
    } else {
        

        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginN" object:nil];
    }
}

///

-(IBAction)selectMenu:(id)sender
{

    [_bandiBoardView.txtField resignFirstResponder];
    NSLog(@"select tab = %d",[sender tag]);
    [self selectMenuIndex:[sender tag]];

   

}

-(void)selectMenuIndexUi:(int)nIndex
{
    if(nIndex == 1)
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(63,56,51)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(158,145,136)];
        self.btMenuReadRadio.selected = YES;
        self.btMenuEngRadio.selected = NO;
        if(bFineCh)
        {
            _vRadioBtn.hidden = NO;
            _seeRadioText.hidden = NO;
        }
        else
        {
            _vRadioBtn.hidden = YES;
            _seeRadioText.hidden = YES;
        }
        
        
    }
    else if(nIndex == 2)
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(158,145,136)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(63,56,51)];
        self.btMenuReadRadio.selected = NO;
        self.btMenuEngRadio.selected = YES;
        _vRadioBtn.hidden = YES;
        _seeRadioText.hidden = YES;
    }

}
-(void)selectMenuIndex:(int)nIndex //1 책읽어주는 라디오 // 2외국어 라디오
{
    if(_vRadioBtn.selected)
        [self watchRadio:nil];

    if (nSelectedMenu == nIndex)
        return;
    
    if(bShowTabMenu)
        [self selectTabIndex:nSelectedTabMenu];


    
  
    if(nIndex == 1)
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(63,56,51)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(158,145,136)];
        self.btMenuReadRadio.selected = YES;
        self.btMenuEngRadio.selected = NO;
        if (againSongTimer != nil) {
            [againSongTimer invalidate];
            againSongTimer = nil;
            streamIndex = -1;
        }
        [songPlayer pause];
        [player stopAudio];
        [player loadSound:[streamingData valueForKey:@"streamUrlIos"]];
        _vRadioBtn.hidden = NO;
        _seeRadioText.hidden = NO;

        
    }
    else if(nIndex == 2)
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(158,145,136)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(63,56,51)];
        self.btMenuReadRadio.selected = NO;
        self.btMenuEngRadio.selected = YES;

        if (againSongTimer != nil) {
            [againSongTimer invalidate];
            againSongTimer = nil;
            streamIndex = -1;
        }

        [songPlayer pause];
        [player stopAudio];
        [player loadSound:[streamingData valueForKey:@"streamUrlIradioIos"]];
        _vRadioBtn.hidden = YES;
        _seeRadioText.hidden = YES;

    }

    _againPlayerInfo.hidden = YES;
    
    nSelectedMenu = nIndex;
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",nSelectedMenu] forKey:@"ChannerNum"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self performSelector:@selector(reloadBoarding) withObject:nil afterDelay:0.0f];

}

-(IBAction)selectTab:(id)sender
{
    [self selectTabIndex:(int)[sender tag]];
    [self.scrListView setContentOffset:CGPointMake(320*([sender tag]-1), 0) animated:NO];
}

-(void)selectTabIndex:(int)nIndex
{
    
    if(nSelectedTabMenu == nIndex)
    {
        nIndex = 0;
    }
    if(nIndex != 1)
    {
        if(streamPlaying)
            [self songPlayerStop];
    }
    

    switch (nIndex) {
        case 0:
            [self.btTabAgain setBackgroundColor:RgbColor(125,116,107)];
            [self.btTabBookInfo setBackgroundColor:RgbColor(137,128,119)];
            [self.btTabBroadInfo setBackgroundColor:RgbColor(158,145,136)];
            [self.btTabSongList setBackgroundColor:RgbColor(172,158,147)];
            self.btTabAgain.selected = NO;
            self.btTabBookInfo.selected = NO;
            self.btTabBroadInfo.selected = NO;
            self.btTabSongList.selected = NO;
            break;
        case 1:
            [self.btTabAgain setBackgroundColor:RgbColor(63,56,51)];
            [self.btTabBookInfo setBackgroundColor:RgbColor(137,128,119)];
            [self.btTabBroadInfo setBackgroundColor:RgbColor(158,145,136)];
            [self.btTabSongList setBackgroundColor:RgbColor(172,158,147)];
            self.btTabAgain.selected = YES;
            self.btTabBookInfo.selected = NO;
            self.btTabBroadInfo.selected = NO;
            self.btTabSongList.selected = NO;
           
            if (nSelectedMenu ==1 ) [ServerSide saveEventLog:@"Aod"];
            else if (nSelectedMenu ==2 ) [ServerSide saveEventLog:@"AodIradio"];

            break;
        case 2:
            [self.btTabAgain setBackgroundColor:RgbColor(125,116,107)];
            [self.btTabBookInfo setBackgroundColor:RgbColor(63,56,51)];
            [self.btTabBroadInfo setBackgroundColor:RgbColor(158,145,136)];
            [self.btTabSongList setBackgroundColor:RgbColor(172,158,147)];
            self.btTabAgain.selected = NO;
            self.btTabBookInfo.selected = YES;
            self.btTabBroadInfo.selected = NO;
            self.btTabSongList.selected = NO;

            if (nSelectedMenu ==1 ) [ServerSide saveEventLog:@"BookInfo"];
            else if (nSelectedMenu ==2 ) [ServerSide saveEventLog:@"BookInfoIradio"];

            break;
        case 3:
            [self.btTabAgain setBackgroundColor:RgbColor(125,116,107)];
            [self.btTabBookInfo setBackgroundColor:RgbColor(137,128,119)];
            [self.btTabBroadInfo setBackgroundColor:RgbColor(63,56,51)];
            [self.btTabSongList setBackgroundColor:RgbColor(172,158,147)];
            self.btTabAgain.selected = NO;
            self.btTabBookInfo.selected = NO;
            self.btTabBroadInfo.selected = YES;
            self.btTabSongList.selected = NO;
            
            if (nSelectedMenu ==1 ) [ServerSide saveEventLog:@"AirContents"];
            else if (nSelectedMenu ==2 ) [ServerSide saveEventLog:@"AirContentsIradio"];
            
            break;
        case 4:
            [self.btTabAgain setBackgroundColor:RgbColor(125,116,107)];
            [self.btTabBookInfo setBackgroundColor:RgbColor(137,128,119)];
            [self.btTabBroadInfo setBackgroundColor:RgbColor(158,145,136)];
            [self.btTabSongList setBackgroundColor:RgbColor(63,56,51)];
            self.btTabAgain.selected = NO;
            self.btTabBookInfo.selected = NO;
            self.btTabBroadInfo.selected = NO;
            self.btTabSongList.selected = YES;
            break;
            
            if (nSelectedMenu ==1 ) [ServerSide saveEventLog:@"SongTable"];
            else if (nSelectedMenu ==2 ) [ServerSide saveEventLog:@"SongTableIradio"];
            
        default:
            break;
    }

    if(!bShowTabMenu)
        [self getAgainInfo:[boardingDate valueForKey:@"id"] page:1 autoC:YES];
    

  
    nSelectedTabMenu = nIndex;
    
    if(nIndex != 0)
    {
        if(!bShowTabMenu)
            [self openTab:YES];
        else
            [self openTabSetScr];
    }
    else
        [self openTab:NO];
    
    
    //** Google Analytics **
    NSString *screen = TAG;
    GAChannel channel = GA_UNKNOWN;
    switch (nIndex)
    {
        case 1:
            screen = @"Aod";
            break;
        case 2:
            screen = @"BookInfo";
            break;
        case 3:
            screen = @"AirContents";
            break;
        case 4:
            screen = @"SongTable";
            break;
        default:
            screen = TAG;
    }
    if (nSelectedMenu == 1) channel = GA_FM;
    else if (nSelectedMenu == 2) channel = GA_IRADIO;
    
    [GA sendScreenView:screen withChannel:channel];
}


/**
 * 사용하지 않는듯.. (2017-04-06 by iford)
 */
-(void)settingTabOpen
{
    if(nSelectedTabMenu != 0)
    {
        if(!bShowTabMenu)
            [self openTab:YES];
        else
            [self openTabSetScr];
    }
    else
        [self openTab:NO];
}

-(void)openTab:(BOOL)bShow
{
    
    int bodyHeight = [SharedAppDelegate.bodyHeight intValue];;

    if(!bShow)
    {
        if(!bShowTabMenu)
            return;
        if(bFineCh && [programDate valueForKey:@"appImg"] != (id)[NSNull null])
        {
            /*
            UIImage *img = [self checkImage:[programDate valueForKey:@"appImg"] bBigMode:YES];
            UIImage * resizeImage = [self imageWithImage:img scaledToWidth:320];
            _programImage.image =resizeImage;
            
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            resizeImage.size.height);
             */
            UIImage *img = [self checkImage:[programDate valueForKey:@"appImg"] bBigMode:YES];
            _programImage.image =img;
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            picH);


        }
        else
        {
  
//            UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg2.png"] scaledToWidth:320];
            _programImage.image = [UIImage imageNamed:@"bandi_main_bg2.png"];
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            picH);
            
        }

        [_programImageView setFrame:CGRectMake(0, 45, 320.0f, bodyHeight-36)];

        [_boardView setFrame:CGRectMake(0, bodyHeight-(textScrollerHeight+tabMenuCloseHeight), 320.0f, textScrollerHeight)];
        [self.radioSubTabView setFrame:CGRectMake(0, bodyHeight-tabMenuCloseHeight, 320.0f, tabMenuCloseHeight)];
       
        float playerHeight = 40.0f;

        if(_vRadioBtn.selected)
        {
            int bandiBoardViewHeight;
            if(IS_IPHONE5)
                bandiBoardViewHeight = 145;
            else
                bandiBoardViewHeight = 100;
            int bandiBdY = bodyHeight-tabMenuCloseHeight-bandiBoardViewHeight;
            float posY = bandiBdY -playerHeight - _boardView.frame.size.height;
            if(bPlayerShowOnairState)
            {
                _boardView.frame = CGRectMake(_boardView.frame.origin.x,
                                              -100,
                                              _boardView.frame.size.width,
                                              _boardView.frame.size.height);

                
                [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
                int nMarg = 0;

                int bandiBoardH = (bodyHeight-tabMenuCloseHeight) - (posY+playerHeight) - nMarg;
                [_bandiBoardView setFrame:CGRectMake(_bandiBoardView.frame.origin.x,
                                                    posY + playerHeight,
                                                     _bandiBoardView.frame.size.width,
                                                     bandiBoardH)];
                
                [_bandiBoardView bandiBoardShowBig];
                [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];
            }
        }
        else
        {

            float posY = bodyHeight-(textScrollerHeight+tabMenuCloseHeight) -playerHeight;
            if(bPlayerShowOnairState)
            {
                [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
                [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];
            }
        }

        bShowTabMenu = bShow;
        
        if(streamPlaying)
            [self songPlayerStop];
        [self.tbAgain reloadData];
        [self openTabSetScr];

        
    }
    else
    {
        if(bShowTabMenu)
            return;

     
       
        if(bFineCh && [programDate valueForKey:@"appImg2"] != (id)[NSNull null])
        {
            UIImage *img = [self checkImage:[programDate valueForKey:@"appImg2"] bBigMode:NO];
            UIImage * resizeImage = [self imageWithImage:img scaledToWidth:320];
            _programImage.image =resizeImage;
            
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            resizeImage.size.height);

       
        }
        else
        {
            UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg.png"] scaledToWidth:320];
            _programImage.image = resizeImage;
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            resizeImage.size.height);
        }
        
        [_programImageView setFrame:CGRectMake(0, 45, 320.0f, bodyHeight-textScrollerHeight)];
        int menuHeight;
        if(IS_IPHONE5)
            menuHeight =tabMenuOpenHeight;
        else
            menuHeight =tabMenuOpenHeight_S;

        [_boardView setFrame:CGRectMake(0, bodyHeight-(textScrollerHeight+menuHeight), 320.0f, textScrollerHeight)];
        [self.radioSubTabView setFrame:CGRectMake(0, bodyHeight-menuHeight, 320.0f, menuHeight)];

        float playerHeight = 40.0f;

        if(_vRadioBtn.selected)
        {
            
            int bandiBoardViewHeight;
            if(IS_IPHONE5)
                bandiBoardViewHeight = 145;
            else
                bandiBoardViewHeight = 100;
            int bandiBdY = bodyHeight-tabMenuCloseHeight-bandiBoardViewHeight;
            
            float posY = bandiBdY -playerHeight - _boardView.frame.size.height;
            if(bPlayerShowOnairState)
            {
                [_wrapperView  bringSubviewToFront:_boardView];
                _boardView.frame = CGRectMake(_boardView.frame.origin.x,
                                              bandiBdY -_boardView.frame.size.height,
                                              _boardView.frame.size.width,
                                              _boardView.frame.size.height);
                [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
            }
        }
        else
        {
            if(IS_IPHONE5)
                menuHeight =tabMenuOpenHeight;
            else
                menuHeight =tabMenuOpenHeight_S;
            float posY = bodyHeight-(textScrollerHeight+menuHeight) -playerHeight;

            if(bPlayerShowOnairState)
            {
                [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];

            }
        }
        bShowTabMenu = bShow;

    }
     [self openTabSetScr];
}

-(void)tabSetting
{
    int bodyHeight = [SharedAppDelegate.bodyHeight intValue];;
    NSLog(@"2_wabFrame %f/%f",_wrapperView.frame.origin.y,_wrapperView.frame.size.height);
    if(!bShowTabMenu)
    {
        NSLog(@"programDate = %@",programDate);
        if(bFineCh)
        {
            if ([programDate valueForKey:@"appImg"] != (id)[NSNull null])
            {
                
                UIImage *img = [self checkImage:[programDate valueForKey:@"appImg"] bBigMode:YES];
//                UIImage * resizeImage = [self imageWithImage:img scaledToWidth:320];
                _programImage.image =img;
                
                _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                                _programImage.frame.origin.y,
                                                320,
                                                picH);
        
            }
            else
            {
                
//                UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg2.png"] scaledToWidth:320];
                _programImage.image = [UIImage imageNamed:@"bandi_main_bg2.png"];
                _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                                _programImage.frame.origin.y,
                                                320,
                                                picH);
                
            }
            
            
        }
        else
        {
            
//            UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg2.png"] scaledToWidth:320];
            _programImage.image = [UIImage imageNamed:@"bandi_main_bg2.png"];
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            picH);
        }
        
        
        
        [_programImageView setFrame:CGRectMake(0, 45, 320.0f, bodyHeight-36)];
        
        [_boardView setFrame:CGRectMake(0, bodyHeight-(textScrollerHeight+tabMenuCloseHeight), 320.0f, textScrollerHeight)];
        [self.radioSubTabView setFrame:CGRectMake(0, bodyHeight-tabMenuCloseHeight, 320.0f, tabMenuCloseHeight)];
        float playerHeight = 40.0f;
        if(_vRadioBtn.selected)
        {
            int bandiBoardViewHeight;
            if(IS_IPHONE5)
                bandiBoardViewHeight = 145;
            else
                bandiBoardViewHeight = 100;
            int bandiBdY = bodyHeight-tabMenuCloseHeight-bandiBoardViewHeight;
            float posY = bandiBdY -playerHeight - _boardView.frame.size.height;
            if(bPlayerShowOnairState)
            {
                _boardView.frame = CGRectMake(_boardView.frame.origin.x,
                                              -100,
                                              _boardView.frame.size.width,
                                              _boardView.frame.size.height);
                
                
                [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
                int nMarg = 0;
//                if(IS_IPHONE5)
//                    nMarg = 8;
                int bandiBoardH = (bodyHeight-tabMenuCloseHeight) - (posY+playerHeight) - nMarg;
                [_bandiBoardView setFrame:CGRectMake(_bandiBoardView.frame.origin.x,
                                                     posY + playerHeight,
                                                     _bandiBoardView.frame.size.width,
                                                     bandiBoardH)];
                
                [_bandiBoardView bandiBoardShowBig];
                [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];
            }
        }
        else
        {
            
            //318,454
            float posY = bodyHeight-(textScrollerHeight+tabMenuCloseHeight) -playerHeight;
            if(bPlayerShowOnairState)
            {
                [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
                [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];

            }
        }
    }
    else
    {
        if(bFineCh)
        {
            NSLog(@"programDate = %@",programDate);

            if ([programDate valueForKey:@"appImg2"] != (id)[NSNull null])
            {
                
                UIImage *img = [self checkImage:[programDate valueForKey:@"appImg2"] bBigMode:NO];
                UIImage * resizeImage = [self imageWithImage:img scaledToWidth:320];
                _programImage.image =resizeImage;
                
                _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                                _programImage.frame.origin.y,
                                                320,
                                                resizeImage.size.height);
     
            }
            else
            {
                
                UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg.png"] scaledToWidth:320];
                _programImage.image = resizeImage;
                _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                                _programImage.frame.origin.y,
                                                320,
                                                resizeImage.size.height);
                
            }

          
        }
        else
        {
            UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg.png"] scaledToWidth:320];
            _programImage.image = resizeImage;
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            resizeImage.size.height);
            
//            _programImage.image = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg2.png"] scaledToWidth:320];
        }

        int menuHeight;
        if(IS_IPHONE5)
            menuHeight =tabMenuOpenHeight;
        else
            menuHeight =tabMenuOpenHeight_S;
        
        
        [_programImageView setFrame:CGRectMake(0, 45, 320.0f, bodyHeight-textScrollerHeight)];
//        [_programImage setFrame:CGRectMake(0, 0, 320.0f, bodyHeight-(menuHeight+textScrollerHeight))]; // xib 파일에서 auto layout 해제해야 함.
        [_boardView setFrame:CGRectMake(0, bodyHeight-(textScrollerHeight+menuHeight), 320.0f, textScrollerHeight)];
        [self.radioSubTabView setFrame:CGRectMake(0, bodyHeight-menuHeight, 320.0f, menuHeight)];
        
        
        float playerHeight = 40.0f;
        
//        if([[programDate valueForKey:@"vfm"] isEqualToString:@"Yes"])
        if(_vRadioBtn.selected)
        {
            
            
            int bandiBoardViewHeight;
            if(IS_IPHONE5)
                bandiBoardViewHeight = 145;
            else
                bandiBoardViewHeight = 100;
            int bandiBdY = bodyHeight-tabMenuCloseHeight-bandiBoardViewHeight;
            
            float posY = bandiBdY -playerHeight - _boardView.frame.size.height;
            if(bPlayerShowOnairState)
            {
                [_wrapperView  bringSubviewToFront:_boardView];
                _boardView.frame = CGRectMake(_boardView.frame.origin.x,
                                              bandiBdY -_boardView.frame.size.height,
                                              _boardView.frame.size.width,
                                              _boardView.frame.size.height);
                [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
            }

        }
        else
        {
            float posY = bodyHeight-(textScrollerHeight+menuHeight) -playerHeight;
            
            if(bPlayerShowOnairState)
            {
                [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
                [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];

            }
        }
        
    }

}
/////
-(void)getAppver
{
 
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/appVerInfo?appNm=bandi&appDsCd=03&fileType=json"];
//    NSString *urlStr = [NSString stringWithFormat:@"http://to302.phps.kr/test/appVerInfo.json"]; // for test
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    
    if(!jsonString || jsonString == nil)
        return;

    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString*  appVer = [infoDict objectForKey:@"CFBundleVersion"];
    NSString* serVer = [[[person valueForKey:@"resultXml"]valueForKey:@"object"]valueForKey:@"appVer"];
    
    [[NSUserDefaults standardUserDefaults] setObject:serVer forKey:@"appSerVer"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

    
//    if([appVer compare:serVer] == NSOrderedAscending)
    if([appVer intValue] < [serVer intValue])
    {
        if(serVer)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"새로운 반디버전이 출시되었습니다. 업데이트 후 이용해주시기 바랍니다."
                                                           delegate:self
                                                  cancelButtonTitle:@"취소"
                                                  otherButtonTitles:@"업데이트",nil ];
            alert.tag = 3011;
            [alert show];
//            return;

            bInitPlayer = YES;
            [self initLoad];
            [self getBroadInfo];
            
        }
        else
        {
            [self initLoad];
            [self getBroadInfo];
        }

        
    }
    else
    {
        [self initLoad];
        [self getBroadInfo];

    }
 
}

#pragma mark - 편성표 정보 처리
-(void)getBroadInfo
{
//    bool __debug__ = NO;
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    NSDateComponents *currentTime = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:[NSDate date]];
    
    NSInteger currentHour = [currentTime hour];
    if (currentHour <= 2) {
        currentHour += 24; // 편성표에 반영된 25시를 표현하기 위해.
    }
    NSString *currentTimeStr = [NSString stringWithFormat:@"%02d:%02d", (int)currentHour, (int)[currentTime minute] ];
    
    
    NSString *strCh;
    if (nSelectedMenu ==1 ) strCh = [NSString stringWithFormat:@"radio"];
    else if (nSelectedMenu ==2 ) strCh = [NSString stringWithFormat:@"iradio"];
    
    NSError *error;
    
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/function/broadcastXml.json?broadType=%@",strCh];
    NSLog(@"onair load getboarding  %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    NSLog(@"load getboarding end");
    if(!jsonString || jsonString == nil)
        return;
    
    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
    NSMutableArray *arr =[[person valueForKey:@"channels"]valueForKey:@"programs"];
    
//    BOOL bFineCh;
    bFineCh = NO;

    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ProgramId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    for(int cnt = 0 ; cnt < [arr count] ; cnt ++)
    {
        
        if([currentTimeStr compare:[[arr objectAtIndex:cnt] valueForKey:@"start"]] == NSOrderedDescending || [currentTimeStr compare:[[arr objectAtIndex:cnt] valueForKey:@"start"]] == NSOrderedSame)
        {
            NSString *strEndTime = [[arr objectAtIndex:cnt] valueForKey:@"end"];
            if(nSelectedMenu ==2)
            {
                if([arr count] <= cnt+1)
                    strEndTime = @"26:00";
                else
                    strEndTime = [[arr objectAtIndex:cnt+1] valueForKey:@"start"];
            }
            
            if([currentTimeStr compare:strEndTime] == NSOrderedAscending )
            {
//                if (__debug__) NSLog(@">> id : %@, %@, %@", currentTimeStr, strEndTime, arr[cnt][@"id"] );
                
                if (arr[cnt][@"id"] == (id)[NSNull null] || [@"" isEqualToString:arr[cnt][@"id"]] )
                {
                    NSLog(@">> call break");
                    [boardingDate removeAllObjects];
                    break;
                }
                bFineCh = YES;
                NSLog(@"board info = %@",[boardingDate valueForKey:@"title"]);
                [boardingDate removeAllObjects];
                [boardingDate addEntriesFromDictionary:[arr objectAtIndex:cnt]];
                [[NSUserDefaults standardUserDefaults] setObject:[boardingDate valueForKey:@"id"] forKey:@"ProgramId"];
                [[NSUserDefaults standardUserDefaults] setObject:[boardingDate valueForKey:@"title"] forKey:@"ProgramTitle"];
                [[NSUserDefaults standardUserDefaults] setObject:[[person valueForKey:@"channels"]valueForKey:@"code"] forKey:@"channelCode"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [self programInfo:[boardingDate valueForKey:@"id"]];
                
//                if (__debug__) NSLog(@">> string equal: %d, %@", [@"10024453" isEqualToString:[boardingDate valueForKey:@"id"]], [boardingDate valueForKey:@"id"]);
                
                _btcheckAdd.selected = [self checkFind];
                _btcheckAdd.hidden = NO;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"BOARGIND_SNS" object:nil];
                 [self getBoardTextData];
                NSLog(@"--%@",[[person valueForKey:@"channels"]valueForKey:@"code"] );
                
                if([strOldPd length] < 1)
                    [self saveOnairEventLog:@"S"];
                if(![strOldPd isEqualToString:[boardingDate valueForKey:@"id"]])
                {
                    if(bInitEnd)
                        [self saveOnairEventLog:@"P"];
                    else
                        bInitEnd = YES;
                }
                
                strOldPd = [NSString stringWithFormat:@"%@",[boardingDate valueForKey:@"id"]];
                
                if(nSelectedMenu == 1)
                {
                    _vRadioBtn.hidden = NO;
                    _seeRadioText.hidden = NO;
                }
                
                break;
            }
            
        }
    }
    if (!bFineCh) {
        //방송없음
//        _vRadioBtn.hidden = YES;
//        _seeRadioText.hidden = YES;
//        _rollingTextView
        UILabel *label = [[UILabel alloc] init];
        label.text = @"해당 프로그램은 반디게시판이 운영되지 않습니다.";
        
        [label setFont:[UIFont systemFontOfSize:13]];
        label.frame = CGRectMake(0, 0, _rollingTextView.frame.size.width, 30);
        [_rollingTextView addSubview:label];
        
        _noAgainList.hidden = NO;
        self.tbAgain.hidden = YES;

        _tbBook.hidden = YES;
        
        if(!bShowTabMenu)
        {
            UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg2.png"] scaledToWidth:320];
            _programImage.image = resizeImage;
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            resizeImage.size.height);

//              _programImage.image = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg.png"] scaledToWidth:320];
        }
        else
        {
//            _programImage.image = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg2.png"] scaledToWidth:320];
            UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg.png"] scaledToWidth:320];
            _programImage.image = resizeImage;
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            resizeImage.size.height);

        }

        [[NSNotificationCenter defaultCenter] postNotificationName:@"NO_BOARGIND_SNS" object:nil];
        _btcheckAdd.hidden = YES;
        
    }
}

-(void)getStreamingInfo
{
    
    NSError* error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/onairUrl?fileType=json"];


    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];

    if(!jsonString || jsonString == nil)
        return;
    
    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
    if (person) {
   
        for (int cnt = 0; cnt < [[person valueForKey:@"streamLists"] count]; cnt ++) {
            if([[[[person valueForKey:@"streamLists"] objectAtIndex:cnt] valueForKey:@"svcCd"] isEqualToString:@"Bandi" ])
            {
                
                [streamingData removeAllObjects];
                [streamingData addEntriesFromDictionary:[[person valueForKey:@"streamLists"] objectAtIndex:cnt]];
                
                break;
            }
        }
    }
    else
    {
        NSDictionary *dic  = [[NSDictionary alloc]initWithObjectsAndKeys:@"http://ebsonairiosaod.ebs.co.kr/fmradiobandiaod/bandiappaac/playlist.m3u8",@"streamUrlIos",@"http://new_iradio.ebs.co.kr/iradio_ios/iradiolive_m4a/playlist.m3u8",@"streamUrlIradioIos", nil];
        [streamingData addEntriesFromDictionary:dic];
    }

}

-(void)getAgainInfo:(NSString *)programId page:(int)nPage autoC:(BOOL)bauto// 다시듣기
{

    againListPage = nPage;
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/replayList?programId=%@&pageVodFig=5&page=%d&fileType=json",programId,nPage];
    NSLog(@"onair load streaming  %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
//    NSLog(@"load getAgainInfo end");
    if(!jsonString || jsonString == nil)
        return;
    

    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
    NSMutableDictionary * broadcastList = [[NSMutableDictionary alloc]initWithDictionary:person copyItems:YES];


    if(nPage == 1)
    {
        [againList removeAllObjects];
        againListPage = 1;
    }

    NSString * strTotl =[[broadcastList valueForKey:@"vods"]valueForKey:@"totalPages"];
    bPgAgainTotal = [strTotl intValue];
//    if([[[broadcastList valueForKey:@"vods"]valueForKey:@"hasNextPage"] isEqualToString:@"1"])
    
    if([[[broadcastList valueForKey:@"vods"]valueForKey:@"content"] count] < 1)
    {
        _noAgainList.hidden = NO;
        self.tbAgain.hidden = YES;
        if(bauto)
            [self getBookInfo:[boardingDate valueForKey:@"id"]];

        return ;
    }
    _noAgainList.hidden = YES;
    self.tbAgain.hidden = NO;
    

    [againList addObjectsFromArray:[[broadcastList valueForKey:@"vods"]valueForKey:@"content"]];
    
    

    [self.tbAgain reloadData];
    [self openTabSetScr];
    bLoadAdd = NO;
    if(bauto)
        [self getBookInfo:[boardingDate valueForKey:@"id"]];
}

#pragma mark 방송내용
/**
 * 방송내용 메뉴 구성 정보 불러오기
 */
-(void)programInfo:(NSString *)programId
{
    
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiProgInfo?version=2&programId=%@&fileType=json",programId];
    NSLog(@"onair load programInfo  %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    
    
    if(!jsonString || jsonString == nil)
        return;
    
    NSDictionary *person = [Util convertJsonToDic:jsonString];
    
    if(![[[person valueForKey:@"resultXml"] valueForKey:@"resultCd"] isEqualToString:@"000"])
    {
        if (nSelectedMenu == 2)
        {
            NSLog(@"iradio data 없음");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"irado data가 없습니다."
                                                           delegate:nil
                                                  cancelButtonTitle:@"닫기"
                                                  otherButtonTitles:nil ];
            [alert show];
            [self selectMenuIndex:1];
            return;
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"radio data가 없습니다."
                                                           delegate:nil
                                                  cancelButtonTitle:@"닫기"
                                                  otherButtonTitles:nil ];
            [alert show];
//            [self selectMenuIndex:1];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadBoardingData" object:nil];
            return;
        }
       
    }
   

    [programDate removeAllObjects];
    [programDate addEntriesFromDictionary:[[person valueForKey:@"resultXml"]valueForKey:@"object"]];

    if ([programDate valueForKey:@"appUseBoardYn"] != (id)[NSNull null])
        [[NSUserDefaults standardUserDefaults] setObject:[programDate valueForKey:@"appUseBoardYn"] forKey:@"UseBoard"];
    else
        [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"UseBoard"];

    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if(person)
    {
        ////// image
        if(bShowTabMenu)
        {
            if(_vRadioBtn.selected)//if([[programDate valueForKey:@"vfm"] isEqualToString:@"Yes"])
            {
                
                if ([programDate valueForKey:@"appImg2"] != (id)[NSNull null])
                {
                    UIImage *img = [self checkImage:[programDate valueForKey:@"appImg2"] bBigMode:YES];
                    UIImage * resizeImage = [self imageWithImage:img scaledToWidth:320];
                    _programImage.image =resizeImage;
                    
                    _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                                    _programImage.frame.origin.y,
                                                    320,
                                                    resizeImage.size.height);
                 
                }

            }
            else
            {
                if ([programDate valueForKey:@"appImg2"] != (id)[NSNull null])
                {
                    
                    UIImage *img = [self checkImage:[programDate valueForKey:@"appImg2"] bBigMode:NO];
                    UIImage * resizeImage = [self imageWithImage:img scaledToWidth:320];
                    _programImage.image =resizeImage;
                    
                    _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                                    _programImage.frame.origin.y,
                                                    320,
                                                    resizeImage.size.height);
                
                    
                }
                else
                {

                    UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"onair_thumb_img.png"] scaledToWidth:320];
                    _programImage.image = resizeImage;
                    _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                                    _programImage.frame.origin.y,
                                                    320,
                                                    resizeImage.size.height);

                }
            }
        }
        else
        {

            if ([programDate valueForKey:@"appImg"] != (id)[NSNull null])
            {
                
                UIImage *img = [self checkImage:[programDate valueForKey:@"appImg"] bBigMode:YES];
//                UIImage * resizeImage = [self imageWithImage:img scaledToWidth:320];
                _programImage.image =img;
                
                _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                                _programImage.frame.origin.y,
                                                320,
                                                picH);
             
            }
            else
            {

                
//                UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg2.png"] scaledToWidth:320];
                _programImage.image = [UIImage imageNamed:@"bandi_main_bg2.png"];
                _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                                _programImage.frame.origin.y,
                                                320,
                                                picH);
            }
        }
        //보이는 라디오


        if([[programDate valueForKey:@"vfm"] isEqualToString:@"Yes"])
        {

            if(!_bandiBoardView.isHidden)
            {

                float posY = _bandiBoardView.frame.origin.y -40;
//                UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg2.png"] scaledToWidth:320];
                _programImage.image = [UIImage imageNamed:@"bandi_main_bg2.png"];
                _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                                _programImage.frame.origin.y,
                                                320,
                                                picH);
                
                if(bPlayerShowOnairState)//aa3
                {
                    [player setFrame:CGRectMake(0, posY, player.frame.size.width, 40)];
                    [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];
                }
            }
        }
        else
        {
  
        }
        
        if ([programDate valueForKey:@"facebookUrl"] != (id)[NSNull null])
        {
            [player facebookEnable:YES];
            [player urlFacebook:[programDate valueForKey:@"facebookUrl"]];
        }
        else
        {
            [player facebookEnable:NO];
            [player urlFacebook:@""];
        }
        
        if ([programDate valueForKey:@"twitter"] != (id)[NSNull null])
        {

            [player twitterEnable:YES];
            [player urlTwitter:[programDate valueForKey:@"twitter"]];
        }
        else
        {
            [player twitterEnable:NO];
            [player urlTwitter:[programDate valueForKey:@""]];
        }
        
        if ([programDate valueForKey:@"kakaostoryUrl"] != (id)[NSNull null])
        {
            
            [player kakaoEnable:YES];
            [player urlKakao:[programDate valueForKey:@"kakaostoryUrl"]];
        }
        else
        {
            [player kakaoEnable:NO];
            [player urlKakao:[programDate valueForKey:@""]];
        }
        

        if ([programDate valueForKey:@"mobHmpUrl"] != (id)[NSNull null])
//        if(![[programDate valueForKey:@"mobHmpUrl"] isEqualToString:@"<null>"] )
        {
            
            [player homeEnable:YES];
            [player urlHome:[programDate valueForKey:@"mobHmpUrl"]];
        }
        else
        {
            [player homeEnable:NO];
            [player urlHome:[programDate valueForKey:@""]];

        }
        //
#pragma mark 방송내용
        NSString * strContent = [NSString stringWithFormat:@"%@",[programDate valueForKey:@"contents"]];
//        strContent = @"<p>우리가 쉽게 지나쳐왔던 사물을 짚어가며, 역사 속 사물 이야기와 상담·음악·문학·대중문화·요리·여행 등 다양한 측면에서 바라보는 사물에 대한 인상을 살핍니다. 또 청취자가 보내온 사물에 대한 개인적인 경험을 토대로, 각 분야 전문가의 시선을 더할 예정입니다!</p><p><br></p><p>(월) 사물의 내면 : 사물과 상담하는 시간<br>(With 장재열 좀놀아본언니 대표)<br></p><p>(화) 사물의 음표 : 음악으로 들어보는 사물<br>(With 가수 나희경)<br></p><p>(수) 사물의 사색 : 문학을 통해 만나는 사물<br>(With 시인 오은)<br></p><p>(목) 사물의 부엌 : 요리와 함께하는 사물<br>(With 요리연구가 박준우)<br></p><p>(금) 사물'놀이' : 사물과 함께 놀기<br>(With 개그맨 박영진)<br></p><p>(토) 사물의 여행 : 여행과 떠나는 사물<br>(With 여행작가 전명진)<br></p>";
    
                 
        if(![strContent isEqualToString:@"<null>"])
        {
//            float fh;
//            fh = [self adjustUILabelHeightSize:strContent widthSize:_boadingInfoTitle.frame.size.width font:_boadingInfoTitle.font];
//            if(fh < 124)
//                fh = 124;
//            NSLog(@"fh -> %f", fh);
//            [_boadingInfoTitle setFrame:CGRectMake(_boadingInfoTitle.frame.origin.x,
//                                                   _boadingInfoTitle.frame.origin.y,
//                                                   _boadingInfoTitle.frame.size.width, fh)];
//            [_boadingInfoScroll setContentSize:CGSizeMake(320, fh+20)];
//            _boadingInfoTitle.text = [programDate valueForKey:@"contents"];
//            _boadingInfoTitle.textAlignment = NSTextAlignmentLeft;
     
            
            // 방송내용 HTML 적용
            if(_boadingWebView)
            {
                _boadingWebView.scrollView.scrollEnabled = NO;
                [_boadingWebView loadHTMLString:strContent baseURL:nil];
                [self performSelector:@selector(loadBroadcastingContents) withObject:nil afterDelay:0.1f];
                
            }


        }
        else{
            _boadingWebView.hidden = YES;
            _boadingInfoTitle.text = [NSString stringWithFormat:@"방송내용이 없습니다."] ;
            _boadingInfoTitle.textAlignment = NSTextAlignmentCenter;

        }
       
    }
    

     [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadBoardingData" object:nil];
        [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];
        
}

-(void)getBookInfo:(NSString *)programId
{
    
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiBookList?programId=%@&appDsCd=03&fileType=json",programId];
    NSLog(@"onair load getBookInfo  %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    NSLog(@"end");
    if(!jsonString || jsonString == nil)
        return;
    
    NSDictionary *person = [Util convertJsonToDic:jsonString];
    
    [bookInfoList removeAllObjects];
    [bookInfoList addObjectsFromArray:[[person valueForKey:@"resultXml"]valueForKey:@"appBookInfos"]];

    if([bookInfoList count])
    {
        
        NSURL *url = [NSURL URLWithString:[[bookInfoList objectAtIndex:0] valueForKey:@"mobImgUrl"]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        if(data)
        {
            self.bookImg.image = [[UIImage alloc] initWithData:data];
        }
        NSLog(@".. %@ / %@",[[bookInfoList objectAtIndex:0] valueForKey:@"bookNm"],[[bookInfoList objectAtIndex:0] valueForKey:@"bookCntn"]);
        self.bookTitle.text = [[bookInfoList objectAtIndex:0] valueForKey:@"bookNm"];
        NSString *strTmp = [NSString stringWithFormat:@"%@",[[bookInfoList objectAtIndex:0] valueForKey:@"bookCntn"]];
        self.bookContent.text = strTmp;
    }
    if([bookInfoList count] < 1)
        self.tbBook.hidden = YES;
    else
        self.tbBook.hidden = NO;
    [self.tbBook reloadData];
    [self getSongInfo:[boardingDate valueForKey:@"id"]];
//    [self getBroadInfo];
    
}

-(void)getStreamInfo:(NSString *)programId userId:(NSString *)strId lectId:(NSString *)strlId
{
//    (로그인전) http://home.ebs.co.kr/bandi/function/vodSteamInfo.xml?userId=&programId=10009930&lectId=10272419
//    (로그인후) http://home.ebs.co.kr/bandi/function/vodSteamInfo.xml?userId=merners&programId=10009930&lectId=10272419
    
    if(!strId)
        strId = @"";
    
    
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/vodSteamInfo?userId=%@&programId=%@&lectId=%@&fileType=json",strId,programId,strlId];
    NSLog(@"onair load = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    if(!jsonString || jsonString == nil)
        return;
    
    NSLog(@"load getStreamInfo end");

    NSDictionary *person = [Util convertJsonToDic:jsonString];
    
    NSString *strUrl = [NSString stringWithFormat:@""];
    if([[[person valueForKey:@"resultXml"]valueForKey:@"resultMsg"] isEqualToString:@"정상"])
    {
        strUrl = [NSString stringWithFormat:@"%@",[[person valueForKey:@"resultXml"]valueForKey:@"vodSteamingUrl"]];
        [self playselectedsong:strUrl];
        
//        
//        NSURL *videosURL = [NSURL URLWithString:strUrl];
//        MPMoviePlayerController *moviePlayController = [[MPMoviePlayerController alloc]initWithContentURL:videosURL];
//        [self addSubview:moviePlayController.view];
//        [moviePlayController.view setFrame:CGRectMake(30, 50, 200, 100)];
//        
//        [moviePlayController prepareToPlay];
//        moviePlayController.controlStyle = MPMovieControlStyleDefault;
//        moviePlayController.shouldAutoplay = YES;
//      [moviePlayController setFullscreen:NO animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:[[person valueForKey:@"resultXml"]valueForKey:@"resultMsg"]
                                                       delegate:self
                                              cancelButtonTitle:@"확인"
                                              otherButtonTitles:nil ];
        alert.tag = 2001;
        [alert show];
    }
}

#pragma mark 선곡표 메뉴
/** 
 * 선곡표 메뉴 내용 불러오기
 */
-(void)getSongInfo:(NSString *)programId
{
    //http://home.ebs.co.kr/bandi/bandiSongList2?fileType=json&progcd=10009931&regdate=20150104
    //
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiSongList2?fileType=json&progcd=%@&regdate=%@",programId,[boardingDate valueForKey:@"onairDate"]];
    NSLog(@"laod song info uri = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    
    NSLog(@"end");
    if(!jsonString || jsonString == nil)
        return;
    
    NSDictionary *person = [Util convertJsonToDic:jsonString];

//    NSLog(@"jsonString image = %@",person);
    if([[[person valueForKey:@"bandi"] valueForKey:@"rescd"] isEqualToString:@"0000"])
    {
        NSString * strContent = [NSString stringWithFormat:@"%@",[[person valueForKey:@"bandi"]valueForKey:@"bandi-contents"]];
        NSLog(@"strContent = %@",strContent);
        //
 
        

        
        if([strContent length]< 1)
        {
            _songsWebView.hidden = YES;
            _SongInfoTitle.text = @"선곡표 내용이 없습니다.";
        }
        else
        {
            if(_songsWebView)
            {
                
                _songsWebView.scrollView.scrollEnabled = NO;
                [_songsWebView loadHTMLString:strContent baseURL:nil];
                [self performSelector:@selector(loadSongTable) withObject:nil afterDelay:0.1f];
                
                
            }
        }
    }
    else
    {
        _songsWebView.hidden = YES;
        _SongInfoTitle.text = @"선곡표 내용이 없습니다.";

    }

//    [_SongInfoTitle sizeToFit];
//    _btcheckAdd.selected = [self checkFind];
    

    
}
////

#pragma -table Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView ==  self.tbAgain)
        return [againList count];
    else if(tableView == self.tbBook)
        return [bookInfoList count];
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.tbAgain == tableView)
    {

        return 60;
    }
        
    return 35.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView ==  self.tbAgain)
    {
        onAirAgainCell *cell = (onAirAgainCell *)[tableView dequeueReusableCellWithIdentifier:@"onAirAgainCell"];
        if (cell == nil)
        {
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"onAirAgainCell" owner:nil options:nil];
            cell = (onAirAgainCell *)[arr objectAtIndex:0];
        }
    


        NSDictionary *Dic = [againList objectAtIndex:indexPath.row];

        cell.lbText.text = [Dic objectForKey:@"lectNm"];
        cell.listIndex = indexPath.row;
        cell.cellDelegate = self;
        cell.playerSlider.hidden = YES;
//        NSString * strDate = [Dic valueForKey:@"shwLectNmSrch"] substringToIndex:<#(NSUInteger)#>
        
        NSRange areaStr;
        
        NSString * strStartYear = [[Dic valueForKey:@"shwLectNmSrch"] substringToIndex:4];
        areaStr.location = 4;
        areaStr.length = 2;
        NSString * strStartMonth = [[Dic valueForKey:@"shwLectNmSrch"] substringWithRange:areaStr];
        areaStr.location = 6;
        areaStr.length = 2;
        NSString * strStartDay = [[Dic valueForKey:@"shwLectNmSrch"] substringWithRange:areaStr];
        cell.lbDate.text = [NSString stringWithFormat:@"%@-%@-%@",strStartYear,strStartMonth,strStartDay];
        
//       NSString *Str = [Dic valueForKey:@"brdcDt"];
//       if([[Dic valueForKey:@"shwLectNmSrch"] isKindOfClass:[NSString class]])
  
  
//       else if ([[Dic valueForKey:@"shwLectNmSrch"] isKindOfClass:[NSDecimalNumber class]])
//       {
//           NSDecimalNumber* dec1 = [Dic valueForKey:@"shwLectNmSrch"];
//           cell.lbDate.text = dec1.stringValue;
//       }
        
        if(indexPath.row %2== 1)
            [cell setBackgroundColor:RgbColor(252,248,238)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (streamIndex == indexPath.row) {
            if(streamPlaying)
            {
                if (songPlayer.rate > 0 && !songPlayer.error) {
                    cell.btPlay.hidden = YES;
                    cell.btStop.hidden = NO;
                }
                else
                {
                    cell.btPlay.hidden = NO;
                    cell.btStop.hidden = YES;
                }
            }
            
            if(songPlayer.status == AVPlayerStatusReadyToPlay)
            {
                cell.playerSlider.hidden = NO;
                cell.lbDate.hidden = YES;
                [cell setSliderValue:againSongPer];
            }
          
            if(!streamPlaying)
            {
                cell.btPlay.selected = NO;
                cell.btStop.hidden = YES;
                cell.playerSlider.hidden = YES;
                 cell.lbDate.hidden = NO;

            }
            
                      

        }
        else
        {

            cell.btPlay.selected = NO;
            cell.btStop.hidden = YES;
            cell.lbDate.hidden = NO;
            

        }

        
        return cell;
    }
    else if(tableView == self.tbBook)
    {
        onAirAgainCell *cell = (onAirAgainCell *)[tableView dequeueReusableCellWithIdentifier:@"onAirAgainCell"];
        if (cell == nil)
        {
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"onAirAgainCell" owner:nil options:nil];
            cell = (onAirAgainCell *)[arr objectAtIndex:0];
        }
        
        NSDictionary *Dic = [bookInfoList objectAtIndex:indexPath.row];
        
        cell.lbText.text = [Dic objectForKey:@"bookNm"];
        cell.listIndex = indexPath.row;
        cell.cellDelegate = self;
        cell.playerSlider.hidden = YES;
        if(indexPath.row %2== 1)
            [cell setBackgroundColor:RgbColor(252,248,238)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.btPlay.hidden = YES;
        cell.btStop.hidden = YES;


        

        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.tbBook == tableView)
    {

        NSURL *url = [NSURL URLWithString:[[bookInfoList objectAtIndex:indexPath.row] valueForKey:@"mobImgUrl"]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        if(data)
        {
            self.bookImg.image = [[UIImage alloc] initWithData:data];
        }
        NSLog(@".. %@ / %@",[[bookInfoList objectAtIndex:indexPath.row] valueForKey:@"bookNm"],[[bookInfoList objectAtIndex:0] valueForKey:@"bookCntn"]);
        self.bookTitle.text = [[bookInfoList objectAtIndex:indexPath.row] valueForKey:@"bookNm"];
        NSString *strTmp = [NSString stringWithFormat:@"%@",[[bookInfoList objectAtIndex:indexPath.row] valueForKey:@"bookCntn"]];
        self.bookContent.text = strTmp;
        selectBookCol = indexPath.row;
        
        [_tbBook scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];

    }

}

#pragma -scrollview Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
    {
        if (!bLoadAdd)
        {
            bLoadAdd = YES;
            if(nSelectedTabMenu == 1)
                [self performSelector:@selector(loadTable) withObject:nil afterDelay:1.0f];
            else
                bLoadAdd = NO;
            // proceed with the loading of more data
        }
    }
    
//    [pullToRefreshManager_ tableViewScrolled];
    
    CGFloat pageWidth = self.scrListView.frame.size.width;
    int pag = floor((self.scrListView.contentOffset.x - pageWidth /2) / pageWidth) + 1;
    if(self.pgControl.currentPage != pag)
    {
//        self.pgControl.currentPage = pag;
        
        switch (pag+1) {
            case 0:
                [self.btTabAgain setBackgroundColor:RgbColor(125,116,107)];
                [self.btTabBookInfo setBackgroundColor:RgbColor(137,128,119)];
                [self.btTabBroadInfo setBackgroundColor:RgbColor(158,145,136)];
                [self.btTabSongList setBackgroundColor:RgbColor(172,158,147)];
                self.btTabAgain.selected = NO;
                self.btTabBookInfo.selected = NO;
                self.btTabBroadInfo.selected = NO;
                self.btTabSongList.selected = NO;
                break;
            case 1:
                [self.btTabAgain setBackgroundColor:RgbColor(63,56,51)];
                [self.btTabBookInfo setBackgroundColor:RgbColor(137,128,119)];
                [self.btTabBroadInfo setBackgroundColor:RgbColor(158,145,136)];
                [self.btTabSongList setBackgroundColor:RgbColor(172,158,147)];
                self.btTabAgain.selected = YES;
                self.btTabBookInfo.selected = NO;
                self.btTabBroadInfo.selected = NO;
                self.btTabSongList.selected = NO;
                break;
            case 2:
                [self.btTabAgain setBackgroundColor:RgbColor(125,116,107)];
                [self.btTabBookInfo setBackgroundColor:RgbColor(63,56,51)];
                [self.btTabBroadInfo setBackgroundColor:RgbColor(158,145,136)];
                [self.btTabSongList setBackgroundColor:RgbColor(172,158,147)];
                self.btTabAgain.selected = NO;
                self.btTabBookInfo.selected = YES;
                self.btTabBroadInfo.selected = NO;
                self.btTabSongList.selected = NO;
                break;
            case 3:
                [self.btTabAgain setBackgroundColor:RgbColor(125,116,107)];
                [self.btTabBookInfo setBackgroundColor:RgbColor(137,128,119)];
                [self.btTabBroadInfo setBackgroundColor:RgbColor(63,56,51)];
                [self.btTabSongList setBackgroundColor:RgbColor(172,158,147)];
                self.btTabAgain.selected = NO;
                self.btTabBookInfo.selected = NO;
                self.btTabBroadInfo.selected = YES;
                self.btTabSongList.selected = NO;
                break;
            case 4:
                [self.btTabAgain setBackgroundColor:RgbColor(125,116,107)];
                [self.btTabBookInfo setBackgroundColor:RgbColor(137,128,119)];
                [self.btTabBroadInfo setBackgroundColor:RgbColor(158,145,136)];
                [self.btTabSongList setBackgroundColor:RgbColor(63,56,51)];
                self.btTabAgain.selected = NO;
                self.btTabBookInfo.selected = NO;
                self.btTabBroadInfo.selected = NO;
                self.btTabSongList.selected = YES;
                break;
                
            default:
                break;
        }
    }

}

-(void)selectPlay:(int)indx
{
    if(_vRadioBtn.selected)
        [self watchRadio:nil];

    
    NSLog(@"select play %d",indx);
//    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
//    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
    
    //[[[cc.view subviews] lastObject] removeFromSuperview];
    //NSLog(@"lastObject -> %@", [[cc.view subviews] lastObject]);
    
//    CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);

    NSLog(@"-- %@",[boardingDate valueForKey:@"id"]);
    
    if([[[againList objectAtIndex:indx]valueForKey:@"vodChrgClsCd"] isEqualToString:@"E"])
        
    {
        if ([ServerSide autoLogin]) {
            if(streamIndex == indx)
            {
                if(songPlayer)
                    [songPlayer play];
            }
            else
            {
                streamIndex = indx;
                [self getStreamInfo:[boardingDate valueForKey:@"id"] userId:SharedAppDelegate.sessionUserId lectId:[[againList objectAtIndex:indx]valueForKey:@"lectId"]];
            }
            
            
        } else {

            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginOnly" object:nil];
//            NSString * strStrView;
//            if (IS_IPHONE5)
//                strStrView = @"LogContentController";
//            else
//                strStrView = @"LogContentController_480";
//            
//            LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
//            [vc setBGoboard:NO];
//            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
            
        }
    }
    else if([[[againList objectAtIndex:indx]valueForKey:@"vodChrgClsCd"] isEqualToString:@"A"])
    {
        if(streamIndex == indx)
        {
            if(songPlayer)
                [songPlayer play];
        }
        else
        {
            streamIndex = indx;
            [self getStreamInfo:[boardingDate valueForKey:@"id"] userId:SharedAppDelegate.sessionUserId lectId:[[againList objectAtIndex:indx]valueForKey:@"lectId"]];
        }

    }


//    [self getStreamInfo:@"10009930" userId:@"" lectId:@"10272419"/*[[againList objectAtIndex:indx]valueForKey:@"lectId"]*/];
}

-(void)selectStop:(int)indx
{
    
//    if (againSongTimer != nil) {
//        [againSongTimer invalidate];
//        againSongTimer = nil;
//    }
    streamIndex = indx;
    [songPlayer pause];
    [self.tbAgain reloadData];
}
-(void)songPlayerStop
{
    if (againSongTimer != nil) {
        [againSongTimer invalidate];
        againSongTimer = nil;
    }


    streamIndex = -1;
    [songPlayer pause];
    streamPlaying = NO;
    _againPlayerInfo.hidden = YES;
    
//    NSIndexPath *ip = [NSIndexPath indexPathForRow:streamIndex inSection:0];
//    [self tableView:self.tbAgain cellForRowAtIndexPath:ip];
    [_tbAgain reloadData];

    


//    [self.tbAgain reloadData];
}


-(float)adjustUILabelHeightSize:(NSString*)textlabel widthSize:(float)width font:(UIFont*)ft{
    
    CGSize maximumHeightSize = CGSizeMake(width, 9999);
    
    CGSize extendLabelSize = [textlabel sizeWithFont:ft
                                   constrainedToSize:maximumHeightSize
                                       lineBreakMode:NSLineBreakByCharWrapping];
    
//    NSLog(@"extendLabelSize.height = %f",extendLabelSize.height);
    return extendLabelSize.height;
}


-(void)setPlayerFrame
{
     NSArray *arr = self.superview.subviews;
    player = nil;
    for (int i=0; i<arr.count; i++) {
        if ([arr[i] isKindOfClass:PlayerView.class]) {
            player = arr[i];
            break;
        }
    }
    if(bPlayerShowOnairState)
    {
        float playerHeight = 40.0f;
        float posY;
        
        if(_bandiBoardView.isHidden)
            posY = _boardView.frame.origin.y -playerHeight;
        else
        {
            posY = _bandiBoardView.frame.origin.y -playerHeight - _boardView.frame.size.height;
            _boardView.frame = CGRectMake(_boardView.frame.origin.x,
                                          _bandiBoardView.frame.origin.y -_boardView.frame.size.height,
                                          _boardView.frame.size.width,
                                          _boardView.frame.size.height);
        }

        [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
        [_wrapperView addSubview:player];
        
        [self.superview bringSubviewToFront:player];
        [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];
    }
   [self getStreamingInfo];
}

- (IBAction)addCheckList:(id)sender
{
    

    NSLog(@"-- %@",boardingDate);
    if(![boardingDate count])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"방송중이 아니기때문에 즐겨찾기 할 수 없습니다."
                                                       delegate:nil
                                              cancelButtonTitle:@"확인"
                                              otherButtonTitles:nil ];
        [alert show];
        return;
    }

//    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
//    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;

//    CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);
    
    if(_btcheckAdd.isSelected)
    {
        
        [self checkFind];
        NSError *error;
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppUserFbmkModify?userId=%@&programId=%@&fbmkSno=%@&&appDsCd=03&fileType=json", SharedAppDelegate.sessionUserId,[boardingDate valueForKey:@"id"],_strCheckId];
        NSLog(@"lll = %@",urlStr);
        NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                              encoding:NSUTF8StringEncoding
                                                                 error:&error];
        if(!jsonString || jsonString == nil)
            return;
        
        
        NSDictionary *person = [Util convertJsonToDic:jsonString];
        
        if([[[person valueForKey:@"resultXml"] valueForKey:@"resultCd"] isEqualToString:@"00"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"즐겨찾기가 해지되었습니다"
                                                           delegate:nil
                                                  cancelButtonTitle:@"닫기"
                                                  otherButtonTitles:nil ];
            [alert show];
            _btcheckAdd.selected = NO;
            [self getBroadInfo];
        }
        else
        {
            NSLog(@"--%@",[person valueForKey:@"resultXml"]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:[[person valueForKey:@"resultXml"] valueForKey:@"resultMsg"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"닫기"
                                                  otherButtonTitles:nil ];
            [alert show];
            
        }

    }
    else
    {
        if ([ServerSide autoLogin]) {
            
            
            
            _btcheckAdd.selected = YES;
            
            NSError *error;
            NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppUserFbmkAdd?userId=%@&programId=%@&appDsCd=03&fileType=json",SharedAppDelegate.sessionUserId,[boardingDate valueForKey:@"id"]];

            NSLog(@"ll2l = %@",urlStr);
            NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                                  encoding:NSUTF8StringEncoding
                                                                     error:&error];
            
            if(!jsonString || jsonString == nil)
                return;
            
            
            NSDictionary *person = [Util convertJsonToDic:jsonString];
            
            if(![[[person valueForKey:@"resultXml"] valueForKey:@"resultCd"]isEqualToString:@"00"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:@"즐겨찾기등록에 실패하였습니다."
                                                               delegate:self
                                                      cancelButtonTitle:@"취소"
                                                      otherButtonTitles:@"확인",nil ];
                [alert show];
                return;
            }

            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"즐겨찾기에 등록하였습니다. 즐겨찾기를 확인하시겠습니까?"
                                                           delegate:self
                                                  cancelButtonTitle:@"취소"
                                                  otherButtonTitles:@"확인",nil ];
            [alert show];
            alert.tag = 1001;
            
        
//            [self getBroadInfo];
            
        } else {
            NSString * strStrView;
            if (IS_IPHONE5)
                strStrView = @"LogContentController";
            else
                strStrView = @"LogContentController_480";
            
            LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
            [vc setBOnlyEBS:YES];
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
//            LoginView *myView = [[LoginView alloc] initWithFrame:frameRect];
//            [myView EBSLoginOnly];
//            [cc.view addSubview:myView];
//            [cc.view bringSubviewToFront:myView];
//            cc.navigationItem.title = @"로그인";
        }
        
    }
    

}

-(BOOL)checkFind
{

    BOOL bCheck = NO;
    _strCheckId = @"";
    if ([ServerSide autoLogin])
    {
        NSError *error;

        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppUserFbmkList?userId=%@&fileType=json",SharedAppDelegate.sessionUserId];
    NSLog(@"laod checkFind uri = %@",urlStr);
        NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                              encoding:NSUTF8StringEncoding
                                                                 error:&error];
        
        if(!jsonString || jsonString == nil)
            return bCheck;
        
        NSLog(@"load checkFind end");
        
        NSDictionary *person = [Util convertJsonToDic:jsonString];
        
        NSMutableArray * arr = [[NSMutableArray alloc]initWithArray:[[person valueForKey:@"appUserFbmkLists"]valueForKey:@"appUserFbmkLists"] copyItems:YES];
        
        @try {
            for (int n = 0; n < [arr count]; n ++) {
                NSDictionary * dic = [arr objectAtIndex:n];
                if([[dic valueForKey:@"courseId"] isEqualToString:[boardingDate valueForKey:@"id"]])
                {
                    bCheck = YES;
                    _strCheckId = [NSString stringWithFormat:@"%@",[dic valueForKey:@"fbmkSno"]];
                    break;
                }
                NSLog(@"%@/%@",[dic valueForKey:@"courseId"],[boardingDate valueForKey:@"id"]);
            }
        }
        @catch (NSException *e) {
            NSLog(@"Error: %@ - %@", [e name], [e reason]);
        }
        
    }
    

        
    return bCheck;

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 0)
    {
        if(alertView.tag == 1001)
            [self getBroadInfo];
        else if(alertView.tag == 2001)
        {
            NSURL *url = [NSURL URLWithString:[[againList objectAtIndex:streamIndex] valueForKey:@"mobHmpUrl"]];
            [[UIApplication sharedApplication] openURL:url];
        }
        else if(alertView.tag == 3011)
        {
//            [self initLoad];
//            [self getBroadInfo];
            if(nSelectedMenu ==2)
                [player loadSound:[streamingData valueForKey:@"streamUrlIradioIos"]];
            else
                [player loadSound:[streamingData valueForKey:@"streamUrlIos"]];
            
            bInitPlayer = NO;

        }

        return;
    }
    
    if(alertView.tag == 1001)
    {

        [[NSNotificationCenter defaultCenter] postNotificationName:@"MoveTab4" object:nil];

    }
    else if(alertView.tag == 3011)
    {
        alertView.delegate = nil;
        alertView  = nil;
        

        NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/kr/app/ebs-bandi/id368886634?l=en&mt=8"];
        [[UIApplication sharedApplication] openURL:url];
//        [self performSelector:@selector(openStore) withObject:nil afterDelay:0.1f];

    }
}


- (IBAction)bookUrl:(id)sender
{
    NSLog(@"[bookInfoList objectAtIndex:0]  = %@",[bookInfoList objectAtIndex:selectBookCol] );
    if ([[bookInfoList objectAtIndex:selectBookCol] valueForKey:@"mobLink"] != (id)[NSNull null])
    {
        NSString *strTmp = [NSString stringWithFormat:@"%@",[[bookInfoList objectAtIndex:selectBookCol] valueForKey:@"bookId"]];
        [self connectBookHit:strTmp];

        NSURL *url = [NSURL URLWithString:[[bookInfoList objectAtIndex:selectBookCol] valueForKey:@"mobLink"] ];
//        [[UIApplication sharedApplication] openURL:url];
        WebViewController *vc = [[WebViewController alloc] init];
        [self.window.rootViewController presentViewController:vc animated:YES completion:^{
            [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
        }];
    }
}

/////////
-(void)playselectedsong:(NSString *)urlString{
    
    urlString = [urlString stringByReplacingOccurrencesOfString:@"httpx" withString:@"http"];
   
    [player stopAudio];

    if(songPlayer)
    {
        [songPlayer pause];
        [songPlayer removeObserver:self forKeyPath:@"status"];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:[songPlayer currentItem]];
        songPlayer = nil;
    }
    
    AVPlayer *playerAgain = [[AVPlayer alloc]initWithURL:[NSURL URLWithString:urlString]];
    songPlayer = playerAgain;
    [songPlayer play];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[songPlayer currentItem]];
    [songPlayer addObserver:self forKeyPath:@"status" options:0 context:nil];

    
    
    
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (object == songPlayer && [keyPath isEqualToString:@"status"]) {
        if (songPlayer.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
            streamPlaying = NO;
            [self.tbAgain reloadData];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"다시듣기를 재생할 수 없습니다."
                                                           delegate:nil
                                                  cancelButtonTitle:@"닫기"
                                                  otherButtonTitles:nil ];
            if (againSongTimer != nil) {
                [againSongTimer invalidate];
                againSongTimer = nil;
                streamIndex = -1;
            }
            [alert show];
            _againPlayerInfo.hidden = YES;
            
        } else if (songPlayer.status == AVPlayerStatusReadyToPlay) {
            NSLog(@"AVPlayerStatusReadyToPlay");
            [songPlayer play];
            streamPlaying = YES;
            [self.tbAgain reloadData];
            if (againSongTimer != nil) {
                [againSongTimer invalidate];
                againSongTimer = nil;

            }

            
            againSongTimer = [NSTimer scheduledTimerWithTimeInterval:1.2f
                                                            target:self
                                                          selector:@selector(updateProgress)
                                                          userInfo:nil
                                                           repeats:YES];
                _againPlayerInfo.hidden = NO;
            
            
        } else if (songPlayer.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
            streamPlaying = NO;
            [self.tbAgain reloadData];
            if (againSongTimer != nil) {
                [againSongTimer invalidate];
                againSongTimer = nil;
                streamIndex = -1;
            }

            _againPlayerInfo.hidden = YES;
        }
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
    //  code here to play next sound file
    if (againSongTimer != nil) {
        [againSongTimer invalidate];
        againSongTimer = nil;
        streamIndex = -1;
    }
    streamPlaying = NO;
    _againPlayerInfo.hidden = YES;
    [self.tbAgain reloadData];
}
- (void)updateProgress
{
    NSLog(@"tem");

    
    Float64 playtime = CMTimeGetSeconds(songPlayer.currentTime);
    Float64 durationSeconds = CMTimeGetSeconds([songPlayer currentItem].duration);
//    NSTimeInterval result = startSeconds + durationSeconds;
//    NSLog(@"fff(%f) - %f / %f",(CGFloat) (playtime / durationSeconds),playtime , durationSeconds);
    againSongPer = (CGFloat) (playtime / durationSeconds);
    if(bShowTabMenu)
    {
        if(!againPorgressing)
        {
            NSLog(@"a");
            if(streamIndex > -1)
            {
//                NSIndexPath *ip = [NSIndexPath indexPathForRow:streamIndex inSection:0];
//                [self tableView:self.tbAgain cellForRowAtIndexPath:ip];
                [_tbAgain reloadData];
            }
            
        }
        
    }
//
//    NSArray *loadedTimeRanges = [[songPlayer currentItem] loadedTimeRanges];
//                NSLog(@"b");
//    if([loadedTimeRanges count])
//    {
//        CMTimeRange timeRange = [[loadedTimeRanges objectAtIndex:0] CMTimeRangeValue];
//        Float64 ss = CMTimeGetSeconds(timeRange.start);
//        Float64 ds = CMTimeGetSeconds(timeRange.duration);
//        NSLog(@"---(%f)> %f/%f",againSongPer,ss,ds);
//    }
    

}

-(void)startVideo:(NSString *)str
{
    if(IS_IPHONE5)
        _WcView.frame = CGRectMake(0, 0, 320, 173);
    else
        _WcView.frame = CGRectMake(0, 0, 320, 130);
    
    [player stopAudio];
    
    ep = [[ebsPlayer alloc]init];
    
    [_WcView addSubview:ep];
    [ep initPlayer:_WcView.frame url:str];
    ep.playerDelegate = self;
    [ep play];
    _vRadioBtn.selected = YES;
    _WcView.hidden = NO;
    _btFullScreen.hidden = NO;

    
    return;
    
}

-(void)statePlaReadyToPlay
{
    
}
-(void)statePlaying:(BOOL)bPlay
{
//    NSLog(@"statePlaying = %d",bPlay);
    player.playBtn.selected = bPlay;
    if(!bPlay)
    {
        player.hidden = NO;
        _radioToolView.hidden = NO;

        if(videoControllerTimer != nil)
        {
            [videoControllerTimer invalidate];
            videoControllerTimer = nil;
        }

    }
}


- (void)eplayPlay:(NSNotification *)notification
{
    NSLog(@"aa");
    if(player.playBtn.selected)
        [ep stop];
    else
        [ep play];
    
}

- (IBAction)fullScreen:(id)sender
{
    MoviePlayerViewController *mpc = [[MoviePlayerViewController alloc] init];

    int posx = 0;
    int posy = 0;
    
    if(IS_IPHONE5)
    {
        posx = -115;
        posy = 125;
    }
    else
    {
        posx = -82;
        posy = 78;
    }
    
    
    UIView *bgV = [[UIView alloc]initWithFrame:CGRectMake(posx, posy,
                                                          self.window.rootViewController.view.frame.size.height,
                                                          self.window.rootViewController.view.frame.size.width)];
    

    
//    UIView *bgV = [[UIView alloc]initWithFrame:CGRectMake(-115, 125,
//                                                         self.window.rootViewController.view.frame.size.height-20,
//                                                          self.window.rootViewController.view.frame.size.width)];
    
    
    [bgV addSubview:ep];
    bgV.transform = CGAffineTransformMakeRotation(degreesToRadian(90));

    [ep playerSetFrame:CGRectMake(0, 0, bgV.frame.size.height, bgV.frame.size.width)];


    [mpc.view addSubview:bgV];
    
    [mpc.view bringSubviewToFront:mpc.homeButton];

    mpc.homeButton.transform = CGAffineTransformMakeRotation(degreesToRadian(90));
    [self.window.rootViewController presentViewController:mpc animated:NO completion:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playAgain:)
                                                 name:@"playAgain"
                                               object:nil];
    


}

- (void)reLoadOnairView:(NSNotification *)notification
{
    [_WcView addSubview:ep];
//    if(IS_IPHONE5)
//        [ep playerSetFrame:CGRectMake(0, 0, 320, 220)];
//    else


    if(IS_IPHONE5)
        [ep playerSetFrame:CGRectMake(0, 0, 320, 173)];
    else
        [ep playerSetFrame:CGRectMake(0, 0, 320, 130)];
    


//    _fullScrView.transform = CGAffineTransformMakeRotation(degreesToRadian(0));
//    mpc.homeButton.transform = CGAffineTransformMakeRotation(degreesToRadian(0));
    
}

-(void)reloadRadioControll
{
    int bodyHeight = [SharedAppDelegate.bodyHeight intValue];;
    NSLog(@"3_wabFrame %f/%f",_wrapperView.frame.origin.y,_wrapperView.frame.size.height);
    if(!bShowTabMenu)
    {
        
        if(bFineCh && ([programDate valueForKey:@"appImg"] != (id)[NSNull null]))
        { 
            
            UIImage *img = [self checkImage:[programDate valueForKey:@"appImg"] bBigMode:YES];
//            UIImage * resizeImage = [self imageWithImage:img scaledToWidth:320];
            _programImage.image =img;
            
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            picH);
        
        }
        else
        {
//            UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg2.png"] scaledToWidth:320];
            _programImage.image = [UIImage imageNamed:@"bandi_main_bg2.png"];
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            picH);
        }
        
        
        [_programImageView setFrame:CGRectMake(0, 45, 320.0f, bodyHeight-36)];
//        [_programImage setFrame:CGRectMake(0, 0, 320.0f, bodyHeight-(tabMenuCloseHeight+textScrollerHeight))]; // xib 파일에서 auto layout 해제해야 함.
        [_boardView setFrame:CGRectMake(0, bodyHeight-(textScrollerHeight+tabMenuCloseHeight), 320.0f, textScrollerHeight)];
        [self.radioSubTabView setFrame:CGRectMake(0, bodyHeight-tabMenuCloseHeight, 320.0f, tabMenuCloseHeight)];
        
        float playerHeight = 40.0f;
        if([[programDate valueForKey:@"vfm"] isEqualToString:@"Yes"])
        {
            if (!_bandiBoardView.isHidden) {
                float posY = _bandiBoardView.frame.origin.y -playerHeight - _boardView.frame.size.height;

                if(bPlayerShowOnairState)
                {
                    _boardView.frame = CGRectMake(_boardView.frame.origin.x,
                                                  _bandiBoardView.frame.origin.y -_boardView.frame.size.height,
                                                  _boardView.frame.size.width,
                                                  _boardView.frame.size.height);
                    [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
                    [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];
                }

            }

        }
        else
        {

            float posY = bodyHeight-(textScrollerHeight+tabMenuCloseHeight) -playerHeight;
            if(bPlayerShowOnairState)
            {
                [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
                [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];
            }
        }
   
    }
    else
    {
        
        if(bFineCh)
        {
            
            UIImage *img = [self checkImage:[programDate valueForKey:@"appImg2"] bBigMode:NO];
            UIImage * resizeImage = [self imageWithImage:img scaledToWidth:320];
            _programImage.image =resizeImage;
            
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            resizeImage.size.height);
            
         
        }
        else
        {
            UIImage * resizeImage = [self imageWithImage:[UIImage imageNamed:@"bandi_main_bg.png"] scaledToWidth:320];
            _programImage.image = resizeImage;
            _programImage.frame =CGRectMake(_programImage.frame.origin.x,
                                            _programImage.frame.origin.y,
                                            320,
                                            resizeImage.size.height);
        }

        
        int menuHeight;
        if(IS_IPHONE5)
            menuHeight =tabMenuOpenHeight;
        else
            menuHeight =tabMenuOpenHeight_S;
        
        [_programImageView setFrame:CGRectMake(0, 45, 320.0f, bodyHeight-textScrollerHeight)];
        [_programImage setFrame:CGRectMake(0, 0, 320.0f, bodyHeight-(menuHeight+textScrollerHeight))]; // xib 파일에서 auto layout 해제해야 함.
        [_boardView setFrame:CGRectMake(0, bodyHeight-(textScrollerHeight+menuHeight), 320.0f, textScrollerHeight)];
        [self.radioSubTabView setFrame:CGRectMake(0, bodyHeight-menuHeight, 320.0f, menuHeight)];
        
        float playerHeight = 40.0f;
        
        if([[programDate valueForKey:@"vfm"] isEqualToString:@"Yes"])
        {
            if(!_bandiBoardView.isHidden)
            {
                float posY = _bandiBoardView.frame.origin.y -playerHeight - _boardView.frame.size.height;
                if(bPlayerShowOnairState)
                {
                    _boardView.frame = CGRectMake(_boardView.frame.origin.x,
                                                  _bandiBoardView.frame.origin.y -_boardView.frame.size.height,
                                                  _boardView.frame.size.width,
                                                  _boardView.frame.size.height);
                    [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
                    [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];
                }
            }
            
        }
        else
        {
            float posY = bodyHeight-(textScrollerHeight+menuHeight) -playerHeight;

            if(bPlayerShowOnairState)
            {
                [player setFrame:CGRectMake(0, posY, player.frame.size.width, playerHeight)];
                [self performSelectorOnMainThread:@selector(loadPlayerBt) withObject:nil waitUntilDone:NO];
            }
        }
        
        
    }

}
- (void)keyboardWillAnimate:(NSNotification *)notification
{

    
    if([notification name] == UIKeyboardWillShowNotification)
    {
        _keypadHideBt.hidden = NO;
    }
    else if([notification name] == UIKeyboardWillHideNotification)
    {
        _keypadHideBt.hidden = YES;
    }
}
-(void)hideKeyPad:(id)sender
{
    [_bandiBoardView hideKeypad];
}

- (void)showGplayer:(NSNotification *)notification
{

//    nSelectedTabMenu = 0;
    
    if(bShowTabMenu)
        [self selectTabIndex:nSelectedTabMenu];
    [_bandiBoardView.txtField resignFirstResponder];
    bPlayerShowOnairState = NO;
    if(_vRadioBtn.selected)
        [self watchRadio:nil];
    
}

- (void)showOnairPlayer:(NSNotification *)notification
{

    [_bandiBoardView.txtField resignFirstResponder];
    if(_vRadioBtn.selected)
        [self watchRadio:nil];
    if(bShowTabMenu)
        [self openTab:NO];

    [mainScr setContentOffset:
     CGPointMake(0, -mainScr.contentInset.top) animated:NO];
    bPlayerShowOnairState = YES;
    
    _btcheckAdd.selected = [self checkFind];
}

- (void)loadNoti
{
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"bShowEvent"] isEqualToString:@"1"])
        return;
    NSMutableArray * eventList = [[NSMutableArray alloc]init];
    
    NSError *error;

    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiEventNoticeList?appDsCd=03&fileType=json"];
    NSLog(@"laod loadnoti uri = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    if(!jsonString || jsonString == nil)
        return;

    NSLog(@"load loadNoti end");
    
    NSDictionary *person = [Util convertJsonToDic:jsonString];
    
    if([[person valueForKey:@"bandiEventLists"] count] < 1)
    {
        bHaveEvt = NO;
        return;
    }
    if([[person valueForKey:@"bandiEventLists"] objectAtIndex:0])
    {
        if(notiData)
            [notiData removeAllObjects];
        [notiData addEntriesFromDictionary:[[person valueForKey:@"bandiEventLists"] objectAtIndex:0]];
        [_bandiBoardView setNotiDic:notiData];
    }

    [eventList removeAllObjects];
    NSMutableArray *arr1 = [[NSMutableArray alloc]initWithCapacity:0];
    [arr1 addObjectsFromArray:[person valueForKey:@"bandiEventLists"]];
    
    
    
    NSDateFormatter *today = [[NSDateFormatter alloc]init];
    [today setDateFormat:@"yyyyMMdd"];
    NSString *date = [today stringFromDate:[NSDate date]];

    for (int n = 0; n < [arr1 count]; n ++) {
        if([[[arr1 objectAtIndex:n] valueForKey:@"evtClsCd"] isEqualToString:@"002"])
        {
            int nEvtStart =[[[arr1 objectAtIndex:n] valueForKey:@"evtStartDt"] intValue];
            int nToday = [date intValue];

            if(nEvtStart - nToday <= 0 && [[[arr1 objectAtIndex:n] valueForKey:@"evtEndDt"] intValue] - nToday >= 0 )
            {
                 bHaveEvt = YES;
                [eventList addObject:[arr1 objectAtIndex:n]];
            }
        }

    }
    if(![eventList count])
        return;

    NSString *imgUrl = [NSString stringWithFormat:@"http://static.ebs.co.kr/images/%@%@",
                        [[eventList objectAtIndex:0] valueForKey:@"mobThmnlFilePathNm"],[[eventList objectAtIndex:0] valueForKey:@"mobThmnlFilePhscNm"]];


    NSLog(@"eventList = %@ / %@",[[eventList objectAtIndex:0] valueForKey:@"evtTitle"],imgUrl);
    

    
    NSDictionary * dic = [[NSDictionary alloc]initWithDictionary:[eventList objectAtIndex:0]];
    
    _detailTitle.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"evtTitle"]];
    
    NSLog(@"bHaveEventUrl = %@",[NSString stringWithFormat:@"%@",[dic valueForKey:@"evtTitle"]]);
    strEventDetailUrl =[NSString stringWithFormat:@"%@",[dic valueForKey:@"linkUrl"]];

    if([[dic valueForKey:@"evtClsCd"] isEqualToString:@"001"])
    {
        if([strEventDetailUrl length] > 2)
        {
            _eventDetailBt.hidden = NO;
            if (IS_IPHONE5)
                _detailContentScroll.frame = CGRectMake(5, 66, 306, 400);
            else
                _detailContentScroll.frame = CGRectMake(5, 73, 306, 317);
        }
        else
        {
            _eventDetailBt.hidden = YES;
            if (IS_IPHONE5)
                _detailContentScroll.frame = CGRectMake(5, 66, 306, 450);
            else
                _detailContentScroll.frame = CGRectMake(5, 73, 306, 350);
        }
    }
    else
    {
        _eventDetailBt.hidden = YES;
        if (IS_IPHONE5)
            _detailContentScroll.frame = CGRectMake(5, 66, 306, 450);
        else
            _detailContentScroll.frame = CGRectMake(5, 73, 306, 350);
    }
    
  

    NSString *strImgUrl = [NSString stringWithFormat:@"http://static.ebs.co.kr/images%@%@",[dic valueForKey:@"mobThmnlFilePathNm"],[dic valueForKey:@"mobThmnlFilePhscNm"]];
    NSLog(@"strImgUrl = %@",strImgUrl);
    NSURL *url = [NSURL URLWithString:strImgUrl];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if(data) {
        UIImage *image = [[UIImage alloc] initWithData:data];
        UIImage *resizeImage = [self imageWithImage:image scaledToWidth:310.f];
        _detailEventImage.image = resizeImage;
        [_detailContentScroll setContentSize:CGSizeMake(resizeImage.size.width,resizeImage.size.height)];
        _detailEventImage.frame = CGRectMake(_detailEventImage.frame.origin.x,
                                             _detailEventImage.frame.origin.y,
                                             resizeImage.size.width,
                                             resizeImage.size.height);
        
        [_detailContentScroll setScrollEnabled:NO];
        _btEventDetail.frame =_detailEventImage.frame;
        NSLog(@"%f/%f",resizeImage.size.width,resizeImage.size.height);
        _detailEventImage.hidden = NO;
        if(!SharedAppDelegate.isOpenTt)
        {
            

            strEventUrl = [[NSString alloc]initWithFormat:@"%@",[dic valueForKey:@"mobLinkUrl"]];
            if([[[NSUserDefaults standardUserDefaults] objectForKey:@"INIT_GUIDE"] isEqualToString:@"1"])
            {
                _notiView.hidden = NO;
                strEventUrl = [[NSString alloc]initWithFormat:@"%@",[dic valueForKey:@"mobLinkUrl"]];
                [_notiView setFrame:CGRectMake( 0, 0, _notiView.frame.size.width, _notiView.frame.size.height)];
                [[UIApplication sharedApplication].keyWindow addSubview:_notiView];
                [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_notiView];
                
            }
            else
                bShowNoti = YES;
            
        }
        else
        {
           
            _notiView.hidden = NO;
            strEventUrl = [[NSString alloc]initWithFormat:@"%@",[dic valueForKey:@"mobLinkUrl"]];
            [_notiView setFrame:CGRectMake( 0, 0, _notiView.frame.size.width, _notiView.frame.size.height)];
            [[UIApplication sharedApplication].keyWindow addSubview:_notiView];
            [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_notiView];

        }
        
        
    }
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"bShowEvent"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}

- (IBAction)openNotiEvent:(id)sender
{
    NSRange strRange;
    strRange = [strEventUrl rangeOfString:@"http://"];
    
    if (strRange.location == NSNotFound) {
        strEventUrl = [NSString stringWithFormat:@"http://%@",strEventUrl];
    }
    
    NSURL *url = [NSURL URLWithString:strEventUrl];

    WebViewController *vc = [[WebViewController alloc] init];
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
        NSLog(@"self -> %@",vc);
        [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }];

    _notiView.hidden = YES;
    
}

-(IBAction)eventDetailUrlOpen:(id)sender
{
    if([strEventDetailUrl length] > 2)
    {
        NSRange strRange;
        strRange = [strEventDetailUrl rangeOfString:@"http://"];
        
        if (strRange.location == NSNotFound) {
            strEventDetailUrl = [NSString stringWithFormat:@"http://%@",strEventDetailUrl];
        }

        
        NSURL *url = [NSURL URLWithString:strEventDetailUrl];
//        WebViewController *vc = [[WebViewController alloc] init];
//        [self.window.rootViewController presentViewController:vc animated:YES completion:^{
//            [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
//        }];

        [[UIApplication sharedApplication] openURL:url];
        _notiView.hidden = YES;

    }
}

- (IBAction)closeNotiDetail:(id)sender
{
    _notiView.hidden = YES;
}
-(void)selectValue:(float)per index:(int)indx
{
    [songPlayer pause];
    //
//  /  CMTime t = CMTimeMake(per, 1);
//    self.nowPlayingCurrentTime.text = [self formatTimeCodeAsString: t.value];
//    self.nowPlayingDuration.text = [self formatTimeCodeAsString:(self.actualDuration - t.value)];
//    [songPlayer seekToTime:t];
    //
    Float64 durationSeconds = CMTimeGetSeconds([songPlayer currentItem].duration);
    CMTime newTime = CMTimeMakeWithSeconds(per * durationSeconds, songPlayer.currentTime.timescale);
    [songPlayer seekToTime:newTime];
    [songPlayer play];
    
}


#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient

/**
 * This is the same delegate method as UIScrollView but required in MNMBottomPullToRefreshManagerClient protocol
 * to warn about its implementation. Here you have to call [MNMBottomPullToRefreshManager tableViewScrolled]
 *
 * Tells the delegate when the user scrolls the content view within the receiver.
 *
 * @param scrollView: The scroll-view object in which the scrolling occurred.
 */
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    [pullToRefreshManager_ tableViewScrolled];
//}

/**
 * This is the same delegate method as UIScrollView but required in MNMBottomPullToRefreshClient protocol
 * to warn about its implementation. Here you have to call [MNMBottomPullToRefreshManager tableViewReleased]
 *
 * Tells the delegate when dragging ended in the scroll view.
 *
 * @param scrollView: The scroll-view object that finished scrolling the content view.
 * @param decelerate: YES if the scrolling movement will continue, but decelerate, after a touch-up gesture during a dragging operation.
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [pullToRefreshManager_ tableViewReleased];
}

/**
 * Tells client that refresh has been triggered
 * After reloading is completed must call [MNMBottomPullToRefreshManager tableViewReloadFinished]
 *
 * @param manager PTR manager
 */
- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager {
    
    [self performSelector:@selector(loadTable) withObject:nil afterDelay:1.0f];
}
/*
 * Loads the table
 */

- (void)testRefresh:(UIRefreshControl *)refreshControl
{
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [NSThread sleepForTimeInterval:3];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"MMM d, h:mm a"];
            NSString *lastUpdate = [NSString stringWithFormat:@"Last updated on %@", [formatter stringFromDate:[NSDate date]]];
            
            refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdate];
            
            [refreshControl endRefreshing];
            [self loadTable];
            NSLog(@"refresh end");
        });
    });
}

- (void)loadTable {
    
    if(bPgAgainTotal > againListPage )
    {
        ++ againListPage;
        [self getAgainInfo:[boardingDate valueForKey:@"id"]  page:againListPage autoC:NO];
    }
    bLoadAdd = NO;
    
}

#pragma mark - 탭 메뉴 웹뷰 열기
/**
 * 선곡표 웹뷰 로딩
 */
-(void)loadSongTable
{
    [_songsWebView setFrame:CGRectMake(0, 0, 320,750)];
    _songsWebView.scrollView.scrollEnabled = NO;
    _songsWebView.hidden = NO;
    [self openTabSetScr];
}

/**
 방송내용 웹뷰 로딩
 */
-(void)loadBroadcastingContents
{
    [_boadingWebView setFrame:CGRectMake(0, 0, 320,750)];
    _boadingWebView.scrollView.scrollEnabled = NO;
    _boadingWebView.hidden = NO;
    [self openTabSetScr];
}


#pragma mark -
-(void)progressing:(BOOL)bSet
{
    againPorgressing = bSet;
}



-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark 탭메뉴 열기
/**
 * 탭 메뉴 열기
 */
-(void)openTabSetScr
{
    if(bShowTabMenu)
    {
        int mainscrH = 0;
        int agTbHeight = 0;
        
        if(nSelectedTabMenu == 1)
        {
            agTbHeight = [againList count]*60;
            if(agTbHeight < 145)
                agTbHeight = 145;
            _tbAgain.frame = CGRectMake(_tbAgain.frame.origin.x, _tbAgain.frame.origin.y,
                                        320, agTbHeight);
            [_tbAgain setScrollEnabled:NO];
        }
        else if(nSelectedTabMenu == 2)
        {
            agTbHeight = [bookInfoList count]*35;
            agTbHeight += 101;
            _tbBook.frame = CGRectMake(_tbBook.frame.origin.x, 0,
                                             320, agTbHeight);
            [_bookInfoConView setFrame:CGRectMake(320, 0, 320, agTbHeight)];
            [_tbBook setScrollEnabled:NO];
        }
        else if(nSelectedTabMenu == 3)
        {

            NSString * strContent = [NSString stringWithFormat:@"%@",[programDate valueForKey:@"contents"]];
            if(![strContent isEqualToString:@"<null>"])
            {
//                float fh;
//                fh = [self adjustUILabelHeightSize:strContent widthSize:_boadingInfoTitle.frame.size.width font:_boadingInfoTitle.font];
//                if(fh < 124)
//                    fh = 124;
//                [_boadingInfoTitle setFrame:CGRectMake(_boadingInfoTitle.frame.origin.x,
//                                                       _boadingInfoTitle.frame.origin.y,
//                                                       _boadingInfoTitle.frame.size.width, fh+2)];
//                [_boadingInfoScroll setContentSize:CGSizeMake(320, fh+20)];
//                agTbHeight = fh+ 20;
                agTbHeight = 750; //<-- new
            }
            else
            {
                agTbHeight = 145;
            }
            
        }
        else if(nSelectedTabMenu == 4)
        {
            if(_songsWebView.isHidden)
                agTbHeight = 145;
            else
                agTbHeight = 750;
        }
        
        [_scrListView setContentSize:CGSizeMake(320, agTbHeight)];
        [_scrListView setScrollEnabled:NO];
        
        [_scrListView setFrame:CGRectMake(0, 55, 320, agTbHeight)];

        [_radioSubTabView setFrame:CGRectMake(0, _radioSubTabView.frame.origin.y,
                                              320, agTbHeight + 55)];
        
        mainscrH = (agTbHeight + 55) + _radioSubTabView.frame.origin.y;
        
        if(mainscrH < 454)
            mainscrH = 454;
        
        [mainScr setContentSize:CGSizeMake(320, mainscrH)];
        [mainScr setScrollEnabled:YES];
        
        _wrapperView.frame =CGRectMake(_wrapperView.frame.origin.x, _wrapperView.frame.origin.y,
                                       _wrapperView.frame.size.width, mainscrH);
        
        [self.scrListView setContentOffset:CGPointMake(320*(nSelectedTabMenu-1), 0) animated:NO];
        
    }
    else
    {

        
        [mainScr setContentSize:CGSizeMake(320, 317)];
        [mainScr setScrollEnabled:NO];
    }
        return;
}


-(void)getBoardTextData
{
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=%@&listType=L&pageSize=10", [boardingDate valueForKey:@"id"]];
//    NSLog(@"laod getBoardTextData uri = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    if(!jsonString || jsonString == nil)
        return;
//    NSLog(@"end");
    
    NSDictionary *person = [Util convertJsonToDic:jsonString];
    
    [boardTextList removeAllObjects];
    [boardTextList addObjectsFromArray:[person valueForKey:@"bandiPosts"]];
    rollingIdxInit = YES;
    [self setRollingTextCaller];
    //
    
//    http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=10009938&listType=L&pageSize=5
}


- (void)saveOnairEventLog:(NSString *)action
{
    [NSThread detachNewThreadSelector:@selector(onaireventLog:) toTarget:self withObject:action];
    
    

}

- (void)onaireventLog:(NSString *)action
{

    NSString *logSno = SharedAppDelegate.logSno;
    
    
    if (logSno) {
        //
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
        
        NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:[NSDate date]];
        
        
        NSDate * thisDate = [NSDate date];
        
        NSDateComponents* nowHour = [calendar components:NSHourCalendarUnit fromDate:thisDate];
        
        if([nowHour hour] < 2)
        {
            
            NSDateComponents *components = [[NSDateComponents alloc] init];
            [components setDay:-1];
            
            NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
            
            thisDate = yesterday;
            
            currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
        }
        

        NSString *strDate = [NSString stringWithFormat:@"%d%02d%02d", [currentTime year], [currentTime month], [currentTime day]];

        //
        NSString *mobId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        NSString *strbType;
        if(nSelectedMenu == 1)
            strbType = [NSString stringWithFormat:@"radio"];
        else
            strbType = [NSString stringWithFormat:@"iradio"];
        
        
        NSString *uId;
        if([SharedAppDelegate.sessionUserId length] < 1)
        {
            uId = [NSString stringWithFormat:@""];
        }
        else
            uId = [NSString stringWithFormat:@"%@",SharedAppDelegate.sessionUserId];
        
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/logProce/bandiLogAjax.json?mobId=%@&u=%@&logDsCd=%@&appDsCd=03&broadType=%@&onairDate=%@&onairStartTime=%@&", mobId,uId,action, strbType, strDate, [boardingDate valueForKey:@"start"] ];

        NSLog(@"laod event uri = %@",urlStr);
        NSLog(@"urlStr = %@",urlStr);
//        NSURL *url = [NSURL URLWithString:urlStr];
//        NSError *error;
//        NSString *xmlStr = [[NSString alloc] initWithContentsOfURL:url
//                                                          encoding:NSUTF8StringEncoding
//                                                             error:&error];
//        NSLog(@"eventLog -> %@", xmlStr);
    }
}


-(UIImage *)checkImage:(NSString *)strUrl bBigMode:(BOOL)bb
{
    
    
    
    NSArray *arr = [strUrl componentsSeparatedByString:@"/"];
    NSString *strFileName;
    UIImage * img = nil;
    if([arr count])
    {
        strFileName = [NSString stringWithFormat:@"%@",[arr objectAtIndex:[arr count]-1] ];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *docs = [paths objectAtIndex:0];
        
        NSString *dataPath = [docs stringByAppendingPathComponent:@"/imageThumb"];
        NSError *error;
//        BOOL bb = [[NSFileManager defaultManager] fileExistsAtPath:dataPath];
        if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])//Check
            [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Will Create folder
        
        
        NSString* path =  [dataPath stringByAppendingFormat:@"/%@",strFileName];

        BOOL isFileExist = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if(!isFileExist)
        {
            
            NSURL *url = [NSURL URLWithString:strUrl];
            NSData *data1 = [NSData dataWithContentsOfURL:url];
            
//            NSError *writeError = nil;
            BOOL bChec = [data1 writeToFile:path atomically:YES];
            NSLog(@"bche = %d",bChec);
            img = [[UIImage alloc] initWithData:data1];
            
            return img;
        }
        else
        {

            
            img = [UIImage imageWithContentsOfFile:path];
            return img;

        }

    }
    else
    {
        if(bb)
        {
            img = [UIImage imageNamed:@"bandi_main_bg2.png"];
            return img;
        }
        else
        {
            img = [UIImage imageNamed:@"bandi_main_bg.png"];
            return img;
        }
    }
}
- (void)connectBookHit:(NSString *)strId
{
    
    NSString *progcd_ = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"]];
    
//    &bookId=@
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiBookHit?programId=%@&appDsCd=03&bookId=%@&fileType=json", progcd_,strId];
    NSLog(@"urlStr = %@",urlStr);
    NSURL *url = [NSURL URLWithString:urlStr];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
  
    
    [request setURL:url];
    [request setHTTPMethod:@"POST"];

    
    [NSURLConnection connectionWithRequest:request delegate:self ];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@",returnString);
}

-(IBAction)hideVideoController:(id)sender
{
    NSLog(@"hide ");
    if(!player.isHidden)
    {
        player.hidden = YES;
        _radioToolView.hidden = YES;

        if(videoControllerTimer != nil)
        {
            [videoControllerTimer invalidate];
            videoControllerTimer = nil;
        }
    }
    else
    {
        player.hidden = NO;
        _radioToolView.hidden = NO;
        
        if(videoControllerTimer != nil)
        {
            [videoControllerTimer invalidate];
            videoControllerTimer = nil;
        }
        videoControllerTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f
                                                                target:self
                                                              selector:@selector(hideVideControll)
                                                              userInfo:nil
                                                               repeats:YES];

        
    }
}

-(void)hideVideControll
{
    NSLog(@"hide ");
    if(!player.isHidden)
    {
        player.hidden = YES;
        _radioToolView.hidden = YES;
        
        if(videoControllerTimer != nil)
        {
            [videoControllerTimer invalidate];
            videoControllerTimer = nil;
        }

    }
}
@end

