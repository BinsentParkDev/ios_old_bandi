//
//  LoginView.m
//  Bandi2 - 사용여부가 확실하지 않음.
//
//  Created by admin on 2014. 1. 16..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "LoginView.h"
#import "IIViewDeckController.h"
#import "BandiBoardWriteView.h"
#import "ServerSide.h"
#import "Dialogs.h"
#import <KakaoOpenSDK/KakaoOpenSDK.h>
#import "FHSTwitterEngine.h"
#import "NSUserDefaults+UserData.h"

#define TAG @"LoginForm"

@implementation LoginView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"frame = %f",frame.size.height);
        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        
        _bGoboard = NO;
        _bGoBack = NO;
        if (IS_IPHONE5)
            [[NSBundle mainBundle] loadNibNamed:@"LoginView" owner:self options:nil];
        else
            [[NSBundle mainBundle] loadNibNamed:@"LoginView_480" owner:self options:nil];
        [_wrapperView setFrame:CGRectMake(0, 0, 320, bodyHeight)];
        float accuHeight = 0.0f;
        [_wrapperView addSubview:_loginFormView];
        [_loginFormView setFrame:CGRectMake(0, 0, 320.0f, _loginFormView.frame.size.height)];
        accuHeight += _loginFormView.frame.size.height;
        
        [_wrapperView addSubview:_loginOptionView];
        [_loginOptionView setFrame:CGRectMake(0, accuHeight, 320.0f, _loginOptionView.frame.size.height)];
        accuHeight += _loginOptionView.frame.size.height;
        
        [_closeFormView setFrame:CGRectMake(0, _wrapperView.frame.size.height - _closeFormView.frame.size.height, 320, _closeFormView.frame.size.height)];
        [_wrapperView addSubview:_closeFormView];
        
        [self addSubview:_wrapperView];
        [self addSubview:_fbLoginView];
        [_fbLoginView setFrame:CGRectMake(35, _loginOptionView.frame.origin.y + _loginOptionView.frame.size.height+20, 250, 40)];
        if(IS_IPHONE5)
            [_twitterLogin setFrame:CGRectMake(35, _fbLoginView.frame.origin.y + _fbLoginView.frame.size.height+5, 250, 40)];
        else
            [_twitterLogin setFrame:CGRectMake(35, _fbLoginView.frame.origin.y + _fbLoginView.frame.size.height+5, 250, 40)];
        
//        UIButton *kakaoAccountConnectButton = [self createKakaoAccountConnectButton];
//        [self addSubview:kakaoAccountConnectButton];

        // 저장된 사용자 설정값 셋팅
        userDefaults = [NSUserDefaults standardUserDefaults];
        if ([userDefaults boolForKey:@"SAVE_ID"]) {
            _saveIdBtn.selected = YES;
            _userId.text = [userDefaults getUserId];
        }
        if ([userDefaults boolForKey:@"AUTO_LOGIN"]) {
            _autoLoginBtn.selected = YES;
            _userId.text = [userDefaults getUserId];
            _passwd.text = [userDefaults getPassword];
        }
    }
    
    [ServerSide saveEventLog:@"로그인"];
    
    [_fbLoginView setReadPermissions:@[@"public_profile"]];
    [_fbLoginView setDelegate:self];
    _objectID = nil;
    
    //
    _fbLoginView.frame = CGRectMake(35,
                                    _fbLoginView.frame.origin.y, 250, 35);
    for (id obj in _fbLoginView.subviews)
    {
        if ([obj isKindOfClass:[UIButton class]])
        {
            UIButton * loginButton =  obj;
            UIImage *loginImage = [UIImage imageNamed:@"login_facebook_off.png"];
            UIImage *loginImageH = [UIImage imageNamed:@"login_facebook_on.png"];
            loginButton.frame = CGRectMake(0,0, 250, 35);
            [loginButton setBackgroundImage:loginImage forState:UIControlStateNormal];
            [loginButton setBackgroundImage:nil forState:UIControlStateSelected];
            [loginButton setBackgroundImage:loginImageH forState:UIControlStateHighlighted];
//            [loginButton sizeToFit];
        }
        if ([obj isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  obj;
            loginLabel.text = @"";
            loginLabel.textAlignment = NSTextAlignmentLeft;
            loginLabel.frame = CGRectMake(0, 0, 250, 30);
            loginLabel.hidden = YES;
        }
    }
    


    


    
    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"] isEqualToString:@"000"])
    {
        _fbLoginView.hidden = YES;
        _twitterLogin.hidden = YES;
    }
    _keyHideBt.frame = self.frame;
    [self addSubview:_keyHideBt];
    _keyHideBt.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAnimate:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAnimate:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    return self;
}

-(void)EBSLoginOnly
{
    _fbLoginView.hidden = YES;
    _twitterLogin.hidden = YES;

}

- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)doLogin:(id)sender
{
    [_userId resignFirstResponder];
    [_passwd resignFirstResponder];

    if([_userId.text length] < 1)
    {
        [Dialogs alertView:@"아이디를 입력해 주세요."];
        return;
    }
    if([_passwd.text length] < 1)
    {
        [Dialogs alertView:@"비밀번호를 입력해 주세요."];
        return;
    }

    

    
    NSString *resultCode = [ServerSide doLogin:_userId.text withPasswd:_passwd.text];
    
    if ([resultCode isEqualToString:@"100"]) {
        
        
        [[NSUserDefaults standardUserDefaults] setObject:@"000" forKey:@"SNSLogin"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SNSId"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SNSName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if (_saveIdBtn.selected) {
            [userDefaults setUserId:_userId.text];
            
        }
        if (_autoLoginBtn.selected) {
            [userDefaults setUserId:_userId.text];
            [userDefaults setPassword:_passwd.text];
        }
        
       if(_bGoboard)
           [self goBoard];
        else
        {
            IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
            UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;

            cc.navigationItem.title = SharedAppDelegate.latestMenu;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
        [self removeFromSuperview];
        
    } else if ([resultCode isEqualToString:@"102"]) {
        [Dialogs alertView:@"로그인 실패 - 등록되지 않은 ID입니다."];
        [userDefaults setBool:NO forKey:@"SAVE_ID"];
    } else if ([resultCode isEqualToString:@"103"]) {
        [Dialogs alertView:@"로그인 실패 - 잘못된 비밀번호입니다."];
        [userDefaults setBool:NO forKey:@"AUTO_LOGIN"];
    } else if ([resultCode isEqualToString:@"104"]) {
        [Dialogs alertView:@"로그인 실패 - 사용되지 않는 ID입니다."];
        [userDefaults setBool:NO forKey:@"SAVE_ID"];
        [userDefaults setBool:NO forKey:@"AUTO_LOGIN"];
    } else {
        [Dialogs alertView:@"ID 또는 비밀번호를 잘못 입력하셨습니다."];
    }
    
}

- (IBAction)saveId:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (btn.selected) {
        btn.selected = NO;
    } else {
        btn.selected = YES;
    }
    
    [userDefaults setBool:btn.selected forKey:@"SAVE_ID"];
}
- (IBAction)autoLoginn:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    if (btn.selected) {
        btn.selected = NO;
    } else {
        btn.selected = YES;
    }
    
    [userDefaults setBool:btn.selected forKey:@"AUTO_LOGIN"];

}
- (IBAction)autoLogin:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (btn.selected) {
        btn.selected = NO;
    } else {
        btn.selected = YES;
    }
    
    [userDefaults setBool:btn.selected forKey:@"AUTO_LOGIN"];
    
    
}

- (IBAction)closeLoginView:(id)sender {
    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;

    cc.navigationItem.title = SharedAppDelegate.latestMenu;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
    if(_bGoBack)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginCloseAction" object:nil];
    [self removeFromSuperview];
}
-(void)goBoard
{
    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
    
    CGRect frameRect = CGRectMake(0, 44+20, 320, [SharedAppDelegate.bodyHeight intValue]);
    BandiBoardWriteView *myView = [[BandiBoardWriteView alloc] initWithFrame:frameRect];
    [cc.view addSubview:myView];
    
    cc.navigationItem.title = @"반디게시판";
}



- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    NSLog(@"id - %@ / name - %@",user.objectID,user.name);
//    [NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"]
    
    [[NSUserDefaults standardUserDefaults] setObject:@"001" forKey:@"SNSLogin"];
    [[NSUserDefaults standardUserDefaults] setObject:user.objectID forKey:@"SNSId"];
    [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"SNSName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //    self.profilePictureView.profileID = user.id;
    //    self.nameLabel.text = user.name;
    
    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
    
    cc.navigationItem.title = SharedAppDelegate.latestMenu;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
    [self removeFromSuperview];
    
}


// You need to override loginView:handleError in order to handle possible errors that can occur during login
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures since that happen outside of the app.
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}





- (UIButton *)createKakaoAccountConnectButton {
    // button position
    int xMargin = 30;

    CGFloat btnWidth = self.frame.size.width - xMargin * 2;
    int btnHeight = 42;
    
    UIButton *btnKakaoLogin
    = [[KOLoginButton alloc] initWithFrame:CGRectMake(xMargin, _fbLoginView.frame.origin.y+_fbLoginView.frame.size.height + 10, btnWidth, btnHeight)];
    btnKakaoLogin.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    
    
    // action
    [btnKakaoLogin addTarget:self action:@selector(invokeLoginWithTarget:) forControlEvents:UIControlEventTouchUpInside];
    
    return btnKakaoLogin;
}


- (IBAction)invokeLoginWithTarget:(id)sender {
    
    [[KOSession sharedSession] close];
    [[KOSession sharedSession] openWithCompletionHandler:^(NSError *error) {
        
        if ([[KOSession sharedSession] isOpen]) {
            // login success.
            [KOSessionTask storyProfileTaskWithCompletionHandler:^(KOStoryProfile *result, NSError *error) {
                if (result) {
//                    [self showStoryProfile:result];
                    
                    NSLog(@"login success.");
                    //            [[[UIApplication sharedApplication] delegate] performSelector:@selector(showMainView)];
                    [[NSUserDefaults standardUserDefaults] setObject:@"006" forKey:@"SNSLogin"];
                    [[NSUserDefaults standardUserDefaults] setObject:result.nickName forKey:@"SNSId"];
                    [[NSUserDefaults standardUserDefaults] setObject:result.nickName forKey:@"SNSName"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
                    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;

                    cc.navigationItem.title = SharedAppDelegate.latestMenu;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
                    [self removeFromSuperview];
                    
                } else {
                    NSLog(@"%@", error);
                }
            }];

        } else {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"에러"
                                                                message:error.localizedDescription
                                                               delegate:nil
                                                      cancelButtonTitle:@"확인"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
        
    }];
}

- (IBAction)twitterLogin:(id)sender
{
    UIViewController *loginController = [[FHSTwitterEngine sharedEngine]loginControllerWithCompletionHandler:^(BOOL success) {
        NSLog(success?@"L0L success":@"O noes!!! Loggen faylur!!!");
        NSLog(@"name = %@",FHSTwitterEngine.sharedEngine.authenticatedUsername);
//        [_theTableView reloadData];
        [[NSUserDefaults standardUserDefaults] setObject:@"002" forKey:@"SNSLogin"];
        [[NSUserDefaults standardUserDefaults] setObject:FHSTwitterEngine.sharedEngine.authenticatedID forKey:@"SNSId"];
        [[NSUserDefaults standardUserDefaults] setObject:FHSTwitterEngine.sharedEngine.authenticatedUsername forKey:@"SNSName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
        UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
        
        cc.navigationItem.title = SharedAppDelegate.latestMenu;
    
        

        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
        
        [self removeFromSuperview];


    }];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:loginController animated:YES completion:nil];

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{//키보드 엔터 delegate
    
        [_userId resignFirstResponder];
        [_passwd resignFirstResponder];
    //    [self postBoard];
    return YES;
    
}

-(IBAction)findId:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"https://sso.ebs.co.kr/idp/mobile/user/find/id.jsp" ];
    [[UIApplication sharedApplication] openURL:url];
}
-(IBAction)findPass:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"https://sso.ebs.co.kr/idp/mobile/user/reissue/password.jsp"];
    [[UIApplication sharedApplication] openURL:url];
}
-(IBAction)Join:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"https://sso.ebs.co.kr/idp/mobile/join/agreement.jsp" ];
    [[UIApplication sharedApplication] openURL:url];
}

-(IBAction)hidekey:(id)sender
{
    [_userId resignFirstResponder];
    [_passwd resignFirstResponder];
}
- (void)keyboardWillAnimate:(NSNotification *)notification
{
    
    if([notification name] == UIKeyboardWillShowNotification)
    {
        _keyHideBt.hidden = NO;
    }
    else if([notification name] == UIKeyboardWillHideNotification)
    {

        _keyHideBt.hidden = YES;
    }
}
-(void)cancleBack:(BOOL) bb
{
    _bGoBack = bb;
}
-(void)dealloc
{
    _fbLoginView.delegate = nil;

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(IBAction)loginTmp:(id)sender
{
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        // Close the session and remove the access token from the cache
        // The session state handler (in the app delegate) will be called automatically
        [FBSession.activeSession closeAndClearTokenInformation];
        
        // If the session state is not any of the two "open" states when the button is clicked
    } else {
        // Open a session showing the user the login UI
        // You must ALWAYS ask for public_profile permissions when opening a session
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error) {
  
//             NSLog(@"sesstion state= %@",state);
             // Retrieve the app delegate
//             Bandi2AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
             // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
//             [appDelegate sessionStateChanged:session state:state error:error];
         }];
    }
}

@end
