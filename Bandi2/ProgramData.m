//
//  ProgramData.m
//  Bandi2
//
//  Created by admin on 2014. 1. 13..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "ProgramData.h"
#import "TimeTable.h"

@implementation ProgramData

- (id)init
{
    self = [super init];
    if (self) {
        dataFileWasCreated = NO;
        programDataDic = [[NSMutableDictionary alloc] init];
    }
    return self;
}


- (void)parseProgramData:(NSString *)progcd
{
    NSString *filePath = [NSString stringWithFormat:@"%@progdata_%@.txt",  NSTemporaryDirectory(), progcd];
    NSString *dataStr;
	NSError *error;
    
    // 파일 존재 확인
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isFileExist = [fileManager fileExistsAtPath:filePath];
    NSDictionary *fileAttr = [fileManager attributesOfItemAtPath:filePath error:&error];
    
    // 파일 생성, 유효 시간 비교
    NSDate *validTime = [NSDate dateWithTimeIntervalSinceNow:-1*60*10]; // 파일 유지시간 10 min.
    NSComparisonResult compResult = [validTime compare:fileAttr[NSFileCreationDate]];

        
    if (dataFileWasCreated || (isFileExist && compResult==NSOrderedAscending)) {
        dataStr = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        NSLog(@"program data file have been created.");
    } else {
        NSLog(@"creating program data file  now.");
        
        //NSString *urlStr = @"http://to302.phps.kr/b/10009931.txt";
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/info/%@.txt", progcd];
        NSURL *dataUrl = [NSURL URLWithString:urlStr];
        dataStr = [[NSString alloc] initWithContentsOfURL:dataUrl
                                                 encoding:CFStringConvertEncodingToNSStringEncoding(0x0422)
                                                    error:&error];
        if (dataStr == nil || dataStr.length == 0) {
            return ;
        } else {
            [dataStr writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
            dataFileWasCreated = YES;
        }
    }
	
    //NSLog(@"dataStr -> %@", dataStr);
    NSArray *strLines = [dataStr componentsSeparatedByString:@"\n"];
    NSString *tagPattern = @"^\\[.*\\]$";
    NSString *key=nil;
    NSMutableString *strBuf = [[NSMutableString alloc] init];
    for (NSString *strLine in strLines) {
        NSString *line = [strLine stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSRange tagRange = [line rangeOfString:tagPattern options:NSRegularExpressionSearch];
        if (tagRange.location != NSNotFound) {
            if (strBuf != nil && key != nil) {
                // key 값에  strBuf 대입하기..
                
                [programDataDic setObject:[strBuf stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:key];
                //NSLog(@"%@ -> %@", key, strBuf);
                
                strBuf = [[NSMutableString alloc] init];
            }
            key = [line substringWithRange:NSMakeRange(tagRange.location+1, tagRange.length-2)];
            //NSLog(@"%@", [line substringWithRange:NSMakeRange(tagRange.location+1, tagRange.length-2)]);
        }
        else {
            [strBuf appendString:strLine];
            [strBuf appendString:@"\n"];
            //NSLog(@"%@", strBuf );
        }
    }
    
}

- (NSDictionary *)get
{
    TimeTable *tt = [[TimeTable alloc] init];
    NSDictionary *ttDic = [tt get];
    NSString *progcd = ttDic[@"progcd"];
    //NSLog(@"progcd -> %@", progcd);
    
    if ([programDataDic count] == 0) {
        [self parseProgramData:progcd];
    }
    
    //NSLog(@"APP_IMG2 -> %@", [programDataDic objectForKey:@"APP_IMG2"]);
//    NSString *appImg2Url = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/info/%@_4.jpg", progcd];
//    [programDataDic setObject:appImg2Url forKey:@"APP_IMG2"];
    
    
    return programDataDic;
}

@end
