//
//  BandiBoardWriteLandscapeView.h
//  Bandi2
//
//  Created by admin on 2014. 2. 5..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BandiBoardWriteLandscapeView : UIView

@property (strong, nonatomic) IBOutlet BandiBoardWriteLandscapeView *wrapperView;

@property (strong, nonatomic) IBOutlet UITextView *textField;
- (IBAction)postBulletin:(id)sender;
- (IBAction)cancel:(id)sender;
@end
