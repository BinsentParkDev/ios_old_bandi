//
//  ProgramListCell.h
//  EBSTVApp
//
//  Created by Hyeoung seok Yoon on 2013. 11. 10..
//  Copyright (c) 2013년 Hyeoung seok Yoon. All rights reserved.
//


#import <UIKit/UIKit.h>

//@protocol onAirAgainCellDelegate;

@interface boardCell : UITableViewCell
{
//    IBOutlet UIButton       *btPlay;
//    IBOutlet UILabel        *lbText;
//    IBOutlet UIImageView    *ivIcon;
    

//    IBOutlet UILabel        *lbTitle;
//    IBOutlet UILabel        *lbContent;
//    IBOutlet UILabel        *lbAdmin;
//    IBOutlet UIImageView    *ivAdmin;
//    
  
//    int listIndex;
    
}
@property (nonatomic, assign) int listIndex;
@property (nonatomic, strong) IBOutlet UILabel        *lbTitle;
@property (nonatomic, strong) IBOutlet UILabel        *lbContent;
@property (nonatomic, strong) IBOutlet UIImageView    *ivAdmin;
@property (nonatomic, strong) IBOutlet UILabel        *lbAdmin;

-(void)cellData:(NSDictionary *)dic;

@end
