//
//  SettingView.h
//  Bandi2
//
//  Created by admin on 2014. 1. 15..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface SettingView : UIScrollView
{
    NSUserDefaults *userDefaults ;
    NSTimer *quitTimer;
    
    
    int nSelectTimePickerMode; //1 자동실행 2자동 종료
    BOOL bHaveNewVer;
}
- (void)onUnload;
- (NSString *)slideToTimeStr:(UISlider *)slider;

@property (strong, nonatomic) IBOutlet SettingView *wrapperView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *networkView;
@property (strong, nonatomic) IBOutlet UIView *networkView2;

@property (strong, nonatomic) IBOutlet UIView *reservedRunView;
@property (strong, nonatomic) IBOutlet UIView *reservedRunView2;
@property (strong, nonatomic) IBOutlet UIView *timePickerView;
- (IBAction)cancelPickedTime:(id)sender;
- (IBAction)setPickedTime:(id)sender;
@property (strong, nonatomic) IBOutlet UIDatePicker *timePicker;

@property (strong, nonatomic) IBOutlet UIView *quitTimerView;
@property (strong, nonatomic) IBOutlet UIView *quitTimerView2;

@property (strong, nonatomic) IBOutlet UIView *autoLoginView;
@property (strong, nonatomic) IBOutlet UIView *autoLoginView2;

@property (strong, nonatomic) IBOutlet UIView *appVersionView;
@property (strong, nonatomic) IBOutlet UIView *appVersionView2;


//@property (strong, nonatomic) IBOutlet UISwitch *paidNetworkSwitch;
@property (strong, nonatomic) IBOutlet UIButton *paidNetworkBtn;
- (IBAction)usePaidNetwork:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *reservedTime;

@property (strong, nonatomic) IBOutlet UIButton *reservedRunBtn;
- (IBAction)useReservedRun:(id)sender;
- (IBAction)selectWeekdateAll:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *sundayBtn;
- (IBAction)runSunday:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *mondayBtn;
- (IBAction)runMonday:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *tuesdayBtn;
- (IBAction)runTuesday:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *wednesdayBtn;
- (IBAction)runWednesday:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *thursdayBtn;
- (IBAction)runThursday:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *fridayBtn;
- (IBAction)runFriday:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *saturdayBtn;
- (IBAction)runSaturday:(id)sender;
// 일~금 버튼 컬렉션
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *weekBtnGrp;


@property (strong, nonatomic) IBOutlet UISlider *quitTimerSlider;
- (IBAction)setQuitTimer:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *timerAutoStop;
- (IBAction)runTimer:(id)sender;
- (IBAction)cancelTimer:(id)sender;


//@property (strong, nonatomic) IBOutlet UISwitch *autoLoginSwitch;
@property (strong, nonatomic) IBOutlet UIButton *autoLoginBtn;
- (IBAction)useAutoLogin:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *autoLoginId;
@property (strong, nonatomic) IBOutlet UIButton *secureLoginBtn;
- (IBAction)useSecureLogin:(id)sender;


/**
 현재 로그인 ID 관련 뷰
 */
@property (strong, nonatomic) IBOutlet UIView *currentLoginView;

/**
 로그아웃 처리함. 현재 세션 정보 초기화

 @param sender 버튼 ID
 */
- (IBAction)logout:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *logoutBtn;

/**
 로그아웃 처리하고 , 웹 회원탈퇴 페이지 호출함

 @param sender <#sender description#>
 */
- (IBAction)withdraw:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *withdrawBtn;

@property (strong, nonatomic) IBOutlet UILabel *appVersionText;
@property (strong, nonatomic) IBOutlet UILabel *appVersionCompare;
@property (strong, nonatomic) IBOutlet UIButton *btVerUp;

@property (strong, nonatomic) IBOutlet UIView *playOpitonView;
@property (strong, nonatomic) IBOutlet UIView *playOptionView2;

@property (strong, nonatomic) IBOutlet UISwitch *playOptionSwitch;
@property (strong, nonatomic) IBOutlet UIButton *playOptionBtn;

@property (strong, nonatomic) IBOutlet UIButton *autoStopBtn;

@property (strong, nonatomic) IBOutlet UIView *startChanner;
@property (strong, nonatomic) IBOutlet UIButton *startCh1;
@property (strong, nonatomic) IBOutlet UIButton *startCh2;

-(IBAction)goStore:(id)sender;

- (IBAction)startChannelSet1:(id)sender;
- (IBAction)startChannelSet2:(id)sender;


/**
 개인정보 처리방침 웹페이지를 웹뷰로 보여준다.

 @param sender 버튼
 */
- (IBAction)personalInformationPolicyLink:(id)sender;


/**
 현재 로그인 중인 EBS id 노출용
 */
@property (strong, nonatomic) IBOutlet UILabel *currentLoginId;


- (IBAction)playOption:(id)sender;

@end
