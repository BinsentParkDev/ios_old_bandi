//
//  ProgramListCell.h
//  EBSTVApp
//
//  Created by Hyeoung seok Yoon on 2013. 11. 10..
//  Copyright (c) 2013년 Hyeoung seok Yoon. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol onNotiCellDelegate;

@interface notiListCell : UITableViewCell
{

    
}

@property (nonatomic, assign) int listIndex;
@property (nonatomic, strong) IBOutlet UILabel        *lbEventTitle;
@property (nonatomic, strong) IBOutlet UILabel        *lbEventDay;
@property (nonatomic, strong) IBOutlet UIImageView    *ivNoti;
@property (nonatomic, strong) IBOutlet UIImageView    *ivEvent;
@property (nonatomic, strong) IBOutlet UILabel        *lbNoti;
@property (nonatomic, assign) id<onNotiCellDelegate> cellDelegate;

-(void)cellData:(NSDictionary *)dic;

-(IBAction)showDetail:(id)sende;

@end

@protocol onNotiCellDelegate <NSObject>

//-(void)selectCheck:(int)indx btState:(BOOL)state;
-(void)selectNoti:(int)indx;

@end
