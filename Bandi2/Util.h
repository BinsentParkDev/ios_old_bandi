//
//  Util.h
//  Bandi2
//
//  Created by admin on 2014. 1. 26..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <netinet/in.h>
#import <netdb.h>
#import <stdlib.h>
#import <arpa/inet.h>
#import "PlayerView.h"
#import "OnAirView.h"
#import "TBXML.h"


#define NETWORK_NONE	0
#define NETWORK_WIFI	1
#define NETWORK_OTHER	2

@interface Util : NSObject

+ (void)setBaseBackgroundImage:(UIView *)view;
+ (NSInteger)getNetworkReachable ;
+ (PlayerView *)getPlayerView:(UIView *)superview;
+ (OnAirView *)getOnAirView:(UIView *)superview;

/**
 * 현재 시간에 대한 정보 반환  반환
 *
 * @param
 * @return 년월일시분초 (20131200000000) 형식 문자열
 */
+ (NSString *)getTimeStamp;

+ (NSCalendar *)getKoreanCalendar;

+ (NSString *)xmlEntitiesDecode:(NSString *)string;

+ (NSString *)encodeURL:(NSString *)urlString;

+ (float)correctionHeight;

+ (BOOL)existsLocalFile:(NSString *)filePath freshTime:(NSInteger)seconds;

+ (NSString *)getTempFilePath:(NSString *)filename;

+ (NSString *)getXMLElementText:(NSString *)xmlStr elementPath:(NSArray *)tagArray;
+ (NSString *)getXMLElementText2:(NSString *)filePath elementPath:(NSArray *)tagArray;

+ (NSString *)getDeviceID;

+ (BOOL)lessThanVersion7;

+ (NSString *)getHostIP:(NSString *)hostname;

/**
 * Convert json String to NSDictionary Type object
 */
+ (NSDictionary *)convertJsonToDic:(NSString *)jsonStr;

/**
 * 문자열에 한글(unicode category : Letter Other) 포함여부 확인)
 */
+ (Boolean)containsHangul:(NSString *)str;

/**
 * 문자열 AES 256 암호화 수행
 */
+ (NSString *)encryptString:(NSString *)str;

/**
 * 문자열 AES 256 복호화 수행
 */
+ (NSString *)decryptString:(NSString *)str;

/**
 * parse query string of URL
 */
+ (NSDictionary*) parseQueryString:(NSString *)_query;

@end
