//
//  BandiBoardWriteView.m
//  Bandi2
//
//  Created by admin on 2014. 1. 18..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "BandiBoardWriteView.h"
#import "TimeTable.h"
#import "IIViewDeckController.h"

@implementation BandiBoardWriteView {
    TimeTable *tt;
    NSDictionary *ttDic;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"BandiBoardWriteView" owner:self options:nil];
//        tt = [[TimeTable alloc] init];
//        ttDic = [tt get];
        _programLabel.text = ttDic[@"progname"];
        
//        // 백그라운드 이미지 설정
//        UIGraphicsBeginImageContext(_wrapperView.frame.size);
//        [[UIImage imageNamed:@"bg_base.png"] drawInRect:_wrapperView.bounds];
//        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        _wrapperView.backgroundColor = [UIColor colorWithPatternImage:image];
        
        // textfield round border
        UIColor *borderColor = [[UIColor alloc] initWithRed:(146/255.0) green:(162/255.0) blue:(174/255.0) alpha:1.0];
        [_textField.layer setCornerRadius:5.0f];
        [_textField.layer setBorderWidth:1.0f];
        [_textField.layer setBorderColor:[borderColor CGColor]];
        [_textField.layer setMasksToBounds:YES];
        
        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        
        [_wrapperView setFrame:CGRectMake(0, 0, 320, bodyHeight)];
        
        [self addSubview:_wrapperView];
        
        
    }
    return self;
}

- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)alertView:(NSString *)msgText
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:msgText
                                                   delegate:nil
                                          cancelButtonTitle:@"닫기"
                                          otherButtonTitles:nil ];
    [alert show];
}

- (IBAction)postBulletin:(id)sender {
    NSString *userId = SharedAppDelegate.sessionUserId;
    NSString *userName = [SharedAppDelegate.sessionUserName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *userText = [_textField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if ([_textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        [self alertView:@"내용을 작성해주세요."];
        return;
    }
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppPost?addDsCd=03&progcd=%@&userid=%@&writer=%@&contents=%@", ttDic[@"progcd"], userId, userName, userText];
    NSLog(@"%@", urlStr);
    NSURL *url = [NSURL URLWithString:urlStr];
    NSError *error;
    
    NSString *resultText = [NSString stringWithContentsOfURL:url
                                                    encoding:NSUTF8StringEncoding
                                                       error:&error];
    if(!resultText || resultText == nil)
        return;
    resultText = [resultText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([resultText isEqualToString:@"0"]) {
        [self alertView:@"게시물이 등록되었습니다."];
        [self removeFromSuperview];
    } else {
        [self alertView:@"게시물 등록에 문제가 있습니다."];
    }
    
}

- (IBAction)cancel:(id)sender {
    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
    
    cc.navigationItem.title = SharedAppDelegate.latestMenu;
    
    [self removeFromSuperview];
}

// keyboard hide when touching background view
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //터치한 객체를 가져온다.
	UITouch *touch = [[event allTouches] anyObject];
    NSLog(@"touch -> %@", [touch view]);
    
	[self endEditing:YES];
}

@end
