//
//  BandiBoardView.h
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MNMBottomPullToRefreshManager.h"

@interface BandiBoardSmallView : UIView<UITableViewDataSource,UITableViewDelegate,MNMBottomPullToRefreshManagerClient>
{
    NSMutableArray *boardList;
    NSMutableDictionary *notiDic;
    MNMBottomPullToRefreshManager *pullToRefreshManager_;

    NSString *strEventDetailUrl;
    
}
- (void)onUnload;
@property (strong, nonatomic) IBOutlet UITableView *tbList;
@property (strong, nonatomic) IBOutlet BandiBoardSmallView *wrapperView;
@property (strong, nonatomic) IBOutlet UILabel *programLabel;
//@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) IBOutlet UIView * notiView;
@property (strong, nonatomic) IBOutlet UIView * inputView;
@property (strong, nonatomic) IBOutlet UIView * tabView;

@property (strong, nonatomic) IBOutlet UILabel * notiTitle;
@property (strong, nonatomic) IBOutlet UILabel * titleText;

@property (strong, nonatomic) IBOutlet UILabel * noUserList;

@property (strong, nonatomic) IBOutlet UIButton * btMenuReadRadio;
@property (strong, nonatomic) IBOutlet UIButton * btMenuEngRadio;

@property (strong, nonatomic) IBOutlet UITextField * txtField;
@property (strong, nonatomic) IBOutlet UIImageView * snsIcon;
@property (strong, nonatomic) IBOutlet UIImageView * inputIcon;

@property (strong, nonatomic) IBOutlet UIView * detailView;
//@property (strong, nonatomic) IBOutlet UIScrollView * detailContentScroll;
@property (strong, nonatomic) IBOutlet UILabel * detailNotiContent;
@property (strong, nonatomic) IBOutlet UILabel * detailTitle;
@property (strong, nonatomic) IBOutlet UIScrollView * detailContentScroll;

@property (strong, nonatomic) IBOutlet UIButton * eventDetailBt;


- (IBAction)tabPlayFmRadio:(id)sender;
- (IBAction)tabPlayEndRadio:(id)sender;
- (IBAction)writeBulletin:(id)sender;
- (IBAction)openNoti:(id)sender;
- (IBAction)writePost:(id)sender;
-(void)hideKeypad;

- (IBAction)closeNotiDetail:(id)sender;
- (void)setNotiDic:(NSDictionary *)dic;
//-(void)bandiBoardShowSmall;
-(void)bandiBoardShowBig;
-(IBAction)eventDetailUrlOpen:(id)sender;
@end
