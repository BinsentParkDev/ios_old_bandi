//
//  ProgramData.h
//  Bandi2
//
//  Created by admin on 2014. 1. 13..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProgramData : NSObject
{
    bool dataFileWasCreated;
    NSMutableDictionary *programDataDic;
}
- (void)parseProgramData:(NSString *)progcd;
- (NSDictionary *)get;
@end
