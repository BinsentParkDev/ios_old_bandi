//
//  ProgramListCell.m
//  EBSTVApp
//
//  Created by Hyeoung seok Yoon on 2013. 11. 10..
//  Copyright (c) 2013년 Hyeoung seok Yoon. All rights reserved.
//


#import "checkListCell.h"

@interface checkListCell ()

@end
 
@implementation checkListCell

//@synthesize lbTitle;
//@synthesize lbContent;



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated]; 
}

-(void)cellData:(NSDictionary *)dic
{
    NSLog(@"dic = %@",dic);
    _lbContent.text = [dic valueForKey:@"bandiTitle"];
    _btState.selected = YES;
}

-(IBAction)btStateSel:(id)sender
{
    _btState.selected =! _btState.selected;
    [_cellDelegate selectCheck:_listIndex state:_btState.selected];
}
@end
