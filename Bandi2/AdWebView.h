//
//  AdWebView.h
//  Bandi2
//
//  Created by admin on 2015. 7. 13..
//  Copyright (c) 2015년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * LeftViewController.m 에서 메뉴간 이동 시 클래스 체크할 때, <br>
 * 광고 영역 WebView 를 UIWebView 클래스와 이름이 다르게 하기 위해서 만든 UIWebView subclass <br>
 */
@interface AdWebView : UIWebView<UIWebViewDelegate>

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType;

@end
