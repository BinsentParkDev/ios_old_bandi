//
//  SettingView.m
//  Bandi2
//
//  Created by admin on 2014. 1. 15..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "SettingView.h"
#import "ServerSide.h"
#import "Dialogs.h"
#import "PlayerView.h"
#import "Util.h"
#import "NSUserDefaults+UserData.h"
#import "WebViewController.h"
#import "GA.h"


#define TAG @"Setting"

@implementation SettingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [[NSBundle mainBundle] loadNibNamed:@"SettingView" owner:self options:nil];
        float accuHeight = 10.0f; // to set position
//        UIColor *borderColor = [[UIColor alloc] initWithRed:(146/255.0) green:(162/255.0) blue:(174/255.0) alpha:1.0];
        
        [_scrollView addSubview:_networkView];
        [_networkView setFrame:CGRectMake(0, accuHeight, _networkView.frame.size.width, _networkView.frame.size.height)];
        accuHeight += _networkView.frame.size.height+10;
        
        
        [_scrollView addSubview:_reservedRunView];
        [_reservedRunView setFrame:CGRectMake(0, accuHeight, _reservedRunView.frame.size.width, _reservedRunView.frame.size.height)];
        accuHeight += _reservedRunView.frame.size.height+10;
        
 
        
        
        [_scrollView addSubview:_quitTimerView];
        [_quitTimerView setFrame:CGRectMake(0, accuHeight, _quitTimerView.frame.size.width, _quitTimerView.frame.size.height)];
        accuHeight += _quitTimerView.frame.size.height+10;
        
//        [_quitTimerView2.layer setCornerRadius:5.0f];
//        [_quitTimerView2.layer setBorderWidth:1.0f];
//        [_quitTimerView2.layer setBorderColor:[borderColor CGColor]];
//        [_quitTimerView2.layer setMasksToBounds:YES];
        
        
        
        [_scrollView addSubview:_autoLoginView];
        [_autoLoginView setFrame:CGRectMake(0, accuHeight, _autoLoginView.frame.size.width, _autoLoginView.frame.size.height)];
        accuHeight += _autoLoginView.frame.size.height + 10;
        
//        [_autoLoginView2.layer setCornerRadius:5.0f];
//        [_autoLoginView2.layer setBorderWidth:1.0f];
//        [_autoLoginView2.layer setBorderColor:[borderColor CGColor]];
//        [_autoLoginView2.layer setMasksToBounds:YES];
//     
        
        
        [_scrollView addSubview:_playOpitonView];
        [_playOpitonView setFrame:CGRectMake(0, accuHeight, _playOpitonView.frame.size.width, _playOpitonView.frame.size.height)];
        accuHeight += _playOpitonView.frame.size.height + 10;
        
//        [_playOptionView2.layer setCornerRadius:5.0f];
//        [_playOptionView2.layer setBorderWidth:1.0f];
//        [_playOptionView2.layer setBorderColor:[borderColor CGColor]];
//        [_playOptionView2.layer setMasksToBounds:YES];
        
        [_scrollView addSubview:_startChanner];
        [_startChanner setFrame:CGRectMake(0, accuHeight, _startChanner.frame.size.width, _startChanner.frame.size.height)];
        accuHeight += _startChanner.frame.size.height + 10;

        
        
        [_scrollView addSubview:_appVersionView];
        [_appVersionView setFrame:CGRectMake(0, accuHeight, _appVersionView.frame.size.width, _appVersionView.frame.size.height)];
        accuHeight += _appVersionView.frame.size.height + 10;
        
//        [_appVersionView2.layer setCornerRadius:5.0f];
//        [_appVersionView2.layer setBorderWidth:1.0f];
//        [_appVersionView2.layer setBorderColor:[borderColor CGColor]];
//        [_appVersionView2.layer setMasksToBounds:YES];
//        
        
        
        [_wrapperView addSubview:_scrollView];
        //** fit wrapper view height
        CGRect newFrame = _wrapperView.frame;
        newFrame.size.height = [SharedAppDelegate.bodyHeight intValue];
        _wrapperView.frame = newFrame;
        
        //** fit _scrollView size to _wrapperView
        newFrame = _wrapperView.frame;
        //newFrame.size.height = _wrapperView.frame.size.height;
        //newFrame.size.width = _wrapperView.frame.size.width;
        [_scrollView setFrame:newFrame];
        [_scrollView setContentSize:CGSizeMake(320, accuHeight+60)];
        
//        [_wrapperView setBackgroundColor:[UIColor colorWithRed:(221/255.0) green:(232/255.0) blue:(236/255.0) alpha:1.0]];
        
        [self addSubview:_wrapperView];
        
        // 시간 설정 UILabel 에 액션 추가.
        _reservedTime.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getTime:)];
        [_reservedTime addGestureRecognizer:tapGesture];
        
        //자동종료
        _timerAutoStop.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapAutoStopGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getAutoStopTime:)];
        [_timerAutoStop addGestureRecognizer:tapAutoStopGesture];

        
        // adjust view height
        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        frame.size.height = bodyHeight;
        [self setFrame:frame];
        
        // 사용자 저장값 불러와서 재설정
        userDefaults = [NSUserDefaults standardUserDefaults];

        _paidNetworkBtn.selected =[userDefaults boolForKey:@"PAID_NETWORK"];
        _autoLoginBtn.selected = [userDefaults boolForKey:@"AUTO_LOGIN"];
        
        [userDefaults registerDefaults:@{@"SECURE_LOGIN":@YES}];
        _secureLoginBtn.selected = [userDefaults boolForKey:@"SECURE_LOGIN"];

        if ([userDefaults boolForKey:@"AUTO_LOGIN"]) {
            _autoLoginId.text = [NSString stringWithFormat:@"(ID: %@)", [userDefaults getUserId]];
        } else {
            _autoLoginId.text = @"";
        }
        

        NSNumber *hour = [userDefaults valueForKey:@"RESERVED_HOUR"];
        NSNumber *minute = [userDefaults valueForKey:@"RESERVED_MINUTE"];
        if ([hour intValue] > 12) {
            _reservedTime.text = [NSString stringWithFormat:@"오후 %02d:%02d", [hour intValue]-12, [minute intValue]];
        } else {
            _reservedTime.text = [NSString stringWithFormat:@"오전 %02d:%02d", [hour intValue], [minute intValue]];
        }
//        [_reservedRunSwitch setOn:[userDefaults boolForKey:@"RESERVED_RUN"]];
        NSLog(@".. %d",[userDefaults boolForKey:@"RESERVED_RUN"]);
        _reservedRunBtn.selected = [userDefaults boolForKey:@"RESERVED_RUN"];
        _sundayBtn.selected = [userDefaults boolForKey:@"WEEK_SUN"];
        _mondayBtn.selected = [userDefaults boolForKey:@"WEEK_MON"];
        _tuesdayBtn.selected = [userDefaults boolForKey:@"WEEK_TUE"];
        _wednesdayBtn.selected = [userDefaults boolForKey:@"WEEK_WED"];
        _thursdayBtn.selected = [userDefaults boolForKey:@"WEEK_THU"];
        _fridayBtn.selected = [userDefaults boolForKey:@"WEEK_FRI"];
        _saturdayBtn.selected = [userDefaults boolForKey:@"WEEK_SAT"];
        
        
        _autoStopBtn.selected =NO;
        BOOL bAutoStop = (BOOL)[userDefaults boolForKey:@"AUTO_OFF"];
      
        NSString *strAutoOffTimerHour = [[userDefaults valueForKey:@"AutoOffTimer"] substringToIndex:2];
        NSString *strAutoOffTimermin = [[userDefaults valueForKey:@"AutoOffTimer"] substringFromIndex:2];
        
        if(bAutoStop)
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"HHmmss"];
            
            NSString *date = [dateFormatter stringFromDate:[NSDate date]];
            NSDate *date1 = [dateFormatter dateFromString:[NSString stringWithFormat:@"%02d%02d00",[strAutoOffTimerHour integerValue],[strAutoOffTimermin integerValue]]];
            NSDate *date2 = [dateFormatter dateFromString:date];
            NSTimeInterval diff = [date1 timeIntervalSinceDate:date2]; // diff = 3600.0
            if(diff < 0)
            {
                diff = 86400 - diff;
            }
            
            [SharedAppDelegate.quitTimer invalidate];
            SharedAppDelegate.quitTimer = [NSTimer scheduledTimerWithTimeInterval:diff target:SharedAppDelegate selector:@selector(updateTimerText:) userInfo:nil repeats:NO];
            
            _autoStopBtn.selected =YES;
        }
        
        
        NSLog(@"%@/%@",strAutoOffTimerHour,strAutoOffTimermin);
        NSString *timeString;
        if ([strAutoOffTimerHour intValue] > 12) {
            timeString = [NSString stringWithFormat:@"오후 %02d:%02d", [strAutoOffTimerHour intValue]-12, [strAutoOffTimermin intValue]];
        } else {
            if ([strAutoOffTimerHour intValue]==12) {
                timeString = [NSString stringWithFormat:@"오후 %02d:%02d", [strAutoOffTimerHour intValue], [strAutoOffTimermin intValue]];
            } else {
                timeString = [NSString stringWithFormat:@"오전 %02d:%02d", [strAutoOffTimerHour intValue], [strAutoOffTimermin intValue]];
            }
        }
        _timerAutoStop.text = timeString;
        

        
        // 재생 옵션 - 초기 설치 후 아무 값이 없을 때는 기본으로 YES 값을 가지게 설정
        NSLog(@"play_bg %@", [userDefaults valueForKey:@"PLAY_BG"]);
        
        if ([userDefaults valueForKey:@"PLAY_BG"] == NULL) {
            [userDefaults setValue:@"YES" forKey:@"PLAY_BG"];
        }
        
        NSString *playBg = (NSString *)[userDefaults valueForKey:@"PLAY_BG"];
        if ([playBg isEqualToString:@"YES"]) {
            _playOptionBtn.selected = YES;
        } else {
            _playOptionBtn.selected = NO;
        }
        
        
        // app version
        NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
        NSString *version = [infoDic objectForKey:@"CFBundleShortVersionString"];
        _appVersionText.text = [NSString stringWithFormat:@"Ver. %@", version];
        
        
        
        NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
        NSString*  appVer = [infoDict objectForKey:@"CFBundleVersion"];
//        if([appVer compare:[[NSUserDefaults standardUserDefaults] objectForKey:@"appSerVer"]] == NSOrderedAscending)

  
        if([appVer intValue] < [[[NSUserDefaults standardUserDefaults] objectForKey:@"appSerVer"] intValue])
        {
            _appVersionCompare.text = @"업그레이드 버전이 있습니다";
            bHaveNewVer = YES;
        }
        else
        {
            _appVersionCompare.text = @"최신버전입니다.";
            bHaveNewVer = NO;
        }

        if([userDefaults integerForKey:@"StartFm"] == 2)
        {
            _startCh1.selected = NO;
            _startCh2.selected = YES;
        }
        else
        {
            _startCh1.selected = YES;
            _startCh2.selected = NO;
        }
        
        //** 로그인 > 현재 로그인 노출 여부 결정
        [self changeLoginStatus];
        //** 로그인 > 현재 로그인 노출 여부 결정 **/
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(AutoStopPlayer:)
                                                 name:@"AUTO_STOP_RADIO"
                                               object:nil];
    
    //** Google Analytics **
    [GA sendScreenView:@"Setting"];
    
    return self;
}

- (void)changeLoginStatus
{
    if (SharedAppDelegate.sessionUserId == nil || [@"" isEqualToString:SharedAppDelegate.sessionUserId]) {
        [_currentLoginId setText:@"로그인 하지 않음"];
        [_logoutBtn removeFromSuperview];
        [_withdrawBtn removeFromSuperview];
        
    } else {
        [_currentLoginId setText:SharedAppDelegate.sessionUserId];
    }
}


- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AUTO_STOP_RADIO" object:nil];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (NSString *)slideToTimeStr:(UISlider *)slider
{
    int hour = (int) slider.value / (60*60);
    int min = (int) (slider.value/60) - (hour*60);
    int sec = (int) slider.value % 60;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d", hour, min, sec];
}

#define SETTING_WIFI_WARNNING @"통신 요금제에 따라 데이터 요금이 별도로 부과될 수 있습니다.\n WiFi망에서만 사용을 원하실 경우 데이터 네트워크(3G/4G)를 반드시 OFF로 해 주시기 바랍니다."

- (IBAction)usePaidNetwork:(id)sender {
//    UISwitch *sw = (UISwitch *)sender;
//    _paidNetworkBtn.selected =[userDefaults boolForKey:@"PAID_NETWORK"];
    
    if(!_paidNetworkBtn.selected)
    {
        UIAlertView *alertV = [[UIAlertView alloc]initWithTitle:@"알림" message:SETTING_WIFI_WARNNING delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [alertV show];
    }
    
    _paidNetworkBtn.selected = !_paidNetworkBtn.selected;
    [userDefaults setBool:_paidNetworkBtn.selected forKey:@"PAID_NETWORK"];
    [userDefaults synchronize];    
}
- (IBAction)useReservedRun:(id)sender {
//    UISwitch *sw = (UISwitch *)sender;
    _reservedRunBtn.selected =! _reservedRunBtn.isSelected;
    [userDefaults setBool:_reservedRunBtn.isSelected forKey:@"RESERVED_RUN"];
    [userDefaults synchronize];
    [self reserveAutoRun];
}

- (IBAction)selectWeekdateAll:(id)sender {
    bool selecting = YES;
    for (int i=0; i<_weekBtnGrp.count; i++) {
        if (((UIButton *)_weekBtnGrp[i]).selected) {
            selecting = NO;
        }
    }
    
    for (int i=0; i<_weekBtnGrp.count; i++) {
        ((UIButton *)_weekBtnGrp[i]).selected = selecting;
    }
    
    NSArray *weekArr = @[@"WEEK_SUN", @"WEEK_MON", @"WEEK_TUE", @"WEEK_WED", @"WEEK_THU", @"WEEK_FRI", @"WEEK_SAT"];
    for (int i=0; i<weekArr.count; i++) {
        [userDefaults setBool:selecting forKey:weekArr[i]];
        [userDefaults synchronize];
    }
    
    [self reserveAutoRun];
}
- (IBAction)setQuitTimer:(id)sender {
    UISlider *slider = (UISlider *)sender;
    _timerAutoStop.text = [self slideToTimeStr:slider];
}

- (void)AutoStopPlayer:(NSNotification *)notification
{

//    _autoStopBtn.selected = NO;
}


- (IBAction)runTimer:(id)sender {
    NSLog(@"timer run");
    if(_autoStopBtn.selected)
    {
        _autoStopBtn.selected = NO;
        [SharedAppDelegate.quitTimer invalidate];
          [userDefaults setBool:NO forKey:@"AUTO_OFF"];
        [userDefaults synchronize];

    }
    else
    {
        

        
        NSString *strAutoOffTimerHour = [[userDefaults valueForKey:@"AutoOffTimer"] substringToIndex:2];
        NSString *strAutoOffTimermin = [[userDefaults valueForKey:@"AutoOffTimer"] substringFromIndex:2];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HHmmss"];
        
        NSString *date = [dateFormatter stringFromDate:[NSDate date]];
        NSDate *date1 = [dateFormatter dateFromString:[NSString stringWithFormat:@"%02d%02d00",[strAutoOffTimerHour integerValue],[strAutoOffTimermin integerValue]]];
        NSDate *date2 = [dateFormatter dateFromString:date];
        NSTimeInterval diff = [date1 timeIntervalSinceDate:date2]; // diff = 3600.0
        if(diff < 0)
        {
            diff = 86400 - diff;
        }
        
        [SharedAppDelegate.quitTimer invalidate];
        SharedAppDelegate.quitTimer = [NSTimer scheduledTimerWithTimeInterval:diff target:SharedAppDelegate selector:@selector(updateTimerText:) userInfo:nil repeats:NO];
        
        //시간을 저장하고., 다음 방문시 시간을 불러와 계산한다.??
        _autoStopBtn.selected = YES;
        [userDefaults setBool:YES forKey:@"AUTO_OFF"];
        [userDefaults synchronize];
        ////

        
    }
    
}


- (IBAction)cancelTimer:(id)sender {
    NSLog(@"quitTimer canceled. %f", SharedAppDelegate.quitTimerSlider.value);
    
    [SharedAppDelegate.quitTimer invalidate];
    
    
}

#pragma mark - login option

- (IBAction)useAutoLogin:(id)sender {
    
    if (!_autoLoginBtn.selected) {
        _autoLoginBtn.selected = NO;

        [Dialogs alertView:@"해제만 할 수 있습니다.\n설정은 로그인 시 가능합니다."];
    } else {
        _autoLoginBtn.selected = NO;
        [userDefaults setBool:NO forKey:@"AUTO_LOGIN"];
        [userDefaults setPassword:@""];
        if (! [userDefaults boolForKey:@"SAVE_ID"]) {
            [userDefaults setUserId:@""];
        }
        [userDefaults synchronize];
        _autoLoginId.text = @"";
        [Dialogs alertView:@"자동로그인이 해제 되었습니다."];
    }
}

- (IBAction)useSecureLogin:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.isSelected;
    [userDefaults setBool:btn.isSelected forKey:@"SECURE_LOGIN"];
    [userDefaults synchronize];
}

- (IBAction)logout:(id)sender {
    if (sender == nil) {
        SharedAppDelegate.sessionUserId = nil;
        SharedAppDelegate.sessionUserName = nil;
        SharedAppDelegate.sessionUserEmail = nil;
        //    SharedAppDelegate.sessionCookie = @"";
        SharedAppDelegate.sessionPasswd = nil;
        
        [userDefaults setBool:NO forKey:@"AUTO_LOGIN"];
        _autoLoginBtn.selected = NO;
        [_autoLoginId setText:@""];
        
        [Dialogs alertView:@"로그아웃 되었습니다."];
        [self changeLoginStatus];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"안내"
                                                        message:@"자동로그인이 설정되어 있다면, 자동로그인이 해지됩니다.\n로그아웃 하시겠습니까?"
                                                       delegate:self
                                              cancelButtonTitle:@"취소"
                                              otherButtonTitles:@"확인", nil];
        alert.tag = 2;  // delegate 에서 구별용
        [alert show];
    }
    
}

- (IBAction)withdraw:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"안내"
                                                    message:@"회원탈퇴를 위해 반디앱에서 로그아웃 처리됩니다."
                                                   delegate:self
                                          cancelButtonTitle:@"취소"
                                          otherButtonTitles:@"확인", nil];
    alert.tag = 1; // delegate 에서 구별용
    [alert show];
    
}

/**
 회원탈퇴(withdraw) UIAlertView delegate 구현, 로그아웃 처리 후, 회원 탈퇴 페이지 웹뷰 호출

 @param alertView alertView
 @param buttonIndex button index
 */
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"btn index : %ld / tag : %ld", (long)buttonIndex, (long)alertView.tag);

    if (alertView.tag == 1) {// 회원 탈퇴 alert view 에서 호출함

        if (buttonIndex == 1) { // 확인
            // sso 인증 체크
            NSDictionary *header ;
            NSString *body = @"" ;
            [ServerSide get:[NSString stringWithFormat:@"https://sso.ebs.co.kr/idp/check.jsp?userId=%@", [ServerSide percentEscapedString:SharedAppDelegate.sessionUserId]]
                 withCookie:SharedAppDelegate.sessionCookie
                    headers:&header
                       body:&body];
            body = [body stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSLog(@"SettingView.m > alertView > body : %@", body);
            if (! [body isEqualToString:@"100"]) { // 100 : login / 101 : not login / 102 : login with other account
                SharedAppDelegate.sessionCookie = @"";
                NSString *statusCode = [ServerSide doLoginNew:SharedAppDelegate.sessionUserId withPassword:SharedAppDelegate.sessionPasswd];
                NSLog(@"Setting.m > sso re-sync (not 100) id : %@, pw : %@ ", SharedAppDelegate.sessionUserId, SharedAppDelegate.sessionPasswd );
                if (![statusCode isEqualToString:@"200"]) {
                    [Dialogs alertView:@"로그인 동기화 실패하여 탈퇴페이지 접근이 되지 않습니다.\n외부 브라우져를 이용해주세요."];
                    return;
                }
            }
            
            // show webview
            NSURL *url = [NSURL URLWithString:@"https://sso.ebs.co.kr/idp/secession/auth.jsp?appYn=Y" ];
            WebViewController *vc = [[WebViewController alloc] init];
            vc.webView.contentMode = UIViewContentModeScaleAspectFit;
            [self.window.rootViewController presentViewController:vc animated:YES completion:^{
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request addValue:SharedAppDelegate.sessionCookie forHTTPHeaderField:@"Cookie"];
                NSLog(@"SettingView.m > alertView > request cookie : %@", [request valueForHTTPHeaderField:@"Cookie"]);
                NSLog(@"SettingView.m > alertView > session cookie : %@", SharedAppDelegate.sessionCookie);
                
                [vc.webView loadRequest:request];
                
                vc.webView.scalesPageToFit = YES;
                
            }];
            
            // 로그아웃
            [self logout:nil];
            
        }
        
    } else if (alertView.tag == 2) { // 로그아웃 alert view 에서 호출함
    
        if (buttonIndex == 1) {
            [self logout:nil];
            
        }
        
    }
    
}

- (IBAction)personalInformationPolicyLink:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://m.ebs.co.kr/cs/privacy" ];
    //    [[UIApplication sharedApplication] openURL:url];
    WebViewController *vc = [[WebViewController alloc] init];
    vc.webView.contentMode = UIViewContentModeScaleAspectFit;
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
        
        [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
        
        //        vc.webView.scalesPageToFit = YES;
        
    }];
    
}


#pragma mark - reserve auto run

- (IBAction)runSunday:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = (! btn.selected);
    [userDefaults setBool:btn.selected forKey:@"WEEK_SUN"];
    [self reserveAutoRun];
}
- (IBAction)runMonday:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = (! btn.selected);
    [userDefaults setBool:btn.selected forKey:@"WEEK_MON"];
//    NSLog(@"reserve - monday %d", btn.selected);
    [self reserveAutoRun];
}
- (IBAction)runTuesday:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = (! btn.selected);
    [userDefaults setBool:btn.selected forKey:@"WEEK_TUE"];
    [self reserveAutoRun];
}
- (IBAction)runWednesday:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = (! btn.selected);
    [userDefaults setBool:btn.selected forKey:@"WEEK_WED"];
    [self reserveAutoRun];
}
- (IBAction)runThursday:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = (! btn.selected);
    [userDefaults setBool:btn.selected forKey:@"WEEK_THU"];
    [self reserveAutoRun];
}
- (IBAction)runFriday:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = (! btn.selected);
    [userDefaults setBool:btn.selected forKey:@"WEEK_FRI"];
    [self reserveAutoRun];
}
- (IBAction)runSaturday:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = (! btn.selected);
    [userDefaults setBool:btn.selected forKey:@"WEEK_SAT"];
    [self reserveAutoRun];
}

- (void)reserveAutoRun
{
    // 기존 예약을 전부 취소하고..
    
//    if(!isLocalNotificationEnabled())
//        return;
    
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    long count;
    while ((count = [[[UIApplication sharedApplication] scheduledLocalNotifications] count]) > 0) {
        NSLog(@"Remaining notificaitons to cancel: %lu",(unsigned long)count);
        [NSThread sleepForTimeInterval:.01f];
    }
    
//    
//    UIApplication *app = [UIApplication sharedApplication];
//    NSArray *arr = [app scheduledLocalNotifications];
//    if (arr.count > 0) {
//        [app cancelAllLocalNotifications];
//    }

    if (_reservedRunBtn.isSelected) {
//        UILocalNotification *localNoti = [[UILocalNotification alloc] init];
//        localNoti.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Seoul"];
//        localNoti.repeatInterval = NSWeekCalendarUnit;
//        localNoti.soundName = UILocalNotificationDefaultSoundName;
//        localNoti.alertBody = @"EBS 반디 예약시각이 되었습니다. 지금 실행하시겠습니까?";
//        localNoti.alertAction = @"EBS 반디";
        
        NSNumber *hour = [userDefaults valueForKey:@"RESERVED_HOUR"];
        NSNumber *minute = [userDefaults valueForKey:@"RESERVED_MINUTE"];
        
        NSCalendar *cal = [NSCalendar autoupdatingCurrentCalendar];
        [cal setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
        NSDateComponents *currentTime = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit)
                                               fromDate:[NSDate date]];
        
        //int year = [currentTime year];
        //int month = [currentTime month];
        //int day = [currentTime day];
        int weekday = [currentTime weekday]; // 1-7 (sun-sat)

        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        formatter.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Seoul"];
        [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        
        
        bool selectedAny = NO;
        for (int i=0; i<_weekBtnGrp.count; i++) {
            
            if (((UIButton *)_weekBtnGrp[i]).selected) {
                selectedAny = YES;
                
                NSCalendar *tcal = [NSCalendar autoupdatingCurrentCalendar];
                [tcal setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
                NSDate *tdate;
                NSDateComponents *tcomp;
                
                int weekdayTag = ((UIButton *)_weekBtnGrp[i]).tag; // 일-토 = 1-7 xib 파일에서 할당함.
                
//                if (weekday == weekdayTag) { // 선택된 요일이 오늘 요일
//                    tdate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*(weekdayTag-weekday)];
//                    tcomp = [tcal components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:tdate];
//                    
//                } else if (weekday > weekdayTag) {
//                    tdate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*(weekdayTag-weekday)];
//                    tcomp = [tcal components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:tdate];
//                    
//                } else if (weekday < weekdayTag) {
//                    tdate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*(weekdayTag-weekday)];
//                    tcomp = [tcal components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:tdate];
//                    
//                }
                
                tdate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*(weekdayTag-weekday)]; 
                tcomp = [tcal components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:tdate];
                
                NSString *dateStr = [NSString stringWithFormat:@"%04d-%02d-%02d %02d:%02d:00",
                                     (int)[tcomp year], (int)[tcomp month], (int)[tcomp day], [hour intValue], [minute intValue] ];
                
                NSDate *runDate = [formatter dateFromString:dateStr];
                
                UILocalNotification *notification = [[UILocalNotification alloc] init];
                notification.timeZone = [NSTimeZone defaultTimeZone]; // The use of the local time zone
                notification.fireDate = runDate;
//                notification.repeatInterval = kCFCalendarUnitDay;
                notification.repeatInterval = NSCalendarUnitWeekOfYear;
                notification.alertBody   = @"EBS 반디 예약시각이 되었습니다. 지금 실행하시겠습니까?";
//                notification.alertAction = @"EBS 반디 예약시각이 되었습니다. 지금 실행하시겠습니까?";
                notification.soundName= UILocalNotificationDefaultSoundName;

                [[UIApplication sharedApplication] scheduleLocalNotification:notification];
                
                NSLog(@"reserve - localNoti.fireDate = %@ on %d (today:%d)\n", [notification fireDate], weekdayTag, weekday);

            }
            
        } // end for
        
    } // end if
    
    // **** begin test
    UIApplication *shrApp = [UIApplication sharedApplication];
    NSArray<UILocalNotification *> *localNotis = [shrApp scheduledLocalNotifications];
    NSLog(@"reserve - count : %ld", localNotis.count);
    // **** end test
}


#pragma mark - time picker
- (void)getTime:(UITapGestureRecognizer *)tapGesture
{
    nSelectTimePickerMode = 1;
    [self addSubview:_timePickerView];
    [_timePickerView setFrame:CGRectMake(0, 0, 320, _timePickerView.frame.size.height)];
    _timePicker.datePickerMode = UIDatePickerModeTime;
    //[_timePicker setDate:[NSDate date]]; // 초기값 설정
    
    UIColor *borderColor = [[UIColor alloc] initWithRed:(146/255.0) green:(162/255.0) blue:(174/255.0) alpha:1.0];

    [_timePickerView.layer setCornerRadius:5.0f];
    [_timePickerView.layer setBorderWidth:1.0f];
    [_timePickerView.layer setBorderColor:[borderColor CGColor]];
    [_timePickerView.layer setMasksToBounds:YES];
}

- (IBAction)cancelPickedTime:(id)sender {
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
//	NSDateComponents *currentTime = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:_timePicker.date];

    [_timePickerView removeFromSuperview];
}

- (IBAction)setPickedTime:(id)sender {
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
	NSDateComponents *currentTime = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:_timePicker.date];
    NSLog(@"time picker - %d/%d", [currentTime hour],[currentTime minute]);
    
    NSString *timeString;
    if ([currentTime hour] > 12) {
        timeString = [NSString stringWithFormat:@"오후 %02d:%02d", [currentTime hour]-12, [currentTime minute]];
    } else {
        if ([currentTime hour]==12) {
            timeString = [NSString stringWithFormat:@"오후 %02d:%02d", [currentTime hour], [currentTime minute]];
        } else {
            timeString = [NSString stringWithFormat:@"오전 %02d:%02d", [currentTime hour], [currentTime minute]];
        }
    }
    if( nSelectTimePickerMode == 1)
    {
        [userDefaults setValue:[NSNumber numberWithInteger:[currentTime hour]] forKey:@"RESERVED_HOUR"];
        [userDefaults setValue:[NSNumber numberWithInteger:[currentTime minute]] forKey:@"RESERVED_MINUTE"];
        
        _reservedTime.text = timeString;
        [self reserveAutoRun];

    }
    else if( nSelectTimePickerMode == 2)
    {
        
        _timerAutoStop.text = timeString;
        NSString *strSetTime = [NSString stringWithFormat:@"%02d%02d",[currentTime hour],[currentTime minute]];

        //
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HHmmss"];

        NSString *date = [dateFormatter stringFromDate:[NSDate date]];
        NSDate *date1 = [dateFormatter dateFromString:[NSString stringWithFormat:@"%02d%02d00",[currentTime hour],[currentTime minute]]];
        NSDate *date2 = [dateFormatter dateFromString:date];
        NSTimeInterval diff = [date1 timeIntervalSinceDate:date2]; // diff = 3600.0
        
        NSLog(@"diff = %f",diff);
        if(diff < 0)
        {
            diff = 86400 - diff;
        }
        //
        
        [userDefaults setObject:strSetTime forKey:@"AutoOffTimer"];
        [userDefaults synchronize];
        if(_autoStopBtn.selected)
        {
            [SharedAppDelegate.quitTimer invalidate];
            SharedAppDelegate.quitTimer = [NSTimer scheduledTimerWithTimeInterval:diff target:SharedAppDelegate selector:@selector(updateTimerText:) userInfo:nil repeats:NO];
//            [userDefaults setBool:YES forKey:@"AUTO_OFF"];
        }
        
    }
        [_timePickerView removeFromSuperview];
}

- (void)getAutoStopTime:(UITapGestureRecognizer *)tapGesture
{

    nSelectTimePickerMode = 2;
    [self addSubview:_timePickerView];
    [_timePickerView setFrame:CGRectMake(0, 0, 320, _timePickerView.frame.size.height)];
    _timePicker.datePickerMode = UIDatePickerModeTime;
    //[_timePicker setDate:[NSDate date]]; // 초기값 설정
    
    UIColor *borderColor = [[UIColor alloc] initWithRed:(146/255.0) green:(162/255.0) blue:(174/255.0) alpha:1.0];
    
    [_timePickerView.layer setCornerRadius:5.0f];
    [_timePickerView.layer setBorderWidth:1.0f];
    [_timePickerView.layer setBorderColor:[borderColor CGColor]];
    [_timePickerView.layer setMasksToBounds:YES];
    
    
}
#pragma mark - 백그라운드 재생 옵션
- (IBAction)playOption:(id)sender {
    _playOptionBtn.selected =! _playOptionBtn.selected;
    NSString *state;
    if (_playOptionBtn.selected) {
        state = @"YES";
    } else {
        state = @"NO";
    }
    
    
    [userDefaults setValue:state forKey:@"PLAY_BG"];
    
    //NSLog(@"play bg, %@", [userDefaults valueForKey:@"PLAY_BG"]);
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"내용을 작성해주세요." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
    [av show];
}

- (IBAction)startChannelSet1:(id)sender
{
    _startCh1.selected = YES;
    _startCh2.selected = NO;
    [userDefaults setInteger:1 forKey:@"StartFm"];
    [userDefaults synchronize];
}
- (IBAction)startChannelSet2:(id)sender
{
    _startCh1.selected = NO;
    _startCh2.selected = YES;
    [userDefaults setInteger:2 forKey:@"StartFm"];
    
    [userDefaults synchronize];
}


-(IBAction)goStore:(id)sender
{
    if(bHaveNewVer)
    {
        NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/kr/app/ebs-bandi/id368886634?l=en&mt=8"];
        [[UIApplication sharedApplication] openURL:url];
    }
//    UILocalNotification *notification = [[UILocalNotification alloc] init];
// UILocalNotification *notification2 = [[UILocalNotification alloc] init];
//    if (notification) {
//        // Set a reminder time notice
//        NSDate *currentDate   = [NSDate date];
//
//        
//        notification.timeZone = [NSTimeZone defaultTimeZone]; // The use of the local time zone
//        notification.fireDate = [currentDate dateByAddingTimeInterval:7.0];
//        notification.repeatInterval = kCFCalendarUnitDay;
//        notification.alertBody   = @"Wake up, man";
//        notification.alertAction = NSLocalizedString(@"Get up", nil);
//        notification.soundName= UILocalNotificationDefaultSoundName;
////        notification.applicationIconBadgeNumber++;
//        notification2.timeZone = [NSTimeZone defaultTimeZone]; // The use of the local time zone
//        notification2.fireDate = [currentDate dateByAddingTimeInterval:15.0];
//        notification2.repeatInterval = kCFCalendarUnitDay;
//        notification2.alertBody   = @"Wake up, man2";
//        notification2.alertAction = NSLocalizedString(@"2Get up", nil);
//        notification2.soundName= UILocalNotificationDefaultSoundName;
//        //
//        // Set notification userInfo, identifies the notice
////        NSMutableDictionary *aUserInfo = [[NSMutableDictionary alloc] init];
////        aUserInfo[kLocalNotificationID] = @"LocalNotificationID";
////        notification.userInfo = aUserInfo;
////        
//        // The notice was added to the system
//        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
//        [[UIApplication sharedApplication] scheduleLocalNotification:notification2];
//    }
    
}




@end
