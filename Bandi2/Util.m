//
//  Util.m
//  Bandi2
//
//  Created by admin on 2014. 1. 26..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "Util.h"
#import "NSString+Encrypt.h"

@implementation Util

// set bandi default background image
+ (void)setBaseBackgroundImage:(UIView *)view
{
    
    UIGraphicsBeginImageContext(view.frame.size);
    [[UIImage imageNamed:@"bg_base.png"] drawInRect:view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    view.backgroundColor = [UIColor colorWithPatternImage:image];
}

+ (NSInteger)getNetworkReachable {
	// 1. Get empty socket.
	struct sockaddr_in zeroAddr;
	bzero(&zeroAddr, sizeof(zeroAddr));
	zeroAddr.sin_len = sizeof(zeroAddr);
	zeroAddr.sin_family = AF_INET;
	
	// 2. Get Ref.
	SCNetworkReachabilityRef target = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddr);
	
	// 3. Get flag value.
	SCNetworkReachabilityFlags flag;
	SCNetworkReachabilityGetFlags(target, &flag);
	
	// Determine the network.
	if(flag & kSCNetworkFlagsReachable) {
		if(flag & kSCNetworkReachabilityFlagsIsWWAN)
			return NETWORK_OTHER;	// 전화망
		else
			return NETWORK_WIFI;	// Wi-Fi
	}
	else {
		return NETWORK_NONE;	// Not reacheable
	}
}


+ (PlayerView *)getPlayerView:(UIView *)superview
{
    NSArray *arr = superview.subviews;
    PlayerView *playerView = nil;
    
    for (int i=0; i<arr.count; i++) {
        if ([arr[i] isKindOfClass:PlayerView.class]) {
            playerView = arr[i];
            break;
        }
    }
    return playerView;
}

+ (OnAirView *)getOnAirView:(UIView *)superview
{
    NSArray *arr = superview.subviews;
    OnAirView *view = nil;
    
    for (int i=0; i<arr.count; i++) {
        if ([arr[i] isKindOfClass:OnAirView.class]) {
            view = arr[i];
            break;
        }
    }
    return view;
}

/**
 * return date string with 'yyyyMMddHHmmss' format
 */
+ (NSString *)getTimeStamp
{
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    
	NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:[NSDate date]];
    
    return [NSString stringWithFormat:@"%04d%02d%02d%02d%02d%02d", (int)[currentTime year], (int)[currentTime month], (int)[currentTime day], (int)[currentTime hour], (int)[currentTime minute], (int)[currentTime second] ];
}

+ (NSCalendar *)getKoreanCalendar
{
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    return calendar;
}

+ (NSString *)xmlEntitiesDecode:(NSString *)string
{
//    NSLog(@"before xml = %@",string);
    

    string = [string stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    string = [string stringByReplacingOccurrencesOfString:@"&#34;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&#35;" withString:@"#"];
    string = [string stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&#40;" withString:@"("];
    string = [string stringByReplacingOccurrencesOfString:@"&#41;" withString:@")"];
    string = [string stringByReplacingOccurrencesOfString:@"&#34;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
//    NSLog(@"after xml = %@",string);
    return string;
}

+ (NSString *)encodeURL:(NSString *)urlString
{
    NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                             NULL,
                                                                                             (__bridge CFStringRef) urlString,
                                                                                             NULL,
                                                                                             CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                             kCFStringEncodingUTF8));
    return result;
}

+ (float)correctionHeight
{
    float STATUS_HEIGHT = 0;
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch]==NSOrderedAscending)
    {
        STATUS_HEIGHT = 20;
    }
    return STATUS_HEIGHT;
}

/**
 유효한(생성 시간이 일정 시간 내 인) 파일이 로컬에 저장되어 있는지 확인
 */
+ (BOOL)existsLocalFile:(NSString *)filePath freshTime:(NSInteger)seconds
{
    NSError *error;
    
    // 파일 존재 확인
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL exist = [fileManager fileExistsAtPath:filePath];
    
    // 파일 생성, 유효 시간 비교
    NSDictionary *fileAttr = [fileManager attributesOfItemAtPath:filePath error:&error];
    NSDate *validTime = [NSDate dateWithTimeIntervalSinceNow:-1*seconds]; // 파일 유지시간.
    NSComparisonResult compResult = [validTime compare:fileAttr[NSFileCreationDate]];
 
    if (exist && compResult==NSOrderedAscending) {
        return YES;
    } else {
        return NO;
    }
}

/**
 전달된 파일이름을 가진 임시 디렉토리(Temporary Directory) 내의 파일 경로명을 전달
 */
+ (NSString *)getTempFilePath:(NSString *)filename
{
    return [NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), filename];
}


/**
 tagArray는 root element 명 부터 순차적으로 하위 태그 이름이 있어야 함.

 예) NSArray *tagArr = @[@"root","data","sometag"];
 */
+ (NSString *)getXMLElementText:(NSString *)xmlStr elementPath:(NSArray *)tagArray
{
    NSError *error;
    TBXML *tbxml = [TBXML newTBXMLWithXMLString:xmlStr error:&error];
    TBXMLElement *element = tbxml.rootXMLElement;
    
    for (int i=1; i<tagArray.count; i++) {
        if (element) {
            element = [TBXML childElementNamed:tagArray[i] parentElement:element];
        }
    }
    
    NSString *resultText=@"";
    @try {
        resultText = [TBXML textForElement:element];
    }
    @catch (NSException *exception) {
        NSLog(@"Error getXMLElementText - %@", [error localizedDescription]);
    }
    
    return resultText;
}


/**
 tagArray는 root element 명 부터 순차적으로 하위 태그 이름이 있어야 함.
 예) NSArray *tagArr = @[@"root","data","sometag"];

 xml URL 포멧이 정확하지 않을 때에는 동작 제대로 하지 못함.
 상단에 DOCUMENT TYPE 이 명확하게 있어야 한다.
 */
+ (NSString *)getXMLElementText2:(NSString *)filePath elementPath:(NSArray *)tagArray
{
    NSError *error;
    TBXML *tbxml = [TBXML newTBXMLWithXMLFile:filePath error:&error];
    NSLog(@"tbxml -> %@", tbxml);
    TBXMLElement *pelement = tbxml.rootXMLElement;
    TBXMLElement *element = nil;
    element = [TBXML childElementNamed:@"rescd" parentElement:pelement];
    
    for (int i=1; i<tagArray.count; i++) {
        NSLog(@"tag -> %@", tagArray[i]);
        //element = [TBXML childElementNamed:tagArray[i] parentElement:pelement];
        //pelement = element;
    }
    
    return @"aaa";//[TBXML textForElement:(__bridge TBXMLElement *)([elemArr lastObject])];;
}

/**
 기기 고유번호 반환
 */
+ (NSString *)getDeviceID
{
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

/**
 7.0 버전보다 아래면 
 */
+ (BOOL)lessThanVersion7
{
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch]==NSOrderedAscending)
    {
        return YES;
    } else {
        return NO;
    }
}


/**
  hostname -> ip 변경
 @date 2014.07.21
 */
+ (NSString *)getHostIP:(NSString *)hostname
{
    NSString *ip = hostname;

    @try {
        Boolean result = FALSE;
        CFHostRef hostRef;
        CFArrayRef addresses;
        hostRef = CFHostCreateWithName(kCFAllocatorDefault, (__bridge CFStringRef)hostname);
        if (hostRef) {
            result = CFHostStartInfoResolution(hostRef, kCFHostAddresses, NULL);
            if (result == TRUE) {
                addresses = CFHostGetAddressing(hostRef, &result);
            }
        }
        
        bool bResolved = (result == TRUE? true:false);
        if (bResolved) {
            struct sockaddr_in *remoteAddr;
            for (int i=0; i<CFArrayGetCount(addresses); i++) {
                CFDataRef saData = (CFDataRef)CFArrayGetValueAtIndex(addresses, i);
                remoteAddr = (struct sockaddr_in *)CFDataGetBytePtr(saData);
                
                if (remoteAddr != NULL) {
                    ip = [NSString stringWithCString:inet_ntoa(remoteAddr->sin_addr) encoding:NSASCIIStringEncoding];
                    //inet_ntoa(remoteAddr->sin_addr);
                    NSLog(@"address : %s, %@", inet_ntoa(remoteAddr->sin_addr), ip);
                }
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Getting Host IP is failure - %@", [exception description]);
    }
    
    return ip;
}


+ (NSDictionary *)convertJsonToDic:(NSString *)jsonStr
{
    NSError *error;
    NSData *jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
//    NSLog(@"jsonDic = %@", jsonDic);
    
    return jsonDic;
}

/**
 * http://www.fileformat.info/info/unicode/category/Lo/list.htm 에 해당하는 문자면 true
 */
+ (Boolean)containsHangul:(NSString *)str
{
    bool isContained = NO;
    
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithRange:NSMakeRange(170, 194931)];
    //NSLog(@"==> %lu, %lu", (unsigned long)[str length], (unsigned long)[str lengthOfBytesUsingEncoding:NSUTF8StringEncoding]);
    for (int i=0; i<[str lengthOfBytesUsingEncoding:NSUTF8StringEncoding]; i++) {
        if ([charSet characterIsMember:[str characterAtIndex:i]]) {
            isContained = YES;
            break;
        }
    }
    
    //CFCharacterSetGetTypeID()
    
    return isContained;
}

#pragma mark - 문자열 암,복호화

/**
 * 문자열 암호화 (원본 스트링 -> AES256 인코딩 -> base64 인코딩 -> 암호화 된 스트링)
 * 참고 : http://gyuha.tistory.com/422
 */
+ (NSString *)encryptString:(NSString *)str
{
    return [str encryptWithKey:[Util getDeviceID]];
}

/**
 * 문자열 복호화 (암호화 된 스트링 -> base64 디코딩 -> AES256 디코딩 -> 원본 스트링)
 * 참고 : http://gyuha.tistory.com/422
 */

+ (NSString *)decryptString:(NSString *)str
{
    return [str decryptWithKey:[Util getDeviceID]];
}


/**
 * url query를 딕셔너리로 반환하는 함수
 */
+ (NSDictionary*) parseQueryString:(NSString *)_query
{
    NSMutableDictionary* pDic = [NSMutableDictionary dictionary];
    NSArray* pairs = [_query componentsSeparatedByString:@"&"];
    for (NSString* sObj in pairs) {
        NSArray* elements = [sObj componentsSeparatedByString:@"="];
        NSString* key =     [[elements objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString* value =   [[elements objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [pDic setObject:value forKey:key];
    }
    
    return pDic;
}
@end
