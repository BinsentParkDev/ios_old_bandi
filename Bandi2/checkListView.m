//
//  BandiBoardView.m
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "checkListView.h"
#import "TimeTable.h"
#import "IIViewDeckController.h"
#import "LoginView.h"
#import "BandiBoardWriteView.h"
#import "ServerSide.h"
#import "LogContentController.h"
#import "WebViewController.h"
#import "Util.h"
#import "GA.h"

#define TAG @"BandiBoard"

#define RgbColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@implementation checkListView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        
        boardList = [[NSMutableArray alloc]init];
        
        [[[NSBundle mainBundle] loadNibNamed:@"checkListView" owner:self options:nil] lastObject];
        
        
//        [NSThread detachNewThreadSelector:@selector(loadCheckList) toTarget:self withObject:nil];

        [_tabView setFrame:CGRectMake(0, 0, 320, _tabView.frame.size.height)];
        [_wrapperView addSubview:_tabView];
        
        [_inputView setFrame:CGRectMake(0, _wrapperView.frame.size.height - _inputView.frame.size.height,
                                       320, _inputView.frame.size.height)];
        [_wrapperView addSubview:_inputView];
        
        [_notiView setFrame:CGRectMake(0, _tabView.frame.size.height, 320, _notiView.frame.size.height)];
        [_wrapperView addSubview:_notiView];
        
        float fh;
        fh = _wrapperView.frame.size.height - (_notiView.frame.origin.y + _notiView.frame.size.height +_inputView.frame.size.height);
              
        [_tbList setFrame:CGRectMake(0, _notiView.frame.origin.y + _notiView.frame.size.height,
                                    320, fh)];
        
        [_wrapperView setFrame:CGRectMake(0, 0, 320, frame.size.height)];
        [self addSubview:_wrapperView];
        _InfoText.hidden = YES;
        [self loadCheckList];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginSuccess:)
                                                 name:@"LoginStateIcon"
                                               object:nil];
    //** Google Analytics **
    [GA sendScreenView:@"Favorite"];
    
    return self;
}

- (void)loginSuccess:(NSNotification *)notification
{
    [self loadCheckList];
}


- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}
-(void)dealloc
{

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LoginStateIcon" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self];

}
- (void)loadCheckList
{
    
    if ([ServerSide autoLogin]) {
    } else {
//        IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
//        UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
        
//        CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);
        
//        SharedAppDelegate.latestMenu = @"즐겨찾기";
//        LoginView *myView = [[LoginView alloc] initWithFrame:frameRect];
////        [myView setBGoBack:YES];
//        [myView cancleBack:YES];
//        [myView EBSLoginOnly];
//        [self addSubview:myView];
        
//            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:loginController animated:YES completion:nil];
        
        

       
        
        NSString * strStrView;
        if (IS_IPHONE5)
            strStrView = @"LogContentController";
        else
            strStrView = @"LogContentController_480";
        
        LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
        
        [vc setBGoBack:YES];
        [vc EBSLoginOnly];

        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];

        
//        [self.window.rootViewController presentViewController:vc animated:YES completion:nil];
//        [cc.view bringSubviewToFront:myView];
//        cc.navigationItem.title = @"로그인";
        return;
    }

    NSError *error;

    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/function/bandiAppUserFbmkList.json?userId=%@", SharedAppDelegate.sessionUserId];
//    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppUserFbmkList?userId=%@&fileType=json", SharedAppDelegate.sessionUserId];
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    if(!jsonString || jsonString == nil)
        return;
    
    
    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
    if([[[person valueForKey:@"appUserFbmkLists"]valueForKey:@"resultCd"] isEqualToString:@"11"])
    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                        message:[[person valueForKey:@"appUserFbmkLists"]valueForKey:@"resultMsg"]
//                                                       delegate:nil
//                                              cancelButtonTitle:@"닫기"
//                                              otherButtonTitles:nil ];
//        [alert show];
        _tbList.hidden = YES;
         _InfoText.hidden = NO;
        return;
    }
    else
    {
        [boardList removeAllObjects];
        [boardList addObjectsFromArray:[[person valueForKey:@"appUserFbmkLists"] valueForKey:@"appUserFbmkLists"]];
        
    }
    if([boardList count])
        _InfoText.hidden = YES;
    else
        _InfoText.hidden = NO;
    NSLog(@"jsonString = %@",person);
//    [boardList addObjectsFromArray:[person valueForKey:@"bandiPosts"]];
    [_tbList reloadData];
//    [self noti];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


#pragma -table Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [boardList count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([[[boardList objectAtIndex:indexPath.row] valueForKey:@"adminYn"] isEqualToString:@"N"])
    {
        NSString *strContent = [NSString stringWithFormat:@"%@",[[boardList objectAtIndex:indexPath.row] valueForKey:@"contents"] ];

        
        if([strContent isKindOfClass:[NSNull class]])
            return 50.f;
        
            
        float fh;
               fh = [self adjustUILabelHeightSize:strContent widthSize:262.f font:[UIFont systemFontOfSize:13]];
        fh = fh + 15.f;
        
        if (fh < 50.f)
            return 50.f;
        else
            return fh;
    }
    
    return 50.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    checkListCell *cell = (checkListCell *)[tableView dequeueReusableCellWithIdentifier:@"checkListCell"];
    if (cell == nil)
    {
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"checkListCell" owner:nil options:nil];
        cell = (checkListCell *)[arr objectAtIndex:0];
    }
    
    NSDictionary *Dic = [boardList objectAtIndex:indexPath.row];
    
    cell.cellDelegate = self;
    cell.listIndex = indexPath.row;
    [cell cellData:Dic];
//    NSLog(@"title = %@",[Dic valueForKey:@"bandiTitle"]);
    
//    float fh;
//    
//    NSString *strContent = [NSString stringWithFormat:@"%@",[[boardList objectAtIndex:indexPath.row] valueForKey:@"contents"] ];
//    
//    
//    if([strContent isKindOfClass:[NSNull class]])
//        strContent = @"aaa";
//    
//    fh = [self adjustUILabelHeightSize:strContent widthSize:262.f font:[UIFont systemFontOfSize:13]];
//    
//    if (fh < 30.f)
//        fh = 30;
//    cell.lbContent.frame = CGRectMake(cell.lbContent.frame.origin.x, cell.lbContent.frame.origin.y,
//                                      cell.lbContent.frame.size.width, fh);
//    
    if(indexPath.row %2== 1)
        [cell setBackgroundColor:RgbColor(252,248,238)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
  

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *Dic = [boardList objectAtIndex:indexPath.row];
    NSLog(@"dic = %@",Dic);
    
    
    if ([Dic valueForKey:@"bandiMobUrl"] == (id)[NSNull null])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"해당 프로그램은 모바일 방송 홈페이지가 없습니다"
                                                       delegate:nil
                                              cancelButtonTitle:@"닫기"
                                              otherButtonTitles:nil ];
        [alert show];
        
        return;
    }

    NSURL *url = [NSURL URLWithString:[Dic valueForKey:@"bandiMobUrl"] ];
//    [[UIApplication sharedApplication] openURL:url];
    WebViewController *vc = [[WebViewController alloc] init];
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
        [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }];


}

-(float)adjustUILabelHeightSize:(NSString*)textlabel widthSize:(float)width font:(UIFont*)ft{
    
    CGSize maximumHeightSize = CGSizeMake(width, 9999);
    
    CGSize extendLabelSize = [textlabel sizeWithFont:ft
                                   constrainedToSize:maximumHeightSize
                                       lineBreakMode:NSLineBreakByCharWrapping];
    
//    NSLog(@"extendLabelSize.height = %f",extendLabelSize.height);
    return extendLabelSize.height;
}
- (IBAction)openNoti:(id)sender
{
    NSLog(@"noti");
}
-(void)selectCheck:(int)indx state:(BOOL)select
{
    NSLog(@"indx = %d / %d",indx,select);
    
    NSDictionary *Dic = [boardList objectAtIndex:indx];
    if(select)
    {
        NSError *error;
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppUserFbmkAdd?userId=%@&programId=%@&appDsCd=03&fileType=json", SharedAppDelegate.sessionUserId,[Dic valueForKey:@"courseId"]];


        NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                              encoding:NSUTF8StringEncoding
                                                                 error:&error];
        if(!jsonString || jsonString == nil)
            return;
      
        NSDictionary* person = [Util convertJsonToDic:jsonString];
        
        if([[[person valueForKey:@"resultXml"] valueForKey:@"resultCd"] isEqualToString:@"00"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"즐겨찾기가 설정되었습니다."
                                                           delegate:self
                                                  cancelButtonTitle:@"닫기"
                                                  otherButtonTitles:nil ];
            [alert show];
            alert.tag = 114;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:[[person valueForKey:@"resultXml"] valueForKey:@"resultMsg"]
                                                           delegate:self
                                                  cancelButtonTitle:@"닫기"
                                                  otherButtonTitles:nil ];
            [alert show];
            alert.tag = 113;

        }
//        [_tbList reloadData];


        
    }
    else
    {//삭제
//
        NSError *error;
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppUserFbmkModify?userId=%@&programId=%@&fbmkSno=%@&&appDsCd=03&fileType=json", SharedAppDelegate.sessionUserId,[Dic valueForKey:@"courseId"],[Dic valueForKey:@"fbmkSno"]];

        NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                              encoding:NSUTF8StringEncoding
                                                                 error:&error];
        NSLog(@"end");
        if(!jsonString || jsonString == nil)
            return;
        
        NSDictionary* person = [Util convertJsonToDic:jsonString];
        
        if([[[person valueForKey:@"resultXml"] valueForKey:@"resultCd"] isEqualToString:@"00"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"즐겨찾기가 해지되었습니다"
                                                           delegate:self
                                                  cancelButtonTitle:@"닫기"
                                                  otherButtonTitles:nil ];
            [alert show];

            alert.tag = 112;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:[[person valueForKey:@"resultXml"] valueForKey:@"resultMsg"]
                                                           delegate:self
                                                  cancelButtonTitle:@"닫기"
                                                  otherButtonTitles:nil ];
            [alert show];
            alert.tag = 111;
            
            
        }
  

        

    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
//    if(buttonIndex)
    {
        [self loadCheckList];
    }
}
@end
