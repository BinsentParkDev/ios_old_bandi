//
//  SongsTableView.m
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "SongsTableView.h"
#import "TimeTable.h"
#import "TBXML+HTTP.h"
#import "ServerSide.h"
#import "Util.h"

#define TAG @"SongsTable"

@implementation SongsTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        
        [[NSBundle mainBundle] loadNibNamed:@"SongsTableView" owner:self options:nil];
        
        [_wrapperView addSubview:_programLabel];
        [_programLabel setFrame:CGRectMake(0, 0, 320.0f, 36.0f)];
        
        [_wrapperView addSubview:_songsWebView];
        [_songsWebView setFrame:CGRectMake(0, 36.0f, 320.0f, bodyHeight-36.0)];
        
        // 프로그램명 설정
        TimeTable *tt = [[TimeTable alloc] init];
        NSDictionary *ttDic = [tt get];
        _programLabel.text = ttDic[@"progname"];
        NSString *progcd = ttDic[@"progcd"];
        
        // 선곡표 내용 백그라운드 이미지 설정
        UIGraphicsBeginImageContext(_songsWebView.frame.size);
        [[UIImage imageNamed:@"bg_base.png"] drawInRect:_songsWebView.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        _songsWebView.backgroundColor = [UIColor colorWithPatternImage:image];
        
        
        [self addSubview:_wrapperView];

        //[self songsList:progcd];
        [NSThread detachNewThreadSelector:@selector(songsList:) toTarget:self withObject:progcd];
    }
    
//    [ServerSide saveEventLog:TAG];
    
    return self;
}

- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}


- (void)songsList:(NSString *)progcd
{
    // 선곡내용 설정
    NSError *error; //for test - progcd=@"BP0PHPI0000000058";
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiSongList?progcd=%@", progcd];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSString *xmlStr = [[NSString alloc] initWithContentsOfURL:url
                                                      encoding:NSUTF8StringEncoding
                                                         error:&error];
    
    TBXMLElement *elemRoot = nil, *elemRescd = nil, *elemData = nil, *elemBandiContents = nil;
    NSString *rescd = nil, *songsListHtml = nil;
    
    
    TBXML *tbxml = [TBXML newTBXMLWithXMLString:xmlStr error:&error];
    
    elemRoot = tbxml.rootXMLElement;
    if (elemRoot) {
        // xml status
        elemRescd = [TBXML childElementNamed:@"rescd" parentElement:elemRoot];
        if (elemRescd) {
            rescd = [TBXML textForElement:elemRescd]; // 0000
        }
        
        if ([rescd isEqualToString:@"0000"]) {
            // verson info
            elemData = [TBXML childElementNamed:@"data" parentElement:elemRoot];
            elemBandiContents = [TBXML childElementNamed:@"bandi-contents" parentElement:elemData];
            
            songsListHtml = [TBXML textForElement:elemBandiContents];
            //NSLog(@"songsList => %@", songsListHtml);
        } else {
            //NSLog(@"rescd element contains %@", rescd);
            songsListHtml = @"<b> 선곡 내용이 없습니다. </b>";
        }
        
        [_songsWebView loadHTMLString:songsListHtml baseURL:nil];
    }
    
    

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
