//
//  ProgramListCell.m
//  EBSTVApp
//
//  Created by Hyeoung seok Yoon on 2013. 11. 10..
//  Copyright (c) 2013년 Hyeoung seok Yoon. All rights reserved.
//


#import "boardCell.h"
#import "Util.h"
@interface boardCell ()

@end
 
@implementation boardCell

@synthesize lbTitle;
@synthesize lbContent;
@synthesize ivAdmin;
@synthesize lbAdmin;

#define RgbColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated]; 
}

-(void)cellData:(NSDictionary *)dic
{
    if([[dic valueForKey:@"adminYn"] isEqualToString:@"N"])
    {
        ivAdmin.hidden = YES;
        lbAdmin.hidden = YES;
        
        NSString* writer = [dic valueForKey:@"writer"];
        if ([Util containsHangul:writer])
        {
            NSMutableString * mstr = [NSMutableString string];
            for (int i=0; i<[writer length]; i++)
            {
                if (i==0 || i==[writer length]-1 || [@" " isEqualToString:[writer substringWithRange:NSMakeRange(i, 1)]])
                {
                    [mstr appendString:[writer substringWithRange:NSMakeRange(i, 1)]];
                }
                else
                {
                    [mstr appendString:@"*"];
                }
            }
            writer = [NSString stringWithString:mstr];
        }
        else
        {
            NSMutableString * mstr = [NSMutableString string];
            for (int i=0; i<[writer length]; i++)
            {
                if (i<3 ||  [@" " isEqualToString:[writer substringWithRange:NSMakeRange(i, 1)]] )
                {
                    [mstr appendString:[writer substringWithRange:NSMakeRange(i, 1)]];
                }
                else
                {
                    [mstr appendString:@"*"];
                }
            }
            writer = [NSString stringWithString:mstr];
        }

//        NSString * strWriter= [NSString stringWithFormat:@"%@",[dic valueForKey:@"writer"]];
        NSString * strWriter= [NSString stringWithFormat:@"%@", writer];

        if([strWriter isEqualToString:@"<null>"])
        {
            strWriter = @"";
            NSString * strWriterSns= [NSString stringWithFormat:@"%@",[dic valueForKey:@"snsUserNm"]];
            if([strWriterSns isEqualToString:@"<null>"])
                strWriter = @"";
            else
                strWriter = [NSString stringWithFormat:@"%@",[dic valueForKey:@"snsUserNm"]];
        }
        
        

        NSString * strTitle =  [NSString stringWithFormat:@"%@   %@",strWriter,[dic valueForKey:@"regdate"]];
               
        lbTitle.text = [Util xmlEntitiesDecode:strTitle];
        
        NSString *strContent = [NSString stringWithFormat:@"%@",[dic valueForKey:@"contents"] ];
        if([strContent isEqualToString:@"<null>"])
            strContent = @"";
        
        lbContent.text = [Util xmlEntitiesDecode:strContent];
        [self setBackgroundColor:RgbColor(255, 255, 255)];
    }
    else
    {
        lbTitle.hidden = YES;
        lbContent.hidden = YES;

        NSString *strContent = [NSString stringWithFormat:@"%@",[dic valueForKey:@"contents"] ];
        if([strContent isEqualToString:@"<null>"])
            strContent = @"";
        lbAdmin.text = [Util xmlEntitiesDecode:strContent];
        [self setBackgroundColor:RgbColor(242, 211, 76)];

    }
}


@end
