//
//  LoginLandscapeView.m
//  Bandi2 - 사용되는지 확실하지 않음.
//
//  Created by admin on 2014. 2. 4..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "LoginLandscapeView.h"
#import "Dialogs.h"
#import "BandiBoardWriteLandscapeView.h"
#import "ServerSide.h"
#import "NSUserDefaults+UserData.h"

#define TAG @"LoginFormLandscape"


@implementation LoginLandscapeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        // Initialization code
        int screenHeight = [SharedAppDelegate.screenHeight intValue];
        
        [[NSBundle mainBundle] loadNibNamed:@"LoginLandscapeView" owner:self options:nil];
        [_wrapperView setFrame:CGRectMake(0, 0, screenHeight, 320)];
        // 백그라운드 이미지 설정
        UIGraphicsBeginImageContext(_wrapperView.frame.size);
        [[UIImage imageNamed:@"bg_base.png"] drawInRect:_wrapperView.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        _wrapperView.backgroundColor = [UIColor colorWithPatternImage:image];
        
        [_wrapperView addSubview:_loginFormView];
        [_loginFormView setFrame:CGRectMake(0, 0, _loginFormView.frame.size.width, _loginFormView.frame.size.height)];
        
        [_wrapperView addSubview:_loginOptionView];
        [_loginOptionView setFrame:CGRectMake(_loginFormView.frame.size.width, 0, _loginOptionView.frame.size.width, _loginOptionView.frame.size.height)];
        
        [self addSubview:_wrapperView];
        
        // set saved value
        userDefaults = [NSUserDefaults standardUserDefaults];
        if ([userDefaults boolForKey:@"SAVE_ID"]) {
            _saveIdBtn.selected = YES;
            _userId.text = [userDefaults getUserId];
        }
        if ([userDefaults boolForKey:@"AUTO_LOGIN"]) {
            _autoLoginBtn.selected = YES;
            _userId.text = (NSString *)[userDefaults getUserId];
            _passwd.text = (NSString *)[userDefaults getPassword];
        }
    }
    
    [ServerSide saveEventLog:TAG];
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)doLogin:(id)sender {
    NSString *str = [NSString stringWithFormat:@"https://sso.ebs.co.kr/idp/bandiLogin?userid=%@&passwd=%@", _userId.text, _passwd.text];
    NSURL *url = [NSURL URLWithString:str];
    NSError *error;
    NSString *dataStr = [[NSString alloc] initWithContentsOfURL:url
                                                       encoding:NSUTF8StringEncoding
                                                          error:&error];
    if (error) {
        NSLog(@"error -> %@", error.localizedDescription);
    } else {
        dataStr = [dataStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:@"([0-9]{3})\\s(.*)"
                                                                          options:NSRegularExpressionCaseInsensitive
                                                                            error:&error];
        
        NSArray *matches = [regEx matchesInString:dataStr options:0 range:NSMakeRange(0, [dataStr length])];
        for (NSTextCheckingResult* match in matches) {
            NSString *resultCode = [dataStr substringWithRange:[match rangeAtIndex:1]];
            NSString *resultText;
            if ([resultCode isEqualToString:@"100"]) {
                resultText = [dataStr substringWithRange:[match rangeAtIndex:2]];
                NSData *dat = [[NSData alloc] initWithBase64EncodedString:resultText options:0];
                NSString *plainText = [[NSString alloc] initWithData:dat encoding:NSUTF8StringEncoding];
                NSArray *resultArr = [plainText componentsSeparatedByString:@"/"];
                
                NSLog(@"result -> %@ , %@, %@", resultArr[0], resultArr[1], resultArr[2]);
                SharedAppDelegate.sessionUserId = resultArr[0];
                SharedAppDelegate.sessionUserName = resultArr[1];
                SharedAppDelegate.sessionUserEmail = resultArr[2];
                if (_saveIdBtn.selected) {
                    [userDefaults setUserId:_userId.text];
                }
                if (_autoLoginBtn.selected) {
                    [userDefaults setUserId:_userId.text];
                    [userDefaults setPassword:_passwd.text];
                }
                
                
                CGRect frameRect = CGRectMake(0, 0, 480, 320);
                BandiBoardWriteLandscapeView *myView = [[BandiBoardWriteLandscapeView alloc] initWithFrame:frameRect];
                [self.superview addSubview:myView];
                
                [self removeFromSuperview];
                
                
            } else if ([resultCode isEqualToString:@"102"]) {
                [Dialogs alertView:@"로그인 실패 - 등록되지 않은 ID입니다."];
                [userDefaults setBool:NO forKey:@"SAVE_ID"];
            } else if ([resultCode isEqualToString:@"103"]) {
                [Dialogs alertView:@"로그인 실패 - 잘못된 비밀번호입니다."];
                [userDefaults setBool:NO forKey:@"AUTO_LOGIN"];
            } else if ([resultCode isEqualToString:@"104"]) {
                [Dialogs alertView:@"로그인 실패 - 사용되지 않는 ID입니다."];
                [userDefaults setBool:NO forKey:@"AUTO_LOGIN"];
                [userDefaults setBool:NO forKey:@"SAVE_ID"];
            } else {
                [Dialogs alertView:@"로그인 실패했습니다."];
            }
            
        }
    }

}
- (IBAction)saveId:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (btn.selected) {
        btn.selected = NO;
    } else {
        btn.selected = YES;
    }
    
    [userDefaults setBool:btn.selected forKey:@"SAVE_ID"];

}
- (IBAction)autoLogin:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (btn.selected) {
        btn.selected = NO;
    } else {
        btn.selected = YES;
    }
    
    [userDefaults setBool:btn.selected forKey:@"AUTO_LOGIN"];

}

- (IBAction)closeLoginView:(id)sender {
    [self removeFromSuperview];
}

// keyboard hide when touching background view
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //터치한 객체를 가져온다.
	UITouch *touch = [[event allTouches] anyObject];
    NSLog(@"touch -> %@", [touch view]);
    
	[self endEditing:YES];
}
@end
