//
//  GA.m
//  Bandi2
//
//  Created by admin on 2017. 4. 5..
//  Copyright © 2017년 EBS. All rights reserved.
//

#import "GA.h"

@implementation GA
{
    
}

+ (void)sendScreenView:(NSString *)screen
{
    //** Google Analytics **
    NSString * screenName = [NSString stringWithFormat:@"Screen-%@", screen];
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

+ (void)sendScreenView:(NSString *)screen withChannel:(GAChannel)channel
{
    //** Google Analytics **
    NSString *channelName = @"UNKNOWN";
    if (channel == GA_FM)
    {
        channelName = @"FM";
    }
    else if (channel == GA_IRADIO)
    {
        channelName = @"IRADIO";
    }
    NSString * screenName = [NSString stringWithFormat:@"Screen-%@|%@", screen, channelName ];
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

@end
