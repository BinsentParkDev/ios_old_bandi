//
//  BandiBoardView.m
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "BandiBoardView.h"
#import "TimeTable.h"
#import "IIViewDeckController.h"
#import "LoginView.h"
#import "BandiBoardWriteView.h"
#import "ServerSide.h"
#import "Util.h"
#import "boardCell.h"
#import <KakaoOpenSDK/KakaoOpenSDK.h>
#import "LogContentController.h"
#import "WebViewController.h"
#import "Dialogs.h"
#import "GA.h"

#define TAG @"BandiBoard"

#define RgbColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@implementation BandiBoardView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        
        bLoginShow = NO;
        boardList = [[NSMutableArray alloc]init];
//        _boardText = [[UITextField alloc]init];
        if(IS_IPHONE5)
            [[[NSBundle mainBundle] loadNibNamed:@"BandiBoardView" owner:self options:nil] lastObject];
        else
            [[[NSBundle mainBundle] loadNibNamed:@"BandiBoardView_480" owner:self options:nil] lastObject];
        
        _btKeyHide.hidden = YES;
        
        lbTitleText = [[UILabel alloc]initWithFrame:CGRectMake(5, 3, 310, 28)];
        [_notiView addSubview:lbTitleText];
        lbTitleText.font = [UIFont systemFontOfSize:17];
        lbTitleText.textColor = [UIColor whiteColor];
        lbTitleText.textAlignment = NSTextAlignmentCenter;
        
        strEventDetailUrl = [[NSString alloc]init];
        
        /** loadBandiBoardWeb 으로 이동.
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppList?progcd=%@", ttDic[@"progcd"]];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [_webView loadRequest:request];
        **/
        
        // adjust _webView size
//        [_webView setFrame:CGRectMake(0, 36.0, 320, bodyHeight-36)];
        
//        [NSThread detachNewThreadSelector:@selector(loadBandiBoardWeb) toTarget:self withObject:nil];

        [_tabView setFrame:CGRectMake(0, 0, 320, _tabView.frame.size.height)];
        [_wrapperView addSubview:_tabView];
        
        [_inputView setFrame:CGRectMake(0, _wrapperView.frame.size.height - _inputView.frame.size.height,
                                       320, _inputView.frame.size.height)];
        [_wrapperView addSubview:_inputView];
        
        [_notiView setFrame:CGRectMake(0, _tabView.frame.size.height, 320, _notiView.frame.size.height)];
        [_wrapperView addSubview:_notiView];
        
        float fh;
        fh = _wrapperView.frame.size.height - (_notiView.frame.origin.y + _notiView.frame.size.height +_inputView.frame.size.height);
              
        [_tbList setFrame:CGRectMake(0, _notiView.frame.origin.y + _notiView.frame.size.height,
                                    320, fh)];
        
        [_wrapperView setFrame:CGRectMake(0, 0, 320, frame.size.height)];
        [self addSubview:_wrapperView];
        
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
        [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
        [_tbList addSubview:refreshControl];

        
        pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:20.0f tableView:_tbList withClient:self];
        
        if(IS_IPHONE5)
            [_detailView setFrame:CGRectMake(0, 0, _detailView.frame.size.width, _detailView.frame.size.height)];
        else
            [_detailView setFrame:CGRectMake(0, 0, _detailView.frame.size.width, _detailView.frame.size.height)];
//        [_wrapperView addSubview:_detailView];
        [[UIApplication sharedApplication].keyWindow addSubview:_detailView];
        [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_detailView];
        _detailView.hidden = YES;
        
        

//        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(playIPradio:)
//                                                     name:@"selectIradioFromOnAir"
//                                                   object:nil];
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(playIPradio:)
//                                                     name:@"selectIradioFromSch"
//                                                   object:nil];
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(playFmPradio:)
//                                                     name:@"selectFmradioFromOnAir"
//                                                   object:nil];
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(playFmPradio:)
//                                                     name:@"selectFmradioFromSch"
//                                                   object:nil];
        
//        _boardText.text = @"123123";
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillAnimate:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillAnimate:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        
        if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"UseBoard"] isEqualToString:@"Y"])
        {
            _tbList.hidden = YES;
            _programLabel.hidden = NO;
        }
        else
        {
            _tbList.hidden = NO;
            _programLabel.hidden = YES;
        }
    }
    
    [self loadBandiBoardWeb];
//    [ServerSide saveEventLog:TAG];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadBoarding:)
                                                 name:@"reloadBoardingData"
                                               object:nil];
    
    [self SNSIcon];
    
    
    //** Google Analytics **
    if (nSelectedMenu == 1) {
        [GA sendScreenView:@"BandiBoard" withChannel:GA_FM];
    } else if (nSelectedMenu == 2) {
        [GA sendScreenView:@"BandiBoard" withChannel:GA_IRADIO];
    } else {
        [GA sendScreenView:@"BandiBoard" withChannel:GA_UNKNOWN];
    }
    
    return self;
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void) handleRefresh:(UIRefreshControl *)controller
{
    NSError *error;

    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=%@&listType=L&pageSize=10", [[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"]];

    NSLog(@"bandi bnotioard handleRefresh call url = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    NSLog(@"load handleRefresh end");
    if(!jsonString || jsonString == nil)
        return;

    
    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
    NSLog(@"jsonString = %@",person);
    [boardList removeAllObjects];
    [boardList addObjectsFromArray:[person valueForKey:@"bandiPosts"]];
    [_tbList reloadData];

    [controller endRefreshing];
}

-(void)reloadRefreshList
{
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=%@&listType=L&pageSize=10", [[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"]];

    NSLog(@"bandi bnotioard reloadRefreshList call url = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    NSLog(@"load reloadRefreshList end");
    if(!jsonString || jsonString == nil)
        return;

//    SBJsonParser* parser = [ [ SBJsonParser alloc ] init ];
//    NSMutableDictionary* person = [ parser objectWithString: jsonString ];
    
    NSDictionary* person = [Util convertJsonToDic:jsonString ];
    
    NSLog(@"jsonString = %@",person);
    [boardList removeAllObjects];
    [boardList addObjectsFromArray:[person valueForKey:@"bandiPosts"]];
    [_tbList reloadData];
    

}



- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}

- (void)reloadBoarding:(NSNotification *)notification
{

    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"UseBoard"] isEqualToString:@"Y"])
    {
        _tbList.hidden = YES;
        _programLabel.hidden = NO;
    }
    else
    {

        _tbList.hidden = NO;
        _programLabel.hidden = YES;
    }
    [NSThread detachNewThreadSelector:@selector(loadBandiBoardWeb) toTarget:self withObject:nil];    

//    [self reloadRefreshList];
//    [self loadBandiBoardWeb];
//         [[NSNotificationCenter defaultCenter] postNotificationName:@"MoveTab3" object:nil];
    
}

-(void)selectMenuIndex:(int)nIndex //1 책읽어주는 라디오 // 2외국어 라디오
{
    
    if(nIndex == 1)
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(63,56,51)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(158,145,136)];
        self.btMenuReadRadio.selected = YES;
        self.btMenuEngRadio.selected = NO;
        nSelectedMenu = 1;
//        [player stopAudio];
//        [player loadSound:[streamingData valueForKey:@"streamUrlIos"]];
//        selectFmradio
        [[NSNotificationCenter defaultCenter] postNotificationName:@"selectFmradioFromBoard" object:nil];
        
    }
    else if(nIndex == 2)
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(158,145,136)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(63,56,51)];
        self.btMenuReadRadio.selected = NO;
        self.btMenuEngRadio.selected = YES;
        nSelectedMenu = 2;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"selectIradioFromBoard" object:nil];
    }
}

- (void)loadBandiBoardWeb
{
    
    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
        nSelectedMenu = 1;
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
        nSelectedMenu = 2;
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
            nSelectedMenu = 2;
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
            nSelectedMenu = 1;
        }
    }
    
    if([channelString isEqualToString:@"RADIO"])
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(63,56,51)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(158,145,136)];
        self.btMenuReadRadio.selected = YES;
        self.btMenuEngRadio.selected = NO;
//        if([boardList count] < 1)
//            self.titleText2.text = @"";
//        else
      
        lbTitleText.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramTitle"];
        nSelectedMenu = 1;
     
    }
    else
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(158,145,136)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(63,56,51)];
        self.btMenuReadRadio.selected = NO;
        self.btMenuEngRadio.selected = YES;
//        if([boardList count] < 1)
//            self.titleText1.text = @"";
//        else
  
        lbTitleText.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramTitle"];
        nSelectedMenu = 2;

    }
    

    NSLog(@"board title = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramTitle"]);
    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"UseBoard"] isEqualToString:@"Y"])
    {
        [self noti];
        return;
    }

    if([boardList count])
       [boardList removeAllObjects];

    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=%@&listType=L&pageSize=10", [[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"]];
    NSLog(@"bandi bnotioard loadBandiBoardWeb call url = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    NSLog(@"load loadBandiBoardWeb end");
    //
    if(!jsonString || jsonString == nil)
        return;

    
    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
    
    [boardList addObjectsFromArray:[person valueForKey:@"bandiPosts"]];
    if([boardList count] < 1)
    {
        _tbList.hidden = YES;
        _programLabel.hidden = NO;
    }
    else
    {
        _tbList.hidden = NO;
        _programLabel.hidden = YES;

    }

    [_tbList reloadData];
     [pullToRefreshManager_ tableViewReloadFinished];
    
    
    [self noti];
}

-(void)noti
{

    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiEventNoticeList?appDsCd=03&fileType=json"];
//       NSLog(@"bandi bnotioard noti call url = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    if(!jsonString || jsonString == nil)
        return;

//    NSLog(@"load noti end");
    
    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
    notiCode = [NSString stringWithFormat:@"%@",[[[person valueForKey:@"bandiEventLists"] objectAtIndex:0] valueForKey:@"evtClsCd"]];
    
    if([notiCode isEqualToString:@"001"])
        _notiTitle.text =[[[person valueForKey:@"bandiEventLists"] objectAtIndex:0] valueForKey:@"evtTitle"];
    
    _detailTitle.text = [[[person valueForKey:@"bandiEventLists"] objectAtIndex:0] valueForKey:@"evtTitle"];
//    NSLog(@"cd == %@",[[[person valueForKey:@"bandiEventLists"] objectAtIndex:0] valueForKey:@"evtClsCd"]);
    
    
    if([notiCode isEqualToString:@"001"])
    {
        strEventDetailUrl =[NSString stringWithFormat:@"%@",[[[person valueForKey:@"bandiEventLists"] objectAtIndex:0] valueForKey:@"linkUrl"]];
        if([strEventDetailUrl length] > 2)
        {
            _eventDetailBt.hidden = NO;
            if (IS_IPHONE5)
                _detailContentScroll.frame = CGRectMake(5, 66, 306, 400);
            else
                _detailContentScroll.frame = CGRectMake(5, 73, 306, 317);
        }
        else
        {
            _eventDetailBt.hidden = YES;
            if (IS_IPHONE5)
                _detailContentScroll.frame = CGRectMake(5, 66, 306, 450);
            else
                _detailContentScroll.frame = CGRectMake(5, 73, 306, 350);
        }
    }
    else
    {
        _eventDetailBt.hidden = YES;
        if (IS_IPHONE5)
            _detailContentScroll.frame = CGRectMake(5, 66, 306, 450);
        else
            _detailContentScroll.frame = CGRectMake(5, 73, 306, 350);
    }
    
    
   

    
    float fh;
    
    NSString *strContent = [NSString stringWithFormat:@"%@",[[[person valueForKey:@"bandiEventLists"] objectAtIndex:0] valueForKey:@"evtCntn"]];
    
    fh =[self adjustUILabelHeightSize:strContent widthSize:_detailNotiContent.frame.size.width font:[UIFont systemFontOfSize:14]];
    
    [_detailContentScroll setContentSize:CGSizeMake(306, fh+10)];
    _detailNotiContent.text = strContent;
    _detailNotiContent.frame = CGRectMake(_detailNotiContent.frame.origin.x,
                                          _detailNotiContent.frame.origin.y,
                                          _detailNotiContent.frame.size.width,
                                          fh+10);
    _detailNotiContent.hidden = NO;

}

- (IBAction)closeNotiDetail:(id)sender
{
    _detailView.hidden = YES;
}

-(IBAction)eventDetailUrlOpen:(id)sender
{
    if([strEventDetailUrl length] > 2)
    {
        NSRange strRange;
        strRange = [strEventDetailUrl rangeOfString:@"http://"];
        
        if (strRange.location == NSNotFound) {
            strEventDetailUrl = [NSString stringWithFormat:@"http://%@",strEventDetailUrl];
        }

        
        NSURL *url = [NSURL URLWithString:strEventDetailUrl];
        [[UIApplication sharedApplication] openURL:url];        
//        WebViewController *vc = [[WebViewController alloc] init];
//        [self.window.rootViewController presentViewController:vc animated:YES completion:^{
//            [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
//        }];
        _detailView.hidden = YES;
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (IBAction)writeAction:(id)sender
{
//    _boardText.text = @"wwww";
    NSLog(@"sy>> writeAction called");

    [_boardText resignFirstResponder];
    
    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"UseBoard"] isEqualToString:@"Y"])
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"해당 프로그램은 게시판이 운영되지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        return;
    }
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"] length] < 1)
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"해당 프로그램은 게시판이 운영되지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        return;
    }
//
    
//    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
//    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
    
    //[[[cc.view subviews] lastObject] removeFromSuperview];
    
//    CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);
    
    SharedAppDelegate.latestMenu = @"반디게시판";
    
    NSLog(@"_boardText.text = %@",_boardText.text);
    if ([ServerSide autoLogin]) {

        [self postBoard];
        
        
    } else {
        NSString * strLoginStateCode =[[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"];
        
        if([strLoginStateCode isEqualToString:@"001"] || [strLoginStateCode isEqualToString:@"002"])
        {
            [self postBoard];
        }
        else
        {
            
            if(!bLoginShow)
            {
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(loginStateIcon:)
                                                             name:@"LoginStateIcon"
                                                           object:nil];
                
//                LoginView *myView = [[LoginView alloc] initWithFrame:frameRect];
//                [cc.view addSubview:myView];
//                [cc.view bringSubviewToFront:myView];
//                cc.navigationItem.title = @"로그인";
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginN" object:nil];

//                NSString * strStrView;
//                if (IS_IPHONE5)
//                    strStrView = @"LogContentController";
//                else
//                    strStrView = @"LogContentController_480";
//                
//                LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
//                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];

                bLoginShow = YES;
            }
            
        }

    }

}

- (void)loginStateIcon:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LoginStateIcon" object:nil];
    [self SNSIcon];
    bLoginShow = NO;
}


/**
 * Do nothing.
 */
- (IBAction)writeBulletin:(id)sender {
    
    
//    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
//    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
//    
//    //[[[cc.view subviews] lastObject] removeFromSuperview];
//    
//    CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);
//    
//    SharedAppDelegate.latestMenu = @"반디게시판";
//    
//    NSLog(@"_boardText.text = %@",_boardText.text);
//    if ([ServerSide autoLogin]) {
////        BandiBoardWriteView *myView = [[BandiBoardWriteView alloc] initWithFrame:frameRect];
////        [cc.view addSubview:myView];
////        [cc.view addSubview:myView];
////        [cc.view bringSubviewToFront:myView];
////        cc.navigationItem.title = @"반디게시판";
////        [self test];
//        
//        
//    } else {
//        LoginView *myView = [[LoginView alloc] initWithFrame:frameRect];
//        [cc.view addSubview:myView];
//        [cc.view bringSubviewToFront:myView];
//        cc.navigationItem.title = @"로그인";
//    }
    
}


-(void)postBoard
{
    
    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"UseBoard"] isEqualToString:@"Y"])
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"해당 프로그램은 게시판이 운영되지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        return;
    }
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"] length] < 1)
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"해당 프로그램은 게시판이 운영되지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        return;
    }

    // show alert and return if member is a kind of bad member.
    if ([ServerSide isBadMember])
    {
        NSString *alertMsg = [NSString stringWithFormat:@"%@", SharedAppDelegate.badMember[@"badMmbExp"]];
        [Dialogs alertView:alertMsg];
        return;
    }
    
    
    NSString *userId = SharedAppDelegate.sessionUserId;
    NSString *userName = [SharedAppDelegate.sessionUserName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSString *userText = [_boardText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *userText = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                          NULL,
                                                                          (__bridge CFStringRef) _boardText.text,
                                                                          NULL,
                                                                          CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                          kCFStringEncodingUTF8));
    //    if([_txtField.text length] < 1)
    //    if ([_txtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
    
    NSLog(@"_txtField.text = %@",_boardText.text);
    if([_boardText.text length] < 1){
        //
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"내용을 작성해주세요." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        
        //
        return;
    }
    NSString *userSnsName = [[[NSUserDefaults standardUserDefaults] valueForKey:@"SNSName"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    if(!userId)
        userId = [NSString stringWithFormat:@""];
    if(!userName)
        userName = [NSString stringWithFormat:@""];
    
    //
    
    NSString *strCh;
    if (self.btMenuReadRadio.selected ==1 )
        strCh = [NSString stringWithFormat:@"radio"];
    else
        strCh = [NSString stringWithFormat:@"iradio"];
    
    NSString *strSnsUse;
    NSString *strSnsCode;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"] isEqualToString:@"000"])
    {
        strSnsUse = [NSString stringWithFormat:@"N"];
        strSnsCode = @"";
    }
    else
    {
        strSnsUse = [NSString stringWithFormat:@"Y"];
        strSnsCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"];
    }
    
    NSString *urlStr =  [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppPost3?progcd=%@&userid=%@&writer=%@&contents=%@&appDsCd=03&broadType=%@&snsDsCd=%@&snsUserId=%@&snsUserNm=%@&snsUseYn=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"],userId,userName,userText,strCh,strSnsCode,[[NSUserDefaults standardUserDefaults] valueForKey:@"SNSId"],userSnsName,strSnsUse];
    
    NSLog(@"urlStr = %@", urlStr);
    NSError *error;
    NSString *xmlString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    NSLog(@"response xml = %@", xmlString);
    if(!xmlString || xmlString == nil)
    {
        [Dialogs alertView:@"글을 등록하던 중 오류가 발생하였습니다."];
        NSLog(@"board write response error - xml nil or empty");
        return;
    }
    
    @try
    {
        TBXML *tbxml = [TBXML newTBXMLWithXMLString:xmlString error:nil];
        TBXMLElement *elemRoot=nil, *elemCode=nil, *elemMessage=nil;
        NSString *code=nil, *message=nil;
        elemRoot = tbxml.rootXMLElement;
        if (elemRoot)
        {
            elemCode = [TBXML childElementNamed:@"code" parentElement:elemRoot];
            if (elemCode)
            {
                code = [TBXML textForElement:elemCode];
            }
            
            if (code != nil )
            {
                if ([@"0" isEqualToString:code])
                {
                    [self loadBandiBoardWeb];
                    _boardText.text = @"";
                }
                else if ([@"9" isEqualToString:code])
                {
                    elemMessage = [TBXML childElementNamed:@"message" parentElement:elemRoot];
                    if (elemMessage)
                    {
                        message = [TBXML textForElement:elemMessage];
                        [Dialogs alertView:message];
                    }
                }
                else
                {
                    [Dialogs alertView:@"글을 등록하던 중 오류가 발생하였습니다. - invalid code"];
                    NSLog(@"board write response error - invalid code %@", code);
                }
            }
            else
            {
                [Dialogs alertView:@"글을 등록하던 중 오류가 발생하였습니다. - code nil"];
                NSLog(@"board write response error - code nil");
            }
        }
    }
    @catch (NSException *e)
    {
        [Dialogs alertView:@"글을 등록하던 중 오류가 발생하였습니다."];
        NSLog(@"board write response error - %@", [e description]);
    }

    
    
//    if([xmlString isEqualToString:@"0"])
//    {
////        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
////                                                        message:@"등록되었습니다."
////                                                       delegate:nil
////                                              cancelButtonTitle:@"닫기"
////                                              otherButtonTitles:nil ];
////        [alert show];
//        [self loadBandiBoardWeb];
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                        message:@"등록에 실패하였습니다."
//                                                       delegate:nil
//                                              cancelButtonTitle:@"닫기"
//                                              otherButtonTitles:nil ];
//        [alert show];
//    }
//    
//    _boardText.text = @"";
}

#pragma -table Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [boardList count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"BandiBoardView.m > tableView heightForRowAtIndexPath");
//    NSLog(@"%lu, %lu", (unsigned long)[boardList count] , (unsigned long)indexPath.row);
    
    if (indexPath.row >= [boardList count]) return 50.f;
    
    if([[[boardList objectAtIndex:indexPath.row] valueForKey:@"adminYn"] isEqualToString:@"N"])
    {
        NSString *strContent = [NSString stringWithFormat:@"%@",[[boardList objectAtIndex:indexPath.row] valueForKey:@"contents"] ];

        
        if([strContent isKindOfClass:[NSNull class]])
            return 50.f;
        
            
        float fh;
               fh = [self adjustUILabelHeightSize:strContent widthSize:262.f font:[UIFont systemFontOfSize:13]];
        fh = fh + 20.f;
        
        if (fh < 50.f)
            return 50.f;
        else
            return fh;
    }
    else
    {

        NSString *strContent = [NSString stringWithFormat:@"%@",[[boardList objectAtIndex:indexPath.row] valueForKey:@"contents"] ];
        
        
        if([strContent isKindOfClass:[NSNull class]])
            return 50.f;
        
        NSLog(@"strContent = %@",strContent);
        float fh;
        fh = [self adjustUILabelHeightSize:strContent widthSize:210.f font:[UIFont systemFontOfSize:14]];
        fh = fh + 10.f;
        
        if (fh < 50.f)
            return 50.f;
        else
            return fh;

    }
    
    return 50.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cc");
    boardCell *cell = (boardCell *)[tableView dequeueReusableCellWithIdentifier:@"boardCell"];
    if (cell == nil)
    {
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"boardCell" owner:nil options:nil];
        cell = (boardCell *)[arr objectAtIndex:0];
    }
    
    if (![boardList count]) {
        return cell;
    }
    NSDictionary *Dic = [boardList objectAtIndex:indexPath.row];
    
    [cell cellData:Dic];
    float fh;
    
    NSString *strContent = [NSString stringWithFormat:@"%@",[[boardList objectAtIndex:indexPath.row] valueForKey:@"contents"] ];
    strContent = [Util xmlEntitiesDecode:strContent];
    NSLog(@"strcon = %@",strContent);
    if([strContent isEqualToString:@"<null>"])
        strContent = @"";
//    if([strContent isKindOfClass:[NSNull class]])
//        strContent = @" ";
//
    
     if([[[boardList objectAtIndex:indexPath.row] valueForKey:@"adminYn"] isEqualToString:@"N"])
     {
         fh = [self adjustUILabelHeightSize:strContent widthSize:262.f font:[UIFont systemFontOfSize:13]];
         
         if (fh < 30.f)
             fh = 30;
         cell.lbContent.frame = CGRectMake(cell.lbContent.frame.origin.x, cell.lbContent.frame.origin.y,
                                           cell.lbContent.frame.size.width, fh);

         cell.lbContent.hidden = NO;
         cell.lbAdmin.hidden = YES;
     }
    else
    {
        fh = [self adjustUILabelHeightSize:strContent widthSize:210.f font:[UIFont systemFontOfSize:14]];
        
        fh = fh + 10;
        if (fh < 30.f)
            fh = 30;
        cell.lbAdmin.frame = CGRectMake(cell.lbAdmin.frame.origin.x, cell.lbAdmin.frame.origin.y,
                                          cell.lbAdmin.frame.size.width, fh);
        cell.lbContent.hidden = YES;
        cell.lbAdmin.hidden = NO;

    }
   
    
    if(indexPath.row %2== 1)
        [cell setBackgroundColor:RgbColor(252,248,238)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
  

}


-(float)adjustUILabelHeightSize:(NSString*)textlabel widthSize:(float)width font:(UIFont*)ft{
    
    CGSize maximumHeightSize = CGSizeMake(width, 9999);
    
    CGSize extendLabelSize = [textlabel sizeWithFont:ft
                                   constrainedToSize:maximumHeightSize
                                       lineBreakMode:NSLineBreakByCharWrapping];
    
//    NSLog(@"extendLabelSize.height = %f",extendLabelSize.height);
    return extendLabelSize.height;
}

- (IBAction)openNoti:(id)sender
{
    if(![notiCode isEqualToString:@"001"])
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"공지사항이 없습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];

        [av show];
        return;
    }
    
    _detailView.hidden = NO;
}

- (IBAction)tabPlayFmRadio:(id)sender
{
    [self selectMenuIndex:1];
}
- (IBAction)tabPlayEndRadio:(id)sender
{
    [self selectMenuIndex:2];
}

/*
 * Loads the table
 */
- (void)loadTable {
    
//    if([boardList count])
//    {
//        
//    }
    if([boardList count] < 1)
        return;
    if(![[[NSUserDefaults standardUserDefaults] valueForKey:@"UseBoard"] isEqualToString:@"Y"])
        return;
    NSLog(@"ld");
    //http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=10009938&listType=N&pageSize=5&seq=86
    NSError *error;
    
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=%@&listType=N&pageSize=10&seq=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"],[[boardList objectAtIndex:[boardList count]-1] valueForKey:@"seq"]];

    NSLog(@"bandi bnotioard loadtable call url = %@",urlStr);
    
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    NSLog(@"load loadTable end");
    if(!jsonString || jsonString == nil)
        return;

    
    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
    [boardList addObjectsFromArray:[person valueForKey:@"bandiPosts"]];
    [_tbList reloadData];
    
    [pullToRefreshManager_ tableViewReloadFinished];
}

//- (void)viewDidLayoutSubviews {
//    
////    [super viewDidLayoutSubviews];
//    
//    [pullToRefreshManager_ relocatePullToRefreshView];
//}

#pragma mark -
#pragma mark MNMBottomPullToRefreshManagerClient

/**
 * This is the same delegate method as UIScrollView but required in MNMBottomPullToRefreshManagerClient protocol
 * to warn about its implementation. Here you have to call [MNMBottomPullToRefreshManager tableViewScrolled]
 *
 * Tells the delegate when the user scrolls the content view within the receiver.
 *
 * @param scrollView: The scroll-view object in which the scrolling occurred.
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [pullToRefreshManager_ tableViewScrolled];
}

/**
 * This is the same delegate method as UIScrollView but required in MNMBottomPullToRefreshClient protocol
 * to warn about its implementation. Here you have to call [MNMBottomPullToRefreshManager tableViewReleased]
 *
 * Tells the delegate when dragging ended in the scroll view.
 *
 * @param scrollView: The scroll-view object that finished scrolling the content view.
 * @param decelerate: YES if the scrolling movement will continue, but decelerate, after a touch-up gesture during a dragging operation.
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [pullToRefreshManager_ tableViewReleased];
}

/**
 * Tells client that refresh has been triggered
 * After reloading is completed must call [MNMBottomPullToRefreshManager tableViewReloadFinished]
 *
 * @param manager PTR manager
 */
- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager {
    
    [self performSelector:@selector(loadTable) withObject:nil afterDelay:1.0f];
}
//- (void)playIPradio:(NSNotification *)notification
//{
//    
//    //    [self selectMenuIndex:2];
////    nSelectedMenu = 2;
//}
//
//- (void)playFmPradio:(NSNotification *)notification
//{
//    //    [self selectMenuIndex:1];
////    nSelectedMenu = 1;
//}
- (void)keyboardWillAnimate:(NSNotification *)notification
{
    if(bLoginShow)
        return;
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    keyboardBounds = [self convertRect:keyboardBounds toView:nil];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    NSLog(@"pad = %f,%f(%f)",keyboardBounds.origin.y,keyboardBounds.size.height,_inputView.frame.origin.y );
    //pad = 379.000000,253.000000
    //pad = 416.000000,216.000000
    if([notification name] == UIKeyboardWillShowNotification)
    {
        if(bShowKeyPad)
        {
            if(keyboardBounds.size.height > 250)
            {
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, 159 + 57, _inputView.frame.size.width, 44)];
            }
            else
            {
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, 159 + 90, _inputView.frame.size.width, 44)];
                
            }
            return;
        }

        if(IS_IPHONE5)
        {
            if(keyboardBounds.size.height > 250)
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, _inputView.frame.origin.y - 163, _inputView.frame.size.width, 44)];
            else
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, _inputView.frame.origin.y - 133, _inputView.frame.size.width, 44)];
        }
        else
        {
            if(keyboardBounds.size.height > 250)
            {
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, 292 - 163, _inputView.frame.size.width, 44)];
            }
            else
            {
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, 292 - 133, _inputView.frame.size.width, 44)];

            }
            
        }

        bShowKeyPad = YES;
        _btKeyHide.hidden =NO;
    }
    else if([notification name] == UIKeyboardWillHideNotification)
    {
        if(!bShowKeyPad)
            return;
        if(IS_IPHONE5)
        {

            if(keyboardBounds.size.height > 250)
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, _inputView.frame.origin.y + 163, _inputView.frame.size.width, 34)];
            else
                [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, _inputView.frame.origin.y + 133, _inputView.frame.size.width, 34)];
        }
        else
            [_inputView setFrame:CGRectMake(_inputView.frame.origin.x, 292, _inputView.frame.size.width, 34)];
        bShowKeyPad = NO;
        _btKeyHide.hidden =YES;
    }
    [UIView commitAnimations];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{//키보드 엔터 delegate

//    textField.text = @"";
//    [textField resignFirstResponder];
//    [self postBoard];
    NSLog(@"sy>> textFieldShouldReturn is called");
    
    [self writeAction:nil];
    return YES;
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

//    [_boardText isFirstResponder];

    
    if (SharedAppDelegate.sessionUserName && SharedAppDelegate.sessionUserId) {

        BOOL isBadMember = [ServerSide isBadMember];
        if (isBadMember) {
            NSDictionary *dic = SharedAppDelegate.badMember;
            [Dialogs alertView:dic[@"badMmbExp"]];
            return NO;
        }
        
        NSLog(@"ebs-bad member check : %@, %d", SharedAppDelegate.sessionUserId, isBadMember);
        
        return YES;
    }
    
    
    NSString * strLoginStateCode =[[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"];
    if([strLoginStateCode isEqualToString:@"001"] || [strLoginStateCode isEqualToString:@"002"])
    {
        NSString *snsUserId = [[NSUserDefaults standardUserDefaults] objectForKey:@"SNSId"];
        
        BOOL isBadMember = [ServerSide isBadMember];
        if (isBadMember) {
            NSDictionary *dic = SharedAppDelegate.badMember;
            [Dialogs alertView:dic[@"badMmbExp"]];
            return NO;
        }
        
        NSLog(@"sns-bad member check : %@, %@, %d", strLoginStateCode, snsUserId, isBadMember);
        
        
        return YES;
    }
    
    
    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"UseBoard"] isEqualToString:@"Y"])
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"해당 프로그램은 게시판이 운영되지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        return NO;
    }
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"] length] < 1)
    {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"알림" message:@"해당 프로그램은 게시판이 운영되지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [av show];
        return NO;
    }

//
//    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
//    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
//
//    CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);
//    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginStateIcon:)
                                                 name:@"LoginStateIcon"
                                               object:nil];
    
    if(!bLoginShow)
    {
//        NSString * strStrView;
//        if (IS_IPHONE5)
//            strStrView = @"LogContentController";
//        else
//            strStrView = @"LogContentController_480";
//        
//        LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
//        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
//
//
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginN" object:nil];

        bLoginShow = YES;
    }
    
    
    return NO;


}
-(void)SNSIcon
{
    NSString * strLoginStateCode =[[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"];
    
    if([strLoginStateCode isEqualToString:@"001"])
    {
        _snsIcon.image = [UIImage imageNamed:@"bandi_sns_ico_3.png"];
        
    }
    else if([strLoginStateCode isEqualToString:@"002"])
    {
        _snsIcon.image = [UIImage imageNamed:@"bandi_sns_ico_2.png"];
    }
    else
    {
        if([SharedAppDelegate.sessionUserId length])
        {
            _snsIcon.image = [UIImage imageNamed:@"bandi_sns_ico_1.png"];

        }
        else
        {
            _snsIcon.hidden = YES;
            [_inputIcon setFrame:CGRectMake(4, 4, 268, 26)];
            [_boardText setFrame:CGRectMake(12, 4, 245, 26)];
            return;
        }
    }

    _snsIcon.hidden = NO;
    [_inputIcon setFrame:CGRectMake(34, 4, 238, 26)];
    [_boardText setFrame:CGRectMake(42, 3, 225, 30)];
    
}
- (IBAction)hideKeyPad:(id)sender
{
    [_boardText resignFirstResponder];
}

@end
