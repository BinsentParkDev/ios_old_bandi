//
//  TimeTable.m
//  Bandi2
//
//  Created by admin on 2014. 1. 10..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "TimeTable.h"
#import "Util.h"


@implementation TimeTable 

- (id)init
{
    self = [super init];
    if (self) {
        scheduleDic = nil;
        xmlFileWasCreated = NO;
    }
    return self;
}


- (void)parseXML:(NSString *)yyyymmdd
{
    NSString *filePath = [NSString stringWithFormat:@"%@timetable_%@.xml",  NSTemporaryDirectory(), yyyymmdd];
    NSString *xmlStr;
    NSError *error;
    
    // 파일 존재 확인
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isFileExist = [fileManager fileExistsAtPath:filePath];
    NSDictionary *fileAttr = [fileManager attributesOfItemAtPath:filePath error:&error];
    
    // 파일 생성, 유효 시간 비교
    NSDate *validTime = [NSDate dateWithTimeIntervalSinceNow:-1*60*60]; // 파일 유지시간 60 min.
    NSComparisonResult compResult = [validTime compare:fileAttr[NSFileCreationDate]];
    
    //NSLog(@"create date -> %d, %@, %@" , compResult, validTime, fileAttr[NSFileCreationDate]);
    
    if (xmlFileWasCreated || (isFileExist && compResult==NSOrderedAscending)) {
        xmlStr = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        NSLog(@"TimeTable have been created.");
    } else {
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/time_table_daily.jsp?yyyymmdd=%@", yyyymmdd];
        NSURL *xmlUrl = [NSURL URLWithString:urlStr];
        
        xmlStr = [[NSString alloc] initWithContentsOfURL:xmlUrl
                                                encoding:CFStringConvertEncodingToNSStringEncoding(0x0422)
                                                   error:&error];
        
        if (xmlStr == nil || xmlStr.length == 0) {
            return ;
        } else {
            [xmlStr writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
            xmlFileWasCreated = YES;
            NSLog(@"creating TimeTable file  now.");
        }
    }
    //NSLog(@"error -> %@", error);
    //NSLog(@"xmlStr -> %@", xmlStr);
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
	NSDateComponents *currentTime = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:[NSDate date]];
    
    NSInteger currentHour = [currentTime hour];
    if (currentHour <= 2) {
        currentHour += 24; // 편성표에 반영된 25시를 표현하기 위해.
    }
    NSString *currentTimeStr = [NSString stringWithFormat:@"%02d:%02d", currentHour, [currentTime minute] ];
    //NSLog(@"currentTimeStr => %@", currentTimeStr);
    
    TBXML *tbxml = [TBXML newTBXMLWithXMLString:xmlStr error:&error];
    TBXMLElement *elemRoot = nil, *elemSchedule=nil, *elemBrodtime=nil, *elemProgname=nil, *elemProgcd=nil, *elemHomeyn=nil, *elemVodck=nil, *prevElemSchedule=nil;
    
    NSString *bseq=nil, *brodtime=nil, *progname=nil, *progcd=nil, *homeyn=nil, *vodck=nil;
    //NSString *rescd = nil;
    
    elemRoot = tbxml.rootXMLElement;
    
    if (elemRoot) {
        // xml status
        elemSchedule = [TBXML childElementNamed:@"SCHEDULE" parentElement:elemRoot];
        while(elemSchedule) {
            bseq = [TBXML valueOfAttributeNamed:@"BSEQ" forElement:elemSchedule];
            
            elemBrodtime = [TBXML childElementNamed:@"BRODTIME" parentElement:elemSchedule];
            elemProgname = [TBXML childElementNamed:@"PROGNAME" parentElement:elemSchedule];
            elemProgcd = [TBXML childElementNamed:@"PROGCD" parentElement:elemSchedule];
            elemHomeyn = [TBXML childElementNamed:@"HOMEYN" parentElement:elemSchedule];
            elemVodck = [TBXML childElementNamed:@"VODCK" parentElement:elemSchedule];
            
            brodtime = [TBXML textForElement:elemBrodtime];
            progname = [TBXML textForElement:elemProgname];
            progcd = [TBXML textForElement:elemProgcd];
            homeyn = [TBXML textForElement:elemHomeyn];
            vodck = [TBXML textForElement:elemVodck];
            
            //NSLog(@"%@ -> %d", brodtime, [currentTimeStr compare:brodtime]);
            
            if ([currentTimeStr compare:brodtime] == -1) {
                bseq = [TBXML valueOfAttributeNamed:@"BSEQ" forElement:prevElemSchedule];
                
                elemBrodtime = [TBXML childElementNamed:@"BRODTIME" parentElement:prevElemSchedule];
                elemProgname = [TBXML childElementNamed:@"PROGNAME" parentElement:prevElemSchedule];
                elemProgcd = [TBXML childElementNamed:@"PROGCD" parentElement:prevElemSchedule];
                elemHomeyn = [TBXML childElementNamed:@"HOMEYN" parentElement:prevElemSchedule];
                elemVodck = [TBXML childElementNamed:@"VODCK" parentElement:prevElemSchedule];
                
                brodtime = [TBXML textForElement:elemBrodtime];
                progname = [TBXML textForElement:elemProgname];
                progcd = [TBXML textForElement:elemProgcd];
                homeyn = [TBXML textForElement:elemHomeyn];
                vodck = [TBXML textForElement:elemVodck];
                if(!brodtime)
                    return;
                    
                
                scheduleDic = @{@"brodtime" : brodtime ,
                                @"progname" : [Util xmlEntitiesDecode:progname] ,
                                @"progcd"   : progcd ,
                                @"homeyn"   : homeyn ,
                                @"vodck"    : vodck ,
                                @"bseq"     : bseq };
                break;
            }
            
            prevElemSchedule = elemSchedule;
            
            elemSchedule = elemSchedule->nextSibling;
            
        }
        
        // 마지막 편성 정보일 경우 대비
        if (scheduleDic == nil && prevElemSchedule != nil) {
            bseq = [TBXML valueOfAttributeNamed:@"BSEQ" forElement:prevElemSchedule];
            
            elemBrodtime = [TBXML childElementNamed:@"BRODTIME" parentElement:prevElemSchedule];
            elemProgname = [TBXML childElementNamed:@"PROGNAME" parentElement:prevElemSchedule];
            elemProgcd = [TBXML childElementNamed:@"PROGCD" parentElement:prevElemSchedule];
            elemHomeyn = [TBXML childElementNamed:@"HOMEYN" parentElement:prevElemSchedule];
            elemVodck = [TBXML childElementNamed:@"VODCK" parentElement:prevElemSchedule];
            
            brodtime = [TBXML textForElement:elemBrodtime];
            progname = [TBXML textForElement:elemProgname];
            progcd = [TBXML textForElement:elemProgcd];
            homeyn = [TBXML textForElement:elemHomeyn];
            vodck = [TBXML textForElement:elemVodck];
            
            scheduleDic = @{@"brodtime" : brodtime ,
                            @"progname" : [Util xmlEntitiesDecode:progname] ,
                            @"progcd"   : progcd ,
                            @"homeyn"   : homeyn ,
                            @"vodck"    : vodck ,
                            @"bseq"     : bseq };
        }
    }
    //NSLog(@"scheduleDic -> %@", scheduleDic);
}

/**
 편성표 데이타를 가지고 온다.

 편성은 다음날 새벽 2시까지 이므로 새벽 0시-2시는 전날 편성표를 참고해야 한다.
 */
- (NSDictionary *)get
{
    //if (scheduleDic == nil)
    {
        NSDate *date2HoursAgo = [NSDate dateWithTimeInterval:(-2.1*60*60) sinceDate:[NSDate date]]; // 2시간 6분 전 시간
        //NSLog(@"date1 => %@, %@", date2HoursAgo, [NSDate date]);
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
        NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                                                    fromDate:date2HoursAgo];
        NSString *currentTimeStr = [NSString stringWithFormat:@"%04d%02d%02d", [currentTime year], [currentTime month], [currentTime day] ];
        //NSLog(@"currentTimeStr -> %@", currentTimeStr);
        [self parseXML:currentTimeStr];
    }
    
    //
    NSLog(@"scheduleDic -> %@", scheduleDic);
    return scheduleDic;
}

@end
