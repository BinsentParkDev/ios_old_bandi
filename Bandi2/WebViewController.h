//
//  WebViewController.h
//  Bandi2
//
//  Created by admin on 2014. 1. 22..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>

- (IBAction)moveBackward:(id)sender;
- (IBAction)moveForward:(id)sender;
- (IBAction)reloadPage:(id)sender;

- (IBAction)closeView:(id)sender;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end
