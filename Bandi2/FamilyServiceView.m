//
//  FamilyServiceView.m
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "FamilyServiceView.h"
#import "WebViewController.h"
#import "ServerSide.h"
#import "GA.h"

#define TAG @"FamilyService"

@implementation FamilyServiceView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        NSURL *url = [NSURL URLWithString:@"http://m.ebs.co.kr/familyService?appYn=bandi"];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [self loadRequest:request];
        // view size 
        frame.size.height = bodyHeight;
        [self setFrame:frame];
        
        NSLog(@"view -> %@", self);
        self.delegate = self;
    }
    
    [GA sendScreenView:@"FamilyService"];
    
    return self;
}

- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"navigationType -> %d", navigationType);
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        NSLog(@"click : %@", [[request URL] absoluteString]);
        
        WebViewController *vc = [[WebViewController alloc] init];
        [self.window.rootViewController presentViewController:vc animated:YES completion:^{
            [(UIWebView *)[vc.view.subviews lastObject] loadRequest:request];
        }];
        return NO;
    }
    return YES;
}
@end
