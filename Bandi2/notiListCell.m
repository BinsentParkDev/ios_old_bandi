//
//  ProgramListCell.m
//  EBSTVApp
//
//  Created by Hyeoung seok Yoon on 2013. 11. 10..
//  Copyright (c) 2013년 Hyeoung seok Yoon. All rights reserved.
//


#import "notiListCell.h"

@interface notiListCell ()

@end
 
@implementation notiListCell


@synthesize lbEventTitle;
@synthesize lbEventDay;
@synthesize ivNoti;
@synthesize ivEvent;
@synthesize lbNoti;
@synthesize listIndex;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated]; 
}

-(void)cellData:(NSDictionary *)dic
{
    if([[dic valueForKey:@"evtClsCd"] isEqualToString:@"001"])
    {
        ivEvent.hidden = YES;
        lbEventTitle.hidden = YES;
        lbEventDay.hidden = YES;
        ivNoti.hidden=NO;
        lbNoti.hidden=NO;
        lbNoti.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"evtTitle"]];

    }
    else
    {
        ivEvent.hidden = NO;
        lbEventTitle.hidden = NO;
        lbEventDay.hidden = NO;
        ivNoti.hidden=YES;
        lbNoti.hidden=YES;
        lbEventTitle.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"evtTitle"]];
      
        
        
        NSRange areaStr;

        NSString * strStartYear = [[dic valueForKey:@"evtStartDt"] substringToIndex:4];
        areaStr.location = 4;
        areaStr.length = 2;
        NSString * strStartMonth = [[dic valueForKey:@"evtStartDt"] substringWithRange:areaStr];
        areaStr.location = 6;
        areaStr.length = 2;
        NSString * strStartDay = [[dic valueForKey:@"evtStartDt"] substringWithRange:areaStr];
        
        NSString * strEndYear = [[dic valueForKey:@"evtEndDt"] substringToIndex:4];
        areaStr.location = 4;
        areaStr.length = 2;
        NSString * strEndMonth = [[dic valueForKey:@"evtEndDt"] substringWithRange:areaStr];
        areaStr.location = 6;
        areaStr.length = 2;
        NSString * strEndDay = [[dic valueForKey:@"evtEndDt"] substringWithRange:areaStr];
      

        NSString * strDay = [NSString stringWithFormat:@"%@-%@-%@ ~ %@-%@-%@",strStartYear,strStartMonth,strStartDay,strEndYear,strEndMonth,strEndDay];
    
        lbEventDay.text = strDay;
        
        
        
        
        
//       // NSString *dateString = @"01-02-2010";
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//        
//        NSDate *dateFromString = [[NSDate alloc] init];
//        dateFromString = [dateFormatter dateFromString:[dic valueForKey:@"evtStartDt"]];
//        
//        
//        NSString *stringDate = [dateFormatter stringFromDate:dateFromString];
//        NSLog(@"%@", stringDate);
//        
//        
////        NSDate *dateFromString = [[NSDate alloc] init];
//        // voila!
//        dateFromString = [dateFormatter dateFromString:[dic valueForKey:@"evtStartDt"]];

    }
}

-(IBAction)showDetail:(id)sende
{
    [_cellDelegate selectNoti:listIndex];
}
@end
