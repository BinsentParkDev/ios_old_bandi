//
//  TimeTable.h
//  Bandi2
//
//  Created by admin on 2014. 1. 10..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBXML+HTTP.h"

@interface TimeTable : NSObject
{
    bool xmlFileWasCreated;
    NSDictionary *scheduleDic;
    
    
}

- (void)parseXML:(NSString *)yyyymmdd;
- (NSDictionary *)get;
@end
