//
//  WebViewController.m
//  Bandi2
//
//  Created by admin on 2014. 1. 22..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "LogContentController.h"
#import "LoginView.h"
#import "BandiBoardWriteView.h"
#import "ServerSide.h"
#import "Dialogs.h"
#import <KakaoOpenSDK/KakaoOpenSDK.h>
#import "FHSTwitterEngine.h"
#import "WebViewController.h"
#import "NSUserDefaults+UserData.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LogContentController ()

@end

@implementation LogContentController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _bOnlyEBS = NO;

    }
    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
    

    userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([userDefaults boolForKey:@"SAVE_ID"]) {
        _saveIdBtn.selected = YES;
        _userId.text = [userDefaults getUserId];
        
    }
    if ([userDefaults boolForKey:@"AUTO_LOGIN"]) {
        _autoLoginBtn.selected = YES;
        _userId.text = [userDefaults getUserId];
        _passwd.text = [userDefaults getPassword];
    }
    
    
    /** facebook sdk 4.* for test ==
    FBSDKLoginButton *loginButtonT = [[FBSDKLoginButton alloc] init];
    loginButtonT.center = self.view.center;
    [self.view addSubview:loginButtonT];
    // facebook sdk 4.* for test  ==**/
    
    
    //* facebook sdk 4.23 - use custom image to facebook login
    NSLog(@"facebook token : %@", [FBSDKAccessToken currentAccessToken]);
    NSLog(@"fb y pos : %f", _fbLoginView.frame.origin.y);
    NSLog(@"twt y pos : %f", _twitterLogin.frame.origin.y);
    UIButton *facebookLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    facebookLogin.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    facebookLogin.frame = CGRectMake(35,280,250,35);
//    facebookLogin.center = self.view.center;
    
    [facebookLogin setImage:[UIImage imageNamed:@"login_facebook_off.png"] forState:UIControlStateNormal];
    [facebookLogin setImage:[UIImage imageNamed:@"login_facebook_on.png"] forState:UIControlStateHighlighted];
    [facebookLogin addTarget:self action:@selector(facebookLoginClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:facebookLogin];
    //* facebook sdk 4.23 - use custom image to facebook login */
    
    // 기존 페이스북 버튼 숨기기
    _fbLoginView.hidden = YES;
    
    
    [_fbLoginView setReadPermissions:@[@"public_profile"]];
    [_fbLoginView setDelegate:self];
    _objectID = nil;
    
    _fbLoginView.frame = CGRectMake(35, _fbLoginView.frame.origin.y, 250, 35);
    for (id obj in _fbLoginView.subviews)
    {
        if ([obj isKindOfClass:[UIButton class]])
        {
            UIButton * loginButton =  obj;
            UIImage *loginImage = [UIImage imageNamed:@"login_facebook_off.png"];
            UIImage *loginImageH = [UIImage imageNamed:@"login_facebook_on.png"];
            loginButton.frame = CGRectMake(0,0, 250, 35);
            [loginButton setBackgroundImage:loginImage forState:UIControlStateNormal];
            [loginButton setBackgroundImage:nil forState:UIControlStateSelected];
            [loginButton setBackgroundImage:loginImageH forState:UIControlStateHighlighted];
            //            [loginButton sizeToFit];
        }
        if ([obj isKindOfClass:[UILabel class]])
        {
            UILabel * loginLabel =  obj;
            loginLabel.text = @"";
            loginLabel.textAlignment = NSTextAlignmentLeft;
            loginLabel.frame = CGRectMake(0, 0, 250, 30);
            loginLabel.hidden = YES;
        }
    }
    _twLoginV.hidden = YES;
    
    
    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"] isEqualToString:@"000"])
    {
        _fbLoginView.hidden = YES;
        _twitterLogin.hidden = YES;
    }
    if(_bOnlyEBS)
    {
        _fbLoginView.hidden = YES;
        _twitterLogin.hidden = YES;
    }

    _keyHideBt.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAnimate:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAnimate:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    

    
    _webView.delegate = self;
    _webControll.hidden = YES;
    
    SharedAppDelegate.isFromLogin = YES;
    
}

#pragma mark - facebook login
/**
 facebook sdk 4.* login with custom image 로그인 처리
 */
-(void) facebookLoginClicked
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorWeb;
    [login logInWithReadPermissions:@[@"public_profile"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            NSLog(@"Process error");
        } else if (result.isCancelled) {
            NSLog(@"Cancelled");
        } else {
            NSLog(@"Logged in");
            [self fetchFacebookUserInfo];
        }
    }];
    
}


/**
 facebook sdk 4.* - 로그인 사용자 정보 요청 받아오기
 */
-(void) fetchFacebookUserInfo
{
    if ([FBSDKAccessToken currentAccessToken]) {
        NSLog(@"Token is available : %@", [[FBSDKAccessToken currentAccessToken] tokenString]);
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                           parameters:@{@"fields":@"id, name, link, first_name, last_name"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (error) {
                NSLog(@"Error : %@ ", error);
            } else {
                NSLog(@"result : %@, %@", result, result[@"id"]);
                
                [[NSUserDefaults standardUserDefaults] setObject:@"001" forKey:@"SNSLogin"];
                [[NSUserDefaults standardUserDefaults] setObject:result[@"id"] forKey:@"SNSId"];
                [[NSUserDefaults standardUserDefaults] setObject:result[@"name"] forKey:@"SNSName"];
                [[NSUserDefaults standardUserDefaults] synchronize];
//
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
                [self dismissViewControllerAnimated:NO completion:nil];
            }
        }];
    }
}

#pragma mark - 

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)close:(id)sender
{
    SharedAppDelegate.isFromLogin = NO;
//    cc.navigationItem.title = SharedAppDelegate.latestMenu;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
    if(_bGoBack)
    {

        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginCloseAction" object:nil];

        [self dismissViewControllerAnimated:NO completion:nil];
//        [self performSelector:@selector(goBackV) withObject:nil afterDelay:0.1f];
        return;
    }
    
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)goBackV
{
//        [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)EBSLoginOnly
{
    _bOnlyEBS = YES;
    _fbLoginView.hidden = YES;
    _twitterLogin.hidden = YES;
    
}

- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

#pragma mark - 로그인 처리

- (IBAction)doLogin:(id)sender
{
    [_userId resignFirstResponder];
    [_passwd resignFirstResponder];
    
    if([_userId.text length] < 1)
    {
        [Dialogs alertView:@"아이디를 입력해 주세요."];
        return;
    }
    if([_passwd.text length] < 1)
    {
        [Dialogs alertView:@"비밀번호를 입력해 주세요."];
        return;
    }
    
    
    NSLog(@"LogContentController.doLogin");
    //** new login **
    NSString* resultCode = [ServerSide doLoginNew:_userId.text withPassword:_passwd.text];
//    NSString* resultCode = [ServerSide doLoginNew:@"langjoin2" withPassword:@"apr2015!"];
    if ([resultCode isEqualToString:@"200"]) {
        [userDefaults setObject:@"000" forKey:@"SNSLogin"];
        [userDefaults setObject:@"" forKey:@"SNSId"];
        [userDefaults setObject:@"" forKey:@"SNSName"];
        [userDefaults synchronize];

        if (_saveIdBtn.selected) {
            [userDefaults setUserId:_userId.text];
        }
        if (_autoLoginBtn.selected) {
            [userDefaults setUserId:_userId.text];
            [userDefaults setPassword:_passwd.text];
        }

        if(_bGoboard)
            [self goBoard];
        else
        {

        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
        [self dismissViewControllerAnimated:NO completion:nil];
        
    } else if ([resultCode isEqualToString:@"401"]) {
        [Dialogs alertView:@"ID 또는 패스워드가 잘못되었습니다."];
        
//        [userDefaults setBool:NO forKey:@"SAVE_ID"];
        [userDefaults setBool:NO forKey:@"AUTO_LOGIN"];
    } else {
        [Dialogs alertView:[NSString stringWithFormat:@"로그인 오류가 발생했습니다. - code : %@", resultCode]];
        [userDefaults setBool:NO forKey:@"AUTO_LOGIN"];
    }
    
    //** new login **/
    
    
    
//    NSString *resultCode = [ServerSide doLogin:_userId.text withPasswd:_passwd.text];
//
//    if ([resultCode isEqualToString:@"100"]) {
//        
//        [userDefaults setObject:@"000" forKey:@"SNSLogin"];
//        [userDefaults setObject:@"" forKey:@"SNSId"];
//        [userDefaults setObject:@"" forKey:@"SNSName"];
//        [userDefaults synchronize];
//        
//        if (_saveIdBtn.selected) {
//            [userDefaults setUserId:_userId.text];
//        }
//        if (_autoLoginBtn.selected) {
//            [userDefaults setUserId:_userId.text];
//            [userDefaults setPassword:_passwd.text];
//        }
//        
//        if(_bGoboard)
//            [self goBoard];
//        else
//        {
//
//        }
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
//        [self dismissViewControllerAnimated:NO completion:nil];
//        
//    } else if ([resultCode isEqualToString:@"102"]) {
//        [Dialogs alertView:@"로그인 실패 - 등록되지 않은 ID입니다."];
//        [userDefaults setBool:NO forKey:@"SAVE_ID"];
//    } else if ([resultCode isEqualToString:@"103"]) {
//        [Dialogs alertView:@"로그인 실패 - 잘못된 비밀번호입니다."];
//        [userDefaults setBool:NO forKey:@"AUTO_LOGIN"];
//    } else if ([resultCode isEqualToString:@"104"]) {
//        [Dialogs alertView:@"로그인 실패 - 사용되지 않는 ID입니다."];
//        [userDefaults setBool:NO forKey:@"SAVE_ID"];
//        [userDefaults setBool:NO forKey:@"AUTO_LOGIN"];
//    } else {
//        [Dialogs alertView:@"ID 또는 비밀번호를 잘못 입력하셨습니다."];
//    }
    
}


- (IBAction)saveId:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (btn.selected) {
        btn.selected = NO;
    } else {
        btn.selected = YES;
    }
    
    [userDefaults setBool:btn.selected forKey:@"SAVE_ID"];
}
- (IBAction)autoLogin:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (btn.selected) {
        btn.selected = NO;
    } else {
        btn.selected = YES;
    }
    
    [userDefaults setBool:btn.selected forKey:@"AUTO_LOGIN"];
    
    
}
//
//- (IBAction)closeLoginView:(id)sender {
//    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
//    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
//    
//    cc.navigationItem.title = SharedAppDelegate.latestMenu;
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
//    if(_bGoBack)
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginCloseAction" object:nil];
//    [self removeFromSuperview];
//}
- (void)loginBack:(BOOL)bb
{
    _bGoBack = bb;
}
-(void)goBoard
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MoveTab3" object:nil];

}


// facebook FBLoginView delegate method
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    NSLog(@"id - %@ / name - %@",user.objectID,user.name);
    //    [NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"]
    
    [[NSUserDefaults standardUserDefaults] setObject:@"001" forKey:@"SNSLogin"];
    [[NSUserDefaults standardUserDefaults] setObject:user.objectID forKey:@"SNSId"];
    [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"SNSName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //    self.profilePictureView.profileID = user.id;
    //    self.nameLabel.text = user.name;
    

    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}


// facebook FBLoginView delegate method
// You need to override loginView:handleError in order to handle possible errors that can occur during login
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    NSLog(@"facebook error.. ");
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures since that happen outside of the app.
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}





- (UIButton *)createKakaoAccountConnectButton {
    // button position
    int xMargin = 30;
    
    CGFloat btnWidth = self.view.frame.size.width - xMargin * 2;
    int btnHeight = 42;
    
    UIButton *btnKakaoLogin
    = [[KOLoginButton alloc] initWithFrame:CGRectMake(xMargin, _fbLoginView.frame.origin.y+_fbLoginView.frame.size.height + 10, btnWidth, btnHeight)];
    btnKakaoLogin.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    
    
    // action
    [btnKakaoLogin addTarget:self action:@selector(invokeLoginWithTarget:) forControlEvents:UIControlEventTouchUpInside];
    
    return btnKakaoLogin;
}


- (IBAction)invokeLoginWithTarget:(id)sender {
    
    [[KOSession sharedSession] close];
    [[KOSession sharedSession] openWithCompletionHandler:^(NSError *error) {
        
        if ([[KOSession sharedSession] isOpen]) {
            // login success.
            [KOSessionTask storyProfileTaskWithCompletionHandler:^(KOStoryProfile *result, NSError *error) {
                if (result) {
                    //                    [self showStoryProfile:result];
                    
                    NSLog(@"login success.");
                    //            [[[UIApplication sharedApplication] delegate] performSelector:@selector(showMainView)];
                    [[NSUserDefaults standardUserDefaults] setObject:@"006" forKey:@"SNSLogin"];
                    [[NSUserDefaults standardUserDefaults] setObject:result.nickName forKey:@"SNSId"];
                    [[NSUserDefaults standardUserDefaults] setObject:result.nickName forKey:@"SNSName"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
     
//                    cc.navigationItem.title = SharedAppDelegate.latestMenu;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
                        [self dismissViewControllerAnimated:NO completion:nil];
                    
                } else {
                    NSLog(@"%@", error);
                }
            }];
            
        } else {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"에러"
                                                                message:error.localizedDescription
                                                               delegate:nil
                                                      cancelButtonTitle:@"확인"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
        
    }];
}

- (IBAction)twitterLogin:(id)sender
{
    
    
    [self dismissViewControllerAnimated:NO completion:nil];    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TwitterLoginN" object:nil];

    

//
//    UIViewController *loginController = [[FHSTwitterEngine sharedEngine]loginControllerWithCompletionHandler:^(BOOL success) {
//        
//        if (success) {
//            NSLog(success?@"L0L success":@"O noes!!! Loggen faylur!!!");
//            NSLog(@"name = %@",FHSTwitterEngine.sharedEngine.authenticatedUsername);
//            //        [_theTableView reloadData];
//            [[NSUserDefaults standardUserDefaults] setObject:@"002" forKey:@"SNSLogin"];
//            [[NSUserDefaults standardUserDefaults] setObject:FHSTwitterEngine.sharedEngine.authenticatedID forKey:@"SNSId"];
//            [[NSUserDefaults standardUserDefaults] setObject:FHSTwitterEngine.sharedEngine.authenticatedUsername forKey:@"SNSName"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginStateIcon" object:nil];
////            [self dismissViewControllerAnimated:NO completion:nil];
//
//        }
//        else
//        {
//            NSLog(@"relogin");
//        }
//        
//    }];
//    
//    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:loginController animated:YES completion:nil];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{//키보드 엔터 delegate
    
    [_userId resignFirstResponder];
    [_passwd resignFirstResponder];
    //    [self postBoard];
    return YES;
    
}

-(IBAction)findId:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"https://sso.ebs.co.kr/idp/mobile/user/find/id.jsp" ];
//    [[UIApplication sharedApplication] openURL:url];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    _webControll.hidden = NO;
}
-(IBAction)findPass:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"https://sso.ebs.co.kr/idp/mobile/user/reissue/password.jsp"];
//    [[UIApplication sharedApplication] openURL:url];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    _webControll.hidden = NO;
}
-(IBAction)Join:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"https://sso.ebs.co.kr/idp/mobile/join/agreement.jsp" ];
//    [[UIApplication sharedApplication] openURL:url];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    _webControll.hidden = NO;

    
}

-(IBAction)hidekey:(id)sender
{
    [_userId resignFirstResponder];
    [_passwd resignFirstResponder];
}
- (void)keyboardWillAnimate:(NSNotification *)notification
{
    
    if([notification name] == UIKeyboardWillShowNotification)
    {
        _keyHideBt.hidden = NO;
    }
    else if([notification name] == UIKeyboardWillHideNotification)
    {
        
        _keyHideBt.hidden = YES;
    }
}

-(void)dealloc
{
    _fbLoginView.delegate = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(IBAction)loginTmp:(id)sender
{
    NSLog(@"loginTmp loaded. - FBSession");
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        // Close the session and remove the access token from the cache
        // The session state handler (in the app delegate) will be called automatically
        [FBSession.activeSession closeAndClearTokenInformation];
        
        // If the session state is not any of the two "open" states when the button is clicked
    } else {
        // Open a session showing the user the login UI
        // You must ALWAYS ask for public_profile permissions when opening a session
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error) {
             
             //             NSLog(@"sesstion state= %@",state);
             // Retrieve the app delegate
             //             Bandi2AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
             // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
             //             [appDelegate sessionStateChanged:session state:state error:error];
         }];
    }
}


- (IBAction)closeView:(id)sender {
    _webControll.hidden = YES;
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
//    NSLog(@"navigationType -> %d", navigationType);
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        NSLog(@"click : %@", [[request URL] absoluteString]);
        NSString *urlStr = [[request URL] absoluteString];
        if ([urlStr isEqualToString:@"bandi://banner.close"]) {
            [self closeView:nil];
            return NO;
        }
    }
    return YES;
}

@end
