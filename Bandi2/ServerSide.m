//
//  ServerSide.m
//  Bandi2
//
//  Created by admin on 2014. 1. 24..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "ServerSide.h"
#import "TBXML.h"
#import "Util.h"
#import "TimeTable.h"
#import "NSData+Base64.h"
#import "Util.h"
#import "NSUserDefaults+UserData.h"


@implementation ServerSide

# pragma mark - 앱 버전 정보
+ (NSString *)appVersion
{
    // 서버에 한 번만 접근하기 위해.. 이미 저장된 값이 있으면 반환
    if (SharedAppDelegate.serverAppVersion) {
        return SharedAppDelegate.serverAppVersion;
    }
    
    
    //NSLog(@"reach here.");
    NSString *urlStr = [NSString stringWithFormat:@"http://m.ebs.co.kr/appVer?appId=bandi&appOs=ios"];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSError *error;
    NSString *xmlStr = [[NSString alloc] initWithContentsOfURL:url
                                                      encoding:NSUTF8StringEncoding
                                                         error:&error];
    TBXML *tbxml = [TBXML newTBXMLWithXMLString:xmlStr error:&error];
    TBXMLElement *elemRoot=nil, *elemRescd=nil, *elemData=nil, *elemEbsAppInfo=nil, *elemItem=nil, *elemAppVer=nil;
    NSString *rescd=nil, *appVer=@"0";
    //NSLog(@"error - %@", error);
    
    elemRoot = tbxml.rootXMLElement;
    if (elemRoot) {
        elemRescd = [TBXML childElementNamed:@"rescd" parentElement:elemRoot];
        if (elemRescd) {
            rescd = [TBXML textForElement:elemRescd];
        }
        
        if ([rescd isEqualToString:@"0000"]) {
            elemData = [TBXML childElementNamed:@"data" parentElement:elemRoot];
            elemEbsAppInfo = [TBXML childElementNamed:@"ebs.appInfo" parentElement:elemData];
            elemItem = [TBXML childElementNamed:@"item" parentElement:elemEbsAppInfo];
            elemAppVer = [TBXML childElementNamed:@"appVer" parentElement:elemItem];
            appVer = [TBXML textForElement:elemAppVer];
        }
    }
    
    SharedAppDelegate.serverAppVersion = appVer;
    return appVer;
}

+ (BOOL)existNewVersion
{
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    //NSString *appDisplayName = [infoDic objectForKey:@"CFBundleDisplayName"];
    //NSString *version = [infoDic objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [infoDic objectForKey:@"CFBundleVersion"];
    NSString *serverAppVersion = [ServerSide appVersion];
    
    int svrVer = (int)[serverAppVersion integerValue];
    int localVer = (int)[build integerValue];
    
    if (svrVer > localVer) {
        return YES;
    } else {
        return NO;
    }
}



# pragma mark - 로그인 관련
/**
 http(s) + post call
 */
+ (NSString *)post:(NSString *)postString url:(NSString *)urlString error:(NSError * _Nullable *)error
{
    NSData *returnData = [[NSData alloc] init];
    
    NSData *postData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    // build the request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-length"];
    [request setHTTPBody:postData];
    
    //send the request
    returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:error];
    
    NSString *response = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    
    NSLog(@"response = %@", response);
    
//    return returnData;
    return response;
}


+ (NSInteger)post:(NSString *)postString url:(NSString *)urlString headers:(NSDictionary **)headers body:(NSString **)body
{
    NSData *returnData = [[NSData alloc] init];
    
    NSData *postData = [postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSHTTPURLResponse *response;
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    // build the request
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                            timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-length"];
    [request setHTTPBody:postData];
    
    //send the request
    NSError *error;
    returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    // get data from response
    NSInteger statusCode = [response statusCode];
    NSLog(@"statusCode : %ld",(long)statusCode);
    if (statusCode != 200) return statusCode;
    
    if ([response respondsToSelector:@selector(allHeaderFields)]) {
        if (headers != nil) {
            *headers = [response allHeaderFields];
        }
    }
    
    if (body != nil) {
        *body = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
        NSLog(@"response = %@", *body);
    }
    
    return statusCode;
}

+ (NSInteger)get:(NSString *)urlString withCookie:(NSString *)cookie headers:(NSDictionary **)headers body:(NSString **)body
{
    *headers = [[NSDictionary alloc] init];
    *body = @"";
    
    // build the request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request addValue:cookie forHTTPHeaderField:@"Cookie"];
    
    //send the request
    NSError *error;
    NSHTTPURLResponse *response;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    // get data from response
    NSInteger statusCode = [response statusCode];
    NSLog(@"statusCode : %ld",(long)statusCode);
    if (statusCode != 200) return statusCode;
    
    if ([response respondsToSelector:@selector(allHeaderFields)]) {
        if (headers != nil) {
            *headers = [response allHeaderFields];
        }
    }
    
    if (body != nil) {
        *body = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
        NSLog(@"response = %@", *body);
    }
    
    return statusCode;
}

+ (NSString *)doLogin:(NSString *)userId withPasswd:(NSString *)passwd
{
//    userId = [userId stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    passwd = [passwd stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    userId = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (__bridge CFStringRef) userId,
                                                                                 NULL,
                                                                                 CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                 kCFStringEncodingUTF8));
    passwd = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (__bridge CFStringRef) passwd,
                                                                                 NULL,
                                                                                 CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                 kCFStringEncodingUTF8));

    NSString *dataStr = @"";
    NSError *error = nil;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults boolForKey:@"SECURE_LOGIN"]) {
        dataStr = [self post:[NSString stringWithFormat:@"userid=%@&passwd=%@", userId, passwd]
                         url:@"https://sso.ebs.co.kr/idp/bandiLogin"
                       error:&error];
        NSLog(@"https + post login");
    } else {
        NSString *str = [NSString stringWithFormat:@"https://sso.ebs.co.kr/idp/bandiLogin?userid=%@&passwd=%@", userId, passwd];
        NSURL *url = [NSURL URLWithString:str];
        dataStr = [[NSString alloc] initWithContentsOfURL:url
                                                 encoding:NSUTF8StringEncoding
                                                    error:&error];
        NSLog(@"https + get login");
    }
    
    
    NSString *resultCode;
    if (error) {
        NSLog(@"login error -> %@", error.localizedDescription);
    } else {
        dataStr = [dataStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:@"([0-9]{3})\\s(.*)"
                                                                          options:NSRegularExpressionCaseInsensitive
                                                                            error:&error];
        
        NSArray *matches = [regEx matchesInString:dataStr options:0 range:NSMakeRange(0, [dataStr length])];
        for (NSTextCheckingResult* match in matches) {
            resultCode = [dataStr substringWithRange:[match rangeAtIndex:1]];
            NSString *resultText;
            if ([resultCode isEqualToString:@"100"]) {
                resultText = [dataStr substringWithRange:[match rangeAtIndex:2]];
                NSString *plainText=@"";
                @try {
                    NSData *dat;
                    if ([Util lessThanVersion7]) {
                        dat = [NSData dataFromBase64String:resultText];
                    } else {
                        dat = [[NSData alloc] initWithBase64EncodedString:resultText options:0];
                    }
                    //NSData *dat = [[NSData alloc] initWithBase64EncodedString:resultText options:0];
                    plainText = [[NSString alloc] initWithData:dat encoding:NSUTF8StringEncoding];
                }
                @catch (NSException *exception) {
                    NSLog(@"Error in doLogin - %@", exception.description);
                }
                
                if (plainText) {
                    NSArray *resultArr = [plainText componentsSeparatedByString:@"/"];
                    
                    if (resultArr.count == 3) {
                        //NSLog(@"server side : result -> %@ , %@, %@", resultArr[0], resultArr[1], resultArr[2]);
                        SharedAppDelegate.sessionUserId = resultArr[0];
                        SharedAppDelegate.sessionUserName = resultArr[1];
                        SharedAppDelegate.sessionUserEmail = resultArr[2];
                    }
                }
            }
        }
    }
    
    if (! SharedAppDelegate.sessionUserId) {
        return @"109"; // 임의로 정한 에러 코드
    }
    
    return resultCode;
}

+ (NSString *)percentEscapedString:(NSString *)inputString
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                          NULL,
                                                                          (__bridge CFStringRef) inputString,
                                                                          NULL,
                                                                          CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                          kCFStringEncodingUTF8));
}

+ (NSString *)getCookieFromHeader:(NSDictionary *)headers
{
    NSMutableString *cookieStr = [[NSMutableString alloc] init];
    
    if ([headers objectForKey:@"Set-Cookie"] != nil) {
        NSArray<NSString *> *cookies = [headers[@"Set-Cookie"] componentsSeparatedByString:@","];
        NSLog(@"cookie : %@", [cookies description]);
        for (int i=0; i<cookies.count; i++) {
            [cookieStr appendString:[cookies[i] componentsSeparatedByString:@";"][0]];
            [cookieStr appendString:@";"];
        }
    }
    
    return [NSString stringWithString:cookieStr];
}


+ (NSDictionary<NSString *, NSString *> *)getUserInfoFromXml:(NSString *)xmlStr
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    NSError *error;
    TBXML *tbxml = [TBXML newTBXMLWithXMLString:xmlStr error:&error];
    TBXMLElement *profile=nil, *userName, *email=nil, *userId=nil;
    profile = tbxml.rootXMLElement;
    if (profile) {
        userId = [TBXML childElementNamed:@"userId" parentElement:profile];
        if (userId) {
            dic[@"userId"] = [TBXML textForElement:userId];
        }
        email = [TBXML childElementNamed:@"email" parentElement:profile];
        if (email) {
            dic[@"email"] = [TBXML textForElement:email];
        }
        userName = [TBXML childElementNamed:@"userName" parentElement:profile];
        if (userName) {
            dic[@"userName"] = [TBXML textForElement:userName];
        }
        
        NSLog(@"userInfo : %@", [dic description]);
    }
    
    return [NSDictionary dictionaryWithDictionary:dic];
}


+ (NSString *)doLoginNew:(NSString *)j_username withPassword:(NSString *)j_password
{
    
    NSString *SAMLRequest = [self makeSAMLRequestBase64Encoded]; // login parameter (need to base64 encoding) with j_username, j_password

    j_username = [self percentEscapedString:j_username];
    j_password = [self percentEscapedString:j_password];
    SAMLRequest = [self percentEscapedString:SAMLRequest];
    
    
    NSInteger responseCode = 0;
    NSDictionary *responseHeaders;
    NSString *responseBody;
    responseCode = [self post:[NSString stringWithFormat:@"j_username=%@&j_password=%@&SAMLRequest=%@", j_username, j_password, SAMLRequest]
                          url:@"https://sso.ebs.co.kr/idp/profile/SAML2/POST-GET/SSO"
                      headers:&responseHeaders
                         body:&responseBody ];
    NSLog(@"ServerSide.m > doLoginNew > response code, body : %ld, %@", (long)responseCode, responseBody);
    NSLog(@"ServerSide.m > doLoginNew > response headers : %@", [responseHeaders description]);
    
    // fetch avaliable data
    NSString *sessionCookie = [self getCookieFromHeader:responseHeaders];
    NSDictionary *userInfo = [self getUserInfoFromXml:responseBody];
    
    // store session data
    SharedAppDelegate.sessionCookie = sessionCookie;
    NSLog(@"ServerSide.m > doLoginNew > session cookie : %@", SharedAppDelegate.sessionCookie);
    if ([userInfo objectForKey:@"userId"] && [userInfo objectForKey:@"email"] && [userInfo objectForKey:@"userName"]) {
        SharedAppDelegate.sessionUserId = userInfo[@"userId"];
        SharedAppDelegate.sessionUserName = userInfo[@"userName"];
        SharedAppDelegate.sessionUserEmail = userInfo[@"email"];
        SharedAppDelegate.sessionPasswd = j_password;
    }
    
    return [NSString stringWithFormat:@"%ld", (long)responseCode];
}


+ (NSString *)makeSAMLRequestBase64Encoded
{
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    int timeStampInt = (int)[NSNumber numberWithDouble:timeStamp];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'hh:mm:ss'.000Z'"];
    NSString *timeString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSString *samlrequest = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?> <saml2p:AuthnRequest xmlns:saml2p=\"urn:oasis:names:tc:SAML:2.0:protocol\" AssertionConsumerServiceURL=\"\" Destination=\"https://sso.ebs.co.kr/idp/profile/SAML2/POST-GET/SSO\" ID=\"ebs.bandi.app-%d\" IssueInstant=\"%@\" Version=\"2.0\"><saml2:Issuer xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\">ebs.bandi.app</saml2:Issuer></saml2p:AuthnRequest>", timeStampInt, timeString];
    
    NSData *dat = [samlrequest dataUsingEncoding:NSASCIIStringEncoding];
//    NSLog(@"samlrequest : %@", samlrequest);
    
    return [dat base64EncodedStringWithSeparateLines:NO];
}


+ (BOOL)autoLogin
{
    if (SharedAppDelegate.sessionUserName && SharedAppDelegate.sessionUserId) {
        return YES;
    }
//    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"] isEqualToString:@"000"])
//        return YES;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"auto login %d", [userDefaults boolForKey:@"AUTO_LOGIN"]);
    if ([userDefaults boolForKey:@"AUTO_LOGIN"]) {
        NSString *userId = [userDefaults getUserId];
        NSString *passwd = [userDefaults getPassword];
//        NSString *resultCode = [ServerSide doLogin:userId withPasswd:passwd];
//        
//        NSLog(@"saved id : %@", [userDefaults getUserId]);
//        NSLog(@"saved pw : %@", [userDefaults getPassword]);
//        NSLog(@"result code : %@", resultCode);
//        if ([resultCode isEqualToString:@"100"]) {
//            return YES;
//        }
        NSString *responseCode = [ServerSide doLoginNew:userId withPassword:passwd];
        if ([responseCode isEqualToString:@"200"]
            && SharedAppDelegate.sessionUserId != nil
            && [@"" isEqualToString:SharedAppDelegate.sessionUserId])
        {
            return YES;
        }
    }
    
    return NO;
}


#pragma mark - 접속 로그
+ (void)saveConnectLog
{
    [NSThread detachNewThreadSelector:@selector(connectLog) toTarget:self withObject:nil];
}

+ (void)connectLog
{
    NSString *logSno=nil, *mobId=nil, *startDtm=nil, *appOsVer=nil;
    NSError *error;
    
    mobId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    appOsVer = [[UIDevice currentDevice] systemVersion];
    startDtm = [Util getTimeStamp];
    
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiUserLogInsert?mobId=%@&startDtm=%@&endDtm=&appId=bandi&appOsCd=ios&appOsVer=%@", mobId, startDtm, appOsVer];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSString *xmlStr = [[NSString alloc] initWithContentsOfURL:url
                                                      encoding:NSUTF8StringEncoding
                                                         error:&error];
    TBXMLElement *elemRoot=nil, *elemRescd=nil, *elemData=nil, *elemLogSno=nil;
    TBXML *tbxml = [TBXML newTBXMLWithXMLString:xmlStr error:&error];
    
    elemRoot = tbxml.rootXMLElement;
    if (elemRoot) {
        elemRescd = [TBXML childElementNamed:@"rescd" parentElement:elemRoot];
        if (elemRescd) {
            NSString *rescd = [TBXML textForElement:elemRescd];
            if ([rescd isEqualToString:@"0000"]) {
                elemData = [TBXML childElementNamed:@"data" parentElement:elemRoot];
                if (elemData) {
                    elemLogSno = [TBXML childElementNamed:@"logSno" parentElement:elemData];
                    if (elemLogSno) {
                        logSno = [TBXML textForElement:elemLogSno];
                    }
                }
            }
        }
    }
    
    SharedAppDelegate.logSno = logSno;
    NSLog(@"logSno -> %@", logSno);
}

# pragma mark - 접속 종료 로그
+ (void)saveDisconnectLog
{
    [NSThread detachNewThreadSelector:@selector(disconnectLog) toTarget:self withObject:nil];
}

+ (void)disconnectLog
{
    NSString *logSno = SharedAppDelegate.logSno;
    
    if (logSno) {
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiUserLogUpdate?logSno=%@&endDtm=%@", logSno, [Util getTimeStamp]];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSError *error;
        NSString *xmlStr = [[NSString alloc] initWithContentsOfURL:url
                                                       encoding:NSUTF8StringEncoding
                                                          error:&error];
        NSLog(@"disconnect result : %@", xmlStr);
        SharedAppDelegate.logSno = nil;
    }
}

# pragma mark - 사용자 이벤트 로그
+ (void)saveEventLog:(NSString *)action
{
    [NSThread detachNewThreadSelector:@selector(pageLog:) toTarget:self withObject:action];
}

+ (void)eventLog:(NSString *)action
{

    NSString *logSno = SharedAppDelegate.logSno;
    
    if (logSno) {
        NSString *mobId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        NSString *progcd = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"]];

        
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiVisitLogInsert?mobId=%@&pageNm=%@&courseId=%@&userLogSno=%@&appDsCd=03", mobId, action, progcd, logSno ];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSError *error;
        NSString *xmlStr = [[NSString alloc] initWithContentsOfURL:url
                                                       encoding:NSUTF8StringEncoding
                                                          error:&error];
        NSLog(@"eventLog -> %@", xmlStr);
        if(!xmlStr)
        {
            NSLog(@"aa");
        }
    }
}

+ (void)bookHitLog
{
    
    NSString *progcd = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"]];
    
    
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiBookHit?programId=%@&appDsCd=03&fileType=json", progcd ];
    NSLog(@"urlStr = %@",urlStr);
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    
    [NSURLConnection connectionWithRequest:request delegate:nil ];

    
//    NSString *logSno = SharedAppDelegate.logSno;
    
//    if (logSno) {
//        NSString *mobId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
//        NSString *progcd = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"]];
//
//
//        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiBookHit?programId=%@&appDsCd=03&fileType=json", progcd ];
//    NSLog(@"urlStr = %@",urlStr);
//        NSURL *url = [NSURL URLWithString:urlStr];
//        NSError *error;
//        NSString *xmlStr = [[NSString alloc] initWithContentsOfURL:url
//                                                          encoding:NSUTF8StringEncoding
//                                                             error:&error];
//        NSLog(@"bookHitLog eventLog -> %@", xmlStr);
//    }


}


#pragma mark - 서버 리소스 받아오기 (Binary)
+ (NSData *)getResource:(NSURL *)url localSavingTime:(NSInteger)seconds
{
    NSError *error;
    NSMutableArray *pathArr = [NSMutableArray arrayWithArray:[url pathComponents]];
    pathArr[0] = @"Bandi";
    NSString *filePath = [Util getTempFilePath:[pathArr componentsJoinedByString:@"_"]];
    //NSLog(@"resource filePath - %@, %@", filePath, [url pathComponents]);
    
    // 파일 존재 확인
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filePath]) {
        NSDictionary *fileAttr = [fileManager attributesOfItemAtPath:filePath error:&error];
        // 파일 생성, 유효 시간 비교
        NSDate *validTime = [NSDate dateWithTimeIntervalSinceNow:-1*seconds]; // 파일 유지시간
        if ([validTime compare:fileAttr[NSFileCreationDate]] == NSOrderedAscending) {
            NSLog(@"use saved file - %@", filePath);
            return [NSData dataWithContentsOfFile:filePath];
        }
    }
    
    NSData *data = [NSData dataWithContentsOfURL:url];
    [data writeToFile:filePath atomically:YES];
    return data;
}


+ (void)pageLog:(NSString *)strPageName
{
     NSString *mobId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    NSString *progcd = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ProgramId"]];
    
    
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiVisitLogInsert?mobId=%@ID&pageNm=%@&courseId=%@ID&userLogSno=%@", mobId,strPageName,progcd,SharedAppDelegate.logSno ];
    NSLog(@"pageLog = %@",urlStr);
    NSURL *url = [NSURL URLWithString:urlStr];
    NSError *error;
    NSString *xmlStr = [[NSString alloc] initWithContentsOfURL:url
                                                      encoding:NSUTF8StringEncoding
                                                         error:&error];
    NSLog(@"eventLog -> %@", xmlStr);

    
}


#pragma mark - 불량회원 체크 로직 추가 (2016.04.07)
/**
 * retrieve bad member info (json format) <br>
 * return NSDictionary object with keys <br>
 * {result, until, userId, sndDsCd, snsUserId, badMmbExp, update}
 */
+ (NSDictionary *)getBadMemberInfo:(NSString *)userId snsDsCd:(NSString *)snsDsCd snsUserId:(NSString *)snsUserId
{
    NSDictionary *dic = SharedAppDelegate.badMember;
    NSLog(@" dic : %@, %d, %d", dic, dic==NULL, dic==nil);
    if (dic != nil
        && [userId isEqualToString:dic[@"userId"]]
        && [snsDsCd isEqualToString:dic[@"snsDsCd"]]
        && [snsUserId isEqualToString:dic[@"snsUserId"]])
    {
        NSDateFormatter * dateFormater = [[NSDateFormatter alloc] init];
        [dateFormater setDateFormat:@"yyyyMMddHHmmss"];
        [dateFormater setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"KST"]];
//        NSLog(@"timezone abbr : %@", [NSTimeZone abbreviationDictionary]); // KST = "Asia/Seoul";
//        NSLog(@"timezone name : %@", [NSTimeZone knownTimeZoneNames]); // "Asia/Seoul",
        NSDate *update = [dateFormater dateFromString:dic[@"update"]];
        NSLog(@"cached update : %@, %@", dic[@"update"], update);
        
        update = [update dateByAddingTimeInterval:60]; //60 sec
        NSDate *now = [NSDate date];
        
        NSLog(@"1. now: %@, update: %@", [dateFormater stringFromDate:now], [dateFormater stringFromDate:update]); // now, update string is based on gmt 0
        if ([now compare:update] == NSOrderedAscending)
        {
            NSLog(@"2. return cached data" );
            return dic;
        }
    }
    
    NSString *badMemberUrlJson = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/badMmbInfo?fileType=json&userId=%@&snsDsCd=%@&snsUserId=%@", userId, snsDsCd, snsUserId];
    
    NSString *jsonStr = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:badMemberUrlJson] encoding:NSUTF8StringEncoding error:nil];
    NSMutableDictionary *badMemberDic = [Util convertJsonToDic:jsonStr][@"data"];
    
    badMemberDic[@"update"] = [Util getTimeStamp];
    NSLog(@"sy>> update : %@, %@", badMemberDic[@"update"], jsonStr );
    
    SharedAppDelegate.badMember = badMemberDic;
    
    return badMemberDic;
}

/**
 * check login user is a bad member.
 */
+ (BOOL)isBadMember
{
    BOOL result = NO;
    NSDictionary *dic = nil;
    NSString *snsDsCd = [[NSUserDefaults standardUserDefaults] objectForKey:@"SNSLogin"];
    NSString *snsUserId = [[NSUserDefaults standardUserDefaults] objectForKey:@"SNSId"];
    if (SharedAppDelegate.sessionUserId)
    {
        NSLog(@"sy serverside>> ebs id : %@", SharedAppDelegate.sessionUserId);
        dic = [self getBadMemberInfo:SharedAppDelegate.sessionUserId snsDsCd:@"" snsUserId:@""];
    }
    else if ([@"001" isEqualToString:snsDsCd] || [@"002" isEqualToString:snsDsCd])
    {
        NSLog(@"sy serverside>> sns cd : %@", snsDsCd);
        NSLog(@"sy serverside>> sns id : %@", snsUserId);
        dic = [self getBadMemberInfo:@"" snsDsCd:snsDsCd snsUserId:snsUserId];
    }
    
    NSLog(@"sy>> dic : %@", dic);
    if ([@"Y" isEqualToString:dic[@"result"]]) {
        result = YES;
    }
    
    return result;
}

@end
