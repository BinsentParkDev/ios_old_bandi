//
//  AdWebView.m
//  Bandi2
//
//  Created by admin on 2015. 7. 13..
//  Copyright (c) 2015년 EBS. All rights reserved.
//

#import "AdWebView.h"

@implementation AdWebView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return FALSE;
    }
    
    return YES;
}


@end
