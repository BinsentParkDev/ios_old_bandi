//
//  WebViewController.m
//  Bandi2
//
//  Created by admin on 2014. 1. 22..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "programContentView.h"

@interface programContentView ()

@end

@implementation programContentView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        npg = 1;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    _webView.delegate = self;
    //NSLog(@"_webView %@", _webView);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)PreView:(id)sender
{
    if(npg == 1)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"guideEnd" object:nil];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"INIT_GUIDE"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self dismissViewControllerAnimated:NO completion:nil];

        return;
    }
    npg --;
    [self imageset:npg];
    
}
-(IBAction)Next:(id)sender
{
    if(npg == 4)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"guideEnd" object:nil];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"INIT_GUIDE"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self dismissViewControllerAnimated:NO completion:nil];
        
        return;
    }
    npg ++;
    [self imageset:npg];

}
-(IBAction)close:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"guideEnd" object:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)imageset:(int)nn
{
    if(nn == 1)
    {
        _iv.image = [UIImage imageNamed:@"onair_tutorial_01.png"];
    }
    else if(nn == 2)
    {
        _iv.image = [UIImage imageNamed:@"onair_tutorial_02.png"];
    }
    else if(nn == 3)
    {
        _iv.image = [UIImage imageNamed:@"onair_tutorial_03.png"];
    }
    else if(nn == 4)
    {
        _iv.image = [UIImage imageNamed:@"onair_tutorial_04.png"];
    }
}

@end
