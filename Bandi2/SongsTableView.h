//
//  SongsTableView.h
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SongsTableView : UIWebView
{
    
}
- (void)onUnload;


@property (strong, nonatomic) IBOutlet SongsTableView *wrapperView;
@property (strong, nonatomic) IBOutlet UILabel *programLabel;
@property (strong, nonatomic) IBOutlet UIWebView *songsWebView;
@end
