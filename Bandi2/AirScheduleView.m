//
//  AirScheduleView.m
//  Bandi2
//
//  Created by admin on 2013. 12. 26..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "AirScheduleView.h"
#import "WebViewController.h"
#import "ServerSide.h"

#import "GA.h"

#define TAG @"AirSchedule"
#define RgbColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@implementation AirScheduleView {
    NSCalendar *calendar;
    NSDate *thisDate; // 사용자의 선택에 따라 바뀌는 날짜를 저장.
    NSArray *weekday;
    NSArray *weekdayBtn;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        
        [[NSBundle mainBundle] loadNibNamed:@"AirScheduleView" owner:self options:nil];
        
        [_tabView setFrame:CGRectMake(0, 0, 320, _tabView.frame.size.height)];
        [_wrapperView addSubview:_tabView];
        
        [_wrapperView addSubview:_dateView];
        float dateViewHeight = 37.0;
        [_dateView setFrame:CGRectMake(0, _tabView.frame.size.height, 320.0f, dateViewHeight)];
        
        [_wrapperView addSubview:_weekDateView];
        float weekDateViewHeight = 37.0;
        [_weekDateView setFrame:CGRectMake(0, _dateView.frame.origin.y+dateViewHeight, 320.0f, weekDateViewHeight)];
        // 백그라운드 이미지 설정
        UIGraphicsBeginImageContext(_weekDateView.frame.size);
        [[UIImage imageNamed:@"bg_dayofweek.png"] drawInRect:_weekDateView.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        _weekDateView.backgroundColor = [UIColor colorWithPatternImage:image];
        
        
        [_wrapperView addSubview:_scheduleWebView];
        float originY = (_weekDateView.frame.origin.y + weekDateViewHeight);
        [_scheduleWebView setFrame:CGRectMake(0, originY, 320.0f, (bodyHeight-originY))];
        
        
        _scheduleWebView.delegate = self;
        
        
        [self addSubview:_wrapperView];

      
        NSString *channelString;
        if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
        else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
            
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else{
            if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
            {
                channelString = [NSString stringWithFormat:@"IRADIO"];
            }
            else
            {
                channelString = [NSString stringWithFormat:@"RADIO"];
            }
        }
        
        if([channelString isEqualToString:@"RADIO"])
        {
            [self.btMenuReadRadio setBackgroundColor:RgbColor(63,56,51)];
            [self.btMenuEngRadio setBackgroundColor:RgbColor(158,145,136)];
            self.btMenuReadRadio.selected = YES;
            self.btMenuEngRadio.selected = NO;
            nSelectedMenu = 1;
        }
        else
        {
            [self.btMenuReadRadio setBackgroundColor:RgbColor(158,145,136)];
            [self.btMenuEngRadio setBackgroundColor:RgbColor(63,56,51)];
            self.btMenuReadRadio.selected = NO;
            self.btMenuEngRadio.selected = YES;
            nSelectedMenu = 2;
        }
        [self todaySchedule:nil];
        
        
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(playIPradio:)
//                                                     name:@"selectIradio"
//                                                   object:nil];
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(playFmPradio:)
//                                                     name:@"selectFmradio"
//                                                   object:nil];
        
        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(playIPradio:)
//                                                     name:@"selectIradioFromOnAir"
//                                                   object:nil];
//        
////        [[NSNotificationCenter defaultCenter] addObserver:self
////                                                 selector:@selector(playIPradio:)
////                                                     name:@"selectIradioFromBoard"
////                                                   object:nil];
////        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(playFmPradio:)
//                                                     name:@"selectFmradioFromOnAir"
//                                                   object:nil];
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(playFmPradio:)
//                                                     name:@"selectFmradioFromBoard"
//                                                   object:nil];
        
    }
    
//    [ServerSide saveEventLog:TAG];
    
    //** Google Analytics **
    if (nSelectedMenu == 1) {
        [GA sendScreenView:@"AirSchedule" withChannel:GA_FM];
    } else if (nSelectedMenu == 2) {
        [GA sendScreenView:@"AirSchedule" withChannel:GA_IRADIO];
    } else {
        [GA sendScreenView:@"AirSchedule" withChannel:GA_UNKNOWN];
    }
    
    return self;
}

- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}

// 편성표의 링크를 앱의 WebView 에서 열기 위해..
// .h 에 프로토콜 <UIWebViewDelegate> 추가 후,
// init 에  WebView.delegate = self 정의하고
// 이 메소드 작성.
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        NSLog(@"click : %@", [[request URL] absoluteString]);
        //[[UIApplication sharedApplication] openURL:[request URL]]; // open with browser
        WebViewController *vc = [[WebViewController alloc] init];
        [self.window.rootViewController presentViewController:vc animated:YES completion:^{
            [(UIWebView *)[vc.view.subviews lastObject] loadRequest:request];
        }];
        return NO;
    }
    return YES;
}

#pragma mark 편성표 URI 반환
/**
 * 편성표 URI 반환
 */
- (NSString *)timeTableUri:(NSString *)channel year:(NSInteger)yyyy month:(NSInteger)mm day:(NSInteger)dd
{
    NSString * uri = [NSString stringWithFormat:@"http://m.ebs.co.kr/appChannel?deviceOs=ios&channel=%@&appId=bandinew&onairDate=%04ld%02ld%02ld", channel, (long)yyyy, (long)mm, (long)dd];
    
    NSString *appVer = (NSString *)[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *appSerVer = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"appSerVer"];
    NSLog(@"ver = local : %@ (%d), svr : %@ (%d)", appVer, [appVer intValue], appSerVer, [appSerVer intValue]);
    
    if ([appVer intValue] <= [appSerVer intValue] ) // 서버에 설정된 버전보다 값이 작거나 같으면 홈페이지 링크 보임
    {
        uri = [NSString stringWithFormat:@"http://m.ebs.co.kr/appChannel?channel=%@&appId=bandinew&onairDate=%04ld%02ld%02ld", channel, (long)yyyy, (long)mm, (long)dd];
    }
    
    
    return uri;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)todaySchedule:(id)sender {
    calendar = [NSCalendar autoupdatingCurrentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    
	NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:[NSDate date]];
    

    thisDate = [NSDate date];
    
    NSDateComponents* nowHour = [calendar components:NSHourCalendarUnit fromDate:thisDate];

    if([nowHour hour] < 2)
    {
        
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setDay:-1];
        
        NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];

        thisDate = yesterday;

        currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    }

    int wIndex = [currentTime weekday]; // 1-7 = 일-토
    weekday = @[@"",@"일",@"월",@"화",@"수",@"목",@"금",@"토"];
    weekdayBtn = @[@"", _sundayBtn, _mondayBtn, _tuesdayBtn, _wednesdayBtn, _thursdayBtn, _fridayBtn, _saturdayBtn];
    _dateLabel.text = [NSString stringWithFormat:@"%d.%02d.%02d (%@)", [currentTime year], [currentTime month], [currentTime day], weekday[wIndex]];
    

    
    [self setWeekdayUnselectedState];
    [weekdayBtn[wIndex] setSelected:YES];
    
    NSString *channelString;
    if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
    {
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else
    {
        channelString = [NSString stringWithFormat:@"RADIO"];
    }


    

    NSString *strUrl = [self timeTableUri:channelString year:[currentTime year] month:[currentTime month] day:[currentTime day]];

    NSURL *familyUrl = [[NSURL alloc]initWithString:strUrl];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:familyUrl];
    [_scheduleWebView loadRequest:urlRequest];
}


- (IBAction)prevDate:(id)sender {
    thisDate = [thisDate initWithTimeInterval:-60*60*24 sinceDate:thisDate];
    NSLog(@"%@", thisDate);
    NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    int wIndex = [currentTime weekday]; // 1-7 = 일-토
    _dateLabel.text = [NSString stringWithFormat:@"%d.%02d.%02d (%@)", [currentTime year], [currentTime month], [currentTime day], weekday[wIndex]];
    
    [self setWeekdayUnselectedState];
    [weekdayBtn[wIndex] setSelected:YES];

    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }


    NSString *scheduleUrl = [self timeTableUri:channelString year:[currentTime year] month:[currentTime month] day:[currentTime day]];
    
    NSURL *familyUrl = [[NSURL alloc]initWithString:scheduleUrl];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:familyUrl];
    [_scheduleWebView loadRequest:urlRequest];

}

- (IBAction)nextDate:(id)sender {
    thisDate = [thisDate initWithTimeInterval:60*60*24 sinceDate:thisDate];
    NSLog(@"%@", thisDate);
    NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    int wIndex = [currentTime weekday]; // 1-7 = 일-토
    _dateLabel.text = [NSString stringWithFormat:@"%d.%02d.%02d (%@)", [currentTime year], [currentTime month], [currentTime day], weekday[wIndex]];
    
    [self setWeekdayUnselectedState];
    [weekdayBtn[wIndex] setSelected:YES];
    
    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }


    NSString *scheduleUrl = [self timeTableUri:channelString year:[currentTime year] month:[currentTime month] day:[currentTime day]];
    NSURL *familyUrl = [[NSURL alloc]initWithString:scheduleUrl];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:familyUrl];
    [_scheduleWebView loadRequest:urlRequest];

}

- (IBAction)sunday:(id)sender {
    [self setWeekdayUnselectedState];
    UIButton *btn = (UIButton *)sender;
    [btn setSelected:YES];
    
    // 현재 날짜에 대한 정보
    NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    int wIndex = [currentTime weekday]; // 1-7 = 일-토
    NSLog(@"%d",(1 - wIndex));
    
    // 선택된 요일에 대한 날짜
    thisDate = [thisDate initWithTimeInterval:(1 - wIndex)*60*60*24 sinceDate:thisDate];
    currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    wIndex = [currentTime weekday];
    _dateLabel.text = [NSString stringWithFormat:@"%d.%02d.%02d (%@)", [currentTime year], [currentTime month], [currentTime day], weekday[wIndex]];
    
    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }


    NSString *scheduleUrl = [self timeTableUri:channelString year:[currentTime year] month:[currentTime month] day:[currentTime day]];
    NSURL *familyUrl = [[NSURL alloc]initWithString:scheduleUrl];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:familyUrl];
    [_scheduleWebView loadRequest:urlRequest];
}

- (IBAction)monday:(id)sender {
    [self setWeekdayUnselectedState];
    UIButton *btn = (UIButton *)sender;
    [btn setSelected:YES];
    
    // 현재 날짜에 대한 정보
    NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    int wIndex = [currentTime weekday]; // 1-7 = 일-토
    
    // 선택된 요일에 대한 날짜
    thisDate = [thisDate initWithTimeInterval:(2 - wIndex)*60*60*24 sinceDate:thisDate];
    currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    wIndex = [currentTime weekday];
    _dateLabel.text = [NSString stringWithFormat:@"%d.%02d.%02d (%@)", [currentTime year], [currentTime month], [currentTime day], weekday[wIndex]];
    
    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }


    
    NSString *scheduleUrl = [self timeTableUri:channelString year:[currentTime year] month:[currentTime month] day:[currentTime day]];
    NSURL *familyUrl = [[NSURL alloc]initWithString:scheduleUrl];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:familyUrl];
    [_scheduleWebView loadRequest:urlRequest];
}

- (IBAction)tuesday:(id)sender {
    [self setWeekdayUnselectedState];
    UIButton *btn = (UIButton *)sender;
    [btn setSelected:YES];
    
    // 현재 날짜에 대한 정보
    NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    int wIndex = [currentTime weekday]; // 1-7 = 일-토
    
    // 선택된 요일에 대한 날짜
    thisDate = [thisDate initWithTimeInterval:(3 - wIndex)*60*60*24 sinceDate:thisDate];
    currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    wIndex = [currentTime weekday];
    _dateLabel.text = [NSString stringWithFormat:@"%d.%02d.%02d (%@)", [currentTime year], [currentTime month], [currentTime day], weekday[wIndex]];
    
    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }


    
    NSString *scheduleUrl = [self timeTableUri:channelString year:[currentTime year] month:[currentTime month] day:[currentTime day]];
    NSURL *familyUrl = [[NSURL alloc]initWithString:scheduleUrl];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:familyUrl];
    [_scheduleWebView loadRequest:urlRequest];
}

- (IBAction)wednesday:(id)sender {
    [self setWeekdayUnselectedState];
    UIButton *btn = (UIButton *)sender;
    [btn setSelected:YES];
    
    // 현재 날짜에 대한 정보
    NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    int wIndex = [currentTime weekday]; // 1-7 = 일-토
    
    // 선택된 요일에 대한 날짜
    thisDate = [thisDate initWithTimeInterval:(4 - wIndex)*60*60*24 sinceDate:thisDate];
    currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    wIndex = [currentTime weekday];
    _dateLabel.text = [NSString stringWithFormat:@"%d.%02d.%02d (%@)", [currentTime year], [currentTime month], [currentTime day], weekday[wIndex]];
    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }


    
    NSString *scheduleUrl = [self timeTableUri:channelString year:[currentTime year] month:[currentTime month] day:[currentTime day]];
    NSURL *familyUrl = [[NSURL alloc]initWithString:scheduleUrl];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:familyUrl];
    [_scheduleWebView loadRequest:urlRequest];
}

- (IBAction)thursday:(id)sender {
    [self setWeekdayUnselectedState];
    UIButton *btn = (UIButton *)sender;
    [btn setSelected:YES];
    
    // 현재 날짜에 대한 정보
    NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    int wIndex = (int)[currentTime weekday]; // 1-7 = 일-토
    
    // 선택된 요일에 대한 날짜
    thisDate = [thisDate initWithTimeInterval:(5 - wIndex)*60*60*24 sinceDate:thisDate];
    currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    wIndex = (int)[currentTime weekday];
    _dateLabel.text = [NSString stringWithFormat:@"%d.%02d.%02d (%@)", (int)[currentTime year], (int)[currentTime month], (int)[currentTime day], weekday[wIndex]];
    
    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }


    
    NSString *scheduleUrl = [self timeTableUri:channelString year:[currentTime year] month:[currentTime month] day:[currentTime day]];
    NSURL *familyUrl = [[NSURL alloc]initWithString:scheduleUrl];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:familyUrl];
    [_scheduleWebView loadRequest:urlRequest];
}

- (IBAction)friday:(id)sender {
    [self setWeekdayUnselectedState];
    UIButton *btn = (UIButton *)sender;
    [btn setSelected:YES];
    
    // 현재 날짜에 대한 정보
    NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    int wIndex = [currentTime weekday]; // 1-7 = 일-토
    
    // 선택된 요일에 대한 날짜
    thisDate = [thisDate initWithTimeInterval:(6 - wIndex)*60*60*24 sinceDate:thisDate];
    currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    wIndex = [currentTime weekday];
    _dateLabel.text = [NSString stringWithFormat:@"%d.%02d.%02d (%@)", [currentTime year], [currentTime month], [currentTime day], weekday[wIndex]];
    
    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
    
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }

    
    NSString *scheduleUrl = [self timeTableUri:channelString year:[currentTime year] month:[currentTime month] day:[currentTime day]];
    NSURL *familyUrl = [[NSURL alloc]initWithString:scheduleUrl];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:familyUrl];
    [_scheduleWebView loadRequest:urlRequest];
}

- (IBAction)saturday:(id)sender {
    [self setWeekdayUnselectedState];
    UIButton *btn = (UIButton *)sender;
    [btn setSelected:YES];
    
    // 현재 날짜에 대한 정보
    NSDateComponents *currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    int wIndex = [currentTime weekday]; // 1-7 = 일-토
    
    // 선택된 요일에 대한 날짜
    thisDate = [thisDate initWithTimeInterval:(7 - wIndex)*60*60*24 sinceDate:thisDate];
    currentTime = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekdayCalendarUnit) fromDate:thisDate];
    wIndex = [currentTime weekday];
    _dateLabel.text = [NSString stringWithFormat:@"%d.%02d.%02d (%@)", [currentTime year], [currentTime month], [currentTime day], weekday[wIndex]];
    
    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }


    
    NSString *scheduleUrl = [self timeTableUri:channelString year:[currentTime year] month:[currentTime month] day:[currentTime day]];
    NSURL *familyUrl = [[NSURL alloc]initWithString:scheduleUrl];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:familyUrl];
    [_scheduleWebView loadRequest:urlRequest];
}

- (void)setWeekdayUnselectedState {
    [_sundayBtn setSelected:NO];
    [_mondayBtn setSelected:NO];
    [_tuesdayBtn setSelected:NO];
    [_wednesdayBtn setSelected:NO];
    [_thursdayBtn setSelected:NO];
    [_fridayBtn setSelected:NO];
    [_saturdayBtn setSelected:NO];
}


-(void)selectMenuIndex:(int)nIndex //1 책읽어주는 라디오 // 2외국어 라디오
{
    if (nSelectedMenu == nIndex)
        return;
    if(nIndex == 1)
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(63,56,51)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(158,145,136)];
        self.btMenuReadRadio.selected = YES;
        self.btMenuEngRadio.selected = NO;
        
    }
    else if(nIndex == 2)
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(158,145,136)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(63,56,51)];
        self.btMenuReadRadio.selected = NO;
        self.btMenuEngRadio.selected = YES;

        
    }

    nSelectedMenu = nIndex;
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",nSelectedMenu] forKey:@"ChannerNum"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self todaySchedule:nil];
//    [self getBroadInfo];
}

- (void)playIPradio:(NSNotification *)notification
{
    
//    [self selectMenuIndex:2];
    nSelectedMenu = 2;

    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }

    if([channelString isEqualToString:@"RADIO"])
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(63,56,51)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(158,145,136)];
        self.btMenuReadRadio.selected = YES;
        self.btMenuEngRadio.selected = NO;
    }
    else
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(158,145,136)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(63,56,51)];
        self.btMenuReadRadio.selected = NO;
        self.btMenuEngRadio.selected = YES;
    }
}

- (void)playFmPradio:(NSNotification *)notification
{
//    [self selectMenuIndex:1];
    nSelectedMenu = 1;
    NSString *channelString;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"RADIO"]){
        channelString = [NSString stringWithFormat:@"RADIO"];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"channelCode"] isEqualToString:@"IRADIO"]){
        
        channelString = [NSString stringWithFormat:@"IRADIO"];
    }
    else{
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChannerNum"] isEqualToString:@"2"])
        {
            channelString = [NSString stringWithFormat:@"IRADIO"];
        }
        else
        {
            channelString = [NSString stringWithFormat:@"RADIO"];
        }
    }

    if([channelString isEqualToString:@"RADIO"])
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(63,56,51)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(158,145,136)];
        self.btMenuReadRadio.selected = YES;
        self.btMenuEngRadio.selected = NO;
    }
    else
    {
        [self.btMenuReadRadio setBackgroundColor:RgbColor(158,145,136)];
        [self.btMenuEngRadio setBackgroundColor:RgbColor(63,56,51)];
        self.btMenuReadRadio.selected = NO;
        self.btMenuEngRadio.selected = YES;
    }
}

- (IBAction)tabPlayFmRadio:(id)sender
{
    [self selectMenuIndex:1];
 
    [[NSNotificationCenter defaultCenter] postNotificationName:@"selectFmradioFromSch" object:nil];

}
- (IBAction)tabPlayEndRadio:(id)sender
{
    [self selectMenuIndex:2];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"selectIradioFromSch" object:nil];

}



@end
