//
//  BandiBoardView.h
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "checkListCell.h"

@interface checkListView : UIView<onCheckListCellDelegate,UIAlertViewDelegate>
{
    NSMutableArray *boardList;
}
- (void)onUnload;
@property (strong, nonatomic) IBOutlet UITableView *tbList;
@property (strong, nonatomic) IBOutlet checkListView *wrapperView;
@property (strong, nonatomic) IBOutlet UILabel *programLabel;
//@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) IBOutlet UIView * notiView;
@property (strong, nonatomic) IBOutlet UIView * inputView;
@property (strong, nonatomic) IBOutlet UIView * tabView;

@property (strong, nonatomic) IBOutlet UILabel * notiTitle;
@property (strong, nonatomic) IBOutlet UILabel * titleText;
@property (strong, nonatomic) IBOutlet UILabel * InfoText;

//- (IBAction)writeBulletin:(id)sender;
- (IBAction)openNoti:(id)sender;
@end
