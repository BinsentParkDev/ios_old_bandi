//
//  WebViewController.m
//  Bandi2
//
//  Created by admin on 2014. 1. 22..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "WebViewController.h"
#import "Dialogs.h"
#import "Util.h"

@interface WebViewController ()

@end

@implementation WebViewController
{
    UIActivityIndicatorView *loadingIndicator;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _webView.delegate = self;
    //NSLog(@"_webView %@", _webView);
    
    // to show loading image
    loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [loadingIndicator setCenter:CGPointMake(_webView.frame.size.width/2, _webView.frame.size.height/2)];
    [_webView addSubview:loadingIndicator];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)moveBackward:(id)sender {
    [_webView stringByEvaluatingJavaScriptFromString:@"window.history.back();"];
}

- (IBAction)moveForward:(id)sender {
    [_webView stringByEvaluatingJavaScriptFromString:@"window.history.forward();"];
}

- (IBAction)reloadPage:(id)sender {
    [_webView stringByEvaluatingJavaScriptFromString:@"window.location.reload();"];
    
}

- (IBAction)closeView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [loadingIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
//    if (webView.isLoading) return;
    [loadingIndicator stopAnimating];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"navigationType -> %ld", (long) navigationType);
    
    // window.open javascript 를 별도의 브라우져에서 띄우기 위해서 url 변조
    [webView stringByEvaluatingJavaScriptFromString:@"window.open = function(url, name, features) {window.location.href='bandi://window.open?url='+url;};"];
    
    NSString *urlStr = [[request URL] absoluteString];
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        if ([urlStr isEqualToString:@"bandi://banner.close"]) {
            [self closeView:nil];
            return NO;
        }
        else if ([urlStr isEqualToString:[NSString stringWithFormat:@"%@%@", webView.request.mainDocumentURL, @"#none"]]) {
//            NSLog(@"check1 : %@, %@", webView.request.mainDocumentURL, urlStr);
            return NO;
        }
    }
    else if (navigationType == UIWebViewNavigationTypeOther)
    {
        if ([urlStr hasPrefix:@"bandi://window.open?url="]) {
            NSURL *url = [NSURL URLWithString:urlStr];
            NSString *openUrl = [[url query] substringFromIndex:4]; // remove 'url='
            NSLog(@"check1 window.open : %@", openUrl );
            if ([openUrl hasPrefix:@"/"]) {
                NSURL *curUrl = webView.request.mainDocumentURL;
                openUrl = [NSString stringWithFormat:@"%@://%@%@", [curUrl scheme], [curUrl host], openUrl ];
            }
            if ([openUrl containsString:@"/mypage/pay/"]) { // 구매 페이지
                [Dialogs alertView:@"반디 앱에서는 AOD 구매가 진행되지 않습니다. 모바일 웹사이트를 이용해주세요.\n\n이미 구매한 콘텐츠라면 로그인 후 사용할 수 있습니다."];
//                NSLog(@"current url - %@", [Util encodeURL:[webView.request.mainDocumentURL absoluteString]]);
//                NSString *loginUrl = [NSString stringWithFormat:@"http://m.ebs.co.kr/login?j_returnurl=%@",[Util encodeURL:[webView.request.mainDocumentURL absoluteString]]];
//                [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:loginUrl]]]; // open in webview
            }
            else
            {
                [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:openUrl]]]; // open in webview
                //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:openUrl]]; // open in browser
            }
            
            return NO;
        }
    }
    

    return YES;
}

@end
