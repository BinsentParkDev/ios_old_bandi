//
//  ebsPlayer.m
//  EBSTVApp
//
//  Created by Hyeoung seok Yoon on 2013. 12. 11..
//  Copyright (c) 2013년 Hyeoung seok Yoon. All rights reserved.
//

#import "ebsPlayer.h"
#import "GA.h"

@implementation ebsPlayer

@synthesize uvController;
@synthesize playerDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void) dealloc
{
    playerDelegate = nil;
    if(seekTimer)
    {
        [seekTimer invalidate];
        seekTimer = nil;
        
    }
    if(bufferTimer)
    {
        [bufferTimer invalidate];
        bufferTimer = nil;
    }
    [uvController release];

    [super dealloc];
}

-(void)initPlayer:(CGRect)frame_ url:(NSString *)str
{
    bPause = YES;
    bPlaying = NO;
    bNoBuffer = NO;
    firstVideoItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:str]];
    queuePlayer = [AVQueuePlayer queuePlayerWithItems:[NSArray arrayWithObjects:firstVideoItem,nil]];
    
    layer = [AVPlayerLayer playerLayerWithPlayer:queuePlayer];
    queuePlayer.actionAtItemEnd = AVPlayerItemStatusReadyToPlay;
    layer.frame = CGRectMake(0, 0, frame_.size.width, frame_.size.height);
    
    [firstVideoItem addObserver:self forKeyPath:@"status" options:0 context:nil];
    [firstVideoItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:0 context:nil];
    [self.layer addSublayer:layer];
    
    //** Google Analytics ** iradio는 보이는 라디오 서비스 대상 아님.
    [GA sendScreenView:@"VisibleRadio" withChannel:GA_FM];
    NSLog(@"ebsPlayer.m > initPlayer");
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isKindOfClass:[AVPlayerItem class]])
    {
        AVPlayerItem *item = (AVPlayerItem *)object;
        //playerItem status value changed?
        if ([keyPath isEqualToString:@"status"])
        {   //yes->check it...
            switch(item.status)
            {
                case AVPlayerItemStatusFailed:
                    NSLog(@"player item status failed");
                    break;
                case AVPlayerItemStatusReadyToPlay:
                    NSLog(@"player item status is ready to play");

                    bPlaying = YES;

                    if(seekTimer == nil)
                        seekTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateState) userInfo:nil repeats:YES];
                    
                    [playerDelegate statePlaReadyToPlay];
                    
                    break;
                case AVPlayerItemStatusUnknown:
                    NSLog(@"player item status is unknown");
                    break;
            }
        }
        else if ([keyPath isEqualToString:@"playbackBufferEmpty"])
        {
            if (item.playbackBufferEmpty)
            {
                NSLog(@"player item playback buffer is empty");
            }
        }
    }
}

#define EMPTY_BUFFERING_TIME        1.0f
-(void)updateState
{
    
    
//    float currentTime = CMTimeGetSeconds(queuePlayer.currentItem.currentTime);
//    float duration = CMTimeGetSeconds(queuePlayer.currentItem.duration);
    

    NSArray *loadedTimeRanges = [[queuePlayer currentItem] loadedTimeRanges];
    if([loadedTimeRanges count] < 1)
        return;

    CMTimeRange timeRange = [[loadedTimeRanges objectAtIndex:0] CMTimeRangeValue];
    
    
//    float startSeconds = CMTimeGetSeconds(timeRange.start);
    float durationSeconds = CMTimeGetSeconds(timeRange.duration);
    
    
//    NSLog(@"currentTime =  %f",currentTime);
//    NSLog(@"duration =  %f",duration);
//    NSLog(@"startSeconds =  %f",startSeconds);
//    NSLog(@"durationSeconds =  %f",duration);
//    //
    
    NSValue *val = [[[queuePlayer currentItem] loadedTimeRanges] objectAtIndex:0];
    CMTimeRange timeRange_;
    [val getValue:&timeRange_];
    CMTime duration_ = timeRange_.duration;
    float timeLoaded = (float) duration_.value / (float) duration_.timescale;
//    NSLog(@"timeLoaded = %f",timeLoaded);
    if (0 == timeLoaded) {
        // AVPlayer not actually ready to play
//        NSLog(@"gogogo");
    } else {
//        NSLog(@"stststst");
        // AVPlayer is ready to play
    }
    
//    NSLog(@"status %d ",[queuePlayer status]);
    if(durationSeconds > EMPTY_BUFFERING_TIME)
    {
//                    NSLog(@"nobuffer");
        
        if(timeLoaded < 5.f)
        {

            if(bPlaying)
            {
                
                if (!bNoBuffer) {
                    [queuePlayer pause];
                    bPlaying = NO;
                    bNoBuffer = YES;
                    
                    bufferTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(playError) userInfo:nil repeats:NO];
                }
                else if (bNoBuffer)
                {
                    if(timeLoaded > 10)
                    {
                        [queuePlayer play];
                        bNoBuffer = NO;
                        [bufferTimer invalidate];
                        bufferTimer = nil;
                        bPlaying = YES;
                        
                    }
                }
                //             bPlaying = NO;
            }
        }
        else
        {
            
            if (bNoBuffer)
            {
                if(timeLoaded > 45)
                {
                    [queuePlayer play];
                    bNoBuffer = NO;
                    bPlaying = YES;
                    
                }
            }

        }
    }
    if(playerDelegate)
        [playerDelegate statePlaying:bPlaying];
}

-(void)playError
{
    [playerDelegate playerNetworkError];
}

-(void)play
{
    if (!bPause) {
        [self stop];
        return;
    }
    bPause = NO;
    
    [queuePlayer play];
    if(bPlaying)
        seekTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateState) userInfo:nil repeats:YES];
    
    if(bNoBuffer)
        bufferTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(playError) userInfo:nil repeats:NO];
}
-(void)stop
{
    [queuePlayer pause];
    [playerDelegate statePlaying:NO];
    bPause = YES;
    if(seekTimer)
    {
        [seekTimer invalidate];
        seekTimer = nil;
        
    }
    if(bufferTimer)
    {
        [bufferTimer invalidate];
        bufferTimer = nil;
    }



}
-(void)playerSetFrame:(CGRect)frame_
{
    layer.frame = frame_;
}
-(void)playerSetUrl:(NSString *)str_
{
    firstVideoItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:str_]];
    queuePlayer = [AVQueuePlayer queuePlayerWithItems:[NSArray arrayWithObjects:firstVideoItem,nil]];
    
    layer = [AVPlayerLayer playerLayerWithPlayer:queuePlayer];
    queuePlayer.actionAtItemEnd = AVPlayerItemStatusReadyToPlay;

    [queuePlayer play];

}

@end
