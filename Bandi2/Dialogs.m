//
//  Dialogs.m
//  Bandi2
//
//  Created by admin on 2014. 1. 24..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import "Dialogs.h"

@implementation Dialogs


+ (void)alertView:(NSString *)msgText
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:msgText
                                                   delegate:nil
                                          cancelButtonTitle:@"닫기"
                                          otherButtonTitles:nil ];
    [alert show];
}

@end
