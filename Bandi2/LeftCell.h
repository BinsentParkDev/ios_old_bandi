//
//  LeftCell.h
//  Bandi2
//
//  Created by admin on 13. 12. 17..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *menuImage;
@property (strong, nonatomic) IBOutlet UILabel *menuLabel;
@end
