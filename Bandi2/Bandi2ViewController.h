//
//  Bandi2ViewController.h
//  Bandi2
//
//  Created by admin on 13. 12. 17..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UplusAd.h"
#import "PlayerView.h"
#import "OnAirView.h"
#import "FHSTwitterEngine.h"

#import <AVFoundation/AVFoundation.h>
#import <FacebookSDK/FacebookSDK.h>


@interface Bandi2ViewController : UIViewController<FHSTwitterEngineAccessTokenDelegate,UIAlertViewDelegate,UIWebViewDelegate>
{
    float screenHeight;
    float naviBarHeight;
    float statusBarHeight;
    
    PlayerView *player;
    OnAirView *mainView;
    UIView *uvGplayer;
    float AudioPosY;
    BOOL bPlayerShowOnairState;
    UILabel * naviTitle;
    UIImageView * naviIcon;

    UIView * titleV;
    UIButton * btHome;
}

- (void)UPlusBanner;

@property (strong, nonatomic) NSString *objectID;
//@property (strong, nonatomic) IBOutlet FBLoginView *fbLoginView;

@property (strong, nonatomic) IBOutlet UIView *wrapperView;
@property (strong, nonatomic) IBOutlet UIView *shareView;

@property (strong, nonatomic) UIButton *btShare;

-(IBAction)shareTwitter:(id)sender;
-(IBAction)shareFaceBook:(id)sender;
-(IBAction)shareKakao:(id)sender;
-(IBAction)sharedViewClose:(id)sender;

@end
