//
//  ebsPlayer.h
//  EBSTVApp
//
//  Created by Hyeoung seok Yoon on 2013. 12. 11..
//  Copyright (c) 2013년 Hyeoung seok Yoon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AVFoundation/AVFoundation.h"

@protocol ebsPlayerDelegate;


@interface ebsPlayer : UIView
{

    UIView *uvBackView;
    UIView *uvController;
    CGSize scrSize;

    AVQueuePlayer* queuePlayer;
    AVPlayerItem *firstVideoItem;
    NSTimer *seekTimer;
    AVPlayerLayer *layer;
    
    BOOL bPlaying;
    BOOL bPause;
    BOOL bNoBuffer;
    
    NSTimer *bufferTimer;
    
    id<ebsPlayerDelegate> playerDelegate;

}

@property (nonatomic, retain) UIView *uvController;
@property (nonatomic, assign) id<ebsPlayerDelegate> playerDelegate;

-(void)initPlayer:(CGRect)frame_ url:(NSString *)str;
-(void)play;
-(void)stop;
-(void)playerSetFrame:(CGRect)frame_;
-(void)playerSetUrl:(NSString *)str_;

@end


@protocol ebsPlayerDelegate <NSObject>


@optional
-(void)playerNetworkError;
-(void)statePlaying:(BOOL)bPlay;
-(void)statePlaReadyToPlay;
//-(void)selectVideoUrlIndex:(int)nSel;


@end
