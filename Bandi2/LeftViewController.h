//
//  LeftViewController.h
//  Bandi2
//
//  Created by admin on 13. 12. 17..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftViewController : UITableViewController
{
    NSArray *menuImages;
    NSArray *menuLabels;
    NSArray *menuBgs;
    
    NSMutableArray * stackSelectList;
    int nShowMode;
}

//- (UIView *)customHeaderView;
@end
