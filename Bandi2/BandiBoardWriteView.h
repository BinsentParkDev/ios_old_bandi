//
//  BandiBoardWriteView.h
//  Bandi2
//
//  Created by admin on 2014. 1. 18..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BandiBoardWriteView : UIView
{
    
}
- (void)onUnload;

@property (strong, nonatomic) IBOutlet BandiBoardWriteView *wrapperView;
@property (strong, nonatomic) IBOutlet UILabel *programLabel;
@property (strong, nonatomic) IBOutlet UITextView *textField;

- (IBAction)postBulletin:(id)sender;
- (IBAction)cancel:(id)sender;
@end
