//
//  MoviePlayerViewController.h
//  Bandi2
//
//  Created by admin on 2014. 2. 4..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "BandiBoardLandscapeView.h"
#import "LoginLandscapeView.h"
#import "BandiBoardWriteLandscapeView.h"

@interface MoviePlayerViewController : UIViewController
{
    NSURL *movieURL;
    
//    UIButton *homeButton;
	UIButton *writeButton;
	UIButton *boardButton;
    
    BandiBoardLandscapeView *boardView;
    LoginLandscapeView *loginView;
    BandiBoardWriteLandscapeView *boardWriteView;
}

- (id)initWithURL:(NSURL *)url;

@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) UIButton *homeButton;

@end
