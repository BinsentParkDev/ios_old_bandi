//
//  AirScheduleView.h
//  Bandi2
//
//  Created by admin on 2013. 12. 26..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AirScheduleView : UIView <UIWebViewDelegate>
{
        int nSelectedMenu;
}
- (void)onUnload;


@property (strong, nonatomic) IBOutlet UIView *wrapperView;

@property (strong, nonatomic) IBOutlet UIView *dateView;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
- (IBAction)todaySchedule:(id)sender;
- (IBAction)nextDate:(id)sender;
- (IBAction)prevDate:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *weekDateView;

@property (strong, nonatomic) IBOutlet UIView *tabView;


- (IBAction)sunday:(id)sender;
- (IBAction)monday:(id)sender;
- (IBAction)tuesday:(id)sender;
- (IBAction)wednesday:(id)sender;
- (IBAction)thursday:(id)sender;
- (IBAction)friday:(id)sender;
- (IBAction)saturday:(id)sender;
//- (IBAction)selectMenu:(id)sender;
- (IBAction)tabPlayFmRadio:(id)sender;
- (IBAction)tabPlayEndRadio:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *sundayBtn;
@property (strong, nonatomic) IBOutlet UIButton *mondayBtn;
@property (strong, nonatomic) IBOutlet UIButton *tuesdayBtn;
@property (strong, nonatomic) IBOutlet UIButton *wednesdayBtn;
@property (strong, nonatomic) IBOutlet UIButton *thursdayBtn;
@property (strong, nonatomic) IBOutlet UIButton *fridayBtn;
@property (strong, nonatomic) IBOutlet UIButton *saturdayBtn;

@property (strong, nonatomic) IBOutlet UIButton *btMenuReadRadio;
@property (strong, nonatomic) IBOutlet UIButton *btMenuEngRadio;



@property (strong, nonatomic) IBOutlet UIWebView *scheduleWebView;
@end
