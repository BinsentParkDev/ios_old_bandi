//
//  BandiBoardView.m
//  Bandi2
//
//  Created by admin on 2013. 12. 23..
//  Copyright (c) 2013년 EBS. All rights reserved.
//

#import "notiListView.h"
#import "TimeTable.h"
#import "IIViewDeckController.h"
#import "LoginView.h"
#import "BandiBoardWriteView.h"
#import "ServerSide.h"
#import "Util.h"
#import "LogContentController.h"
#import "WebViewController.h"
#import "GA.h"


#define RgbColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@implementation notiListView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        int bodyHeight = [SharedAppDelegate.bodyHeight intValue];
        
        boardList = [[NSMutableArray alloc]init];
        if(IS_IPHONE5)
            [[[NSBundle mainBundle] loadNibNamed:@"notiListView" owner:self options:nil] lastObject];
        else
            [[[NSBundle mainBundle] loadNibNamed:@"notiListView_480" owner:self options:nil] lastObject];

        strEventDetailUrl = [[NSString alloc]init];
        
        /** loadBandiBoardWeb 으로 이동.
        NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiAppList?progcd=%@", ttDic[@"progcd"]];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [_webView loadRequest:request];
        **/
        
        // adjust _webView size
//        [_webView setFrame:CGRectMake(0, 36.0, 320, bodyHeight-36)];
        
        [NSThread detachNewThreadSelector:@selector(loadNoti) toTarget:self withObject:nil];

        [_tbList setFrame:CGRectMake(0, 0, 320, _tbList.frame.size.height)];
        [_detailView setFrame:CGRectMake(0, 0, _detailView.frame.size.width, _detailView.frame.size.height)];
        [_wrapperView addSubview:_tbList];
        
        [[UIApplication sharedApplication].keyWindow addSubview:_detailView];
        [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_detailView];

        
//        [_wrapperView addSubview:_detailView];
        _detailView.hidden = YES;
        
        
        [_wrapperView setFrame:CGRectMake(0, 0, 320, frame.size.height)];
        [self addSubview:_wrapperView];
    }
    
    [GA sendScreenView:@"NoticeEvent"];
    
    return self;
}


- (void)onUnload
{
    NSLog(@"Unload : %@", [self class]);
}

- (void)loadNoti
{
    
    NSError *error;
    NSString *urlStr = [NSString stringWithFormat:@"http://home.ebs.co.kr/bandi/bandiEventNoticeList?appDsCd=03&fileType=json"];
    NSLog(@"bandi notiview loadNoti call url = %@",urlStr);
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlStr]
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
    NSLog(@"end");
    
    NSDictionary* person = [Util convertJsonToDic:jsonString];
    
    if(!jsonString || jsonString == nil)
        return;
    [boardList addObjectsFromArray:[person valueForKey:@"bandiEventLists"]];

    
    
    NSDateFormatter *today = [[NSDateFormatter alloc]init];
    [today setDateFormat:@"yyyyMMdd"];
    NSString *date = [today stringFromDate:[NSDate date]];
    
    for (int n = 0; n < [boardList count]; n ++) {
        if(![[[boardList objectAtIndex:n] valueForKey:@"evtClsCd"] isEqualToString:@"001"])
        {
            int nEvtStart =[[[boardList objectAtIndex:n] valueForKey:@"evtStartDt"] intValue];
            int nToday = [date intValue];
            if(nEvtStart > nToday)
            {
                [boardList removeObjectAtIndex:n];
            }
        }
        
    }
    for (int n = 0; n < [boardList count]; n ++) {
        if([[[boardList objectAtIndex:n] valueForKey:@"shwSiteDsCd"] isEqualToString:@"BANDI_PC"])
        {
            [boardList removeObjectAtIndex:n];
        }
    }
    
    
        [_tbList reloadData];
    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)writeBulletin:(id)sender {
    IIViewDeckController *controller = (IIViewDeckController *)self.window.rootViewController;
    UIViewController* cc = (UIViewController*)((UINavigationController*)controller.centerController).topViewController;
    
    //[[[cc.view subviews] lastObject] removeFromSuperview];
    
    CGRect frameRect = CGRectMake(0, 44+20-[Util correctionHeight], 320, [SharedAppDelegate.bodyHeight intValue]);
    
    SharedAppDelegate.latestMenu = @"반디게시판";
    if ([ServerSide autoLogin]) {
        BandiBoardWriteView *myView = [[BandiBoardWriteView alloc] initWithFrame:frameRect];
        [cc.view addSubview:myView];
        [cc.view addSubview:myView];
        [cc.view bringSubviewToFront:myView];
        cc.navigationItem.title = @"반디게시판";
    } else {
//        NSString * strStrView;
//        if (IS_IPHONE5)
//            strStrView = @"LogContentController";
//        else
//            strStrView = @"LogContentController_480";
//        
//        LogContentController *vc = [[LogContentController alloc] initWithNibName:strStrView bundle:nil];
//        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
//        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginN" object:nil];
//        LoginView *myView = [[LoginView alloc] initWithFrame:frameRect];
////        [myView EBSLoginOnly];
//        [cc.view addSubview:myView];
//        [cc.view bringSubviewToFront:myView];
//        cc.navigationItem.title = @"로그인";
    }
    
}

#pragma -table Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [boardList count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    return 50.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    notiListCell *cell = (notiListCell *)[tableView dequeueReusableCellWithIdentifier:@"notiListCell"];
    if (cell == nil)
    {
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"notiListCell" owner:nil options:nil];
        cell = (notiListCell *)[arr objectAtIndex:0];
    }
    
    NSDictionary *Dic = [boardList objectAtIndex:indexPath.row];
    
    [cell cellData:Dic];
    cell.listIndex  = indexPath.row;
    cell.cellDelegate = self;
    if(indexPath.row %2== 1)
        [cell setBackgroundColor:RgbColor(252,248,238)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
  
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self selectNoti:indexPath.row];

    
   

}
- (IBAction)openNoti:(id)sender
{
    NSLog(@"noti");
}

-(void)selectNoti:(int)indx
{
    
    
   

    NSDictionary *dic = [boardList objectAtIndex:indx];
    NSLog(@"dic = %@",dic);
    
    
    if([[dic valueForKey:@"evtClsCd"] isEqualToString:@"001"])
    {
        _detailIcon.image = [UIImage imageNamed:@"ico_notice.png"];
        _detailTitle.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"evtTitle"]];
        
        float fh;
        fh =[self adjustUILabelHeightSize:[NSString stringWithFormat:@"%@",[dic valueForKey:@"evtCntn"]] widthSize:_detailNotiContent.frame.size.width font:[UIFont systemFontOfSize:14]];
        
        [_detailContentScroll setContentSize:CGSizeMake(306, fh+50)];
        _detailNotiContent.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"evtCntn"]];
        _detailNotiContent.frame = CGRectMake(_detailNotiContent.frame.origin.x,
                                              _detailNotiContent.frame.origin.y,
                                              _detailNotiContent.frame.size.width,
                                              fh+60);
        NSLog(@"aa = %@",[dic valueForKey:@"evtCntn"]);
        _detailNotiContent.hidden = NO;
        _detailEventImage.hidden = YES;
        _btEventDetail.hidden = YES;

        
        strEventDetailUrl =[NSString stringWithFormat:@"%@",[dic valueForKey:@"linkUrl"]];
        if([strEventDetailUrl length] > 2)
        {
            _eventDetailBt.hidden = NO;
            if (IS_IPHONE5)
                _detailContentScroll.frame = CGRectMake(5, 66, 306, 400);
            else
                _detailContentScroll.frame = CGRectMake(5, 73, 306, 317);
        }
        else
        {
            _eventDetailBt.hidden = YES;
            if (IS_IPHONE5)
                _detailContentScroll.frame = CGRectMake(5, 66, 306, 450);
            else
                _detailContentScroll.frame = CGRectMake(5, 73, 306, 350);
        }
        

    }
    else
    {
        NSDateFormatter *today = [[NSDateFormatter alloc]init];
        [today setDateFormat:@"yyyyMMdd"];
        NSString *date = [today stringFromDate:[NSDate date]];
        
        
        int nEvtEnd =[[dic valueForKey:@"evtEndDt"] intValue];
        int nToday = [date intValue];
        if(nEvtEnd < nToday)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"이벤트 기간이 지났습니다."
                                                           delegate:nil
                                                  cancelButtonTitle:@"닫기"
                                                  otherButtonTitles:nil ];
            [alert show];
            return;
            
            //            [boardList removeObjectAtIndex:indexPath.row];
        }



        _detailIcon.image = [UIImage imageNamed:@"ico_event"];
        _detailTitle.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"evtTitle"]];
        _detailNotiContent.hidden = YES;
        NSString *strImgUrl = [NSString stringWithFormat:@"http://static.ebs.co.kr/images%@%@",[dic valueForKey:@"mobThmnlFilePathNm"],[dic valueForKey:@"mobThmnlFilePhscNm"]];
        NSLog(@"strImgUrl = %@",strImgUrl);
        NSURL *url = [NSURL URLWithString:strImgUrl];
        NSData *data = [NSData dataWithContentsOfURL:url];
        if(data) {
//            UIImage *image = [[UIImage alloc] initWithData:data];
            UIImage *resizeImage = [self imageWithImage:[[UIImage alloc] initWithData:data] scaledToWidth:310];
    
            _detailEventImage.image = resizeImage;
            [_detailContentScroll setContentSize:CGSizeMake(resizeImage.size.width,resizeImage.size.height)];
            _detailEventImage.frame = CGRectMake(_detailEventImage.frame.origin.x,
                                                 _detailEventImage.frame.origin.y,
                                                 resizeImage.size.width,
                                                 resizeImage.size.height);

            _detailEventImage.hidden = NO;
            _btEventDetail.frame =_detailEventImage.frame;

        }
        strEventUrl = [[NSString alloc]initWithFormat:@"%@",[dic valueForKey:@"mobLinkUrl"]];
        _btEventDetail.hidden = NO;
        
        _eventDetailBt.hidden = YES;
        if (IS_IPHONE5)
            _detailContentScroll.frame = CGRectMake(5, 66, 306, 450);
        else
            _detailContentScroll.frame = CGRectMake(5, 73, 306, 350);
        
        
    }

    
    _detailView.hidden = NO;
}

- (IBAction)closeNotiDetail:(id)sender
{
    _detailView.hidden = YES;
}

-(IBAction)eventDetailUrlOpen:(id)sender
{
    if([strEventDetailUrl length] > 2)
    {
        NSRange strRange;
        strRange = [strEventDetailUrl rangeOfString:@"http://"];
        
        if (strRange.location == NSNotFound) {
            strEventDetailUrl = [NSString stringWithFormat:@"http://%@",strEventDetailUrl];
        }

        NSURL *url = [NSURL URLWithString:strEventDetailUrl];
//        WebViewController *vc = [[WebViewController alloc] init];
//        [self.window.rootViewController presentViewController:vc animated:YES completion:^{
//            [vc.webView loadRequest:[NSURLRequest requestWithURL:url]];
//        }];
        [[UIApplication sharedApplication] openURL:url];        
        _detailView.hidden = YES;

    }
}


-(float)adjustUILabelHeightSize:(NSString*)textlabel widthSize:(float)width font:(UIFont*)ft{
    
    CGSize maximumHeightSize = CGSizeMake(width, 9999);
    
    CGSize extendLabelSize = [textlabel sizeWithFont:ft
                                   constrainedToSize:maximumHeightSize
                                       lineBreakMode:NSLineBreakByClipping];
    
//    NSLog(@"extendLabelSize.height = %f",extendLabelSize.height);
    return extendLabelSize.height;
}

- (IBAction)openEventUrl:(id)sender
{
    NSRange strRange;
    strRange = [strEventUrl rangeOfString:@"http://"];
    
    if (strRange.location == NSNotFound) {
        strEventUrl = [NSString stringWithFormat:@"http://%@",strEventUrl];
    }
    
    NSURL *url = [NSURL URLWithString:strEventUrl];
//    [[UIApplication sharedApplication] openURL:url];
   
    WebViewController *vc = [[WebViewController alloc] init];
    [self.window.rootViewController presentViewController:vc animated:YES completion:^{
        NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
        [(UIWebView *)[vc.view.subviews lastObject] loadRequest:urlRequest];
    }];

    
    _detailView.hidden = YES;
    
}
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
@end
