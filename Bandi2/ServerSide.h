//
//  ServerSide.h
//  Bandi2
//
//  Created by admin on 2014. 1. 24..
//  Copyright (c) 2014년 EBS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ServerSide : NSObject <NSURLConnectionDelegate>
{
    
}

+ (NSString *)appVersion;
+ (BOOL)existNewVersion;
+ (NSString *)post:(NSString *)postString url:(NSString *)urlString error:(NSError **)error;

+ (NSInteger)post:(NSString *)postString url:(NSString *)urlString headers:(NSDictionary **)headers body:(NSString **)body;


/**
 쿠키값을 지정해서 http get 으로 웹페이지 호출한다.

 @param urlString 서버 웹페이지 경로 (http://... or https://...)
 @param cookie key=value; key=value ...
 @param headers 서버에서 응답으로 내려오는 header 정보 저장
 @param body 서버 응답 text or html
 @return http response status code  (200 is ok)
 */
+ (NSInteger)get:(NSString *)urlString withCookie:(NSString *)cookie headers:(NSDictionary **)headers body:(NSString **)body;

/**
 로그인 후, 결과 코드를 반환한다. (100:정상)
 정상 로그인일 경우 결과값을 저장(ID, Name, Email)
 
 @return resultCode
 */
+ (NSString *)doLogin:(NSString *)userId withPasswd:(NSString *)passwd __attribute__((deprecated("Use doLoginNew method.")));

/**
 파라메터 전달을 위한 가공 (URL encode??)

 @param inputString 변경 대상 문자열
 @return 변환된 문자열
 */
+ (NSString *)percentEscapedString:(NSString *)inputString ;

/**
 로그인 세션을 웹서버쪽과 공유하기 위해서 새로운 로그인 방식 도입 (since 2017-05-31)
 
 @param userId EBS 웹서비스 로그인 ID
 @param passwd EBS 웹서비스 로그인 암호
 @return http response code (200 이면 정상)
 */
+ (NSString *)doLoginNew:(NSString *)userId withPassword:(NSString *)passwd;

/**
 doLoginNew 에서 필요한 파라메터 데이타 생성

 @return 로그인에 필요한 xml 문자열을 base64 엔코딩한 문자열을 반환한다.
 */
+ (NSString *)makeSAMLRequestBase64Encoded;


/**
 http response header 에서 cookie 정보를 받아온다.

 @param headers header 정보가 담긴 dictionary object
 @return cookie string like "session_id=aabbcc; idp=abc123;"
 */
+ (NSString *)getCookieFromHeader:(NSDictionary *)headers;


/**
 서버에서 받아온 xml 에서 로그인 사용자 정보를 추출한다. <br/>
 keys : 'userId', 'email', 'userName'

 @param xmlStr xml string
 @return NSDictionary object with keys {'userId', 'email', 'userName'}
 */
+ (NSDictionary<NSString *, NSString *> *)getUserInfoFromXml:(NSString *)xmlStr;

+ (BOOL)autoLogin;

#pragma mark - 접속 로그
+ (void)saveConnectLog;
+ (void)connectLog;

+ (void)saveDisconnectLog;
+ (void)disconnectLog;

+ (void)saveEventLog:(NSString *)action;
+ (void)eventLog:(NSString *)action;
+ (void)bookHitLog;
+ (void)pageLog:(NSString *)strPageName;
#pragma mark - 서버 리소스 받아오기 (Binary)
+ (NSData *)getResource:(NSURL *)url localSavingTime:(NSInteger) seconds;
+ (NSDictionary *)getBadMemberInfo:(NSString *)userId snsDsCd:(NSString *)snsDsCd snsUserId:(NSString *)snsUserId;
+ (BOOL)isBadMember;
@end
