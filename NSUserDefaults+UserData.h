//
//  NSUserDefaults+UserData.h
//  Bandi2
//
//  Created by admin on 2016. 8. 5..
//  Copyright © 2016년 EBS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (UserData)

- (void) setUserId:(NSString *)userId;
- (void) setPassword:(NSString *)password;
- (NSString *) getUserId;
- (NSString *) getPassword;
- (BOOL) isEncrypted;
- (void) doEncrypt:(BOOL)yesOrNo;

@end
